<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="13" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="3" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun">
<packages>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="EIA7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="PANASONIC_E">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="0.9" x2="-4.1" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.2032" layer="21"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="0.9" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-0.9" x2="4.1" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.2032" layer="21" curve="-153.684915" cap="flat"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.2032" layer="21" curve="153.684915" cap="flat"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3" y="0" dx="3.8" dy="1.4" layer="1"/>
<smd name="+" x="3" y="0" dx="3.8" dy="1.4" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5" y1="-0.35" x2="-3.8" y2="0.35" layer="51"/>
<rectangle x1="3.8" y1="-0.35" x2="4.5" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="STAND-OFF">
<description>&lt;b&gt;Stand Off&lt;/b&gt;&lt;p&gt;
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components. SKU : PRT-00447</description>
<wire x1="0" y1="1.8542" x2="0" y2="-1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="-180"/>
<circle x="0" y="0" radius="2.794" width="0.127" layer="39"/>
<hole x="0" y="0" drill="3.302"/>
</package>
<package name="STAND-OFF-TIGHT">
<description>&lt;b&gt;Stand Off&lt;/b&gt;&lt;p&gt;
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components. SKU : PRT-00447</description>
<wire x1="0" y1="1.8542" x2="0" y2="-1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="-180"/>
<circle x="0" y="0" radius="2.794" width="0.127" layer="39"/>
<hole x="0" y="0" drill="3.048"/>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="CPOL-RADIAL-100UF-25V">
<wire x1="-0.635" y1="1.27" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.25" width="0.2032" layer="21"/>
<pad name="2" x="-1.27" y="0" drill="0.7" diameter="1.651"/>
<pad name="1" x="1.27" y="0" drill="0.7" diameter="1.651" shape="square"/>
<text x="-1.905" y="-4.318" size="0.8128" layer="27">&gt;Value</text>
<text x="-0.762" y="1.651" size="0.4064" layer="25">&gt;Name</text>
</package>
<package name="PANASONIC_C">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.2032" layer="21"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-2.85" x2="-2.6" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="0.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="2.7" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="0.35" x2="2.45" y2="0.3" width="0.2032" layer="21" curve="-156.699401"/>
<wire x1="2.5" y1="-0.7" x2="-2.4" y2="-0.75" width="0.2032" layer="21" curve="-154.694887"/>
<circle x="0.05" y="-0.2" radius="2.5004" width="0.1016" layer="51"/>
<smd name="-" x="-1.8" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<smd name="+" x="1.9" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<text x="-2.6" y="2.7" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.6" y="-3.45" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="NIPPON_F80">
<wire x1="-3.3" y1="3.3" x2="1.7" y2="3.3" width="0.2032" layer="21"/>
<wire x1="1.7" y1="3.3" x2="3.3" y2="2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2" x2="1.7" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="1.7" y2="3.3" width="0.1016" layer="51"/>
<wire x1="1.7" y1="3.3" x2="3.3" y2="2" width="0.1016" layer="51"/>
<wire x1="3.3" y1="-2" x2="1.7" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-3.3" x2="-3.3" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="0.685" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-0.685" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2" x2="3.3" y2="0.685" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2" x2="3.3" y2="-0.685" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="3.3" y1="2" x2="3.3" y2="-2" width="0.1016" layer="51"/>
<wire x1="-3.1" y1="0.685" x2="3.1" y2="0.685" width="0.2032" layer="21" curve="-156.500033"/>
<wire x1="3.1" y1="-0.685" x2="-3.1" y2="-0.685" width="0.2032" layer="21" curve="-154.748326"/>
<circle x="0" y="0" radius="3.15" width="0.1016" layer="51"/>
<smd name="-" x="-2.4" y="0" dx="2.95" dy="1" layer="1"/>
<smd name="+" x="2.4" y="0" dx="2.95" dy="1" layer="1"/>
<text x="-3.2" y="3.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.85" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PANASONIC_D">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.95" x2="-2.95" y2="0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL-RADIAL-1000UF-63V">
<wire x1="-3.175" y1="1.905" x2="-4.445" y2="1.905" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="8.001" width="0.2032" layer="21"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.6764"/>
<pad name="1" x="3.81" y="0" drill="1.016" diameter="1.651" shape="square"/>
<text x="-2.54" y="8.89" size="0.8128" layer="27">&gt;Value</text>
<text x="-2.54" y="10.16" size="0.8128" layer="25">&gt;Name</text>
</package>
<package name="CPOL-RADIAL-1000UF-25V">
<wire x1="-1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="5.461" width="0.2032" layer="21"/>
<pad name="2" x="-2.54" y="0" drill="0.7" diameter="1.651"/>
<pad name="1" x="2.54" y="0" drill="0.7" diameter="1.651" shape="square"/>
<text x="-1.905" y="-4.318" size="0.8128" layer="27">&gt;Value</text>
<text x="-0.762" y="2.921" size="0.4064" layer="25">&gt;Name</text>
</package>
<package name="VISHAY_C">
<wire x1="0" y1="1.27" x2="0" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.0574" y1="4.2926" x2="-2.0574" y2="-4.2926" width="0.127" layer="21"/>
<wire x1="-2.0574" y1="-4.2926" x2="2.0574" y2="-4.2926" width="0.127" layer="21"/>
<wire x1="2.0574" y1="-4.2926" x2="2.0574" y2="4.2926" width="0.127" layer="21"/>
<wire x1="2.0574" y1="4.2926" x2="-2.0574" y2="4.2926" width="0.127" layer="21"/>
<smd name="+" x="0" y="3.048" dx="3.556" dy="1.778" layer="1"/>
<smd name="-" x="0" y="-3.048" dx="3.556" dy="1.778" layer="1"/>
<text x="-1.905" y="4.445" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="PANASONIC_G">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package G&lt;/b&gt;</description>
<wire x1="-5.1" y1="5.1" x2="2.8" y2="5.1" width="0.1016" layer="51"/>
<wire x1="2.8" y1="5.1" x2="5.1" y2="2.8" width="0.1016" layer="51"/>
<wire x1="5.1" y1="2.8" x2="5.1" y2="-2.8" width="0.1016" layer="51"/>
<wire x1="5.1" y1="-2.8" x2="2.8" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="2.8" y1="-5.1" x2="-5.1" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="-5.1" y1="-5.1" x2="-5.1" y2="5.1" width="0.1016" layer="51"/>
<wire x1="-5.1" y1="1" x2="-5.1" y2="5.1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="5.1" x2="2.8" y2="5.1" width="0.2032" layer="21"/>
<wire x1="2.8" y1="5.1" x2="5.1" y2="2.8" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.8" x2="5.1" y2="1" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-1" x2="5.1" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.8" x2="2.8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-5.1" x2="-5.1" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="-5.1" x2="-5.1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-1" x2="4.85" y2="-1" width="0.2032" layer="21" curve="156.699401" cap="flat"/>
<wire x1="-4.85" y1="1" x2="4.85" y2="1" width="0.2032" layer="21" curve="-156.699401" cap="flat"/>
<wire x1="-3.25" y1="3.7" x2="-3.25" y2="-3.65" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="4.95" width="0.1016" layer="51"/>
<smd name="-" x="-4.25" y="0" dx="3.9" dy="1.6" layer="1"/>
<smd name="+" x="4.25" y="0" dx="3.9" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-5.85" y1="-0.45" x2="-4.9" y2="0.45" layer="51"/>
<rectangle x1="4.9" y1="-0.45" x2="5.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-3.3" y="3.6"/>
<vertex x="-4.05" y="2.75"/>
<vertex x="-4.65" y="1.55"/>
<vertex x="-4.85" y="0.45"/>
<vertex x="-4.85" y="-0.45"/>
<vertex x="-4.65" y="-1.55"/>
<vertex x="-4.05" y="-2.75"/>
<vertex x="-3.3" y="-3.6"/>
<vertex x="-3.3" y="3.55"/>
</polygon>
</package>
<package name="PANASONIC_H13">
<wire x1="-6.75" y1="6.75" x2="4" y2="6.75" width="0.1016" layer="51"/>
<wire x1="4" y1="6.75" x2="6.75" y2="4" width="0.1016" layer="51"/>
<wire x1="6.75" y1="4" x2="6.75" y2="-4" width="0.1016" layer="51"/>
<wire x1="6.75" y1="-4" x2="4" y2="-6.75" width="0.1016" layer="51"/>
<wire x1="4" y1="-6.75" x2="-6.75" y2="-6.75" width="0.1016" layer="51"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="6.75" width="0.1016" layer="51"/>
<wire x1="-6.75" y1="1" x2="-6.75" y2="6.75" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="6.75" x2="4" y2="6.75" width="0.2032" layer="21"/>
<wire x1="4" y1="6.75" x2="6.75" y2="4" width="0.2032" layer="21"/>
<wire x1="6.75" y1="4" x2="6.75" y2="1" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-1" x2="6.75" y2="-4" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-4" x2="4" y2="-6.75" width="0.2032" layer="21"/>
<wire x1="4" y1="-6.75" x2="-6.75" y2="-6.75" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="-6.55" y1="-1.2" x2="6.45" y2="-1.2" width="0.2032" layer="21" curve="156.692742" cap="flat"/>
<wire x1="-6.55" y1="1.2" x2="6.55" y2="1.2" width="0.2032" layer="21" curve="-156.697982" cap="flat"/>
<wire x1="-5" y1="4.25" x2="-4.95" y2="-4.35" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="6.6" width="0.1016" layer="51"/>
<smd name="-" x="-4.7" y="0" dx="5" dy="1.6" layer="1"/>
<smd name="+" x="4.7" y="0" dx="5" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-7.55" y1="-0.45" x2="-6.6" y2="0.45" layer="51"/>
<rectangle x1="6.6" y1="-0.45" x2="7.55" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-5" y="4.2"/>
<vertex x="-5.75" y="3.15"/>
<vertex x="-6.25" y="2.05"/>
<vertex x="-6.55" y="0.45"/>
<vertex x="-6.55" y="-0.45"/>
<vertex x="-6.35" y="-1.65"/>
<vertex x="-5.75" y="-3.25"/>
<vertex x="-5" y="-4.2"/>
</polygon>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="EIA3528">
<wire x1="-0.9" y1="-1.6" x2="-2.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.6" x2="-2.6" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.2" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.55" x2="2.6" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.2" x2="2.6" y2="1.25" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.25" x2="2.2" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.2032" layer="21" style="longdash"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="CPOL-RADIAL-10UF-25V">
<wire x1="-0.762" y1="1.397" x2="-1.778" y2="1.397" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<pad name="1" x="1.27" y="0" drill="0.7" diameter="1.651" shape="square"/>
<pad name="2" x="-1.27" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.889" y="1.524" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.905" y="-3.683" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="EIA6032">
<wire x1="3.2" y1="-1.6" x2="3.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="3.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.2" y1="1.6" x2="-2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="1.6" x2="-3.4" y2="1" width="0.127" layer="21"/>
<wire x1="-3.4" y1="1" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<smd name="P$1" x="-2.3" y="0" dx="1.5" dy="2.4" layer="1"/>
<smd name="P$2" x="2.3" y="0" dx="1.5" dy="2.4" layer="1"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="EN_J2">
<description>Type J2 package for SMD supercap PRT-10317 (p# EEC-EN0F204J2)</description>
<wire x1="-2.5" y1="-3.5" x2="2.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="3.5" x2="2.1" y2="3.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="3.5" x2="-2.5" y2="3.1" width="0.127" layer="51"/>
<wire x1="-2.5" y1="3.1" x2="-2.5" y2="2.3" width="0.127" layer="51"/>
<wire x1="2.1" y1="3.5" x2="2.5" y2="3.1" width="0.127" layer="51"/>
<wire x1="2.5" y1="3.1" x2="2.5" y2="2.3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3.5" x2="-2.5" y2="-2.3" width="0.127" layer="51"/>
<wire x1="2.5" y1="-3.5" x2="2.5" y2="-2.3" width="0.127" layer="51"/>
<wire x1="-2.5908" y1="-2.413" x2="-2.5654" y2="2.4384" width="0.127" layer="21" curve="-91.212564"/>
<wire x1="2.5908" y1="-2.413" x2="2.5654" y2="2.4384" width="0.127" layer="21" curve="86.79344"/>
<wire x1="1.7272" y1="-1.27" x2="1.7272" y2="-2.0828" width="0.127" layer="21"/>
<wire x1="1.3462" y1="-1.6764" x2="2.159" y2="-1.6764" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.4" width="0.127" layer="51"/>
<smd name="-" x="0" y="2.8" dx="5" dy="2.4" layer="1"/>
<smd name="+" x="0" y="-3.2" dx="5" dy="1.6" layer="1"/>
<text x="-2.28" y="0.66" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.31" y="-1.21" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<wire x1="-4.5" y1="-5" x2="-5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5" x2="-5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-6.3" x2="-6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6.3" x2="-6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.1" x2="6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6" y1="1.1" x2="6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="6" y1="-6.3" x2="5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-6.3" x2="5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-5" x2="4.5" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-5" drill="0.7"/>
<pad name="2" x="-1" y="-5" drill="0.7"/>
<pad name="3" x="1" y="-5" drill="0.7"/>
<pad name="4" x="3" y="-5" drill="0.7"/>
<text x="-2.27" y="0.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="-1" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="-4.3" size="1.27" layer="51">+</text>
<text x="-1.4" y="-4.3" size="1.27" layer="51">-</text>
<text x="0.7" y="-4.1" size="0.8" layer="51">S</text>
<text x="2.7" y="-4.1" size="0.8" layer="51">S</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="LGA14">
<wire x1="2.5" y1="1.5" x2="-2.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="1.5" x2="-2.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-1.5" x2="2.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.5" x2="2.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.5" x2="2.5" y2="0.7" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.7" x2="2.5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.5" x2="-2.5" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.7" x2="-2.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.254" y1="-0.127" x2="0.508" y2="-0.127" width="0.0508" layer="51"/>
<wire x1="0.508" y1="-0.127" x2="0.381" y2="0" width="0.0508" layer="51"/>
<wire x1="0.508" y1="-0.127" x2="0.381" y2="-0.254" width="0.0508" layer="51"/>
<wire x1="-0.381" y1="0.127" x2="-0.381" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-0.381" y1="0.381" x2="-0.508" y2="0.254" width="0.0508" layer="51"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.254" width="0.0508" layer="51"/>
<circle x="1" y="0" radius="0.1" width="0.2032" layer="21"/>
<circle x="0.254" y="0.254" radius="0.127" width="0.0508" layer="51"/>
<circle x="0.254" y="0.254" radius="0.0254" width="0.0508" layer="51"/>
<smd name="6" x="-2" y="1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="5" x="-1.2" y="1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="4" x="-0.4" y="1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="3" x="0.4" y="1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="2" x="1.2" y="1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="1" x="2" y="1.1" dx="0.5" dy="1.2" layer="1"/>
<smd name="14" x="2.1" y="0" dx="0.5" dy="1.2" layer="1" rot="R90"/>
<smd name="13" x="2" y="-1.1" dx="0.5" dy="1.2" layer="1"/>
<smd name="12" x="1.2" y="-1.1" dx="0.5" dy="1.2" layer="1"/>
<smd name="11" x="0.4" y="-1.1" dx="0.5" dy="1.2" layer="1"/>
<smd name="10" x="-0.4" y="-1.1" dx="0.5" dy="1.2" layer="1"/>
<smd name="9" x="-1.2" y="-1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="8" x="-2" y="-1.1" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="7" x="-2.1" y="0" dx="0.5" dy="1.2" layer="1" rot="R270"/>
<text x="-2.032" y="2.159" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.032" y="-2.413" size="0.4064" layer="27">&gt;Value</text>
<text x="-0.127" y="-0.254" size="0.3048" layer="51" ratio="15">X</text>
<text x="-0.508" y="-0.254" size="0.3048" layer="51" ratio="15">Y</text>
<text x="-0.127" y="0.127" size="0.3048" layer="51" ratio="15">Z</text>
</package>
<package name="16LPCC">
<wire x1="-1.7" y1="-1.7" x2="-1.7" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-1.7" x2="-1.1" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-1.7" x2="1.7" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-1.7" x2="1.7" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.7" y1="1.1" x2="1.7" y2="1.7" width="0.2032" layer="21"/>
<wire x1="1.7" y1="1.7" x2="1.1" y2="1.7" width="0.2032" layer="21"/>
<wire x1="-1.28" y1="1.635" x2="-1.635" y2="1.26" width="0.2032" layer="21"/>
<smd name="4" x="-1.37" y="-0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="3" x="-1.37" y="-0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="2" x="-1.37" y="0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="1" x="-1.37" y="0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="5" x="-0.75" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R270"/>
<smd name="15" x="-0.25" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="14" x="0.25" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="13" x="0.75" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="12" x="1.37" y="0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="11" x="1.37" y="0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="10" x="1.37" y="-0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="9" x="1.37" y="-0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="6" x="-0.25" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="8" x="0.75" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="16" x="-0.75" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="7" x="0.25" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="EIA3528-KIT">
<description>&lt;h3&gt;EIA3528-KIT&lt;/h3&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has longer pads to make hand soldering easier.&lt;br&gt;</description>
<wire x1="-0.9" y1="-1.6" x2="-3.1" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.6" x2="-3.1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.7" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.55" x2="3.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-1.2" x2="3.1" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.1" y1="1.25" x2="2.7" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.4" layer="21" style="longdash"/>
<smd name="C" x="-1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<smd name="A" x="1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="EIA3216-KIT">
<description>&lt;h3&gt;EIA3216-KIT&lt;/h3&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has longer pads to make hand soldering easier.&lt;br&gt;</description>
<wire x1="-1" y1="-1.2" x2="-3" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.2" x2="-3" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.6" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.2" x2="3" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="3" y1="-0.8" x2="3" y2="0.8" width="0.2032" layer="21"/>
<wire x1="3" y1="0.8" x2="2.6" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.65" y="0" dx="1.9" dy="1.6" layer="1"/>
<smd name="A" x="1.65" y="0" dx="1.9" dy="1.6" layer="1"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="INDUCTOR-1206">
<wire x1="-1.778" y1="2.032" x2="-3.81" y2="2.032" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.032" x2="-3.81" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.032" x2="-1.524" y2="-2.032" width="0.127" layer="21"/>
<wire x1="1.524" y1="2.032" x2="3.81" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.032" x2="3.81" y2="-2.032" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.032" x2="1.524" y2="-2.032" width="0.127" layer="21"/>
<smd name="P$1" x="-2.54" y="0" dx="3.556" dy="2.032" layer="1" rot="R90"/>
<smd name="P$2" x="2.54" y="0" dx="3.556" dy="2.032" layer="1" rot="R90"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="0603">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CR54">
<wire x1="2.8" y1="2.98" x2="-2.8" y2="2.98" width="0.127" layer="51"/>
<wire x1="-2.8" y1="2.98" x2="-2.8" y2="-3" width="0.127" layer="51"/>
<wire x1="-2.8" y1="-3" x2="2.8" y2="-3" width="0.127" layer="51"/>
<wire x1="2.8" y1="-3" x2="2.8" y2="2.98" width="0.127" layer="51"/>
<wire x1="-3.048" y1="2.794" x2="-3.048" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="-3.048" x2="-2.794" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-3.302" x2="2.794" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="2.794" y1="-3.302" x2="3.048" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="-3.048" x2="3.048" y2="3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="3.048" x2="2.794" y2="3.302" width="0.2032" layer="21"/>
<wire x1="2.794" y1="3.302" x2="-2.794" y2="3.302" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="3.302" x2="-3.048" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="3.048" x2="-3.048" y2="2.794" width="0.2032" layer="21"/>
<circle x="0" y="0.508" radius="0.127" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="1.92" dx="5.5" dy="2.15" layer="1"/>
<smd name="P$2" x="0" y="-1.92" dx="5.5" dy="2.15" layer="1"/>
<text x="-2.54" y="3.81" size="0.4064" layer="25">&gt;Name</text>
<text x="0" y="3.81" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CDRH125">
<wire x1="-3.5" y1="6" x2="-6" y2="6" width="0.2032" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6" x2="-3.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-6" x2="6" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="6" x2="3.5" y2="6" width="0.2032" layer="21"/>
<smd name="1" x="0" y="4.9" dx="5.4" dy="4" layer="1"/>
<smd name="2" x="0" y="-4.9" dx="5.4" dy="4" layer="1"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.54" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="B82462G">
<wire x1="3.15" y1="3.15" x2="-3.15" y2="3.15" width="0.127" layer="51"/>
<wire x1="-3.15" y1="3.15" x2="-3.15" y2="-3.15" width="0.127" layer="51"/>
<wire x1="-3.15" y1="-3.15" x2="3.15" y2="-3.15" width="0.127" layer="51"/>
<wire x1="3.15" y1="-3.15" x2="3.15" y2="3.15" width="0.127" layer="51"/>
<wire x1="-3.15" y1="3.15" x2="-2" y2="3.15" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.15" x2="-3.15" y2="-3.15" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-3.15" x2="-2" y2="-3.15" width="0.2032" layer="21"/>
<wire x1="2" y1="-3.15" x2="3.15" y2="-3.15" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-3.15" x2="3.15" y2="3.15" width="0.2032" layer="21"/>
<wire x1="3.15" y1="3.15" x2="2" y2="3.15" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="2.75" dx="2.4" dy="1.5" layer="1"/>
<smd name="P$2" x="0" y="-2.75" dx="2.4" dy="1.5" layer="1"/>
</package>
<package name="CR75">
<wire x1="-1" y1="3.65" x2="7" y2="3.65" width="0.127" layer="21"/>
<wire x1="7" y1="3.65" x2="7" y2="2.55" width="0.127" layer="21"/>
<wire x1="-1" y1="3.65" x2="-1" y2="2.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-3.65" x2="7" y2="-3.65" width="0.127" layer="21"/>
<wire x1="7" y1="-3.65" x2="7" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-3.65" x2="-1" y2="-2.55" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
<smd name="P$2" x="6.05" y="0" dx="4.7" dy="1.75" layer="1" rot="R90"/>
</package>
<package name="1007">
<description>1007 (2518 metric) package</description>
<wire x1="0.9" y1="1.25" x2="-0.9" y2="1.25" width="0.127" layer="51"/>
<wire x1="-0.9" y1="1.25" x2="-0.9" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-1.25" x2="0.9" y2="-1.25" width="0.127" layer="51"/>
<wire x1="0.9" y1="-1.25" x2="0.9" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.4" x2="-1" y2="0.4" width="0.127" layer="21"/>
<wire x1="1" y1="-0.4" x2="1" y2="0.4" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="2" dy="0.8" layer="1"/>
<smd name="2" x="0" y="-1" dx="2" dy="0.8" layer="1"/>
<text x="-1" y="1.6" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="COOPER_UP4B">
<wire x1="-6.3" y1="7" x2="-7.5" y2="2" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="2" x2="-7.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="-2" x2="-6.3" y2="-7" width="0.2032" layer="21"/>
<wire x1="7.5" y1="2" x2="7.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="7.5" y1="2" x2="6.3" y2="7" width="0.2032" layer="21"/>
<wire x1="7.5" y1="-2" x2="6.3" y2="-7" width="0.2032" layer="21"/>
<smd name="1" x="0" y="8.9" dx="12" dy="4.3" layer="1"/>
<smd name="2" x="0" y="-8.9" dx="12" dy="4.3" layer="1"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.54" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CDRH3D28">
<wire x1="-2.0503" y1="-0.677" x2="-2.0505" y2="0.6629" width="0.127" layer="51"/>
<wire x1="0.6192" y1="1.9926" x2="-0.7206" y2="1.9927" width="0.127" layer="51"/>
<wire x1="1.9491" y1="0.6627" x2="1.9491" y2="-0.677" width="0.127" layer="51"/>
<wire x1="-2.0505" y1="0.6629" x2="-0.7206" y2="1.9927" width="0.127" layer="51"/>
<wire x1="1.9491" y1="0.6627" x2="0.6192" y2="1.9926" width="0.127" layer="51"/>
<wire x1="1.9503" y1="-0.6737" x2="-0.0506" y2="-2.6748" width="0.127" layer="51"/>
<wire x1="-0.0436" y1="-2.6999" x2="1.2914" y2="-1.3649" width="0.127" layer="21"/>
<wire x1="-0.0436" y1="-2.6999" x2="-1.3785" y2="-1.3649" width="0.127" layer="21"/>
<wire x1="-2.0434" y1="-0.68" x2="-0.0535" y2="-2.6698" width="0.127" layer="51"/>
<wire x1="-1.7435" y1="1" x2="-0.7895" y2="1.954" width="0.127" layer="21"/>
<wire x1="1.6563" y1="0.9999" x2="0.7024" y2="1.9538" width="0.127" layer="21"/>
<smd name="2" x="1.849" y="-0.007" dx="1.5" dy="1.4" layer="1" rot="R270"/>
<smd name="1" x="-1.9504" y="-0.007" dx="1.5" dy="1.4" layer="1" rot="R270"/>
<text x="-2.492" y="2.524" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.492" y="-3.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CR75_V2">
<wire x1="-1" y1="3.65" x2="7" y2="3.65" width="0.127" layer="21"/>
<wire x1="7" y1="3.65" x2="7" y2="2.55" width="0.127" layer="21"/>
<wire x1="-1" y1="3.65" x2="-1" y2="2.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-3.65" x2="7" y2="-3.65" width="0.127" layer="21"/>
<wire x1="7" y1="-3.65" x2="7" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-3.65" x2="-1" y2="-2.55" width="0.127" layer="21"/>
<smd name="P$1" x="-0.254" y="0" dx="2.54" dy="1.524" layer="1" rot="R90"/>
<smd name="P$2" x="6.304" y="0" dx="2.54" dy="1.524" layer="1" rot="R90"/>
</package>
<package name="CDRH2D09">
<description>1.3x1.3mm 1.7mm between. Fits Sumida CDRH2D09, CDRH2D18 inductor</description>
<wire x1="-1.2" y1="0.9" x2="-0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="1.5" x2="1.2" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-0.9" x2="-0.6783" y2="-1.3739" width="0.2032" layer="21"/>
<wire x1="-0.6783" y1="-1.3739" x2="0.6783" y2="-1.3739" width="0.2032" layer="21" curve="85.420723"/>
<wire x1="0.6783" y1="-1.3739" x2="1.2" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.6" x2="-0.7071" y2="-1.3929" width="0.03" layer="51"/>
<wire x1="-0.7071" y1="-1.3929" x2="0.7071" y2="-1.3929" width="0.03" layer="51" curve="90"/>
<wire x1="0.7071" y1="-1.3929" x2="1.5" y2="-0.6" width="0.03" layer="51"/>
<wire x1="1.5" y1="-0.6" x2="1.5" y2="0.6" width="0.03" layer="51"/>
<wire x1="1.5" y1="0.6" x2="0.6" y2="1.5" width="0.03" layer="51"/>
<wire x1="0.6" y1="1.5" x2="-0.6" y2="1.5" width="0.03" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="-1.5" y2="0.6" width="0.03" layer="51"/>
<wire x1="-1.5" y1="0.6" x2="-1.5" y2="-0.6" width="0.03" layer="51"/>
<smd name="P$1" x="-1.5" y="0" dx="1.3" dy="1.3" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="1.3" dy="1.3" layer="1" rot="R90"/>
<text x="2.8" y="0.7" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.8" y="-1" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="NPI75C">
<wire x1="-3.9" y1="3.5" x2="-3.9" y2="-3.5" width="0.1778" layer="51"/>
<wire x1="-3.9" y1="-3.5" x2="3.9" y2="-3.5" width="0.1778" layer="51"/>
<wire x1="3.9" y1="-3.5" x2="3.9" y2="3.5" width="0.1778" layer="51"/>
<wire x1="0.8" y1="3.5" x2="-0.8" y2="3.5" width="0.1778" layer="21"/>
<wire x1="-0.8" y1="-3.5" x2="0.8" y2="-3.5" width="0.1778" layer="21"/>
<wire x1="3.9" y1="3.5" x2="-3.9" y2="3.5" width="0.1778" layer="51"/>
<smd name="1" x="-2.5" y="0" dx="3" dy="7.5" layer="1"/>
<smd name="2" x="2.5" y="0" dx="3" dy="7.5" layer="1"/>
</package>
<package name="SRU5028">
<wire x1="1.2048" y1="-2.473" x2="2.4476" y2="-1.2048" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-0.9" x2="2.6" y2="0.9" width="0.2032" layer="51"/>
<wire x1="2.473" y1="1.2048" x2="1.2048" y2="2.4476" width="0.2032" layer="21"/>
<wire x1="0.9" y1="2.6" x2="-0.9" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-1.1794" y1="2.4222" x2="-2.4222" y2="1.2048" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="0.9" x2="-2.6" y2="-0.9" width="0.2032" layer="51"/>
<wire x1="-2.3968" y1="-1.1794" x2="-1.2048" y2="-2.4476" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-2.6" x2="0.9" y2="-2.6" width="0.2032" layer="51"/>
<circle x="1.5" y="0" radius="0.1414" width="0.4" layer="21"/>
<smd name="P$1" x="0" y="2.4" dx="2" dy="1.1" layer="1"/>
<smd name="P$2" x="0" y="-2.4" dx="2" dy="1.1" layer="1"/>
<smd name="1" x="2.4" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="-2.4" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<text x="-2.54" y="3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SRU1038">
<wire x1="-5" y1="-1.6" x2="-5" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-5" y1="1.6" x2="-1.6" y2="5" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="5" x2="1.6" y2="5" width="0.2032" layer="51"/>
<wire x1="1.6" y1="5" x2="5" y2="1.6" width="0.2032" layer="51"/>
<wire x1="5" y1="1.6" x2="5" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="5" y1="-1.6" x2="1.6" y2="-5" width="0.2032" layer="51"/>
<wire x1="1.6" y1="-5" x2="-1.6" y2="-5" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-5" x2="-5" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.1" x2="-1.6" y2="5" width="0.254" layer="21"/>
<wire x1="-1.6" y1="5" x2="1.6" y2="5" width="0.254" layer="21"/>
<wire x1="1.6" y1="-5" x2="-1.6" y2="-5" width="0.254" layer="21"/>
<wire x1="1.6" y1="5" x2="4.5" y2="2.1" width="0.254" layer="21"/>
<wire x1="-4.5" y1="-2.1" x2="-1.6" y2="-5" width="0.254" layer="21"/>
<wire x1="1.6" y1="-5" x2="4.5" y2="-2.1" width="0.254" layer="21"/>
<smd name="2" x="4.5" y="0" dx="1.8" dy="3.6" layer="1"/>
<smd name="1" x="-4.5" y="0" dx="1.8" dy="3.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.016" layer="25">&gt;Name</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;Value</text>
</package>
<package name="CR54-KIT">
<wire x1="-3.048" y1="-3.548" x2="-2.794" y2="-3.802" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-3.802" x2="2.794" y2="-3.802" width="0.2032" layer="21"/>
<wire x1="2.794" y1="-3.802" x2="3.048" y2="-3.548" width="0.2032" layer="21"/>
<wire x1="3.048" y1="-3.548" x2="3.048" y2="3.548" width="0.2032" layer="21"/>
<wire x1="3.048" y1="3.548" x2="2.794" y2="3.802" width="0.2032" layer="21"/>
<wire x1="2.794" y1="3.802" x2="-2.794" y2="3.802" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="3.802" x2="-3.048" y2="3.548" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="3.548" x2="-3.048" y2="-3.548" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.1778" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.1778" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.1778" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.1778" layer="51"/>
<circle x="0" y="0.508" radius="0.127" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="2.17" dx="5" dy="2.65" layer="1"/>
<smd name="P$2" x="0" y="-2.17" dx="5" dy="2.65" layer="1"/>
<text x="-2.54" y="4.01" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.7" y="-4.39" size="0.4064" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="VCC2">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="STAND-OFF">
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="3.3V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="ADXL345">
<wire x1="-7.62" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-10.16" y="10.16" length="short"/>
<pin name="NC@11" x="-10.16" y="-2.54" length="short"/>
<pin name="NC@3" x="-10.16" y="2.54" length="short"/>
<pin name="GND@4" x="-10.16" y="-10.16" length="short"/>
<pin name="GND@5" x="-10.16" y="-12.7" length="short"/>
<pin name="VSS" x="-10.16" y="7.62" length="short"/>
<pin name="CS" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="INT1" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="INT2" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="GND@2" x="-10.16" y="-7.62" length="short"/>
<pin name="NC@10" x="-10.16" y="0" length="short"/>
<pin name="SDO" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="SDA" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="SCL" x="12.7" y="0" length="short" rot="R180"/>
</symbol>
<symbol name="HMC5883L">
<wire x1="10.16" y1="15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SCL" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="NC@1" x="-15.24" y="12.7" length="middle"/>
<pin name="S1" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="NC@2" x="-15.24" y="10.16" length="middle"/>
<pin name="NC@3" x="-15.24" y="7.62" length="middle"/>
<pin name="NC@4" x="-15.24" y="5.08" length="middle"/>
<pin name="SETP" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="GND@1" x="-15.24" y="-7.62" length="middle"/>
<pin name="C1" x="-15.24" y="-2.54" length="middle"/>
<pin name="GND@2" x="-15.24" y="-10.16" length="middle"/>
<pin name="SETC" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="VDDIO" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="NC@5" x="-15.24" y="2.54" length="middle"/>
<pin name="DRDY" x="15.24" y="-12.7" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="VCC2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STAND-OFF" prefix="JP">
<description>&lt;b&gt;Stand Off&lt;/b&gt;&lt;p&gt;
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components. SKU : PRT-00447</description>
<gates>
<gate name="G$1" symbol="STAND-OFF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="STAND-OFF">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TIGHT" package="STAND-OFF-TIGHT">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POL" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor Polarized&lt;/b&gt;
These are standard SMD and PTH capacitors. Normally 10uF, 47uF, and 100uF in electrolytic and tantalum varieties. Always verify the external diameter of the through hole cap, it varies with capacity, voltage, and manufacturer. The EIA devices should be standard.</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528" package="EIA3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CPOL-RADIAL-100UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CPOL-RADIAL-10UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7343" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="G" package="PANASONIC_G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="PANASONIC_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="PANASONIC_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="F80" package="NIPPON_F80">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="PANASONIC_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CPOL-RADIAL-1000UF-63V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH4" package="CPOL-RADIAL-1000UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="VISHAY_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="H13" package="PANASONIC_H13">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032" package="EIA6032">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EN_J2" package="EN_J2">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528-KIT" package="EIA3528-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-KIT" package="EIA3216-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADXL345" prefix="U">
<gates>
<gate name="G$1" symbol="ADXL345" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA14">
<connects>
<connect gate="G$1" pin="CS" pad="7"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="INT1" pad="8"/>
<connect gate="G$1" pin="INT2" pad="9"/>
<connect gate="G$1" pin="NC@10" pad="10"/>
<connect gate="G$1" pin="NC@11" pad="11"/>
<connect gate="G$1" pin="NC@3" pad="3"/>
<connect gate="G$1" pin="SCL" pad="14"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="SDO" pad="12"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HMC5883L">
<description>3 Axis Digital Compass IC</description>
<gates>
<gate name="G$1" symbol="HMC5883L" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="16LPCC">
<connects>
<connect gate="G$1" pin="C1" pad="10"/>
<connect gate="G$1" pin="DRDY" pad="15"/>
<connect gate="G$1" pin="GND@1" pad="9"/>
<connect gate="G$1" pin="GND@2" pad="11"/>
<connect gate="G$1" pin="NC@1" pad="3"/>
<connect gate="G$1" pin="NC@2" pad="5"/>
<connect gate="G$1" pin="NC@3" pad="6"/>
<connect gate="G$1" pin="NC@4" pad="7"/>
<connect gate="G$1" pin="NC@5" pad="14"/>
<connect gate="G$1" pin="S1" pad="4"/>
<connect gate="G$1" pin="SCL" pad="1"/>
<connect gate="G$1" pin="SDA" pad="16"/>
<connect gate="G$1" pin="SETC" pad="12"/>
<connect gate="G$1" pin="SETP" pad="8"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VDDIO" pad="13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<description>&lt;b&gt;Inductors&lt;/b&gt;
Basic Inductor/Choke - 0603 and 1206. Footprints are not proven and vary greatly between part numbers.</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="H*" package="INDUCTOR-1206">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="1206"/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR54" package="CR54">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PWR" package="CDRH125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B82462G" package="B82462G">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR75" package="CR75">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1007" package="1007">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="COOPER_UP4B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-" package="CDRH3D28">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR75_V2" package="CR75_V2">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="." package="CDRH2D09">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NPI75" package="NPI75C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SRU5028" package="SRU5028">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SRU1038" package="SRU1038">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CR54-KIT" package="CR54-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ACL">
<packages>
<package name="ACL-LOGO">
<rectangle x1="3.14325" y1="0.03175" x2="3.37185" y2="0.04445" layer="21"/>
<rectangle x1="3.14325" y1="0.04445" x2="3.37185" y2="0.05715" layer="21"/>
<rectangle x1="3.14325" y1="0.05715" x2="3.37185" y2="0.06985" layer="21"/>
<rectangle x1="3.14325" y1="0.06985" x2="3.37185" y2="0.08255" layer="21"/>
<rectangle x1="3.14325" y1="0.08255" x2="3.37185" y2="0.09525" layer="21"/>
<rectangle x1="3.14325" y1="0.09525" x2="3.37185" y2="0.10795" layer="21"/>
<rectangle x1="3.14325" y1="0.10795" x2="3.37185" y2="0.12065" layer="21"/>
<rectangle x1="3.14325" y1="0.12065" x2="3.37185" y2="0.13335" layer="21"/>
<rectangle x1="6.21665" y1="0.12065" x2="6.29285" y2="0.13335" layer="21"/>
<rectangle x1="0.99695" y1="0.13335" x2="1.13665" y2="0.14605" layer="21"/>
<rectangle x1="2.07645" y1="0.13335" x2="2.24155" y2="0.14605" layer="21"/>
<rectangle x1="2.72415" y1="0.13335" x2="2.87655" y2="0.14605" layer="21"/>
<rectangle x1="3.14325" y1="0.13335" x2="3.37185" y2="0.14605" layer="21"/>
<rectangle x1="3.49885" y1="0.13335" x2="3.58775" y2="0.14605" layer="21"/>
<rectangle x1="3.91795" y1="0.13335" x2="4.00685" y2="0.14605" layer="21"/>
<rectangle x1="4.62915" y1="0.13335" x2="4.78155" y2="0.14605" layer="21"/>
<rectangle x1="5.27685" y1="0.13335" x2="5.42925" y2="0.14605" layer="21"/>
<rectangle x1="6.14045" y1="0.13335" x2="6.35635" y2="0.14605" layer="21"/>
<rectangle x1="6.83895" y1="0.13335" x2="7.00405" y2="0.14605" layer="21"/>
<rectangle x1="8.99795" y1="0.13335" x2="9.16305" y2="0.14605" layer="21"/>
<rectangle x1="10.00125" y1="0.13335" x2="10.15365" y2="0.14605" layer="21"/>
<rectangle x1="11.23315" y1="0.13335" x2="11.32205" y2="0.14605" layer="21"/>
<rectangle x1="12.12215" y1="0.13335" x2="12.21105" y2="0.14605" layer="21"/>
<rectangle x1="0.09525" y1="0.14605" x2="0.33655" y2="0.15875" layer="21"/>
<rectangle x1="0.48895" y1="0.14605" x2="0.73025" y2="0.15875" layer="21"/>
<rectangle x1="0.94615" y1="0.14605" x2="1.18745" y2="0.15875" layer="21"/>
<rectangle x1="1.44145" y1="0.14605" x2="1.67005" y2="0.15875" layer="21"/>
<rectangle x1="2.02565" y1="0.14605" x2="2.29235" y2="0.15875" layer="21"/>
<rectangle x1="2.67335" y1="0.14605" x2="2.92735" y2="0.15875" layer="21"/>
<rectangle x1="3.14325" y1="0.14605" x2="3.37185" y2="0.15875" layer="21"/>
<rectangle x1="3.47345" y1="0.14605" x2="3.62585" y2="0.15875" layer="21"/>
<rectangle x1="3.87985" y1="0.14605" x2="4.03225" y2="0.15875" layer="21"/>
<rectangle x1="4.12115" y1="0.14605" x2="4.33705" y2="0.15875" layer="21"/>
<rectangle x1="4.59105" y1="0.14605" x2="4.83235" y2="0.15875" layer="21"/>
<rectangle x1="5.22605" y1="0.14605" x2="5.48005" y2="0.15875" layer="21"/>
<rectangle x1="6.10235" y1="0.14605" x2="6.39445" y2="0.15875" layer="21"/>
<rectangle x1="6.78815" y1="0.14605" x2="7.05485" y2="0.15875" layer="21"/>
<rectangle x1="7.30885" y1="0.14605" x2="7.53745" y2="0.15875" layer="21"/>
<rectangle x1="7.63905" y1="0.14605" x2="7.86765" y2="0.15875" layer="21"/>
<rectangle x1="8.09625" y1="0.14605" x2="8.29945" y2="0.15875" layer="21"/>
<rectangle x1="8.36295" y1="0.14605" x2="8.59155" y2="0.15875" layer="21"/>
<rectangle x1="8.94715" y1="0.14605" x2="9.21385" y2="0.15875" layer="21"/>
<rectangle x1="9.46785" y1="0.14605" x2="9.70915" y2="0.15875" layer="21"/>
<rectangle x1="9.95045" y1="0.14605" x2="10.20445" y2="0.15875" layer="21"/>
<rectangle x1="10.64895" y1="0.14605" x2="11.04265" y2="0.15875" layer="21"/>
<rectangle x1="11.19505" y1="0.14605" x2="11.36015" y2="0.15875" layer="21"/>
<rectangle x1="11.43635" y1="0.14605" x2="11.65225" y2="0.15875" layer="21"/>
<rectangle x1="11.76655" y1="0.14605" x2="11.98245" y2="0.15875" layer="21"/>
<rectangle x1="12.08405" y1="0.14605" x2="12.23645" y2="0.15875" layer="21"/>
<rectangle x1="0.09525" y1="0.15875" x2="0.33655" y2="0.17145" layer="21"/>
<rectangle x1="0.48895" y1="0.15875" x2="0.73025" y2="0.17145" layer="21"/>
<rectangle x1="0.90805" y1="0.15875" x2="1.21285" y2="0.17145" layer="21"/>
<rectangle x1="1.44145" y1="0.15875" x2="1.67005" y2="0.17145" layer="21"/>
<rectangle x1="2.00025" y1="0.15875" x2="2.31775" y2="0.17145" layer="21"/>
<rectangle x1="2.64795" y1="0.15875" x2="2.96545" y2="0.17145" layer="21"/>
<rectangle x1="3.14325" y1="0.15875" x2="3.37185" y2="0.17145" layer="21"/>
<rectangle x1="3.44805" y1="0.15875" x2="3.63855" y2="0.17145" layer="21"/>
<rectangle x1="3.85445" y1="0.15875" x2="4.05765" y2="0.17145" layer="21"/>
<rectangle x1="4.12115" y1="0.15875" x2="4.33705" y2="0.17145" layer="21"/>
<rectangle x1="4.55295" y1="0.15875" x2="4.85775" y2="0.17145" layer="21"/>
<rectangle x1="5.20065" y1="0.15875" x2="5.50545" y2="0.17145" layer="21"/>
<rectangle x1="6.07695" y1="0.15875" x2="6.41985" y2="0.17145" layer="21"/>
<rectangle x1="6.76275" y1="0.15875" x2="7.08025" y2="0.17145" layer="21"/>
<rectangle x1="7.30885" y1="0.15875" x2="7.53745" y2="0.17145" layer="21"/>
<rectangle x1="7.63905" y1="0.15875" x2="7.86765" y2="0.17145" layer="21"/>
<rectangle x1="8.05815" y1="0.15875" x2="8.29945" y2="0.17145" layer="21"/>
<rectangle x1="8.36295" y1="0.15875" x2="8.59155" y2="0.17145" layer="21"/>
<rectangle x1="8.92175" y1="0.15875" x2="9.23925" y2="0.17145" layer="21"/>
<rectangle x1="9.46785" y1="0.15875" x2="9.70915" y2="0.17145" layer="21"/>
<rectangle x1="9.91235" y1="0.15875" x2="10.22985" y2="0.17145" layer="21"/>
<rectangle x1="10.64895" y1="0.15875" x2="11.04265" y2="0.17145" layer="21"/>
<rectangle x1="11.16965" y1="0.15875" x2="11.37285" y2="0.17145" layer="21"/>
<rectangle x1="11.43635" y1="0.15875" x2="11.65225" y2="0.17145" layer="21"/>
<rectangle x1="11.76655" y1="0.15875" x2="11.98245" y2="0.17145" layer="21"/>
<rectangle x1="12.05865" y1="0.15875" x2="12.26185" y2="0.17145" layer="21"/>
<rectangle x1="0.09525" y1="0.17145" x2="0.33655" y2="0.18415" layer="21"/>
<rectangle x1="0.48895" y1="0.17145" x2="0.71755" y2="0.18415" layer="21"/>
<rectangle x1="0.88265" y1="0.17145" x2="1.23825" y2="0.18415" layer="21"/>
<rectangle x1="1.44145" y1="0.17145" x2="1.67005" y2="0.18415" layer="21"/>
<rectangle x1="1.97485" y1="0.17145" x2="2.34315" y2="0.18415" layer="21"/>
<rectangle x1="2.62255" y1="0.17145" x2="2.97815" y2="0.18415" layer="21"/>
<rectangle x1="3.14325" y1="0.17145" x2="3.37185" y2="0.18415" layer="21"/>
<rectangle x1="3.43535" y1="0.17145" x2="3.66395" y2="0.18415" layer="21"/>
<rectangle x1="3.84175" y1="0.17145" x2="4.07035" y2="0.18415" layer="21"/>
<rectangle x1="4.12115" y1="0.17145" x2="4.33705" y2="0.18415" layer="21"/>
<rectangle x1="4.54025" y1="0.17145" x2="4.88315" y2="0.18415" layer="21"/>
<rectangle x1="5.17525" y1="0.17145" x2="5.53085" y2="0.18415" layer="21"/>
<rectangle x1="6.05155" y1="0.17145" x2="6.44525" y2="0.18415" layer="21"/>
<rectangle x1="6.73735" y1="0.17145" x2="7.10565" y2="0.18415" layer="21"/>
<rectangle x1="7.30885" y1="0.17145" x2="7.53745" y2="0.18415" layer="21"/>
<rectangle x1="7.63905" y1="0.17145" x2="7.86765" y2="0.18415" layer="21"/>
<rectangle x1="8.03275" y1="0.17145" x2="8.29945" y2="0.18415" layer="21"/>
<rectangle x1="8.36295" y1="0.17145" x2="8.59155" y2="0.18415" layer="21"/>
<rectangle x1="8.89635" y1="0.17145" x2="9.26465" y2="0.18415" layer="21"/>
<rectangle x1="9.46785" y1="0.17145" x2="9.70915" y2="0.18415" layer="21"/>
<rectangle x1="9.88695" y1="0.17145" x2="10.25525" y2="0.18415" layer="21"/>
<rectangle x1="10.64895" y1="0.17145" x2="11.04265" y2="0.18415" layer="21"/>
<rectangle x1="11.15695" y1="0.17145" x2="11.38555" y2="0.18415" layer="21"/>
<rectangle x1="11.43635" y1="0.17145" x2="11.65225" y2="0.18415" layer="21"/>
<rectangle x1="11.76655" y1="0.17145" x2="11.98245" y2="0.18415" layer="21"/>
<rectangle x1="12.04595" y1="0.17145" x2="12.27455" y2="0.18415" layer="21"/>
<rectangle x1="0.09525" y1="0.18415" x2="0.33655" y2="0.19685" layer="21"/>
<rectangle x1="0.48895" y1="0.18415" x2="0.71755" y2="0.19685" layer="21"/>
<rectangle x1="0.86995" y1="0.18415" x2="1.25095" y2="0.19685" layer="21"/>
<rectangle x1="1.44145" y1="0.18415" x2="1.67005" y2="0.19685" layer="21"/>
<rectangle x1="1.96215" y1="0.18415" x2="2.35585" y2="0.19685" layer="21"/>
<rectangle x1="2.59715" y1="0.18415" x2="2.99085" y2="0.19685" layer="21"/>
<rectangle x1="3.14325" y1="0.18415" x2="3.37185" y2="0.19685" layer="21"/>
<rectangle x1="3.42265" y1="0.18415" x2="3.66395" y2="0.19685" layer="21"/>
<rectangle x1="3.82905" y1="0.18415" x2="4.08305" y2="0.19685" layer="21"/>
<rectangle x1="4.12115" y1="0.18415" x2="4.33705" y2="0.19685" layer="21"/>
<rectangle x1="4.51485" y1="0.18415" x2="4.89585" y2="0.19685" layer="21"/>
<rectangle x1="5.16255" y1="0.18415" x2="5.54355" y2="0.19685" layer="21"/>
<rectangle x1="6.03885" y1="0.18415" x2="6.45795" y2="0.19685" layer="21"/>
<rectangle x1="6.72465" y1="0.18415" x2="7.11835" y2="0.19685" layer="21"/>
<rectangle x1="7.30885" y1="0.18415" x2="7.53745" y2="0.19685" layer="21"/>
<rectangle x1="7.63905" y1="0.18415" x2="7.86765" y2="0.19685" layer="21"/>
<rectangle x1="8.02005" y1="0.18415" x2="8.29945" y2="0.19685" layer="21"/>
<rectangle x1="8.36295" y1="0.18415" x2="8.59155" y2="0.19685" layer="21"/>
<rectangle x1="8.88365" y1="0.18415" x2="9.27735" y2="0.19685" layer="21"/>
<rectangle x1="9.46785" y1="0.18415" x2="9.70915" y2="0.19685" layer="21"/>
<rectangle x1="9.87425" y1="0.18415" x2="10.26795" y2="0.19685" layer="21"/>
<rectangle x1="10.64895" y1="0.18415" x2="11.04265" y2="0.19685" layer="21"/>
<rectangle x1="11.14425" y1="0.18415" x2="11.39825" y2="0.19685" layer="21"/>
<rectangle x1="11.43635" y1="0.18415" x2="11.65225" y2="0.19685" layer="21"/>
<rectangle x1="11.76655" y1="0.18415" x2="11.99515" y2="0.19685" layer="21"/>
<rectangle x1="12.03325" y1="0.18415" x2="12.28725" y2="0.19685" layer="21"/>
<rectangle x1="0.10795" y1="0.19685" x2="0.34925" y2="0.20955" layer="21"/>
<rectangle x1="0.48895" y1="0.19685" x2="0.71755" y2="0.20955" layer="21"/>
<rectangle x1="0.85725" y1="0.19685" x2="1.26365" y2="0.20955" layer="21"/>
<rectangle x1="1.44145" y1="0.19685" x2="1.67005" y2="0.20955" layer="21"/>
<rectangle x1="1.94945" y1="0.19685" x2="2.36855" y2="0.20955" layer="21"/>
<rectangle x1="2.58445" y1="0.19685" x2="3.00355" y2="0.20955" layer="21"/>
<rectangle x1="3.14325" y1="0.19685" x2="3.37185" y2="0.20955" layer="21"/>
<rectangle x1="3.40995" y1="0.19685" x2="3.67665" y2="0.20955" layer="21"/>
<rectangle x1="3.81635" y1="0.19685" x2="4.08305" y2="0.20955" layer="21"/>
<rectangle x1="4.12115" y1="0.19685" x2="4.33705" y2="0.20955" layer="21"/>
<rectangle x1="4.50215" y1="0.19685" x2="4.90855" y2="0.20955" layer="21"/>
<rectangle x1="5.13715" y1="0.19685" x2="5.55625" y2="0.20955" layer="21"/>
<rectangle x1="6.02615" y1="0.19685" x2="6.47065" y2="0.20955" layer="21"/>
<rectangle x1="6.71195" y1="0.19685" x2="7.13105" y2="0.20955" layer="21"/>
<rectangle x1="7.30885" y1="0.19685" x2="7.53745" y2="0.20955" layer="21"/>
<rectangle x1="7.63905" y1="0.19685" x2="7.86765" y2="0.20955" layer="21"/>
<rectangle x1="8.00735" y1="0.19685" x2="8.29945" y2="0.20955" layer="21"/>
<rectangle x1="8.36295" y1="0.19685" x2="8.59155" y2="0.20955" layer="21"/>
<rectangle x1="8.87095" y1="0.19685" x2="9.29005" y2="0.20955" layer="21"/>
<rectangle x1="9.46785" y1="0.19685" x2="9.70915" y2="0.20955" layer="21"/>
<rectangle x1="9.86155" y1="0.19685" x2="10.28065" y2="0.20955" layer="21"/>
<rectangle x1="10.64895" y1="0.19685" x2="11.04265" y2="0.20955" layer="21"/>
<rectangle x1="11.14425" y1="0.19685" x2="11.41095" y2="0.20955" layer="21"/>
<rectangle x1="11.43635" y1="0.19685" x2="11.65225" y2="0.20955" layer="21"/>
<rectangle x1="11.76655" y1="0.19685" x2="11.99515" y2="0.20955" layer="21"/>
<rectangle x1="12.02055" y1="0.19685" x2="12.29995" y2="0.20955" layer="21"/>
<rectangle x1="0.10795" y1="0.20955" x2="0.34925" y2="0.22225" layer="21"/>
<rectangle x1="0.47625" y1="0.20955" x2="0.71755" y2="0.22225" layer="21"/>
<rectangle x1="0.84455" y1="0.20955" x2="1.27635" y2="0.22225" layer="21"/>
<rectangle x1="1.44145" y1="0.20955" x2="1.67005" y2="0.22225" layer="21"/>
<rectangle x1="1.93675" y1="0.20955" x2="2.38125" y2="0.22225" layer="21"/>
<rectangle x1="2.57175" y1="0.20955" x2="3.01625" y2="0.22225" layer="21"/>
<rectangle x1="3.14325" y1="0.20955" x2="3.37185" y2="0.22225" layer="21"/>
<rectangle x1="3.39725" y1="0.20955" x2="3.67665" y2="0.22225" layer="21"/>
<rectangle x1="3.81635" y1="0.20955" x2="4.09575" y2="0.22225" layer="21"/>
<rectangle x1="4.10845" y1="0.20955" x2="4.33705" y2="0.22225" layer="21"/>
<rectangle x1="4.50215" y1="0.20955" x2="4.92125" y2="0.22225" layer="21"/>
<rectangle x1="5.12445" y1="0.20955" x2="5.56895" y2="0.22225" layer="21"/>
<rectangle x1="6.01345" y1="0.20955" x2="6.48335" y2="0.22225" layer="21"/>
<rectangle x1="6.69925" y1="0.20955" x2="7.14375" y2="0.22225" layer="21"/>
<rectangle x1="7.30885" y1="0.20955" x2="7.53745" y2="0.22225" layer="21"/>
<rectangle x1="7.63905" y1="0.20955" x2="7.86765" y2="0.22225" layer="21"/>
<rectangle x1="8.00735" y1="0.20955" x2="8.29945" y2="0.22225" layer="21"/>
<rectangle x1="8.36295" y1="0.20955" x2="8.59155" y2="0.22225" layer="21"/>
<rectangle x1="8.85825" y1="0.20955" x2="9.30275" y2="0.22225" layer="21"/>
<rectangle x1="9.46785" y1="0.20955" x2="9.70915" y2="0.22225" layer="21"/>
<rectangle x1="9.84885" y1="0.20955" x2="10.29335" y2="0.22225" layer="21"/>
<rectangle x1="10.64895" y1="0.20955" x2="11.04265" y2="0.22225" layer="21"/>
<rectangle x1="11.13155" y1="0.20955" x2="11.65225" y2="0.22225" layer="21"/>
<rectangle x1="11.76655" y1="0.20955" x2="11.99515" y2="0.22225" layer="21"/>
<rectangle x1="12.00785" y1="0.20955" x2="12.29995" y2="0.22225" layer="21"/>
<rectangle x1="0.10795" y1="0.22225" x2="0.34925" y2="0.23495" layer="21"/>
<rectangle x1="0.47625" y1="0.22225" x2="0.71755" y2="0.23495" layer="21"/>
<rectangle x1="0.83185" y1="0.22225" x2="1.28905" y2="0.23495" layer="21"/>
<rectangle x1="1.44145" y1="0.22225" x2="1.67005" y2="0.23495" layer="21"/>
<rectangle x1="1.92405" y1="0.22225" x2="2.39395" y2="0.23495" layer="21"/>
<rectangle x1="2.57175" y1="0.22225" x2="3.02895" y2="0.23495" layer="21"/>
<rectangle x1="3.14325" y1="0.22225" x2="3.37185" y2="0.23495" layer="21"/>
<rectangle x1="3.38455" y1="0.22225" x2="3.68935" y2="0.23495" layer="21"/>
<rectangle x1="3.81635" y1="0.22225" x2="4.33705" y2="0.23495" layer="21"/>
<rectangle x1="4.48945" y1="0.22225" x2="4.93395" y2="0.23495" layer="21"/>
<rectangle x1="5.12445" y1="0.22225" x2="5.58165" y2="0.23495" layer="21"/>
<rectangle x1="6.00075" y1="0.22225" x2="6.49605" y2="0.23495" layer="21"/>
<rectangle x1="6.68655" y1="0.22225" x2="7.15645" y2="0.23495" layer="21"/>
<rectangle x1="7.30885" y1="0.22225" x2="7.53745" y2="0.23495" layer="21"/>
<rectangle x1="7.63905" y1="0.22225" x2="7.86765" y2="0.23495" layer="21"/>
<rectangle x1="7.99465" y1="0.22225" x2="8.29945" y2="0.23495" layer="21"/>
<rectangle x1="8.36295" y1="0.22225" x2="8.59155" y2="0.23495" layer="21"/>
<rectangle x1="8.84555" y1="0.22225" x2="9.31545" y2="0.23495" layer="21"/>
<rectangle x1="9.46785" y1="0.22225" x2="9.70915" y2="0.23495" layer="21"/>
<rectangle x1="9.83615" y1="0.22225" x2="10.29335" y2="0.23495" layer="21"/>
<rectangle x1="10.64895" y1="0.22225" x2="11.04265" y2="0.23495" layer="21"/>
<rectangle x1="11.13155" y1="0.22225" x2="11.65225" y2="0.23495" layer="21"/>
<rectangle x1="11.76655" y1="0.22225" x2="12.31265" y2="0.23495" layer="21"/>
<rectangle x1="0.10795" y1="0.23495" x2="0.34925" y2="0.24765" layer="21"/>
<rectangle x1="0.47625" y1="0.23495" x2="0.71755" y2="0.24765" layer="21"/>
<rectangle x1="0.81915" y1="0.23495" x2="1.30175" y2="0.24765" layer="21"/>
<rectangle x1="1.44145" y1="0.23495" x2="1.67005" y2="0.24765" layer="21"/>
<rectangle x1="1.92405" y1="0.23495" x2="2.40665" y2="0.24765" layer="21"/>
<rectangle x1="2.55905" y1="0.23495" x2="3.02895" y2="0.24765" layer="21"/>
<rectangle x1="3.14325" y1="0.23495" x2="3.68935" y2="0.24765" layer="21"/>
<rectangle x1="3.80365" y1="0.23495" x2="4.33705" y2="0.24765" layer="21"/>
<rectangle x1="4.47675" y1="0.23495" x2="4.93395" y2="0.24765" layer="21"/>
<rectangle x1="5.11175" y1="0.23495" x2="5.58165" y2="0.24765" layer="21"/>
<rectangle x1="6.00075" y1="0.23495" x2="6.50875" y2="0.24765" layer="21"/>
<rectangle x1="6.68655" y1="0.23495" x2="7.16915" y2="0.24765" layer="21"/>
<rectangle x1="7.30885" y1="0.23495" x2="7.53745" y2="0.24765" layer="21"/>
<rectangle x1="7.63905" y1="0.23495" x2="7.86765" y2="0.24765" layer="21"/>
<rectangle x1="7.99465" y1="0.23495" x2="8.29945" y2="0.24765" layer="21"/>
<rectangle x1="8.36295" y1="0.23495" x2="8.59155" y2="0.24765" layer="21"/>
<rectangle x1="8.84555" y1="0.23495" x2="9.32815" y2="0.24765" layer="21"/>
<rectangle x1="9.46785" y1="0.23495" x2="9.70915" y2="0.24765" layer="21"/>
<rectangle x1="9.83615" y1="0.23495" x2="10.30605" y2="0.24765" layer="21"/>
<rectangle x1="10.64895" y1="0.23495" x2="11.04265" y2="0.24765" layer="21"/>
<rectangle x1="11.13155" y1="0.23495" x2="11.65225" y2="0.24765" layer="21"/>
<rectangle x1="11.76655" y1="0.23495" x2="12.31265" y2="0.24765" layer="21"/>
<rectangle x1="0.10795" y1="0.24765" x2="0.34925" y2="0.26035" layer="21"/>
<rectangle x1="0.47625" y1="0.24765" x2="0.70485" y2="0.26035" layer="21"/>
<rectangle x1="0.81915" y1="0.24765" x2="1.04775" y2="0.26035" layer="21"/>
<rectangle x1="1.07315" y1="0.24765" x2="1.30175" y2="0.26035" layer="21"/>
<rectangle x1="1.44145" y1="0.24765" x2="1.67005" y2="0.26035" layer="21"/>
<rectangle x1="1.91135" y1="0.24765" x2="2.15265" y2="0.26035" layer="21"/>
<rectangle x1="2.17805" y1="0.24765" x2="2.40665" y2="0.26035" layer="21"/>
<rectangle x1="2.55905" y1="0.24765" x2="2.77495" y2="0.26035" layer="21"/>
<rectangle x1="2.80035" y1="0.24765" x2="3.04165" y2="0.26035" layer="21"/>
<rectangle x1="3.14325" y1="0.24765" x2="3.40995" y2="0.26035" layer="21"/>
<rectangle x1="3.43535" y1="0.24765" x2="3.68935" y2="0.26035" layer="21"/>
<rectangle x1="3.80365" y1="0.24765" x2="4.04495" y2="0.26035" layer="21"/>
<rectangle x1="4.07035" y1="0.24765" x2="4.33705" y2="0.26035" layer="21"/>
<rectangle x1="4.47675" y1="0.24765" x2="4.71805" y2="0.26035" layer="21"/>
<rectangle x1="4.74345" y1="0.24765" x2="4.94665" y2="0.26035" layer="21"/>
<rectangle x1="5.09905" y1="0.24765" x2="5.34035" y2="0.26035" layer="21"/>
<rectangle x1="5.36575" y1="0.24765" x2="5.59435" y2="0.26035" layer="21"/>
<rectangle x1="5.98805" y1="0.24765" x2="6.50875" y2="0.26035" layer="21"/>
<rectangle x1="6.67385" y1="0.24765" x2="6.91515" y2="0.26035" layer="21"/>
<rectangle x1="6.94055" y1="0.24765" x2="7.16915" y2="0.26035" layer="21"/>
<rectangle x1="7.30885" y1="0.24765" x2="7.53745" y2="0.26035" layer="21"/>
<rectangle x1="7.63905" y1="0.24765" x2="7.86765" y2="0.26035" layer="21"/>
<rectangle x1="7.99465" y1="0.24765" x2="8.28675" y2="0.26035" layer="21"/>
<rectangle x1="8.36295" y1="0.24765" x2="8.59155" y2="0.26035" layer="21"/>
<rectangle x1="8.83285" y1="0.24765" x2="9.07415" y2="0.26035" layer="21"/>
<rectangle x1="9.09955" y1="0.24765" x2="9.32815" y2="0.26035" layer="21"/>
<rectangle x1="9.46785" y1="0.24765" x2="9.70915" y2="0.26035" layer="21"/>
<rectangle x1="9.82345" y1="0.24765" x2="10.03935" y2="0.26035" layer="21"/>
<rectangle x1="10.07745" y1="0.24765" x2="10.30605" y2="0.26035" layer="21"/>
<rectangle x1="10.64895" y1="0.24765" x2="11.04265" y2="0.26035" layer="21"/>
<rectangle x1="11.11885" y1="0.24765" x2="11.37285" y2="0.26035" layer="21"/>
<rectangle x1="11.39825" y1="0.24765" x2="11.65225" y2="0.26035" layer="21"/>
<rectangle x1="11.76655" y1="0.24765" x2="12.03325" y2="0.26035" layer="21"/>
<rectangle x1="12.05865" y1="0.24765" x2="12.31265" y2="0.26035" layer="21"/>
<rectangle x1="0.10795" y1="0.26035" x2="0.34925" y2="0.27305" layer="21"/>
<rectangle x1="0.47625" y1="0.26035" x2="0.70485" y2="0.27305" layer="21"/>
<rectangle x1="0.80645" y1="0.26035" x2="1.02235" y2="0.27305" layer="21"/>
<rectangle x1="1.09855" y1="0.26035" x2="1.31445" y2="0.27305" layer="21"/>
<rectangle x1="1.44145" y1="0.26035" x2="1.67005" y2="0.27305" layer="21"/>
<rectangle x1="1.91135" y1="0.26035" x2="2.12725" y2="0.27305" layer="21"/>
<rectangle x1="2.20345" y1="0.26035" x2="2.41935" y2="0.27305" layer="21"/>
<rectangle x1="2.54635" y1="0.26035" x2="2.74955" y2="0.27305" layer="21"/>
<rectangle x1="2.83845" y1="0.26035" x2="3.04165" y2="0.27305" layer="21"/>
<rectangle x1="3.14325" y1="0.26035" x2="3.38455" y2="0.27305" layer="21"/>
<rectangle x1="3.46075" y1="0.26035" x2="3.68935" y2="0.27305" layer="21"/>
<rectangle x1="3.80365" y1="0.26035" x2="4.01955" y2="0.27305" layer="21"/>
<rectangle x1="4.09575" y1="0.26035" x2="4.33705" y2="0.27305" layer="21"/>
<rectangle x1="4.46405" y1="0.26035" x2="4.69265" y2="0.27305" layer="21"/>
<rectangle x1="4.75615" y1="0.26035" x2="4.94665" y2="0.27305" layer="21"/>
<rectangle x1="5.09905" y1="0.26035" x2="5.31495" y2="0.27305" layer="21"/>
<rectangle x1="5.39115" y1="0.26035" x2="5.59435" y2="0.27305" layer="21"/>
<rectangle x1="5.98805" y1="0.26035" x2="6.52145" y2="0.27305" layer="21"/>
<rectangle x1="6.67385" y1="0.26035" x2="6.88975" y2="0.27305" layer="21"/>
<rectangle x1="6.96595" y1="0.26035" x2="7.18185" y2="0.27305" layer="21"/>
<rectangle x1="7.30885" y1="0.26035" x2="7.53745" y2="0.27305" layer="21"/>
<rectangle x1="7.63905" y1="0.26035" x2="7.86765" y2="0.27305" layer="21"/>
<rectangle x1="7.99465" y1="0.26035" x2="8.22325" y2="0.27305" layer="21"/>
<rectangle x1="8.36295" y1="0.26035" x2="8.59155" y2="0.27305" layer="21"/>
<rectangle x1="8.83285" y1="0.26035" x2="9.04875" y2="0.27305" layer="21"/>
<rectangle x1="9.12495" y1="0.26035" x2="9.34085" y2="0.27305" layer="21"/>
<rectangle x1="9.46785" y1="0.26035" x2="9.70915" y2="0.27305" layer="21"/>
<rectangle x1="9.82345" y1="0.26035" x2="10.02665" y2="0.27305" layer="21"/>
<rectangle x1="10.10285" y1="0.26035" x2="10.31875" y2="0.27305" layer="21"/>
<rectangle x1="10.64895" y1="0.26035" x2="11.04265" y2="0.27305" layer="21"/>
<rectangle x1="11.11885" y1="0.26035" x2="11.34745" y2="0.27305" layer="21"/>
<rectangle x1="11.42365" y1="0.26035" x2="11.65225" y2="0.27305" layer="21"/>
<rectangle x1="11.76655" y1="0.26035" x2="12.00785" y2="0.27305" layer="21"/>
<rectangle x1="12.08405" y1="0.26035" x2="12.31265" y2="0.27305" layer="21"/>
<rectangle x1="0.10795" y1="0.27305" x2="0.34925" y2="0.28575" layer="21"/>
<rectangle x1="0.47625" y1="0.27305" x2="0.70485" y2="0.28575" layer="21"/>
<rectangle x1="0.80645" y1="0.27305" x2="1.02235" y2="0.28575" layer="21"/>
<rectangle x1="1.11125" y1="0.27305" x2="1.31445" y2="0.28575" layer="21"/>
<rectangle x1="1.44145" y1="0.27305" x2="1.67005" y2="0.28575" layer="21"/>
<rectangle x1="1.91135" y1="0.27305" x2="2.12725" y2="0.28575" layer="21"/>
<rectangle x1="2.20345" y1="0.27305" x2="2.41935" y2="0.28575" layer="21"/>
<rectangle x1="2.54635" y1="0.27305" x2="2.73685" y2="0.28575" layer="21"/>
<rectangle x1="2.83845" y1="0.27305" x2="3.04165" y2="0.28575" layer="21"/>
<rectangle x1="3.14325" y1="0.27305" x2="3.38455" y2="0.28575" layer="21"/>
<rectangle x1="3.47345" y1="0.27305" x2="3.68935" y2="0.28575" layer="21"/>
<rectangle x1="3.80365" y1="0.27305" x2="4.01955" y2="0.28575" layer="21"/>
<rectangle x1="4.10845" y1="0.27305" x2="4.33705" y2="0.28575" layer="21"/>
<rectangle x1="4.46405" y1="0.27305" x2="4.67995" y2="0.28575" layer="21"/>
<rectangle x1="4.76885" y1="0.27305" x2="4.95935" y2="0.28575" layer="21"/>
<rectangle x1="5.09905" y1="0.27305" x2="5.30225" y2="0.28575" layer="21"/>
<rectangle x1="5.40385" y1="0.27305" x2="5.60705" y2="0.28575" layer="21"/>
<rectangle x1="5.97535" y1="0.27305" x2="6.21665" y2="0.28575" layer="21"/>
<rectangle x1="6.29285" y1="0.27305" x2="6.52145" y2="0.28575" layer="21"/>
<rectangle x1="6.67385" y1="0.27305" x2="6.88975" y2="0.28575" layer="21"/>
<rectangle x1="6.96595" y1="0.27305" x2="7.18185" y2="0.28575" layer="21"/>
<rectangle x1="7.30885" y1="0.27305" x2="7.53745" y2="0.28575" layer="21"/>
<rectangle x1="7.63905" y1="0.27305" x2="7.86765" y2="0.28575" layer="21"/>
<rectangle x1="7.99465" y1="0.27305" x2="8.22325" y2="0.28575" layer="21"/>
<rectangle x1="8.36295" y1="0.27305" x2="8.59155" y2="0.28575" layer="21"/>
<rectangle x1="8.83285" y1="0.27305" x2="9.04875" y2="0.28575" layer="21"/>
<rectangle x1="9.13765" y1="0.27305" x2="9.34085" y2="0.28575" layer="21"/>
<rectangle x1="9.46785" y1="0.27305" x2="9.70915" y2="0.28575" layer="21"/>
<rectangle x1="9.82345" y1="0.27305" x2="10.01395" y2="0.28575" layer="21"/>
<rectangle x1="10.11555" y1="0.27305" x2="10.31875" y2="0.28575" layer="21"/>
<rectangle x1="10.64895" y1="0.27305" x2="11.04265" y2="0.28575" layer="21"/>
<rectangle x1="11.11885" y1="0.27305" x2="11.33475" y2="0.28575" layer="21"/>
<rectangle x1="11.42365" y1="0.27305" x2="11.65225" y2="0.28575" layer="21"/>
<rectangle x1="11.76655" y1="0.27305" x2="12.00785" y2="0.28575" layer="21"/>
<rectangle x1="12.08405" y1="0.27305" x2="12.31265" y2="0.28575" layer="21"/>
<rectangle x1="0.12065" y1="0.28575" x2="0.34925" y2="0.29845" layer="21"/>
<rectangle x1="0.47625" y1="0.28575" x2="0.70485" y2="0.29845" layer="21"/>
<rectangle x1="0.80645" y1="0.28575" x2="1.00965" y2="0.29845" layer="21"/>
<rectangle x1="1.11125" y1="0.28575" x2="1.31445" y2="0.29845" layer="21"/>
<rectangle x1="1.44145" y1="0.28575" x2="1.67005" y2="0.29845" layer="21"/>
<rectangle x1="1.89865" y1="0.28575" x2="2.11455" y2="0.29845" layer="21"/>
<rectangle x1="2.21615" y1="0.28575" x2="2.41935" y2="0.29845" layer="21"/>
<rectangle x1="2.54635" y1="0.28575" x2="2.73685" y2="0.29845" layer="21"/>
<rectangle x1="2.85115" y1="0.28575" x2="3.05435" y2="0.29845" layer="21"/>
<rectangle x1="3.14325" y1="0.28575" x2="3.38455" y2="0.29845" layer="21"/>
<rectangle x1="3.47345" y1="0.28575" x2="3.70205" y2="0.29845" layer="21"/>
<rectangle x1="3.80365" y1="0.28575" x2="4.00685" y2="0.29845" layer="21"/>
<rectangle x1="4.10845" y1="0.28575" x2="4.33705" y2="0.29845" layer="21"/>
<rectangle x1="4.46405" y1="0.28575" x2="4.67995" y2="0.29845" layer="21"/>
<rectangle x1="4.76885" y1="0.28575" x2="4.95935" y2="0.29845" layer="21"/>
<rectangle x1="5.08635" y1="0.28575" x2="5.30225" y2="0.29845" layer="21"/>
<rectangle x1="5.40385" y1="0.28575" x2="5.60705" y2="0.29845" layer="21"/>
<rectangle x1="5.97535" y1="0.28575" x2="6.20395" y2="0.29845" layer="21"/>
<rectangle x1="6.30555" y1="0.28575" x2="6.53415" y2="0.29845" layer="21"/>
<rectangle x1="6.66115" y1="0.28575" x2="6.87705" y2="0.29845" layer="21"/>
<rectangle x1="6.97865" y1="0.28575" x2="7.18185" y2="0.29845" layer="21"/>
<rectangle x1="7.30885" y1="0.28575" x2="7.53745" y2="0.29845" layer="21"/>
<rectangle x1="7.63905" y1="0.28575" x2="7.86765" y2="0.29845" layer="21"/>
<rectangle x1="7.99465" y1="0.28575" x2="8.22325" y2="0.29845" layer="21"/>
<rectangle x1="8.36295" y1="0.28575" x2="8.59155" y2="0.29845" layer="21"/>
<rectangle x1="8.83285" y1="0.28575" x2="9.04875" y2="0.29845" layer="21"/>
<rectangle x1="9.13765" y1="0.28575" x2="9.34085" y2="0.29845" layer="21"/>
<rectangle x1="9.46785" y1="0.28575" x2="9.70915" y2="0.29845" layer="21"/>
<rectangle x1="9.81075" y1="0.28575" x2="10.01395" y2="0.29845" layer="21"/>
<rectangle x1="10.11555" y1="0.28575" x2="10.31875" y2="0.29845" layer="21"/>
<rectangle x1="10.64895" y1="0.28575" x2="11.04265" y2="0.29845" layer="21"/>
<rectangle x1="11.11885" y1="0.28575" x2="11.33475" y2="0.29845" layer="21"/>
<rectangle x1="11.42365" y1="0.28575" x2="11.65225" y2="0.29845" layer="21"/>
<rectangle x1="11.76655" y1="0.28575" x2="11.99515" y2="0.29845" layer="21"/>
<rectangle x1="12.09675" y1="0.28575" x2="12.32535" y2="0.29845" layer="21"/>
<rectangle x1="0.12065" y1="0.29845" x2="0.34925" y2="0.31115" layer="21"/>
<rectangle x1="0.47625" y1="0.29845" x2="0.70485" y2="0.31115" layer="21"/>
<rectangle x1="0.79375" y1="0.29845" x2="1.00965" y2="0.31115" layer="21"/>
<rectangle x1="1.12395" y1="0.29845" x2="1.32715" y2="0.31115" layer="21"/>
<rectangle x1="1.44145" y1="0.29845" x2="1.67005" y2="0.31115" layer="21"/>
<rectangle x1="1.89865" y1="0.29845" x2="2.11455" y2="0.31115" layer="21"/>
<rectangle x1="2.21615" y1="0.29845" x2="2.43205" y2="0.31115" layer="21"/>
<rectangle x1="2.54635" y1="0.29845" x2="2.73685" y2="0.31115" layer="21"/>
<rectangle x1="2.85115" y1="0.29845" x2="3.05435" y2="0.31115" layer="21"/>
<rectangle x1="3.14325" y1="0.29845" x2="3.37185" y2="0.31115" layer="21"/>
<rectangle x1="3.47345" y1="0.29845" x2="3.70205" y2="0.31115" layer="21"/>
<rectangle x1="3.80365" y1="0.29845" x2="4.00685" y2="0.31115" layer="21"/>
<rectangle x1="4.10845" y1="0.29845" x2="4.33705" y2="0.31115" layer="21"/>
<rectangle x1="4.45135" y1="0.29845" x2="4.66725" y2="0.31115" layer="21"/>
<rectangle x1="4.78155" y1="0.29845" x2="4.95935" y2="0.31115" layer="21"/>
<rectangle x1="5.08635" y1="0.29845" x2="5.30225" y2="0.31115" layer="21"/>
<rectangle x1="5.40385" y1="0.29845" x2="5.60705" y2="0.31115" layer="21"/>
<rectangle x1="5.97535" y1="0.29845" x2="6.20395" y2="0.31115" layer="21"/>
<rectangle x1="6.30555" y1="0.29845" x2="6.53415" y2="0.31115" layer="21"/>
<rectangle x1="6.66115" y1="0.29845" x2="6.87705" y2="0.31115" layer="21"/>
<rectangle x1="6.97865" y1="0.29845" x2="7.19455" y2="0.31115" layer="21"/>
<rectangle x1="7.30885" y1="0.29845" x2="7.53745" y2="0.31115" layer="21"/>
<rectangle x1="7.63905" y1="0.29845" x2="7.86765" y2="0.31115" layer="21"/>
<rectangle x1="7.99465" y1="0.29845" x2="8.22325" y2="0.31115" layer="21"/>
<rectangle x1="8.36295" y1="0.29845" x2="8.59155" y2="0.31115" layer="21"/>
<rectangle x1="8.82015" y1="0.29845" x2="9.03605" y2="0.31115" layer="21"/>
<rectangle x1="9.13765" y1="0.29845" x2="9.35355" y2="0.31115" layer="21"/>
<rectangle x1="9.46785" y1="0.29845" x2="9.70915" y2="0.31115" layer="21"/>
<rectangle x1="9.81075" y1="0.29845" x2="10.00125" y2="0.31115" layer="21"/>
<rectangle x1="10.11555" y1="0.29845" x2="10.31875" y2="0.31115" layer="21"/>
<rectangle x1="10.64895" y1="0.29845" x2="11.04265" y2="0.31115" layer="21"/>
<rectangle x1="11.11885" y1="0.29845" x2="11.33475" y2="0.31115" layer="21"/>
<rectangle x1="11.43635" y1="0.29845" x2="11.65225" y2="0.31115" layer="21"/>
<rectangle x1="11.76655" y1="0.29845" x2="11.99515" y2="0.31115" layer="21"/>
<rectangle x1="12.09675" y1="0.29845" x2="12.32535" y2="0.31115" layer="21"/>
<rectangle x1="0.12065" y1="0.31115" x2="0.34925" y2="0.32385" layer="21"/>
<rectangle x1="0.47625" y1="0.31115" x2="0.70485" y2="0.32385" layer="21"/>
<rectangle x1="0.79375" y1="0.31115" x2="1.00965" y2="0.32385" layer="21"/>
<rectangle x1="1.12395" y1="0.31115" x2="1.32715" y2="0.32385" layer="21"/>
<rectangle x1="1.44145" y1="0.31115" x2="1.67005" y2="0.32385" layer="21"/>
<rectangle x1="1.89865" y1="0.31115" x2="2.11455" y2="0.32385" layer="21"/>
<rectangle x1="2.21615" y1="0.31115" x2="2.43205" y2="0.32385" layer="21"/>
<rectangle x1="2.54635" y1="0.31115" x2="2.73685" y2="0.32385" layer="21"/>
<rectangle x1="2.85115" y1="0.31115" x2="3.05435" y2="0.32385" layer="21"/>
<rectangle x1="3.14325" y1="0.31115" x2="3.37185" y2="0.32385" layer="21"/>
<rectangle x1="3.47345" y1="0.31115" x2="3.70205" y2="0.32385" layer="21"/>
<rectangle x1="3.80365" y1="0.31115" x2="4.00685" y2="0.32385" layer="21"/>
<rectangle x1="4.10845" y1="0.31115" x2="4.33705" y2="0.32385" layer="21"/>
<rectangle x1="4.45135" y1="0.31115" x2="4.66725" y2="0.32385" layer="21"/>
<rectangle x1="4.78155" y1="0.31115" x2="4.97205" y2="0.32385" layer="21"/>
<rectangle x1="5.08635" y1="0.31115" x2="5.30225" y2="0.32385" layer="21"/>
<rectangle x1="5.40385" y1="0.31115" x2="5.61975" y2="0.32385" layer="21"/>
<rectangle x1="5.97535" y1="0.31115" x2="6.19125" y2="0.32385" layer="21"/>
<rectangle x1="6.30555" y1="0.31115" x2="6.53415" y2="0.32385" layer="21"/>
<rectangle x1="6.66115" y1="0.31115" x2="6.87705" y2="0.32385" layer="21"/>
<rectangle x1="6.97865" y1="0.31115" x2="7.19455" y2="0.32385" layer="21"/>
<rectangle x1="7.30885" y1="0.31115" x2="7.53745" y2="0.32385" layer="21"/>
<rectangle x1="7.63905" y1="0.31115" x2="7.86765" y2="0.32385" layer="21"/>
<rectangle x1="7.99465" y1="0.31115" x2="8.21055" y2="0.32385" layer="21"/>
<rectangle x1="8.36295" y1="0.31115" x2="8.59155" y2="0.32385" layer="21"/>
<rectangle x1="8.82015" y1="0.31115" x2="9.03605" y2="0.32385" layer="21"/>
<rectangle x1="9.13765" y1="0.31115" x2="9.35355" y2="0.32385" layer="21"/>
<rectangle x1="9.46785" y1="0.31115" x2="9.70915" y2="0.32385" layer="21"/>
<rectangle x1="9.81075" y1="0.31115" x2="10.00125" y2="0.32385" layer="21"/>
<rectangle x1="10.11555" y1="0.31115" x2="10.31875" y2="0.32385" layer="21"/>
<rectangle x1="10.64895" y1="0.31115" x2="11.04265" y2="0.32385" layer="21"/>
<rectangle x1="11.11885" y1="0.31115" x2="11.32205" y2="0.32385" layer="21"/>
<rectangle x1="11.43635" y1="0.31115" x2="11.65225" y2="0.32385" layer="21"/>
<rectangle x1="11.76655" y1="0.31115" x2="11.99515" y2="0.32385" layer="21"/>
<rectangle x1="12.09675" y1="0.31115" x2="12.32535" y2="0.32385" layer="21"/>
<rectangle x1="0.12065" y1="0.32385" x2="0.70485" y2="0.33655" layer="21"/>
<rectangle x1="0.79375" y1="0.32385" x2="1.00965" y2="0.33655" layer="21"/>
<rectangle x1="1.12395" y1="0.32385" x2="1.32715" y2="0.33655" layer="21"/>
<rectangle x1="1.44145" y1="0.32385" x2="1.67005" y2="0.33655" layer="21"/>
<rectangle x1="1.89865" y1="0.32385" x2="2.11455" y2="0.33655" layer="21"/>
<rectangle x1="2.21615" y1="0.32385" x2="2.43205" y2="0.33655" layer="21"/>
<rectangle x1="2.53365" y1="0.32385" x2="2.73685" y2="0.33655" layer="21"/>
<rectangle x1="2.85115" y1="0.32385" x2="3.05435" y2="0.33655" layer="21"/>
<rectangle x1="3.14325" y1="0.32385" x2="3.37185" y2="0.33655" layer="21"/>
<rectangle x1="3.47345" y1="0.32385" x2="3.70205" y2="0.33655" layer="21"/>
<rectangle x1="3.80365" y1="0.32385" x2="4.00685" y2="0.33655" layer="21"/>
<rectangle x1="4.10845" y1="0.32385" x2="4.33705" y2="0.33655" layer="21"/>
<rectangle x1="4.45135" y1="0.32385" x2="4.66725" y2="0.33655" layer="21"/>
<rectangle x1="4.78155" y1="0.32385" x2="4.97205" y2="0.33655" layer="21"/>
<rectangle x1="5.08635" y1="0.32385" x2="5.30225" y2="0.33655" layer="21"/>
<rectangle x1="5.40385" y1="0.32385" x2="5.61975" y2="0.33655" layer="21"/>
<rectangle x1="5.96265" y1="0.32385" x2="6.19125" y2="0.33655" layer="21"/>
<rectangle x1="6.30555" y1="0.32385" x2="6.53415" y2="0.33655" layer="21"/>
<rectangle x1="6.66115" y1="0.32385" x2="6.87705" y2="0.33655" layer="21"/>
<rectangle x1="6.97865" y1="0.32385" x2="7.19455" y2="0.33655" layer="21"/>
<rectangle x1="7.30885" y1="0.32385" x2="7.53745" y2="0.33655" layer="21"/>
<rectangle x1="7.63905" y1="0.32385" x2="7.86765" y2="0.33655" layer="21"/>
<rectangle x1="7.99465" y1="0.32385" x2="8.21055" y2="0.33655" layer="21"/>
<rectangle x1="8.36295" y1="0.32385" x2="8.59155" y2="0.33655" layer="21"/>
<rectangle x1="8.82015" y1="0.32385" x2="9.03605" y2="0.33655" layer="21"/>
<rectangle x1="9.13765" y1="0.32385" x2="9.35355" y2="0.33655" layer="21"/>
<rectangle x1="9.46785" y1="0.32385" x2="9.70915" y2="0.33655" layer="21"/>
<rectangle x1="9.81075" y1="0.32385" x2="10.00125" y2="0.33655" layer="21"/>
<rectangle x1="10.11555" y1="0.32385" x2="10.31875" y2="0.33655" layer="21"/>
<rectangle x1="10.64895" y1="0.32385" x2="10.89025" y2="0.33655" layer="21"/>
<rectangle x1="11.11885" y1="0.32385" x2="11.32205" y2="0.33655" layer="21"/>
<rectangle x1="11.43635" y1="0.32385" x2="11.65225" y2="0.33655" layer="21"/>
<rectangle x1="11.76655" y1="0.32385" x2="11.99515" y2="0.33655" layer="21"/>
<rectangle x1="12.09675" y1="0.32385" x2="12.32535" y2="0.33655" layer="21"/>
<rectangle x1="0.12065" y1="0.33655" x2="0.69215" y2="0.34925" layer="21"/>
<rectangle x1="0.79375" y1="0.33655" x2="1.00965" y2="0.34925" layer="21"/>
<rectangle x1="1.12395" y1="0.33655" x2="1.32715" y2="0.34925" layer="21"/>
<rectangle x1="1.44145" y1="0.33655" x2="1.67005" y2="0.34925" layer="21"/>
<rectangle x1="1.89865" y1="0.33655" x2="2.11455" y2="0.34925" layer="21"/>
<rectangle x1="2.21615" y1="0.33655" x2="2.43205" y2="0.34925" layer="21"/>
<rectangle x1="2.53365" y1="0.33655" x2="2.73685" y2="0.34925" layer="21"/>
<rectangle x1="2.85115" y1="0.33655" x2="3.05435" y2="0.34925" layer="21"/>
<rectangle x1="3.14325" y1="0.33655" x2="3.37185" y2="0.34925" layer="21"/>
<rectangle x1="3.47345" y1="0.33655" x2="3.70205" y2="0.34925" layer="21"/>
<rectangle x1="3.80365" y1="0.33655" x2="4.00685" y2="0.34925" layer="21"/>
<rectangle x1="4.10845" y1="0.33655" x2="4.33705" y2="0.34925" layer="21"/>
<rectangle x1="4.45135" y1="0.33655" x2="4.66725" y2="0.34925" layer="21"/>
<rectangle x1="4.78155" y1="0.33655" x2="4.97205" y2="0.34925" layer="21"/>
<rectangle x1="5.07365" y1="0.33655" x2="5.30225" y2="0.34925" layer="21"/>
<rectangle x1="5.40385" y1="0.33655" x2="5.61975" y2="0.34925" layer="21"/>
<rectangle x1="5.96265" y1="0.33655" x2="6.19125" y2="0.34925" layer="21"/>
<rectangle x1="6.30555" y1="0.33655" x2="6.53415" y2="0.34925" layer="21"/>
<rectangle x1="6.66115" y1="0.33655" x2="6.87705" y2="0.34925" layer="21"/>
<rectangle x1="6.97865" y1="0.33655" x2="7.19455" y2="0.34925" layer="21"/>
<rectangle x1="7.30885" y1="0.33655" x2="7.53745" y2="0.34925" layer="21"/>
<rectangle x1="7.63905" y1="0.33655" x2="7.86765" y2="0.34925" layer="21"/>
<rectangle x1="7.99465" y1="0.33655" x2="8.21055" y2="0.34925" layer="21"/>
<rectangle x1="8.36295" y1="0.33655" x2="8.59155" y2="0.34925" layer="21"/>
<rectangle x1="8.82015" y1="0.33655" x2="9.03605" y2="0.34925" layer="21"/>
<rectangle x1="9.13765" y1="0.33655" x2="9.35355" y2="0.34925" layer="21"/>
<rectangle x1="9.46785" y1="0.33655" x2="9.70915" y2="0.34925" layer="21"/>
<rectangle x1="9.81075" y1="0.33655" x2="10.00125" y2="0.34925" layer="21"/>
<rectangle x1="10.11555" y1="0.33655" x2="10.33145" y2="0.34925" layer="21"/>
<rectangle x1="10.64895" y1="0.33655" x2="10.89025" y2="0.34925" layer="21"/>
<rectangle x1="11.11885" y1="0.33655" x2="11.32205" y2="0.34925" layer="21"/>
<rectangle x1="11.43635" y1="0.33655" x2="11.65225" y2="0.34925" layer="21"/>
<rectangle x1="11.76655" y1="0.33655" x2="11.99515" y2="0.34925" layer="21"/>
<rectangle x1="12.09675" y1="0.33655" x2="12.32535" y2="0.34925" layer="21"/>
<rectangle x1="0.12065" y1="0.34925" x2="0.69215" y2="0.36195" layer="21"/>
<rectangle x1="0.79375" y1="0.34925" x2="1.00965" y2="0.36195" layer="21"/>
<rectangle x1="1.12395" y1="0.34925" x2="1.32715" y2="0.36195" layer="21"/>
<rectangle x1="1.44145" y1="0.34925" x2="1.67005" y2="0.36195" layer="21"/>
<rectangle x1="1.89865" y1="0.34925" x2="2.11455" y2="0.36195" layer="21"/>
<rectangle x1="2.21615" y1="0.34925" x2="2.43205" y2="0.36195" layer="21"/>
<rectangle x1="2.53365" y1="0.34925" x2="2.73685" y2="0.36195" layer="21"/>
<rectangle x1="2.85115" y1="0.34925" x2="3.05435" y2="0.36195" layer="21"/>
<rectangle x1="3.14325" y1="0.34925" x2="3.37185" y2="0.36195" layer="21"/>
<rectangle x1="3.47345" y1="0.34925" x2="3.70205" y2="0.36195" layer="21"/>
<rectangle x1="3.80365" y1="0.34925" x2="4.00685" y2="0.36195" layer="21"/>
<rectangle x1="4.10845" y1="0.34925" x2="4.33705" y2="0.36195" layer="21"/>
<rectangle x1="4.45135" y1="0.34925" x2="4.66725" y2="0.36195" layer="21"/>
<rectangle x1="4.78155" y1="0.34925" x2="4.97205" y2="0.36195" layer="21"/>
<rectangle x1="5.07365" y1="0.34925" x2="5.30225" y2="0.36195" layer="21"/>
<rectangle x1="5.40385" y1="0.34925" x2="5.61975" y2="0.36195" layer="21"/>
<rectangle x1="5.96265" y1="0.34925" x2="6.19125" y2="0.36195" layer="21"/>
<rectangle x1="6.30555" y1="0.34925" x2="6.54685" y2="0.36195" layer="21"/>
<rectangle x1="6.66115" y1="0.34925" x2="6.87705" y2="0.36195" layer="21"/>
<rectangle x1="6.97865" y1="0.34925" x2="7.19455" y2="0.36195" layer="21"/>
<rectangle x1="7.30885" y1="0.34925" x2="7.53745" y2="0.36195" layer="21"/>
<rectangle x1="7.63905" y1="0.34925" x2="7.86765" y2="0.36195" layer="21"/>
<rectangle x1="7.99465" y1="0.34925" x2="8.21055" y2="0.36195" layer="21"/>
<rectangle x1="8.36295" y1="0.34925" x2="8.59155" y2="0.36195" layer="21"/>
<rectangle x1="8.82015" y1="0.34925" x2="9.03605" y2="0.36195" layer="21"/>
<rectangle x1="9.13765" y1="0.34925" x2="9.35355" y2="0.36195" layer="21"/>
<rectangle x1="9.46785" y1="0.34925" x2="9.70915" y2="0.36195" layer="21"/>
<rectangle x1="9.81075" y1="0.34925" x2="10.00125" y2="0.36195" layer="21"/>
<rectangle x1="10.11555" y1="0.34925" x2="10.33145" y2="0.36195" layer="21"/>
<rectangle x1="10.64895" y1="0.34925" x2="10.89025" y2="0.36195" layer="21"/>
<rectangle x1="11.11885" y1="0.34925" x2="11.32205" y2="0.36195" layer="21"/>
<rectangle x1="11.43635" y1="0.34925" x2="11.65225" y2="0.36195" layer="21"/>
<rectangle x1="11.76655" y1="0.34925" x2="11.99515" y2="0.36195" layer="21"/>
<rectangle x1="12.09675" y1="0.34925" x2="12.32535" y2="0.36195" layer="21"/>
<rectangle x1="0.12065" y1="0.36195" x2="0.69215" y2="0.37465" layer="21"/>
<rectangle x1="0.78105" y1="0.36195" x2="1.00965" y2="0.37465" layer="21"/>
<rectangle x1="1.12395" y1="0.36195" x2="1.32715" y2="0.37465" layer="21"/>
<rectangle x1="1.44145" y1="0.36195" x2="1.67005" y2="0.37465" layer="21"/>
<rectangle x1="1.89865" y1="0.36195" x2="2.11455" y2="0.37465" layer="21"/>
<rectangle x1="2.21615" y1="0.36195" x2="2.43205" y2="0.37465" layer="21"/>
<rectangle x1="2.53365" y1="0.36195" x2="2.73685" y2="0.37465" layer="21"/>
<rectangle x1="2.85115" y1="0.36195" x2="3.05435" y2="0.37465" layer="21"/>
<rectangle x1="3.14325" y1="0.36195" x2="3.37185" y2="0.37465" layer="21"/>
<rectangle x1="3.47345" y1="0.36195" x2="3.70205" y2="0.37465" layer="21"/>
<rectangle x1="3.80365" y1="0.36195" x2="4.00685" y2="0.37465" layer="21"/>
<rectangle x1="4.10845" y1="0.36195" x2="4.33705" y2="0.37465" layer="21"/>
<rectangle x1="4.45135" y1="0.36195" x2="4.66725" y2="0.37465" layer="21"/>
<rectangle x1="4.78155" y1="0.36195" x2="4.97205" y2="0.37465" layer="21"/>
<rectangle x1="5.07365" y1="0.36195" x2="5.30225" y2="0.37465" layer="21"/>
<rectangle x1="5.40385" y1="0.36195" x2="5.61975" y2="0.37465" layer="21"/>
<rectangle x1="5.96265" y1="0.36195" x2="6.19125" y2="0.37465" layer="21"/>
<rectangle x1="6.30555" y1="0.36195" x2="6.54685" y2="0.37465" layer="21"/>
<rectangle x1="6.66115" y1="0.36195" x2="6.87705" y2="0.37465" layer="21"/>
<rectangle x1="6.97865" y1="0.36195" x2="7.19455" y2="0.37465" layer="21"/>
<rectangle x1="7.30885" y1="0.36195" x2="7.53745" y2="0.37465" layer="21"/>
<rectangle x1="7.63905" y1="0.36195" x2="7.86765" y2="0.37465" layer="21"/>
<rectangle x1="7.99465" y1="0.36195" x2="8.21055" y2="0.37465" layer="21"/>
<rectangle x1="8.36295" y1="0.36195" x2="8.59155" y2="0.37465" layer="21"/>
<rectangle x1="8.82015" y1="0.36195" x2="9.03605" y2="0.37465" layer="21"/>
<rectangle x1="9.13765" y1="0.36195" x2="9.35355" y2="0.37465" layer="21"/>
<rectangle x1="9.46785" y1="0.36195" x2="9.70915" y2="0.37465" layer="21"/>
<rectangle x1="9.81075" y1="0.36195" x2="10.00125" y2="0.37465" layer="21"/>
<rectangle x1="10.11555" y1="0.36195" x2="10.33145" y2="0.37465" layer="21"/>
<rectangle x1="10.64895" y1="0.36195" x2="10.89025" y2="0.37465" layer="21"/>
<rectangle x1="11.11885" y1="0.36195" x2="11.32205" y2="0.37465" layer="21"/>
<rectangle x1="11.43635" y1="0.36195" x2="11.65225" y2="0.37465" layer="21"/>
<rectangle x1="11.76655" y1="0.36195" x2="11.99515" y2="0.37465" layer="21"/>
<rectangle x1="12.09675" y1="0.36195" x2="12.32535" y2="0.37465" layer="21"/>
<rectangle x1="0.12065" y1="0.37465" x2="0.69215" y2="0.38735" layer="21"/>
<rectangle x1="0.78105" y1="0.37465" x2="1.00965" y2="0.38735" layer="21"/>
<rectangle x1="1.12395" y1="0.37465" x2="1.33985" y2="0.38735" layer="21"/>
<rectangle x1="1.44145" y1="0.37465" x2="1.67005" y2="0.38735" layer="21"/>
<rectangle x1="1.89865" y1="0.37465" x2="2.11455" y2="0.38735" layer="21"/>
<rectangle x1="2.21615" y1="0.37465" x2="2.43205" y2="0.38735" layer="21"/>
<rectangle x1="2.53365" y1="0.37465" x2="2.73685" y2="0.38735" layer="21"/>
<rectangle x1="2.85115" y1="0.37465" x2="3.05435" y2="0.38735" layer="21"/>
<rectangle x1="3.14325" y1="0.37465" x2="3.37185" y2="0.38735" layer="21"/>
<rectangle x1="3.47345" y1="0.37465" x2="3.70205" y2="0.38735" layer="21"/>
<rectangle x1="3.80365" y1="0.37465" x2="4.00685" y2="0.38735" layer="21"/>
<rectangle x1="4.10845" y1="0.37465" x2="4.33705" y2="0.38735" layer="21"/>
<rectangle x1="4.45135" y1="0.37465" x2="4.66725" y2="0.38735" layer="21"/>
<rectangle x1="4.78155" y1="0.37465" x2="4.97205" y2="0.38735" layer="21"/>
<rectangle x1="5.07365" y1="0.37465" x2="5.30225" y2="0.38735" layer="21"/>
<rectangle x1="5.40385" y1="0.37465" x2="5.61975" y2="0.38735" layer="21"/>
<rectangle x1="5.96265" y1="0.37465" x2="6.19125" y2="0.38735" layer="21"/>
<rectangle x1="6.30555" y1="0.37465" x2="6.54685" y2="0.38735" layer="21"/>
<rectangle x1="6.66115" y1="0.37465" x2="6.87705" y2="0.38735" layer="21"/>
<rectangle x1="6.97865" y1="0.37465" x2="7.19455" y2="0.38735" layer="21"/>
<rectangle x1="7.30885" y1="0.37465" x2="7.53745" y2="0.38735" layer="21"/>
<rectangle x1="7.63905" y1="0.37465" x2="7.86765" y2="0.38735" layer="21"/>
<rectangle x1="7.99465" y1="0.37465" x2="8.21055" y2="0.38735" layer="21"/>
<rectangle x1="8.36295" y1="0.37465" x2="8.59155" y2="0.38735" layer="21"/>
<rectangle x1="8.82015" y1="0.37465" x2="9.03605" y2="0.38735" layer="21"/>
<rectangle x1="9.13765" y1="0.37465" x2="9.36625" y2="0.38735" layer="21"/>
<rectangle x1="9.46785" y1="0.37465" x2="9.70915" y2="0.38735" layer="21"/>
<rectangle x1="9.81075" y1="0.37465" x2="10.00125" y2="0.38735" layer="21"/>
<rectangle x1="10.11555" y1="0.37465" x2="10.33145" y2="0.38735" layer="21"/>
<rectangle x1="10.64895" y1="0.37465" x2="10.89025" y2="0.38735" layer="21"/>
<rectangle x1="11.11885" y1="0.37465" x2="11.32205" y2="0.38735" layer="21"/>
<rectangle x1="11.43635" y1="0.37465" x2="11.65225" y2="0.38735" layer="21"/>
<rectangle x1="11.76655" y1="0.37465" x2="11.99515" y2="0.38735" layer="21"/>
<rectangle x1="12.09675" y1="0.37465" x2="12.32535" y2="0.38735" layer="21"/>
<rectangle x1="0.13335" y1="0.38735" x2="0.69215" y2="0.40005" layer="21"/>
<rectangle x1="0.78105" y1="0.38735" x2="1.00965" y2="0.40005" layer="21"/>
<rectangle x1="1.12395" y1="0.38735" x2="1.33985" y2="0.40005" layer="21"/>
<rectangle x1="1.44145" y1="0.38735" x2="1.67005" y2="0.40005" layer="21"/>
<rectangle x1="1.88595" y1="0.38735" x2="2.11455" y2="0.40005" layer="21"/>
<rectangle x1="2.21615" y1="0.38735" x2="2.44475" y2="0.40005" layer="21"/>
<rectangle x1="2.53365" y1="0.38735" x2="2.73685" y2="0.40005" layer="21"/>
<rectangle x1="2.85115" y1="0.38735" x2="3.05435" y2="0.40005" layer="21"/>
<rectangle x1="3.14325" y1="0.38735" x2="3.37185" y2="0.40005" layer="21"/>
<rectangle x1="3.47345" y1="0.38735" x2="3.70205" y2="0.40005" layer="21"/>
<rectangle x1="3.80365" y1="0.38735" x2="4.00685" y2="0.40005" layer="21"/>
<rectangle x1="4.10845" y1="0.38735" x2="4.33705" y2="0.40005" layer="21"/>
<rectangle x1="4.45135" y1="0.38735" x2="4.66725" y2="0.40005" layer="21"/>
<rectangle x1="4.78155" y1="0.38735" x2="4.97205" y2="0.40005" layer="21"/>
<rectangle x1="5.07365" y1="0.38735" x2="5.30225" y2="0.40005" layer="21"/>
<rectangle x1="5.40385" y1="0.38735" x2="5.61975" y2="0.40005" layer="21"/>
<rectangle x1="5.96265" y1="0.38735" x2="6.19125" y2="0.40005" layer="21"/>
<rectangle x1="6.31825" y1="0.38735" x2="6.54685" y2="0.40005" layer="21"/>
<rectangle x1="6.64845" y1="0.38735" x2="6.87705" y2="0.40005" layer="21"/>
<rectangle x1="6.97865" y1="0.38735" x2="7.20725" y2="0.40005" layer="21"/>
<rectangle x1="7.30885" y1="0.38735" x2="7.53745" y2="0.40005" layer="21"/>
<rectangle x1="7.63905" y1="0.38735" x2="7.86765" y2="0.40005" layer="21"/>
<rectangle x1="7.99465" y1="0.38735" x2="8.21055" y2="0.40005" layer="21"/>
<rectangle x1="8.36295" y1="0.38735" x2="8.59155" y2="0.40005" layer="21"/>
<rectangle x1="8.82015" y1="0.38735" x2="9.03605" y2="0.40005" layer="21"/>
<rectangle x1="9.13765" y1="0.38735" x2="9.36625" y2="0.40005" layer="21"/>
<rectangle x1="9.46785" y1="0.38735" x2="9.70915" y2="0.40005" layer="21"/>
<rectangle x1="9.81075" y1="0.38735" x2="10.00125" y2="0.40005" layer="21"/>
<rectangle x1="10.11555" y1="0.38735" x2="10.33145" y2="0.40005" layer="21"/>
<rectangle x1="10.64895" y1="0.38735" x2="10.89025" y2="0.40005" layer="21"/>
<rectangle x1="11.11885" y1="0.38735" x2="11.32205" y2="0.40005" layer="21"/>
<rectangle x1="11.43635" y1="0.38735" x2="11.65225" y2="0.40005" layer="21"/>
<rectangle x1="11.76655" y1="0.38735" x2="11.99515" y2="0.40005" layer="21"/>
<rectangle x1="12.09675" y1="0.38735" x2="12.32535" y2="0.40005" layer="21"/>
<rectangle x1="0.13335" y1="0.40005" x2="0.69215" y2="0.41275" layer="21"/>
<rectangle x1="0.78105" y1="0.40005" x2="1.00965" y2="0.41275" layer="21"/>
<rectangle x1="1.12395" y1="0.40005" x2="1.33985" y2="0.41275" layer="21"/>
<rectangle x1="1.44145" y1="0.40005" x2="1.67005" y2="0.41275" layer="21"/>
<rectangle x1="1.88595" y1="0.40005" x2="2.11455" y2="0.41275" layer="21"/>
<rectangle x1="2.21615" y1="0.40005" x2="2.44475" y2="0.41275" layer="21"/>
<rectangle x1="2.53365" y1="0.40005" x2="2.73685" y2="0.41275" layer="21"/>
<rectangle x1="2.83845" y1="0.40005" x2="3.05435" y2="0.41275" layer="21"/>
<rectangle x1="3.14325" y1="0.40005" x2="3.37185" y2="0.41275" layer="21"/>
<rectangle x1="3.47345" y1="0.40005" x2="3.70205" y2="0.41275" layer="21"/>
<rectangle x1="3.80365" y1="0.40005" x2="4.00685" y2="0.41275" layer="21"/>
<rectangle x1="4.10845" y1="0.40005" x2="4.33705" y2="0.41275" layer="21"/>
<rectangle x1="4.43865" y1="0.40005" x2="4.66725" y2="0.41275" layer="21"/>
<rectangle x1="4.78155" y1="0.40005" x2="4.97205" y2="0.41275" layer="21"/>
<rectangle x1="5.07365" y1="0.40005" x2="5.30225" y2="0.41275" layer="21"/>
<rectangle x1="5.40385" y1="0.40005" x2="5.61975" y2="0.41275" layer="21"/>
<rectangle x1="5.96265" y1="0.40005" x2="6.19125" y2="0.41275" layer="21"/>
<rectangle x1="6.31825" y1="0.40005" x2="6.54685" y2="0.41275" layer="21"/>
<rectangle x1="6.64845" y1="0.40005" x2="6.87705" y2="0.41275" layer="21"/>
<rectangle x1="6.97865" y1="0.40005" x2="7.20725" y2="0.41275" layer="21"/>
<rectangle x1="7.30885" y1="0.40005" x2="7.53745" y2="0.41275" layer="21"/>
<rectangle x1="7.63905" y1="0.40005" x2="7.86765" y2="0.41275" layer="21"/>
<rectangle x1="7.99465" y1="0.40005" x2="8.21055" y2="0.41275" layer="21"/>
<rectangle x1="8.36295" y1="0.40005" x2="8.59155" y2="0.41275" layer="21"/>
<rectangle x1="8.82015" y1="0.40005" x2="9.03605" y2="0.41275" layer="21"/>
<rectangle x1="9.13765" y1="0.40005" x2="9.36625" y2="0.41275" layer="21"/>
<rectangle x1="9.46785" y1="0.40005" x2="9.70915" y2="0.41275" layer="21"/>
<rectangle x1="9.81075" y1="0.40005" x2="10.00125" y2="0.41275" layer="21"/>
<rectangle x1="10.11555" y1="0.40005" x2="10.31875" y2="0.41275" layer="21"/>
<rectangle x1="10.64895" y1="0.40005" x2="10.89025" y2="0.41275" layer="21"/>
<rectangle x1="11.11885" y1="0.40005" x2="11.32205" y2="0.41275" layer="21"/>
<rectangle x1="11.43635" y1="0.40005" x2="11.65225" y2="0.41275" layer="21"/>
<rectangle x1="11.76655" y1="0.40005" x2="11.99515" y2="0.41275" layer="21"/>
<rectangle x1="12.09675" y1="0.40005" x2="12.32535" y2="0.41275" layer="21"/>
<rectangle x1="0.13335" y1="0.41275" x2="0.69215" y2="0.42545" layer="21"/>
<rectangle x1="0.78105" y1="0.41275" x2="1.00965" y2="0.42545" layer="21"/>
<rectangle x1="1.12395" y1="0.41275" x2="1.33985" y2="0.42545" layer="21"/>
<rectangle x1="1.44145" y1="0.41275" x2="1.67005" y2="0.42545" layer="21"/>
<rectangle x1="1.88595" y1="0.41275" x2="2.11455" y2="0.42545" layer="21"/>
<rectangle x1="2.21615" y1="0.41275" x2="2.44475" y2="0.42545" layer="21"/>
<rectangle x1="2.83845" y1="0.41275" x2="3.05435" y2="0.42545" layer="21"/>
<rectangle x1="3.14325" y1="0.41275" x2="3.37185" y2="0.42545" layer="21"/>
<rectangle x1="3.47345" y1="0.41275" x2="3.70205" y2="0.42545" layer="21"/>
<rectangle x1="3.80365" y1="0.41275" x2="4.00685" y2="0.42545" layer="21"/>
<rectangle x1="4.10845" y1="0.41275" x2="4.33705" y2="0.42545" layer="21"/>
<rectangle x1="4.43865" y1="0.41275" x2="4.66725" y2="0.42545" layer="21"/>
<rectangle x1="4.78155" y1="0.41275" x2="4.97205" y2="0.42545" layer="21"/>
<rectangle x1="5.07365" y1="0.41275" x2="5.30225" y2="0.42545" layer="21"/>
<rectangle x1="5.40385" y1="0.41275" x2="5.61975" y2="0.42545" layer="21"/>
<rectangle x1="5.96265" y1="0.41275" x2="6.19125" y2="0.42545" layer="21"/>
<rectangle x1="6.31825" y1="0.41275" x2="6.54685" y2="0.42545" layer="21"/>
<rectangle x1="6.64845" y1="0.41275" x2="6.87705" y2="0.42545" layer="21"/>
<rectangle x1="6.97865" y1="0.41275" x2="7.20725" y2="0.42545" layer="21"/>
<rectangle x1="7.30885" y1="0.41275" x2="7.53745" y2="0.42545" layer="21"/>
<rectangle x1="7.63905" y1="0.41275" x2="7.86765" y2="0.42545" layer="21"/>
<rectangle x1="7.99465" y1="0.41275" x2="8.21055" y2="0.42545" layer="21"/>
<rectangle x1="8.36295" y1="0.41275" x2="8.59155" y2="0.42545" layer="21"/>
<rectangle x1="8.80745" y1="0.41275" x2="9.03605" y2="0.42545" layer="21"/>
<rectangle x1="9.13765" y1="0.41275" x2="9.36625" y2="0.42545" layer="21"/>
<rectangle x1="9.46785" y1="0.41275" x2="9.70915" y2="0.42545" layer="21"/>
<rectangle x1="10.10285" y1="0.41275" x2="10.31875" y2="0.42545" layer="21"/>
<rectangle x1="10.64895" y1="0.41275" x2="10.89025" y2="0.42545" layer="21"/>
<rectangle x1="11.11885" y1="0.41275" x2="11.33475" y2="0.42545" layer="21"/>
<rectangle x1="11.43635" y1="0.41275" x2="11.65225" y2="0.42545" layer="21"/>
<rectangle x1="11.76655" y1="0.41275" x2="11.99515" y2="0.42545" layer="21"/>
<rectangle x1="12.09675" y1="0.41275" x2="12.32535" y2="0.42545" layer="21"/>
<rectangle x1="0.13335" y1="0.42545" x2="0.67945" y2="0.43815" layer="21"/>
<rectangle x1="0.78105" y1="0.42545" x2="1.00965" y2="0.43815" layer="21"/>
<rectangle x1="1.12395" y1="0.42545" x2="1.33985" y2="0.43815" layer="21"/>
<rectangle x1="1.44145" y1="0.42545" x2="1.67005" y2="0.43815" layer="21"/>
<rectangle x1="1.88595" y1="0.42545" x2="2.11455" y2="0.43815" layer="21"/>
<rectangle x1="2.21615" y1="0.42545" x2="2.44475" y2="0.43815" layer="21"/>
<rectangle x1="2.81305" y1="0.42545" x2="3.05435" y2="0.43815" layer="21"/>
<rectangle x1="3.14325" y1="0.42545" x2="3.37185" y2="0.43815" layer="21"/>
<rectangle x1="3.47345" y1="0.42545" x2="3.70205" y2="0.43815" layer="21"/>
<rectangle x1="3.80365" y1="0.42545" x2="4.00685" y2="0.43815" layer="21"/>
<rectangle x1="4.10845" y1="0.42545" x2="4.33705" y2="0.43815" layer="21"/>
<rectangle x1="4.43865" y1="0.42545" x2="4.66725" y2="0.43815" layer="21"/>
<rectangle x1="4.78155" y1="0.42545" x2="4.97205" y2="0.43815" layer="21"/>
<rectangle x1="5.07365" y1="0.42545" x2="5.30225" y2="0.43815" layer="21"/>
<rectangle x1="5.40385" y1="0.42545" x2="5.61975" y2="0.43815" layer="21"/>
<rectangle x1="5.96265" y1="0.42545" x2="6.19125" y2="0.43815" layer="21"/>
<rectangle x1="6.31825" y1="0.42545" x2="6.54685" y2="0.43815" layer="21"/>
<rectangle x1="6.64845" y1="0.42545" x2="6.87705" y2="0.43815" layer="21"/>
<rectangle x1="6.97865" y1="0.42545" x2="7.20725" y2="0.43815" layer="21"/>
<rectangle x1="7.30885" y1="0.42545" x2="7.53745" y2="0.43815" layer="21"/>
<rectangle x1="7.63905" y1="0.42545" x2="7.86765" y2="0.43815" layer="21"/>
<rectangle x1="7.99465" y1="0.42545" x2="8.21055" y2="0.43815" layer="21"/>
<rectangle x1="8.36295" y1="0.42545" x2="8.59155" y2="0.43815" layer="21"/>
<rectangle x1="8.80745" y1="0.42545" x2="9.03605" y2="0.43815" layer="21"/>
<rectangle x1="9.13765" y1="0.42545" x2="9.36625" y2="0.43815" layer="21"/>
<rectangle x1="9.46785" y1="0.42545" x2="9.70915" y2="0.43815" layer="21"/>
<rectangle x1="10.09015" y1="0.42545" x2="10.31875" y2="0.43815" layer="21"/>
<rectangle x1="10.64895" y1="0.42545" x2="10.89025" y2="0.43815" layer="21"/>
<rectangle x1="11.11885" y1="0.42545" x2="11.33475" y2="0.43815" layer="21"/>
<rectangle x1="11.43635" y1="0.42545" x2="11.65225" y2="0.43815" layer="21"/>
<rectangle x1="11.76655" y1="0.42545" x2="11.99515" y2="0.43815" layer="21"/>
<rectangle x1="12.09675" y1="0.42545" x2="12.32535" y2="0.43815" layer="21"/>
<rectangle x1="0.13335" y1="0.43815" x2="0.67945" y2="0.45085" layer="21"/>
<rectangle x1="0.78105" y1="0.43815" x2="1.00965" y2="0.45085" layer="21"/>
<rectangle x1="1.12395" y1="0.43815" x2="1.33985" y2="0.45085" layer="21"/>
<rectangle x1="1.44145" y1="0.43815" x2="1.67005" y2="0.45085" layer="21"/>
<rectangle x1="1.88595" y1="0.43815" x2="2.11455" y2="0.45085" layer="21"/>
<rectangle x1="2.21615" y1="0.43815" x2="2.44475" y2="0.45085" layer="21"/>
<rectangle x1="2.80035" y1="0.43815" x2="3.05435" y2="0.45085" layer="21"/>
<rectangle x1="3.14325" y1="0.43815" x2="3.37185" y2="0.45085" layer="21"/>
<rectangle x1="3.47345" y1="0.43815" x2="3.70205" y2="0.45085" layer="21"/>
<rectangle x1="3.80365" y1="0.43815" x2="4.01955" y2="0.45085" layer="21"/>
<rectangle x1="4.10845" y1="0.43815" x2="4.33705" y2="0.45085" layer="21"/>
<rectangle x1="4.43865" y1="0.43815" x2="4.66725" y2="0.45085" layer="21"/>
<rectangle x1="4.78155" y1="0.43815" x2="4.97205" y2="0.45085" layer="21"/>
<rectangle x1="5.07365" y1="0.43815" x2="5.30225" y2="0.45085" layer="21"/>
<rectangle x1="5.40385" y1="0.43815" x2="5.61975" y2="0.45085" layer="21"/>
<rectangle x1="5.96265" y1="0.43815" x2="6.19125" y2="0.45085" layer="21"/>
<rectangle x1="6.31825" y1="0.43815" x2="6.54685" y2="0.45085" layer="21"/>
<rectangle x1="6.64845" y1="0.43815" x2="6.87705" y2="0.45085" layer="21"/>
<rectangle x1="6.97865" y1="0.43815" x2="7.20725" y2="0.45085" layer="21"/>
<rectangle x1="7.30885" y1="0.43815" x2="7.53745" y2="0.45085" layer="21"/>
<rectangle x1="7.63905" y1="0.43815" x2="7.86765" y2="0.45085" layer="21"/>
<rectangle x1="7.99465" y1="0.43815" x2="8.21055" y2="0.45085" layer="21"/>
<rectangle x1="8.36295" y1="0.43815" x2="8.59155" y2="0.45085" layer="21"/>
<rectangle x1="8.80745" y1="0.43815" x2="9.03605" y2="0.45085" layer="21"/>
<rectangle x1="9.13765" y1="0.43815" x2="9.36625" y2="0.45085" layer="21"/>
<rectangle x1="9.46785" y1="0.43815" x2="9.70915" y2="0.45085" layer="21"/>
<rectangle x1="10.06475" y1="0.43815" x2="10.31875" y2="0.45085" layer="21"/>
<rectangle x1="10.64895" y1="0.43815" x2="10.89025" y2="0.45085" layer="21"/>
<rectangle x1="11.11885" y1="0.43815" x2="11.33475" y2="0.45085" layer="21"/>
<rectangle x1="11.43635" y1="0.43815" x2="11.65225" y2="0.45085" layer="21"/>
<rectangle x1="11.76655" y1="0.43815" x2="11.99515" y2="0.45085" layer="21"/>
<rectangle x1="12.09675" y1="0.43815" x2="12.32535" y2="0.45085" layer="21"/>
<rectangle x1="0.13335" y1="0.45085" x2="0.67945" y2="0.46355" layer="21"/>
<rectangle x1="0.78105" y1="0.45085" x2="1.00965" y2="0.46355" layer="21"/>
<rectangle x1="1.12395" y1="0.45085" x2="1.33985" y2="0.46355" layer="21"/>
<rectangle x1="1.44145" y1="0.45085" x2="1.67005" y2="0.46355" layer="21"/>
<rectangle x1="1.88595" y1="0.45085" x2="2.11455" y2="0.46355" layer="21"/>
<rectangle x1="2.21615" y1="0.45085" x2="2.44475" y2="0.46355" layer="21"/>
<rectangle x1="2.77495" y1="0.45085" x2="3.04165" y2="0.46355" layer="21"/>
<rectangle x1="3.14325" y1="0.45085" x2="3.37185" y2="0.46355" layer="21"/>
<rectangle x1="3.47345" y1="0.45085" x2="3.70205" y2="0.46355" layer="21"/>
<rectangle x1="3.80365" y1="0.45085" x2="4.01955" y2="0.46355" layer="21"/>
<rectangle x1="4.10845" y1="0.45085" x2="4.33705" y2="0.46355" layer="21"/>
<rectangle x1="4.43865" y1="0.45085" x2="4.66725" y2="0.46355" layer="21"/>
<rectangle x1="4.78155" y1="0.45085" x2="4.98475" y2="0.46355" layer="21"/>
<rectangle x1="5.07365" y1="0.45085" x2="5.30225" y2="0.46355" layer="21"/>
<rectangle x1="5.40385" y1="0.45085" x2="5.61975" y2="0.46355" layer="21"/>
<rectangle x1="5.94995" y1="0.45085" x2="6.19125" y2="0.46355" layer="21"/>
<rectangle x1="6.31825" y1="0.45085" x2="6.54685" y2="0.46355" layer="21"/>
<rectangle x1="6.64845" y1="0.45085" x2="6.87705" y2="0.46355" layer="21"/>
<rectangle x1="6.97865" y1="0.45085" x2="7.20725" y2="0.46355" layer="21"/>
<rectangle x1="7.30885" y1="0.45085" x2="7.53745" y2="0.46355" layer="21"/>
<rectangle x1="7.63905" y1="0.45085" x2="7.86765" y2="0.46355" layer="21"/>
<rectangle x1="7.99465" y1="0.45085" x2="8.21055" y2="0.46355" layer="21"/>
<rectangle x1="8.36295" y1="0.45085" x2="8.59155" y2="0.46355" layer="21"/>
<rectangle x1="8.80745" y1="0.45085" x2="9.03605" y2="0.46355" layer="21"/>
<rectangle x1="9.13765" y1="0.45085" x2="9.36625" y2="0.46355" layer="21"/>
<rectangle x1="9.46785" y1="0.45085" x2="9.70915" y2="0.46355" layer="21"/>
<rectangle x1="10.05205" y1="0.45085" x2="10.31875" y2="0.46355" layer="21"/>
<rectangle x1="10.64895" y1="0.45085" x2="10.89025" y2="0.46355" layer="21"/>
<rectangle x1="11.11885" y1="0.45085" x2="11.33475" y2="0.46355" layer="21"/>
<rectangle x1="11.43635" y1="0.45085" x2="11.65225" y2="0.46355" layer="21"/>
<rectangle x1="11.76655" y1="0.45085" x2="11.99515" y2="0.46355" layer="21"/>
<rectangle x1="12.09675" y1="0.45085" x2="12.32535" y2="0.46355" layer="21"/>
<rectangle x1="0.13335" y1="0.46355" x2="0.67945" y2="0.47625" layer="21"/>
<rectangle x1="0.78105" y1="0.46355" x2="1.00965" y2="0.47625" layer="21"/>
<rectangle x1="1.44145" y1="0.46355" x2="1.67005" y2="0.47625" layer="21"/>
<rectangle x1="1.88595" y1="0.46355" x2="2.11455" y2="0.47625" layer="21"/>
<rectangle x1="2.21615" y1="0.46355" x2="2.44475" y2="0.47625" layer="21"/>
<rectangle x1="2.74955" y1="0.46355" x2="3.04165" y2="0.47625" layer="21"/>
<rectangle x1="3.14325" y1="0.46355" x2="3.37185" y2="0.47625" layer="21"/>
<rectangle x1="3.47345" y1="0.46355" x2="3.70205" y2="0.47625" layer="21"/>
<rectangle x1="3.80365" y1="0.46355" x2="4.01955" y2="0.47625" layer="21"/>
<rectangle x1="4.10845" y1="0.46355" x2="4.33705" y2="0.47625" layer="21"/>
<rectangle x1="4.43865" y1="0.46355" x2="4.66725" y2="0.47625" layer="21"/>
<rectangle x1="5.07365" y1="0.46355" x2="5.30225" y2="0.47625" layer="21"/>
<rectangle x1="5.94995" y1="0.46355" x2="6.19125" y2="0.47625" layer="21"/>
<rectangle x1="6.31825" y1="0.46355" x2="6.54685" y2="0.47625" layer="21"/>
<rectangle x1="6.64845" y1="0.46355" x2="6.87705" y2="0.47625" layer="21"/>
<rectangle x1="6.97865" y1="0.46355" x2="7.20725" y2="0.47625" layer="21"/>
<rectangle x1="7.30885" y1="0.46355" x2="7.53745" y2="0.47625" layer="21"/>
<rectangle x1="7.63905" y1="0.46355" x2="7.86765" y2="0.47625" layer="21"/>
<rectangle x1="7.99465" y1="0.46355" x2="8.21055" y2="0.47625" layer="21"/>
<rectangle x1="8.36295" y1="0.46355" x2="8.59155" y2="0.47625" layer="21"/>
<rectangle x1="8.80745" y1="0.46355" x2="9.03605" y2="0.47625" layer="21"/>
<rectangle x1="9.13765" y1="0.46355" x2="9.36625" y2="0.47625" layer="21"/>
<rectangle x1="9.46785" y1="0.46355" x2="9.70915" y2="0.47625" layer="21"/>
<rectangle x1="10.02665" y1="0.46355" x2="10.30605" y2="0.47625" layer="21"/>
<rectangle x1="10.64895" y1="0.46355" x2="10.89025" y2="0.47625" layer="21"/>
<rectangle x1="11.13155" y1="0.46355" x2="11.34745" y2="0.47625" layer="21"/>
<rectangle x1="11.43635" y1="0.46355" x2="11.65225" y2="0.47625" layer="21"/>
<rectangle x1="11.76655" y1="0.46355" x2="11.99515" y2="0.47625" layer="21"/>
<rectangle x1="12.09675" y1="0.46355" x2="12.32535" y2="0.47625" layer="21"/>
<rectangle x1="0.14605" y1="0.47625" x2="0.36195" y2="0.48895" layer="21"/>
<rectangle x1="0.46355" y1="0.47625" x2="0.67945" y2="0.48895" layer="21"/>
<rectangle x1="0.78105" y1="0.47625" x2="1.00965" y2="0.48895" layer="21"/>
<rectangle x1="1.44145" y1="0.47625" x2="1.67005" y2="0.48895" layer="21"/>
<rectangle x1="1.88595" y1="0.47625" x2="2.11455" y2="0.48895" layer="21"/>
<rectangle x1="2.21615" y1="0.47625" x2="2.44475" y2="0.48895" layer="21"/>
<rectangle x1="2.73685" y1="0.47625" x2="3.04165" y2="0.48895" layer="21"/>
<rectangle x1="3.14325" y1="0.47625" x2="3.37185" y2="0.48895" layer="21"/>
<rectangle x1="3.47345" y1="0.47625" x2="3.70205" y2="0.48895" layer="21"/>
<rectangle x1="3.81635" y1="0.47625" x2="4.03225" y2="0.48895" layer="21"/>
<rectangle x1="4.10845" y1="0.47625" x2="4.33705" y2="0.48895" layer="21"/>
<rectangle x1="4.43865" y1="0.47625" x2="4.66725" y2="0.48895" layer="21"/>
<rectangle x1="5.07365" y1="0.47625" x2="5.30225" y2="0.48895" layer="21"/>
<rectangle x1="5.94995" y1="0.47625" x2="6.19125" y2="0.48895" layer="21"/>
<rectangle x1="6.31825" y1="0.47625" x2="6.54685" y2="0.48895" layer="21"/>
<rectangle x1="6.64845" y1="0.47625" x2="6.87705" y2="0.48895" layer="21"/>
<rectangle x1="6.97865" y1="0.47625" x2="7.20725" y2="0.48895" layer="21"/>
<rectangle x1="7.30885" y1="0.47625" x2="7.53745" y2="0.48895" layer="21"/>
<rectangle x1="7.63905" y1="0.47625" x2="7.86765" y2="0.48895" layer="21"/>
<rectangle x1="7.99465" y1="0.47625" x2="8.21055" y2="0.48895" layer="21"/>
<rectangle x1="8.36295" y1="0.47625" x2="8.59155" y2="0.48895" layer="21"/>
<rectangle x1="8.80745" y1="0.47625" x2="9.03605" y2="0.48895" layer="21"/>
<rectangle x1="9.13765" y1="0.47625" x2="9.36625" y2="0.48895" layer="21"/>
<rectangle x1="9.46785" y1="0.47625" x2="9.70915" y2="0.48895" layer="21"/>
<rectangle x1="10.00125" y1="0.47625" x2="10.30605" y2="0.48895" layer="21"/>
<rectangle x1="10.64895" y1="0.47625" x2="10.89025" y2="0.48895" layer="21"/>
<rectangle x1="11.13155" y1="0.47625" x2="11.34745" y2="0.48895" layer="21"/>
<rectangle x1="11.43635" y1="0.47625" x2="11.65225" y2="0.48895" layer="21"/>
<rectangle x1="11.76655" y1="0.47625" x2="11.99515" y2="0.48895" layer="21"/>
<rectangle x1="12.09675" y1="0.47625" x2="12.32535" y2="0.48895" layer="21"/>
<rectangle x1="0.14605" y1="0.48895" x2="0.34925" y2="0.50165" layer="21"/>
<rectangle x1="0.46355" y1="0.48895" x2="0.67945" y2="0.50165" layer="21"/>
<rectangle x1="0.78105" y1="0.48895" x2="1.00965" y2="0.50165" layer="21"/>
<rectangle x1="1.44145" y1="0.48895" x2="1.67005" y2="0.50165" layer="21"/>
<rectangle x1="1.88595" y1="0.48895" x2="2.11455" y2="0.50165" layer="21"/>
<rectangle x1="2.21615" y1="0.48895" x2="2.44475" y2="0.50165" layer="21"/>
<rectangle x1="2.71145" y1="0.48895" x2="3.02895" y2="0.50165" layer="21"/>
<rectangle x1="3.14325" y1="0.48895" x2="3.37185" y2="0.50165" layer="21"/>
<rectangle x1="3.47345" y1="0.48895" x2="3.70205" y2="0.50165" layer="21"/>
<rectangle x1="3.81635" y1="0.48895" x2="4.04495" y2="0.50165" layer="21"/>
<rectangle x1="4.10845" y1="0.48895" x2="4.33705" y2="0.50165" layer="21"/>
<rectangle x1="4.43865" y1="0.48895" x2="4.66725" y2="0.50165" layer="21"/>
<rectangle x1="5.07365" y1="0.48895" x2="5.30225" y2="0.50165" layer="21"/>
<rectangle x1="5.94995" y1="0.48895" x2="6.19125" y2="0.50165" layer="21"/>
<rectangle x1="6.31825" y1="0.48895" x2="6.54685" y2="0.50165" layer="21"/>
<rectangle x1="6.64845" y1="0.48895" x2="6.87705" y2="0.50165" layer="21"/>
<rectangle x1="6.97865" y1="0.48895" x2="7.20725" y2="0.50165" layer="21"/>
<rectangle x1="7.30885" y1="0.48895" x2="7.53745" y2="0.50165" layer="21"/>
<rectangle x1="7.63905" y1="0.48895" x2="7.86765" y2="0.50165" layer="21"/>
<rectangle x1="7.99465" y1="0.48895" x2="8.21055" y2="0.50165" layer="21"/>
<rectangle x1="8.36295" y1="0.48895" x2="8.59155" y2="0.50165" layer="21"/>
<rectangle x1="8.80745" y1="0.48895" x2="9.03605" y2="0.50165" layer="21"/>
<rectangle x1="9.13765" y1="0.48895" x2="9.36625" y2="0.50165" layer="21"/>
<rectangle x1="9.46785" y1="0.48895" x2="9.70915" y2="0.50165" layer="21"/>
<rectangle x1="9.97585" y1="0.48895" x2="10.30605" y2="0.50165" layer="21"/>
<rectangle x1="10.64895" y1="0.48895" x2="10.89025" y2="0.50165" layer="21"/>
<rectangle x1="11.14425" y1="0.48895" x2="11.36015" y2="0.50165" layer="21"/>
<rectangle x1="11.43635" y1="0.48895" x2="11.65225" y2="0.50165" layer="21"/>
<rectangle x1="11.76655" y1="0.48895" x2="11.99515" y2="0.50165" layer="21"/>
<rectangle x1="12.09675" y1="0.48895" x2="12.32535" y2="0.50165" layer="21"/>
<rectangle x1="0.14605" y1="0.50165" x2="0.34925" y2="0.51435" layer="21"/>
<rectangle x1="0.46355" y1="0.50165" x2="0.66675" y2="0.51435" layer="21"/>
<rectangle x1="0.78105" y1="0.50165" x2="1.00965" y2="0.51435" layer="21"/>
<rectangle x1="1.44145" y1="0.50165" x2="1.67005" y2="0.51435" layer="21"/>
<rectangle x1="1.88595" y1="0.50165" x2="2.11455" y2="0.51435" layer="21"/>
<rectangle x1="2.21615" y1="0.50165" x2="2.44475" y2="0.51435" layer="21"/>
<rectangle x1="2.68605" y1="0.50165" x2="3.01625" y2="0.51435" layer="21"/>
<rectangle x1="3.14325" y1="0.50165" x2="3.37185" y2="0.51435" layer="21"/>
<rectangle x1="3.47345" y1="0.50165" x2="3.70205" y2="0.51435" layer="21"/>
<rectangle x1="3.82905" y1="0.50165" x2="4.05765" y2="0.51435" layer="21"/>
<rectangle x1="4.10845" y1="0.50165" x2="4.33705" y2="0.51435" layer="21"/>
<rectangle x1="4.43865" y1="0.50165" x2="4.66725" y2="0.51435" layer="21"/>
<rectangle x1="5.07365" y1="0.50165" x2="5.30225" y2="0.51435" layer="21"/>
<rectangle x1="5.94995" y1="0.50165" x2="6.19125" y2="0.51435" layer="21"/>
<rectangle x1="6.31825" y1="0.50165" x2="6.54685" y2="0.51435" layer="21"/>
<rectangle x1="6.64845" y1="0.50165" x2="6.87705" y2="0.51435" layer="21"/>
<rectangle x1="6.97865" y1="0.50165" x2="7.20725" y2="0.51435" layer="21"/>
<rectangle x1="7.30885" y1="0.50165" x2="7.53745" y2="0.51435" layer="21"/>
<rectangle x1="7.63905" y1="0.50165" x2="7.86765" y2="0.51435" layer="21"/>
<rectangle x1="7.99465" y1="0.50165" x2="8.21055" y2="0.51435" layer="21"/>
<rectangle x1="8.36295" y1="0.50165" x2="8.59155" y2="0.51435" layer="21"/>
<rectangle x1="8.80745" y1="0.50165" x2="9.03605" y2="0.51435" layer="21"/>
<rectangle x1="9.13765" y1="0.50165" x2="9.36625" y2="0.51435" layer="21"/>
<rectangle x1="9.46785" y1="0.50165" x2="9.70915" y2="0.51435" layer="21"/>
<rectangle x1="9.96315" y1="0.50165" x2="10.29335" y2="0.51435" layer="21"/>
<rectangle x1="10.64895" y1="0.50165" x2="10.89025" y2="0.51435" layer="21"/>
<rectangle x1="11.14425" y1="0.50165" x2="11.37285" y2="0.51435" layer="21"/>
<rectangle x1="11.43635" y1="0.50165" x2="11.65225" y2="0.51435" layer="21"/>
<rectangle x1="11.76655" y1="0.50165" x2="11.99515" y2="0.51435" layer="21"/>
<rectangle x1="12.09675" y1="0.50165" x2="12.32535" y2="0.51435" layer="21"/>
<rectangle x1="0.14605" y1="0.51435" x2="0.34925" y2="0.52705" layer="21"/>
<rectangle x1="0.46355" y1="0.51435" x2="0.66675" y2="0.52705" layer="21"/>
<rectangle x1="0.78105" y1="0.51435" x2="1.00965" y2="0.52705" layer="21"/>
<rectangle x1="1.44145" y1="0.51435" x2="1.67005" y2="0.52705" layer="21"/>
<rectangle x1="1.88595" y1="0.51435" x2="2.11455" y2="0.52705" layer="21"/>
<rectangle x1="2.21615" y1="0.51435" x2="2.44475" y2="0.52705" layer="21"/>
<rectangle x1="2.66065" y1="0.51435" x2="3.01625" y2="0.52705" layer="21"/>
<rectangle x1="3.14325" y1="0.51435" x2="3.37185" y2="0.52705" layer="21"/>
<rectangle x1="3.47345" y1="0.51435" x2="3.70205" y2="0.52705" layer="21"/>
<rectangle x1="3.84175" y1="0.51435" x2="4.07035" y2="0.52705" layer="21"/>
<rectangle x1="4.10845" y1="0.51435" x2="4.33705" y2="0.52705" layer="21"/>
<rectangle x1="4.43865" y1="0.51435" x2="4.66725" y2="0.52705" layer="21"/>
<rectangle x1="5.07365" y1="0.51435" x2="5.30225" y2="0.52705" layer="21"/>
<rectangle x1="5.94995" y1="0.51435" x2="6.19125" y2="0.52705" layer="21"/>
<rectangle x1="6.31825" y1="0.51435" x2="6.54685" y2="0.52705" layer="21"/>
<rectangle x1="6.64845" y1="0.51435" x2="6.87705" y2="0.52705" layer="21"/>
<rectangle x1="6.97865" y1="0.51435" x2="7.20725" y2="0.52705" layer="21"/>
<rectangle x1="7.30885" y1="0.51435" x2="7.53745" y2="0.52705" layer="21"/>
<rectangle x1="7.63905" y1="0.51435" x2="7.86765" y2="0.52705" layer="21"/>
<rectangle x1="7.99465" y1="0.51435" x2="8.21055" y2="0.52705" layer="21"/>
<rectangle x1="8.36295" y1="0.51435" x2="8.59155" y2="0.52705" layer="21"/>
<rectangle x1="8.80745" y1="0.51435" x2="9.03605" y2="0.52705" layer="21"/>
<rectangle x1="9.13765" y1="0.51435" x2="9.36625" y2="0.52705" layer="21"/>
<rectangle x1="9.46785" y1="0.51435" x2="9.70915" y2="0.52705" layer="21"/>
<rectangle x1="9.93775" y1="0.51435" x2="10.28065" y2="0.52705" layer="21"/>
<rectangle x1="10.64895" y1="0.51435" x2="10.89025" y2="0.52705" layer="21"/>
<rectangle x1="11.16965" y1="0.51435" x2="11.38555" y2="0.52705" layer="21"/>
<rectangle x1="11.43635" y1="0.51435" x2="11.65225" y2="0.52705" layer="21"/>
<rectangle x1="11.76655" y1="0.51435" x2="11.99515" y2="0.52705" layer="21"/>
<rectangle x1="12.09675" y1="0.51435" x2="12.32535" y2="0.52705" layer="21"/>
<rectangle x1="0.14605" y1="0.52705" x2="0.34925" y2="0.53975" layer="21"/>
<rectangle x1="0.46355" y1="0.52705" x2="0.66675" y2="0.53975" layer="21"/>
<rectangle x1="0.78105" y1="0.52705" x2="1.00965" y2="0.53975" layer="21"/>
<rectangle x1="1.44145" y1="0.52705" x2="1.67005" y2="0.53975" layer="21"/>
<rectangle x1="1.88595" y1="0.52705" x2="2.11455" y2="0.53975" layer="21"/>
<rectangle x1="2.21615" y1="0.52705" x2="2.44475" y2="0.53975" layer="21"/>
<rectangle x1="2.64795" y1="0.52705" x2="3.00355" y2="0.53975" layer="21"/>
<rectangle x1="3.14325" y1="0.52705" x2="3.37185" y2="0.53975" layer="21"/>
<rectangle x1="3.47345" y1="0.52705" x2="3.70205" y2="0.53975" layer="21"/>
<rectangle x1="3.86715" y1="0.52705" x2="4.08305" y2="0.53975" layer="21"/>
<rectangle x1="4.10845" y1="0.52705" x2="4.33705" y2="0.53975" layer="21"/>
<rectangle x1="4.43865" y1="0.52705" x2="4.66725" y2="0.53975" layer="21"/>
<rectangle x1="5.07365" y1="0.52705" x2="5.30225" y2="0.53975" layer="21"/>
<rectangle x1="5.94995" y1="0.52705" x2="6.19125" y2="0.53975" layer="21"/>
<rectangle x1="6.64845" y1="0.52705" x2="6.87705" y2="0.53975" layer="21"/>
<rectangle x1="6.97865" y1="0.52705" x2="7.20725" y2="0.53975" layer="21"/>
<rectangle x1="7.30885" y1="0.52705" x2="7.53745" y2="0.53975" layer="21"/>
<rectangle x1="7.63905" y1="0.52705" x2="7.86765" y2="0.53975" layer="21"/>
<rectangle x1="7.99465" y1="0.52705" x2="8.21055" y2="0.53975" layer="21"/>
<rectangle x1="8.36295" y1="0.52705" x2="8.59155" y2="0.53975" layer="21"/>
<rectangle x1="8.80745" y1="0.52705" x2="9.03605" y2="0.53975" layer="21"/>
<rectangle x1="9.13765" y1="0.52705" x2="9.36625" y2="0.53975" layer="21"/>
<rectangle x1="9.46785" y1="0.52705" x2="9.70915" y2="0.53975" layer="21"/>
<rectangle x1="9.91235" y1="0.52705" x2="10.26795" y2="0.53975" layer="21"/>
<rectangle x1="10.64895" y1="0.52705" x2="10.89025" y2="0.53975" layer="21"/>
<rectangle x1="11.18235" y1="0.52705" x2="11.41095" y2="0.53975" layer="21"/>
<rectangle x1="11.43635" y1="0.52705" x2="11.65225" y2="0.53975" layer="21"/>
<rectangle x1="11.76655" y1="0.52705" x2="11.99515" y2="0.53975" layer="21"/>
<rectangle x1="12.09675" y1="0.52705" x2="12.32535" y2="0.53975" layer="21"/>
<rectangle x1="0.14605" y1="0.53975" x2="0.36195" y2="0.55245" layer="21"/>
<rectangle x1="0.46355" y1="0.53975" x2="0.66675" y2="0.55245" layer="21"/>
<rectangle x1="0.78105" y1="0.53975" x2="1.32715" y2="0.55245" layer="21"/>
<rectangle x1="1.44145" y1="0.53975" x2="1.67005" y2="0.55245" layer="21"/>
<rectangle x1="1.88595" y1="0.53975" x2="2.11455" y2="0.55245" layer="21"/>
<rectangle x1="2.21615" y1="0.53975" x2="2.44475" y2="0.55245" layer="21"/>
<rectangle x1="2.62255" y1="0.53975" x2="2.97815" y2="0.55245" layer="21"/>
<rectangle x1="3.14325" y1="0.53975" x2="3.37185" y2="0.55245" layer="21"/>
<rectangle x1="3.47345" y1="0.53975" x2="3.70205" y2="0.55245" layer="21"/>
<rectangle x1="3.89255" y1="0.53975" x2="4.33705" y2="0.55245" layer="21"/>
<rectangle x1="4.43865" y1="0.53975" x2="4.66725" y2="0.55245" layer="21"/>
<rectangle x1="5.07365" y1="0.53975" x2="5.61975" y2="0.55245" layer="21"/>
<rectangle x1="5.94995" y1="0.53975" x2="6.19125" y2="0.55245" layer="21"/>
<rectangle x1="6.64845" y1="0.53975" x2="6.87705" y2="0.55245" layer="21"/>
<rectangle x1="6.97865" y1="0.53975" x2="7.20725" y2="0.55245" layer="21"/>
<rectangle x1="7.30885" y1="0.53975" x2="7.53745" y2="0.55245" layer="21"/>
<rectangle x1="7.63905" y1="0.53975" x2="7.86765" y2="0.55245" layer="21"/>
<rectangle x1="7.99465" y1="0.53975" x2="8.21055" y2="0.55245" layer="21"/>
<rectangle x1="8.36295" y1="0.53975" x2="8.59155" y2="0.55245" layer="21"/>
<rectangle x1="8.80745" y1="0.53975" x2="9.03605" y2="0.55245" layer="21"/>
<rectangle x1="9.13765" y1="0.53975" x2="9.36625" y2="0.55245" layer="21"/>
<rectangle x1="9.46785" y1="0.53975" x2="9.70915" y2="0.55245" layer="21"/>
<rectangle x1="9.89965" y1="0.53975" x2="10.25525" y2="0.55245" layer="21"/>
<rectangle x1="10.64895" y1="0.53975" x2="10.89025" y2="0.55245" layer="21"/>
<rectangle x1="11.20775" y1="0.53975" x2="11.65225" y2="0.55245" layer="21"/>
<rectangle x1="11.76655" y1="0.53975" x2="11.99515" y2="0.55245" layer="21"/>
<rectangle x1="12.09675" y1="0.53975" x2="12.32535" y2="0.55245" layer="21"/>
<rectangle x1="0.14605" y1="0.55245" x2="0.36195" y2="0.56515" layer="21"/>
<rectangle x1="0.46355" y1="0.55245" x2="0.66675" y2="0.56515" layer="21"/>
<rectangle x1="0.78105" y1="0.55245" x2="1.33985" y2="0.56515" layer="21"/>
<rectangle x1="1.44145" y1="0.55245" x2="1.67005" y2="0.56515" layer="21"/>
<rectangle x1="1.88595" y1="0.55245" x2="2.11455" y2="0.56515" layer="21"/>
<rectangle x1="2.21615" y1="0.55245" x2="2.44475" y2="0.56515" layer="21"/>
<rectangle x1="2.60985" y1="0.55245" x2="2.96545" y2="0.56515" layer="21"/>
<rectangle x1="3.14325" y1="0.55245" x2="3.37185" y2="0.56515" layer="21"/>
<rectangle x1="3.47345" y1="0.55245" x2="3.70205" y2="0.56515" layer="21"/>
<rectangle x1="3.91795" y1="0.55245" x2="4.33705" y2="0.56515" layer="21"/>
<rectangle x1="4.43865" y1="0.55245" x2="4.66725" y2="0.56515" layer="21"/>
<rectangle x1="5.07365" y1="0.55245" x2="5.61975" y2="0.56515" layer="21"/>
<rectangle x1="5.94995" y1="0.55245" x2="6.19125" y2="0.56515" layer="21"/>
<rectangle x1="6.64845" y1="0.55245" x2="6.87705" y2="0.56515" layer="21"/>
<rectangle x1="6.97865" y1="0.55245" x2="7.20725" y2="0.56515" layer="21"/>
<rectangle x1="7.30885" y1="0.55245" x2="7.53745" y2="0.56515" layer="21"/>
<rectangle x1="7.63905" y1="0.55245" x2="7.86765" y2="0.56515" layer="21"/>
<rectangle x1="7.99465" y1="0.55245" x2="8.21055" y2="0.56515" layer="21"/>
<rectangle x1="8.36295" y1="0.55245" x2="8.59155" y2="0.56515" layer="21"/>
<rectangle x1="8.80745" y1="0.55245" x2="9.03605" y2="0.56515" layer="21"/>
<rectangle x1="9.13765" y1="0.55245" x2="9.36625" y2="0.56515" layer="21"/>
<rectangle x1="9.46785" y1="0.55245" x2="9.70915" y2="0.56515" layer="21"/>
<rectangle x1="9.87425" y1="0.55245" x2="10.22985" y2="0.56515" layer="21"/>
<rectangle x1="10.64895" y1="0.55245" x2="10.89025" y2="0.56515" layer="21"/>
<rectangle x1="11.23315" y1="0.55245" x2="11.65225" y2="0.56515" layer="21"/>
<rectangle x1="11.76655" y1="0.55245" x2="11.99515" y2="0.56515" layer="21"/>
<rectangle x1="12.09675" y1="0.55245" x2="12.32535" y2="0.56515" layer="21"/>
<rectangle x1="0.14605" y1="0.56515" x2="0.36195" y2="0.57785" layer="21"/>
<rectangle x1="0.46355" y1="0.56515" x2="0.66675" y2="0.57785" layer="21"/>
<rectangle x1="0.78105" y1="0.56515" x2="1.33985" y2="0.57785" layer="21"/>
<rectangle x1="1.44145" y1="0.56515" x2="1.67005" y2="0.57785" layer="21"/>
<rectangle x1="1.88595" y1="0.56515" x2="2.11455" y2="0.57785" layer="21"/>
<rectangle x1="2.21615" y1="0.56515" x2="2.44475" y2="0.57785" layer="21"/>
<rectangle x1="2.59715" y1="0.56515" x2="2.94005" y2="0.57785" layer="21"/>
<rectangle x1="3.14325" y1="0.56515" x2="3.37185" y2="0.57785" layer="21"/>
<rectangle x1="3.47345" y1="0.56515" x2="3.70205" y2="0.57785" layer="21"/>
<rectangle x1="3.95605" y1="0.56515" x2="4.33705" y2="0.57785" layer="21"/>
<rectangle x1="4.43865" y1="0.56515" x2="4.66725" y2="0.57785" layer="21"/>
<rectangle x1="5.07365" y1="0.56515" x2="5.61975" y2="0.57785" layer="21"/>
<rectangle x1="5.94995" y1="0.56515" x2="6.19125" y2="0.57785" layer="21"/>
<rectangle x1="6.64845" y1="0.56515" x2="6.87705" y2="0.57785" layer="21"/>
<rectangle x1="6.97865" y1="0.56515" x2="7.20725" y2="0.57785" layer="21"/>
<rectangle x1="7.30885" y1="0.56515" x2="7.53745" y2="0.57785" layer="21"/>
<rectangle x1="7.63905" y1="0.56515" x2="7.86765" y2="0.57785" layer="21"/>
<rectangle x1="7.99465" y1="0.56515" x2="8.21055" y2="0.57785" layer="21"/>
<rectangle x1="8.36295" y1="0.56515" x2="8.59155" y2="0.57785" layer="21"/>
<rectangle x1="8.80745" y1="0.56515" x2="9.03605" y2="0.57785" layer="21"/>
<rectangle x1="9.13765" y1="0.56515" x2="9.36625" y2="0.57785" layer="21"/>
<rectangle x1="9.46785" y1="0.56515" x2="9.70915" y2="0.57785" layer="21"/>
<rectangle x1="9.86155" y1="0.56515" x2="10.21715" y2="0.57785" layer="21"/>
<rectangle x1="10.64895" y1="0.56515" x2="10.89025" y2="0.57785" layer="21"/>
<rectangle x1="11.27125" y1="0.56515" x2="11.65225" y2="0.57785" layer="21"/>
<rectangle x1="11.76655" y1="0.56515" x2="11.99515" y2="0.57785" layer="21"/>
<rectangle x1="12.09675" y1="0.56515" x2="12.32535" y2="0.57785" layer="21"/>
<rectangle x1="0.15875" y1="0.57785" x2="0.36195" y2="0.59055" layer="21"/>
<rectangle x1="0.45085" y1="0.57785" x2="0.66675" y2="0.59055" layer="21"/>
<rectangle x1="0.78105" y1="0.57785" x2="1.33985" y2="0.59055" layer="21"/>
<rectangle x1="1.44145" y1="0.57785" x2="1.67005" y2="0.59055" layer="21"/>
<rectangle x1="1.88595" y1="0.57785" x2="2.11455" y2="0.59055" layer="21"/>
<rectangle x1="2.21615" y1="0.57785" x2="2.44475" y2="0.59055" layer="21"/>
<rectangle x1="2.57175" y1="0.57785" x2="2.91465" y2="0.59055" layer="21"/>
<rectangle x1="3.14325" y1="0.57785" x2="3.37185" y2="0.59055" layer="21"/>
<rectangle x1="3.47345" y1="0.57785" x2="3.70205" y2="0.59055" layer="21"/>
<rectangle x1="3.98145" y1="0.57785" x2="4.33705" y2="0.59055" layer="21"/>
<rectangle x1="4.43865" y1="0.57785" x2="4.66725" y2="0.59055" layer="21"/>
<rectangle x1="5.07365" y1="0.57785" x2="5.61975" y2="0.59055" layer="21"/>
<rectangle x1="5.94995" y1="0.57785" x2="6.19125" y2="0.59055" layer="21"/>
<rectangle x1="6.64845" y1="0.57785" x2="6.87705" y2="0.59055" layer="21"/>
<rectangle x1="6.97865" y1="0.57785" x2="7.20725" y2="0.59055" layer="21"/>
<rectangle x1="7.30885" y1="0.57785" x2="7.53745" y2="0.59055" layer="21"/>
<rectangle x1="7.63905" y1="0.57785" x2="7.86765" y2="0.59055" layer="21"/>
<rectangle x1="7.99465" y1="0.57785" x2="8.21055" y2="0.59055" layer="21"/>
<rectangle x1="8.36295" y1="0.57785" x2="8.59155" y2="0.59055" layer="21"/>
<rectangle x1="8.80745" y1="0.57785" x2="9.03605" y2="0.59055" layer="21"/>
<rectangle x1="9.13765" y1="0.57785" x2="9.36625" y2="0.59055" layer="21"/>
<rectangle x1="9.46785" y1="0.57785" x2="9.70915" y2="0.59055" layer="21"/>
<rectangle x1="9.84885" y1="0.57785" x2="10.19175" y2="0.59055" layer="21"/>
<rectangle x1="10.64895" y1="0.57785" x2="10.89025" y2="0.59055" layer="21"/>
<rectangle x1="11.29665" y1="0.57785" x2="11.65225" y2="0.59055" layer="21"/>
<rectangle x1="11.76655" y1="0.57785" x2="11.99515" y2="0.59055" layer="21"/>
<rectangle x1="12.09675" y1="0.57785" x2="12.32535" y2="0.59055" layer="21"/>
<rectangle x1="0.15875" y1="0.59055" x2="0.36195" y2="0.60325" layer="21"/>
<rectangle x1="0.45085" y1="0.59055" x2="0.65405" y2="0.60325" layer="21"/>
<rectangle x1="0.78105" y1="0.59055" x2="1.33985" y2="0.60325" layer="21"/>
<rectangle x1="1.44145" y1="0.59055" x2="1.67005" y2="0.60325" layer="21"/>
<rectangle x1="1.88595" y1="0.59055" x2="2.11455" y2="0.60325" layer="21"/>
<rectangle x1="2.21615" y1="0.59055" x2="2.44475" y2="0.60325" layer="21"/>
<rectangle x1="2.57175" y1="0.59055" x2="2.90195" y2="0.60325" layer="21"/>
<rectangle x1="3.14325" y1="0.59055" x2="3.37185" y2="0.60325" layer="21"/>
<rectangle x1="3.47345" y1="0.59055" x2="3.70205" y2="0.60325" layer="21"/>
<rectangle x1="4.01955" y1="0.59055" x2="4.33705" y2="0.60325" layer="21"/>
<rectangle x1="4.43865" y1="0.59055" x2="4.66725" y2="0.60325" layer="21"/>
<rectangle x1="5.07365" y1="0.59055" x2="5.61975" y2="0.60325" layer="21"/>
<rectangle x1="5.94995" y1="0.59055" x2="6.19125" y2="0.60325" layer="21"/>
<rectangle x1="6.64845" y1="0.59055" x2="6.87705" y2="0.60325" layer="21"/>
<rectangle x1="6.97865" y1="0.59055" x2="7.20725" y2="0.60325" layer="21"/>
<rectangle x1="7.30885" y1="0.59055" x2="7.53745" y2="0.60325" layer="21"/>
<rectangle x1="7.63905" y1="0.59055" x2="7.86765" y2="0.60325" layer="21"/>
<rectangle x1="7.99465" y1="0.59055" x2="8.21055" y2="0.60325" layer="21"/>
<rectangle x1="8.36295" y1="0.59055" x2="8.59155" y2="0.60325" layer="21"/>
<rectangle x1="8.80745" y1="0.59055" x2="9.03605" y2="0.60325" layer="21"/>
<rectangle x1="9.13765" y1="0.59055" x2="9.36625" y2="0.60325" layer="21"/>
<rectangle x1="9.46785" y1="0.59055" x2="9.70915" y2="0.60325" layer="21"/>
<rectangle x1="9.83615" y1="0.59055" x2="10.16635" y2="0.60325" layer="21"/>
<rectangle x1="10.64895" y1="0.59055" x2="10.89025" y2="0.60325" layer="21"/>
<rectangle x1="11.33475" y1="0.59055" x2="11.65225" y2="0.60325" layer="21"/>
<rectangle x1="11.76655" y1="0.59055" x2="11.99515" y2="0.60325" layer="21"/>
<rectangle x1="12.09675" y1="0.59055" x2="12.32535" y2="0.60325" layer="21"/>
<rectangle x1="0.15875" y1="0.60325" x2="0.36195" y2="0.61595" layer="21"/>
<rectangle x1="0.45085" y1="0.60325" x2="0.65405" y2="0.61595" layer="21"/>
<rectangle x1="0.78105" y1="0.60325" x2="1.33985" y2="0.61595" layer="21"/>
<rectangle x1="1.44145" y1="0.60325" x2="1.67005" y2="0.61595" layer="21"/>
<rectangle x1="1.88595" y1="0.60325" x2="2.11455" y2="0.61595" layer="21"/>
<rectangle x1="2.21615" y1="0.60325" x2="2.44475" y2="0.61595" layer="21"/>
<rectangle x1="2.55905" y1="0.60325" x2="2.87655" y2="0.61595" layer="21"/>
<rectangle x1="3.14325" y1="0.60325" x2="3.37185" y2="0.61595" layer="21"/>
<rectangle x1="3.47345" y1="0.60325" x2="3.70205" y2="0.61595" layer="21"/>
<rectangle x1="4.04495" y1="0.60325" x2="4.33705" y2="0.61595" layer="21"/>
<rectangle x1="4.43865" y1="0.60325" x2="4.66725" y2="0.61595" layer="21"/>
<rectangle x1="5.07365" y1="0.60325" x2="5.61975" y2="0.61595" layer="21"/>
<rectangle x1="5.94995" y1="0.60325" x2="6.19125" y2="0.61595" layer="21"/>
<rectangle x1="6.64845" y1="0.60325" x2="6.87705" y2="0.61595" layer="21"/>
<rectangle x1="6.97865" y1="0.60325" x2="7.20725" y2="0.61595" layer="21"/>
<rectangle x1="7.30885" y1="0.60325" x2="7.53745" y2="0.61595" layer="21"/>
<rectangle x1="7.63905" y1="0.60325" x2="7.86765" y2="0.61595" layer="21"/>
<rectangle x1="7.99465" y1="0.60325" x2="8.21055" y2="0.61595" layer="21"/>
<rectangle x1="8.36295" y1="0.60325" x2="8.59155" y2="0.61595" layer="21"/>
<rectangle x1="8.80745" y1="0.60325" x2="9.03605" y2="0.61595" layer="21"/>
<rectangle x1="9.13765" y1="0.60325" x2="9.36625" y2="0.61595" layer="21"/>
<rectangle x1="9.46785" y1="0.60325" x2="9.70915" y2="0.61595" layer="21"/>
<rectangle x1="9.82345" y1="0.60325" x2="10.14095" y2="0.61595" layer="21"/>
<rectangle x1="10.64895" y1="0.60325" x2="10.89025" y2="0.61595" layer="21"/>
<rectangle x1="11.36015" y1="0.60325" x2="11.65225" y2="0.61595" layer="21"/>
<rectangle x1="11.76655" y1="0.60325" x2="11.99515" y2="0.61595" layer="21"/>
<rectangle x1="12.09675" y1="0.60325" x2="12.32535" y2="0.61595" layer="21"/>
<rectangle x1="0.15875" y1="0.61595" x2="0.36195" y2="0.62865" layer="21"/>
<rectangle x1="0.45085" y1="0.61595" x2="0.65405" y2="0.62865" layer="21"/>
<rectangle x1="0.78105" y1="0.61595" x2="1.33985" y2="0.62865" layer="21"/>
<rectangle x1="1.44145" y1="0.61595" x2="1.68275" y2="0.62865" layer="21"/>
<rectangle x1="1.88595" y1="0.61595" x2="2.11455" y2="0.62865" layer="21"/>
<rectangle x1="2.21615" y1="0.61595" x2="2.44475" y2="0.62865" layer="21"/>
<rectangle x1="2.54635" y1="0.61595" x2="2.85115" y2="0.62865" layer="21"/>
<rectangle x1="3.14325" y1="0.61595" x2="3.37185" y2="0.62865" layer="21"/>
<rectangle x1="3.47345" y1="0.61595" x2="3.70205" y2="0.62865" layer="21"/>
<rectangle x1="4.07035" y1="0.61595" x2="4.33705" y2="0.62865" layer="21"/>
<rectangle x1="4.43865" y1="0.61595" x2="4.66725" y2="0.62865" layer="21"/>
<rectangle x1="5.07365" y1="0.61595" x2="5.61975" y2="0.62865" layer="21"/>
<rectangle x1="5.94995" y1="0.61595" x2="6.19125" y2="0.62865" layer="21"/>
<rectangle x1="6.64845" y1="0.61595" x2="6.87705" y2="0.62865" layer="21"/>
<rectangle x1="6.97865" y1="0.61595" x2="7.20725" y2="0.62865" layer="21"/>
<rectangle x1="7.30885" y1="0.61595" x2="7.53745" y2="0.62865" layer="21"/>
<rectangle x1="7.63905" y1="0.61595" x2="7.86765" y2="0.62865" layer="21"/>
<rectangle x1="7.99465" y1="0.61595" x2="8.21055" y2="0.62865" layer="21"/>
<rectangle x1="8.36295" y1="0.61595" x2="8.60425" y2="0.62865" layer="21"/>
<rectangle x1="8.80745" y1="0.61595" x2="9.03605" y2="0.62865" layer="21"/>
<rectangle x1="9.13765" y1="0.61595" x2="9.36625" y2="0.62865" layer="21"/>
<rectangle x1="9.46785" y1="0.61595" x2="9.70915" y2="0.62865" layer="21"/>
<rectangle x1="9.82345" y1="0.61595" x2="10.11555" y2="0.62865" layer="21"/>
<rectangle x1="10.64895" y1="0.61595" x2="10.89025" y2="0.62865" layer="21"/>
<rectangle x1="11.38555" y1="0.61595" x2="11.65225" y2="0.62865" layer="21"/>
<rectangle x1="11.76655" y1="0.61595" x2="11.99515" y2="0.62865" layer="21"/>
<rectangle x1="12.09675" y1="0.61595" x2="12.32535" y2="0.62865" layer="21"/>
<rectangle x1="0.15875" y1="0.62865" x2="0.36195" y2="0.64135" layer="21"/>
<rectangle x1="0.45085" y1="0.62865" x2="0.65405" y2="0.64135" layer="21"/>
<rectangle x1="0.78105" y1="0.62865" x2="1.33985" y2="0.64135" layer="21"/>
<rectangle x1="1.44145" y1="0.62865" x2="1.68275" y2="0.64135" layer="21"/>
<rectangle x1="1.88595" y1="0.62865" x2="2.11455" y2="0.64135" layer="21"/>
<rectangle x1="2.21615" y1="0.62865" x2="2.44475" y2="0.64135" layer="21"/>
<rectangle x1="2.54635" y1="0.62865" x2="2.82575" y2="0.64135" layer="21"/>
<rectangle x1="3.14325" y1="0.62865" x2="3.37185" y2="0.64135" layer="21"/>
<rectangle x1="3.47345" y1="0.62865" x2="3.70205" y2="0.64135" layer="21"/>
<rectangle x1="4.09575" y1="0.62865" x2="4.33705" y2="0.64135" layer="21"/>
<rectangle x1="4.43865" y1="0.62865" x2="4.66725" y2="0.64135" layer="21"/>
<rectangle x1="5.07365" y1="0.62865" x2="5.61975" y2="0.64135" layer="21"/>
<rectangle x1="5.94995" y1="0.62865" x2="6.19125" y2="0.64135" layer="21"/>
<rectangle x1="6.64845" y1="0.62865" x2="6.87705" y2="0.64135" layer="21"/>
<rectangle x1="6.97865" y1="0.62865" x2="7.20725" y2="0.64135" layer="21"/>
<rectangle x1="7.30885" y1="0.62865" x2="7.53745" y2="0.64135" layer="21"/>
<rectangle x1="7.63905" y1="0.62865" x2="7.86765" y2="0.64135" layer="21"/>
<rectangle x1="7.99465" y1="0.62865" x2="8.21055" y2="0.64135" layer="21"/>
<rectangle x1="8.36295" y1="0.62865" x2="8.60425" y2="0.64135" layer="21"/>
<rectangle x1="8.80745" y1="0.62865" x2="9.03605" y2="0.64135" layer="21"/>
<rectangle x1="9.13765" y1="0.62865" x2="9.36625" y2="0.64135" layer="21"/>
<rectangle x1="9.46785" y1="0.62865" x2="9.70915" y2="0.64135" layer="21"/>
<rectangle x1="9.82345" y1="0.62865" x2="10.09015" y2="0.64135" layer="21"/>
<rectangle x1="10.64895" y1="0.62865" x2="10.89025" y2="0.64135" layer="21"/>
<rectangle x1="11.41095" y1="0.62865" x2="11.65225" y2="0.64135" layer="21"/>
<rectangle x1="11.76655" y1="0.62865" x2="11.99515" y2="0.64135" layer="21"/>
<rectangle x1="12.09675" y1="0.62865" x2="12.32535" y2="0.64135" layer="21"/>
<rectangle x1="0.15875" y1="0.64135" x2="0.36195" y2="0.65405" layer="21"/>
<rectangle x1="0.45085" y1="0.64135" x2="0.65405" y2="0.65405" layer="21"/>
<rectangle x1="0.78105" y1="0.64135" x2="1.33985" y2="0.65405" layer="21"/>
<rectangle x1="1.44145" y1="0.64135" x2="1.69545" y2="0.65405" layer="21"/>
<rectangle x1="1.88595" y1="0.64135" x2="2.11455" y2="0.65405" layer="21"/>
<rectangle x1="2.21615" y1="0.64135" x2="2.44475" y2="0.65405" layer="21"/>
<rectangle x1="2.54635" y1="0.64135" x2="2.80035" y2="0.65405" layer="21"/>
<rectangle x1="3.14325" y1="0.64135" x2="3.37185" y2="0.65405" layer="21"/>
<rectangle x1="3.47345" y1="0.64135" x2="3.70205" y2="0.65405" layer="21"/>
<rectangle x1="4.10845" y1="0.64135" x2="4.33705" y2="0.65405" layer="21"/>
<rectangle x1="4.43865" y1="0.64135" x2="4.66725" y2="0.65405" layer="21"/>
<rectangle x1="5.07365" y1="0.64135" x2="5.61975" y2="0.65405" layer="21"/>
<rectangle x1="5.94995" y1="0.64135" x2="6.19125" y2="0.65405" layer="21"/>
<rectangle x1="6.64845" y1="0.64135" x2="6.87705" y2="0.65405" layer="21"/>
<rectangle x1="6.97865" y1="0.64135" x2="7.20725" y2="0.65405" layer="21"/>
<rectangle x1="7.30885" y1="0.64135" x2="7.53745" y2="0.65405" layer="21"/>
<rectangle x1="7.63905" y1="0.64135" x2="7.86765" y2="0.65405" layer="21"/>
<rectangle x1="7.99465" y1="0.64135" x2="8.21055" y2="0.65405" layer="21"/>
<rectangle x1="8.36295" y1="0.64135" x2="8.61695" y2="0.65405" layer="21"/>
<rectangle x1="8.80745" y1="0.64135" x2="9.03605" y2="0.65405" layer="21"/>
<rectangle x1="9.13765" y1="0.64135" x2="9.36625" y2="0.65405" layer="21"/>
<rectangle x1="9.46785" y1="0.64135" x2="9.70915" y2="0.65405" layer="21"/>
<rectangle x1="9.81075" y1="0.64135" x2="10.06475" y2="0.65405" layer="21"/>
<rectangle x1="10.64895" y1="0.64135" x2="10.89025" y2="0.65405" layer="21"/>
<rectangle x1="11.42365" y1="0.64135" x2="11.65225" y2="0.65405" layer="21"/>
<rectangle x1="11.76655" y1="0.64135" x2="11.99515" y2="0.65405" layer="21"/>
<rectangle x1="12.09675" y1="0.64135" x2="12.32535" y2="0.65405" layer="21"/>
<rectangle x1="0.15875" y1="0.65405" x2="0.36195" y2="0.66675" layer="21"/>
<rectangle x1="0.45085" y1="0.65405" x2="0.65405" y2="0.66675" layer="21"/>
<rectangle x1="0.78105" y1="0.65405" x2="1.00965" y2="0.66675" layer="21"/>
<rectangle x1="1.09855" y1="0.65405" x2="1.33985" y2="0.66675" layer="21"/>
<rectangle x1="1.44145" y1="0.65405" x2="1.69545" y2="0.66675" layer="21"/>
<rectangle x1="1.88595" y1="0.65405" x2="2.11455" y2="0.66675" layer="21"/>
<rectangle x1="2.21615" y1="0.65405" x2="2.44475" y2="0.66675" layer="21"/>
<rectangle x1="2.53365" y1="0.65405" x2="2.77495" y2="0.66675" layer="21"/>
<rectangle x1="3.14325" y1="0.65405" x2="3.37185" y2="0.66675" layer="21"/>
<rectangle x1="3.47345" y1="0.65405" x2="3.70205" y2="0.66675" layer="21"/>
<rectangle x1="3.80365" y1="0.65405" x2="4.00685" y2="0.66675" layer="21"/>
<rectangle x1="4.10845" y1="0.65405" x2="4.33705" y2="0.66675" layer="21"/>
<rectangle x1="4.43865" y1="0.65405" x2="4.66725" y2="0.66675" layer="21"/>
<rectangle x1="5.07365" y1="0.65405" x2="5.30225" y2="0.66675" layer="21"/>
<rectangle x1="5.39115" y1="0.65405" x2="5.61975" y2="0.66675" layer="21"/>
<rectangle x1="5.94995" y1="0.65405" x2="6.19125" y2="0.66675" layer="21"/>
<rectangle x1="6.64845" y1="0.65405" x2="6.87705" y2="0.66675" layer="21"/>
<rectangle x1="6.97865" y1="0.65405" x2="7.20725" y2="0.66675" layer="21"/>
<rectangle x1="7.30885" y1="0.65405" x2="7.53745" y2="0.66675" layer="21"/>
<rectangle x1="7.63905" y1="0.65405" x2="7.86765" y2="0.66675" layer="21"/>
<rectangle x1="7.99465" y1="0.65405" x2="8.21055" y2="0.66675" layer="21"/>
<rectangle x1="8.36295" y1="0.65405" x2="8.61695" y2="0.66675" layer="21"/>
<rectangle x1="8.80745" y1="0.65405" x2="9.03605" y2="0.66675" layer="21"/>
<rectangle x1="9.13765" y1="0.65405" x2="9.36625" y2="0.66675" layer="21"/>
<rectangle x1="9.46785" y1="0.65405" x2="9.70915" y2="0.66675" layer="21"/>
<rectangle x1="9.81075" y1="0.65405" x2="10.05205" y2="0.66675" layer="21"/>
<rectangle x1="10.64895" y1="0.65405" x2="10.89025" y2="0.66675" layer="21"/>
<rectangle x1="11.11885" y1="0.65405" x2="11.32205" y2="0.66675" layer="21"/>
<rectangle x1="11.42365" y1="0.65405" x2="11.65225" y2="0.66675" layer="21"/>
<rectangle x1="11.76655" y1="0.65405" x2="11.99515" y2="0.66675" layer="21"/>
<rectangle x1="12.09675" y1="0.65405" x2="12.32535" y2="0.66675" layer="21"/>
<rectangle x1="0.17145" y1="0.66675" x2="0.37465" y2="0.67945" layer="21"/>
<rectangle x1="0.45085" y1="0.66675" x2="0.65405" y2="0.67945" layer="21"/>
<rectangle x1="0.78105" y1="0.66675" x2="1.00965" y2="0.67945" layer="21"/>
<rectangle x1="1.11125" y1="0.66675" x2="1.33985" y2="0.67945" layer="21"/>
<rectangle x1="1.44145" y1="0.66675" x2="1.72085" y2="0.67945" layer="21"/>
<rectangle x1="1.88595" y1="0.66675" x2="2.11455" y2="0.67945" layer="21"/>
<rectangle x1="2.21615" y1="0.66675" x2="2.44475" y2="0.67945" layer="21"/>
<rectangle x1="2.53365" y1="0.66675" x2="2.76225" y2="0.67945" layer="21"/>
<rectangle x1="3.14325" y1="0.66675" x2="3.37185" y2="0.67945" layer="21"/>
<rectangle x1="3.47345" y1="0.66675" x2="3.70205" y2="0.67945" layer="21"/>
<rectangle x1="3.80365" y1="0.66675" x2="4.00685" y2="0.67945" layer="21"/>
<rectangle x1="4.10845" y1="0.66675" x2="4.33705" y2="0.67945" layer="21"/>
<rectangle x1="4.43865" y1="0.66675" x2="4.66725" y2="0.67945" layer="21"/>
<rectangle x1="4.76885" y1="0.66675" x2="4.97205" y2="0.67945" layer="21"/>
<rectangle x1="5.07365" y1="0.66675" x2="5.30225" y2="0.67945" layer="21"/>
<rectangle x1="5.39115" y1="0.66675" x2="5.61975" y2="0.67945" layer="21"/>
<rectangle x1="5.94995" y1="0.66675" x2="6.19125" y2="0.67945" layer="21"/>
<rectangle x1="6.64845" y1="0.66675" x2="6.87705" y2="0.67945" layer="21"/>
<rectangle x1="6.97865" y1="0.66675" x2="7.20725" y2="0.67945" layer="21"/>
<rectangle x1="7.30885" y1="0.66675" x2="7.53745" y2="0.67945" layer="21"/>
<rectangle x1="7.63905" y1="0.66675" x2="7.86765" y2="0.67945" layer="21"/>
<rectangle x1="7.99465" y1="0.66675" x2="8.21055" y2="0.67945" layer="21"/>
<rectangle x1="8.36295" y1="0.66675" x2="8.64235" y2="0.67945" layer="21"/>
<rectangle x1="8.82015" y1="0.66675" x2="9.03605" y2="0.67945" layer="21"/>
<rectangle x1="9.13765" y1="0.66675" x2="9.36625" y2="0.67945" layer="21"/>
<rectangle x1="9.46785" y1="0.66675" x2="9.70915" y2="0.67945" layer="21"/>
<rectangle x1="9.81075" y1="0.66675" x2="10.03935" y2="0.67945" layer="21"/>
<rectangle x1="10.64895" y1="0.66675" x2="10.89025" y2="0.67945" layer="21"/>
<rectangle x1="11.11885" y1="0.66675" x2="11.32205" y2="0.67945" layer="21"/>
<rectangle x1="11.42365" y1="0.66675" x2="11.65225" y2="0.67945" layer="21"/>
<rectangle x1="11.76655" y1="0.66675" x2="11.99515" y2="0.67945" layer="21"/>
<rectangle x1="12.09675" y1="0.66675" x2="12.32535" y2="0.67945" layer="21"/>
<rectangle x1="0.17145" y1="0.67945" x2="0.37465" y2="0.69215" layer="21"/>
<rectangle x1="0.45085" y1="0.67945" x2="0.64135" y2="0.69215" layer="21"/>
<rectangle x1="0.78105" y1="0.67945" x2="1.00965" y2="0.69215" layer="21"/>
<rectangle x1="1.11125" y1="0.67945" x2="1.32715" y2="0.69215" layer="21"/>
<rectangle x1="1.44145" y1="0.67945" x2="1.74625" y2="0.69215" layer="21"/>
<rectangle x1="1.88595" y1="0.67945" x2="2.11455" y2="0.69215" layer="21"/>
<rectangle x1="2.21615" y1="0.67945" x2="2.44475" y2="0.69215" layer="21"/>
<rectangle x1="2.53365" y1="0.67945" x2="2.74955" y2="0.69215" layer="21"/>
<rectangle x1="3.14325" y1="0.67945" x2="3.37185" y2="0.69215" layer="21"/>
<rectangle x1="3.47345" y1="0.67945" x2="3.70205" y2="0.69215" layer="21"/>
<rectangle x1="3.80365" y1="0.67945" x2="4.00685" y2="0.69215" layer="21"/>
<rectangle x1="4.10845" y1="0.67945" x2="4.33705" y2="0.69215" layer="21"/>
<rectangle x1="4.43865" y1="0.67945" x2="4.66725" y2="0.69215" layer="21"/>
<rectangle x1="4.76885" y1="0.67945" x2="4.97205" y2="0.69215" layer="21"/>
<rectangle x1="5.07365" y1="0.67945" x2="5.30225" y2="0.69215" layer="21"/>
<rectangle x1="5.39115" y1="0.67945" x2="5.61975" y2="0.69215" layer="21"/>
<rectangle x1="5.94995" y1="0.67945" x2="6.19125" y2="0.69215" layer="21"/>
<rectangle x1="6.64845" y1="0.67945" x2="6.87705" y2="0.69215" layer="21"/>
<rectangle x1="6.97865" y1="0.67945" x2="7.20725" y2="0.69215" layer="21"/>
<rectangle x1="7.30885" y1="0.67945" x2="7.53745" y2="0.69215" layer="21"/>
<rectangle x1="7.63905" y1="0.67945" x2="7.86765" y2="0.69215" layer="21"/>
<rectangle x1="7.99465" y1="0.67945" x2="8.21055" y2="0.69215" layer="21"/>
<rectangle x1="8.36295" y1="0.67945" x2="8.66775" y2="0.69215" layer="21"/>
<rectangle x1="8.82015" y1="0.67945" x2="9.03605" y2="0.69215" layer="21"/>
<rectangle x1="9.13765" y1="0.67945" x2="9.36625" y2="0.69215" layer="21"/>
<rectangle x1="9.46785" y1="0.67945" x2="9.70915" y2="0.69215" layer="21"/>
<rectangle x1="9.79805" y1="0.67945" x2="10.02665" y2="0.69215" layer="21"/>
<rectangle x1="10.64895" y1="0.67945" x2="10.89025" y2="0.69215" layer="21"/>
<rectangle x1="11.11885" y1="0.67945" x2="11.32205" y2="0.69215" layer="21"/>
<rectangle x1="11.43635" y1="0.67945" x2="11.65225" y2="0.69215" layer="21"/>
<rectangle x1="11.76655" y1="0.67945" x2="11.99515" y2="0.69215" layer="21"/>
<rectangle x1="12.09675" y1="0.67945" x2="12.32535" y2="0.69215" layer="21"/>
<rectangle x1="0.17145" y1="0.69215" x2="0.37465" y2="0.70485" layer="21"/>
<rectangle x1="0.45085" y1="0.69215" x2="0.64135" y2="0.70485" layer="21"/>
<rectangle x1="0.78105" y1="0.69215" x2="1.00965" y2="0.70485" layer="21"/>
<rectangle x1="1.11125" y1="0.69215" x2="1.32715" y2="0.70485" layer="21"/>
<rectangle x1="1.44145" y1="0.69215" x2="1.82245" y2="0.70485" layer="21"/>
<rectangle x1="1.89865" y1="0.69215" x2="2.11455" y2="0.70485" layer="21"/>
<rectangle x1="2.21615" y1="0.69215" x2="2.44475" y2="0.70485" layer="21"/>
<rectangle x1="2.53365" y1="0.69215" x2="2.73685" y2="0.70485" layer="21"/>
<rectangle x1="3.14325" y1="0.69215" x2="3.37185" y2="0.70485" layer="21"/>
<rectangle x1="3.47345" y1="0.69215" x2="3.70205" y2="0.70485" layer="21"/>
<rectangle x1="3.80365" y1="0.69215" x2="4.00685" y2="0.70485" layer="21"/>
<rectangle x1="4.10845" y1="0.69215" x2="4.33705" y2="0.70485" layer="21"/>
<rectangle x1="4.43865" y1="0.69215" x2="4.66725" y2="0.70485" layer="21"/>
<rectangle x1="4.76885" y1="0.69215" x2="4.97205" y2="0.70485" layer="21"/>
<rectangle x1="5.07365" y1="0.69215" x2="5.30225" y2="0.70485" layer="21"/>
<rectangle x1="5.39115" y1="0.69215" x2="5.61975" y2="0.70485" layer="21"/>
<rectangle x1="5.94995" y1="0.69215" x2="6.19125" y2="0.70485" layer="21"/>
<rectangle x1="6.66115" y1="0.69215" x2="6.87705" y2="0.70485" layer="21"/>
<rectangle x1="6.97865" y1="0.69215" x2="7.20725" y2="0.70485" layer="21"/>
<rectangle x1="7.30885" y1="0.69215" x2="7.53745" y2="0.70485" layer="21"/>
<rectangle x1="7.63905" y1="0.69215" x2="7.86765" y2="0.70485" layer="21"/>
<rectangle x1="7.99465" y1="0.69215" x2="8.21055" y2="0.70485" layer="21"/>
<rectangle x1="8.36295" y1="0.69215" x2="8.74395" y2="0.70485" layer="21"/>
<rectangle x1="8.82015" y1="0.69215" x2="9.03605" y2="0.70485" layer="21"/>
<rectangle x1="9.13765" y1="0.69215" x2="9.36625" y2="0.70485" layer="21"/>
<rectangle x1="9.46785" y1="0.69215" x2="9.70915" y2="0.70485" layer="21"/>
<rectangle x1="9.79805" y1="0.69215" x2="10.01395" y2="0.70485" layer="21"/>
<rectangle x1="10.64895" y1="0.69215" x2="10.89025" y2="0.70485" layer="21"/>
<rectangle x1="11.11885" y1="0.69215" x2="11.32205" y2="0.70485" layer="21"/>
<rectangle x1="11.43635" y1="0.69215" x2="11.65225" y2="0.70485" layer="21"/>
<rectangle x1="11.76655" y1="0.69215" x2="11.99515" y2="0.70485" layer="21"/>
<rectangle x1="12.09675" y1="0.69215" x2="12.32535" y2="0.70485" layer="21"/>
<rectangle x1="0.17145" y1="0.70485" x2="0.37465" y2="0.71755" layer="21"/>
<rectangle x1="0.45085" y1="0.70485" x2="0.64135" y2="0.71755" layer="21"/>
<rectangle x1="0.78105" y1="0.70485" x2="1.00965" y2="0.71755" layer="21"/>
<rectangle x1="1.11125" y1="0.70485" x2="1.32715" y2="0.71755" layer="21"/>
<rectangle x1="1.44145" y1="0.70485" x2="1.82245" y2="0.71755" layer="21"/>
<rectangle x1="1.89865" y1="0.70485" x2="2.11455" y2="0.71755" layer="21"/>
<rectangle x1="2.21615" y1="0.70485" x2="2.44475" y2="0.71755" layer="21"/>
<rectangle x1="2.53365" y1="0.70485" x2="2.73685" y2="0.71755" layer="21"/>
<rectangle x1="2.83845" y1="0.70485" x2="3.04165" y2="0.71755" layer="21"/>
<rectangle x1="3.14325" y1="0.70485" x2="3.37185" y2="0.71755" layer="21"/>
<rectangle x1="3.47345" y1="0.70485" x2="3.70205" y2="0.71755" layer="21"/>
<rectangle x1="3.80365" y1="0.70485" x2="4.00685" y2="0.71755" layer="21"/>
<rectangle x1="4.10845" y1="0.70485" x2="4.33705" y2="0.71755" layer="21"/>
<rectangle x1="4.43865" y1="0.70485" x2="4.66725" y2="0.71755" layer="21"/>
<rectangle x1="4.76885" y1="0.70485" x2="4.97205" y2="0.71755" layer="21"/>
<rectangle x1="5.07365" y1="0.70485" x2="5.30225" y2="0.71755" layer="21"/>
<rectangle x1="5.39115" y1="0.70485" x2="5.61975" y2="0.71755" layer="21"/>
<rectangle x1="5.94995" y1="0.70485" x2="6.19125" y2="0.71755" layer="21"/>
<rectangle x1="6.66115" y1="0.70485" x2="6.87705" y2="0.71755" layer="21"/>
<rectangle x1="6.97865" y1="0.70485" x2="7.20725" y2="0.71755" layer="21"/>
<rectangle x1="7.30885" y1="0.70485" x2="7.53745" y2="0.71755" layer="21"/>
<rectangle x1="7.63905" y1="0.70485" x2="7.86765" y2="0.71755" layer="21"/>
<rectangle x1="7.99465" y1="0.70485" x2="8.21055" y2="0.71755" layer="21"/>
<rectangle x1="8.36295" y1="0.70485" x2="8.74395" y2="0.71755" layer="21"/>
<rectangle x1="8.82015" y1="0.70485" x2="9.03605" y2="0.71755" layer="21"/>
<rectangle x1="9.13765" y1="0.70485" x2="9.36625" y2="0.71755" layer="21"/>
<rectangle x1="9.46785" y1="0.70485" x2="9.70915" y2="0.71755" layer="21"/>
<rectangle x1="9.79805" y1="0.70485" x2="10.00125" y2="0.71755" layer="21"/>
<rectangle x1="10.11555" y1="0.70485" x2="10.30605" y2="0.71755" layer="21"/>
<rectangle x1="10.64895" y1="0.70485" x2="10.89025" y2="0.71755" layer="21"/>
<rectangle x1="11.11885" y1="0.70485" x2="11.32205" y2="0.71755" layer="21"/>
<rectangle x1="11.43635" y1="0.70485" x2="11.65225" y2="0.71755" layer="21"/>
<rectangle x1="11.76655" y1="0.70485" x2="11.99515" y2="0.71755" layer="21"/>
<rectangle x1="12.09675" y1="0.70485" x2="12.32535" y2="0.71755" layer="21"/>
<rectangle x1="0.17145" y1="0.71755" x2="0.37465" y2="0.73025" layer="21"/>
<rectangle x1="0.43815" y1="0.71755" x2="0.64135" y2="0.73025" layer="21"/>
<rectangle x1="0.78105" y1="0.71755" x2="1.00965" y2="0.73025" layer="21"/>
<rectangle x1="1.11125" y1="0.71755" x2="1.32715" y2="0.73025" layer="21"/>
<rectangle x1="1.44145" y1="0.71755" x2="1.82245" y2="0.73025" layer="21"/>
<rectangle x1="1.89865" y1="0.71755" x2="2.11455" y2="0.73025" layer="21"/>
<rectangle x1="2.21615" y1="0.71755" x2="2.44475" y2="0.73025" layer="21"/>
<rectangle x1="2.53365" y1="0.71755" x2="2.73685" y2="0.73025" layer="21"/>
<rectangle x1="2.83845" y1="0.71755" x2="3.04165" y2="0.73025" layer="21"/>
<rectangle x1="3.14325" y1="0.71755" x2="3.37185" y2="0.73025" layer="21"/>
<rectangle x1="3.47345" y1="0.71755" x2="3.70205" y2="0.73025" layer="21"/>
<rectangle x1="3.80365" y1="0.71755" x2="4.00685" y2="0.73025" layer="21"/>
<rectangle x1="4.10845" y1="0.71755" x2="4.33705" y2="0.73025" layer="21"/>
<rectangle x1="4.45135" y1="0.71755" x2="4.66725" y2="0.73025" layer="21"/>
<rectangle x1="4.76885" y1="0.71755" x2="4.97205" y2="0.73025" layer="21"/>
<rectangle x1="5.07365" y1="0.71755" x2="5.30225" y2="0.73025" layer="21"/>
<rectangle x1="5.39115" y1="0.71755" x2="5.61975" y2="0.73025" layer="21"/>
<rectangle x1="5.94995" y1="0.71755" x2="6.19125" y2="0.73025" layer="21"/>
<rectangle x1="6.31825" y1="0.71755" x2="6.54685" y2="0.73025" layer="21"/>
<rectangle x1="6.66115" y1="0.71755" x2="6.87705" y2="0.73025" layer="21"/>
<rectangle x1="6.97865" y1="0.71755" x2="7.20725" y2="0.73025" layer="21"/>
<rectangle x1="7.30885" y1="0.71755" x2="7.53745" y2="0.73025" layer="21"/>
<rectangle x1="7.63905" y1="0.71755" x2="7.86765" y2="0.73025" layer="21"/>
<rectangle x1="7.99465" y1="0.71755" x2="8.21055" y2="0.73025" layer="21"/>
<rectangle x1="8.36295" y1="0.71755" x2="8.74395" y2="0.73025" layer="21"/>
<rectangle x1="8.82015" y1="0.71755" x2="9.03605" y2="0.73025" layer="21"/>
<rectangle x1="9.13765" y1="0.71755" x2="9.36625" y2="0.73025" layer="21"/>
<rectangle x1="9.46785" y1="0.71755" x2="9.70915" y2="0.73025" layer="21"/>
<rectangle x1="9.79805" y1="0.71755" x2="10.00125" y2="0.73025" layer="21"/>
<rectangle x1="10.11555" y1="0.71755" x2="10.30605" y2="0.73025" layer="21"/>
<rectangle x1="10.64895" y1="0.71755" x2="10.89025" y2="0.73025" layer="21"/>
<rectangle x1="11.11885" y1="0.71755" x2="11.32205" y2="0.73025" layer="21"/>
<rectangle x1="11.43635" y1="0.71755" x2="11.65225" y2="0.73025" layer="21"/>
<rectangle x1="11.76655" y1="0.71755" x2="11.99515" y2="0.73025" layer="21"/>
<rectangle x1="12.09675" y1="0.71755" x2="12.32535" y2="0.73025" layer="21"/>
<rectangle x1="0.17145" y1="0.73025" x2="0.37465" y2="0.74295" layer="21"/>
<rectangle x1="0.43815" y1="0.73025" x2="0.64135" y2="0.74295" layer="21"/>
<rectangle x1="0.79375" y1="0.73025" x2="1.00965" y2="0.74295" layer="21"/>
<rectangle x1="1.11125" y1="0.73025" x2="1.32715" y2="0.74295" layer="21"/>
<rectangle x1="1.44145" y1="0.73025" x2="1.82245" y2="0.74295" layer="21"/>
<rectangle x1="1.89865" y1="0.73025" x2="2.11455" y2="0.74295" layer="21"/>
<rectangle x1="2.21615" y1="0.73025" x2="2.43205" y2="0.74295" layer="21"/>
<rectangle x1="2.53365" y1="0.73025" x2="2.73685" y2="0.74295" layer="21"/>
<rectangle x1="2.83845" y1="0.73025" x2="3.04165" y2="0.74295" layer="21"/>
<rectangle x1="3.14325" y1="0.73025" x2="3.37185" y2="0.74295" layer="21"/>
<rectangle x1="3.47345" y1="0.73025" x2="3.70205" y2="0.74295" layer="21"/>
<rectangle x1="3.80365" y1="0.73025" x2="4.00685" y2="0.74295" layer="21"/>
<rectangle x1="4.10845" y1="0.73025" x2="4.33705" y2="0.74295" layer="21"/>
<rectangle x1="4.45135" y1="0.73025" x2="4.66725" y2="0.74295" layer="21"/>
<rectangle x1="4.76885" y1="0.73025" x2="4.97205" y2="0.74295" layer="21"/>
<rectangle x1="5.07365" y1="0.73025" x2="5.30225" y2="0.74295" layer="21"/>
<rectangle x1="5.39115" y1="0.73025" x2="5.61975" y2="0.74295" layer="21"/>
<rectangle x1="5.94995" y1="0.73025" x2="6.19125" y2="0.74295" layer="21"/>
<rectangle x1="6.31825" y1="0.73025" x2="6.54685" y2="0.74295" layer="21"/>
<rectangle x1="6.66115" y1="0.73025" x2="6.87705" y2="0.74295" layer="21"/>
<rectangle x1="6.97865" y1="0.73025" x2="7.19455" y2="0.74295" layer="21"/>
<rectangle x1="7.30885" y1="0.73025" x2="7.53745" y2="0.74295" layer="21"/>
<rectangle x1="7.63905" y1="0.73025" x2="7.86765" y2="0.74295" layer="21"/>
<rectangle x1="7.99465" y1="0.73025" x2="8.21055" y2="0.74295" layer="21"/>
<rectangle x1="8.36295" y1="0.73025" x2="8.74395" y2="0.74295" layer="21"/>
<rectangle x1="8.82015" y1="0.73025" x2="9.03605" y2="0.74295" layer="21"/>
<rectangle x1="9.13765" y1="0.73025" x2="9.36625" y2="0.74295" layer="21"/>
<rectangle x1="9.46785" y1="0.73025" x2="9.70915" y2="0.74295" layer="21"/>
<rectangle x1="9.79805" y1="0.73025" x2="10.00125" y2="0.74295" layer="21"/>
<rectangle x1="10.11555" y1="0.73025" x2="10.30605" y2="0.74295" layer="21"/>
<rectangle x1="10.64895" y1="0.73025" x2="10.89025" y2="0.74295" layer="21"/>
<rectangle x1="11.11885" y1="0.73025" x2="11.32205" y2="0.74295" layer="21"/>
<rectangle x1="11.43635" y1="0.73025" x2="11.65225" y2="0.74295" layer="21"/>
<rectangle x1="11.76655" y1="0.73025" x2="11.99515" y2="0.74295" layer="21"/>
<rectangle x1="12.09675" y1="0.73025" x2="12.32535" y2="0.74295" layer="21"/>
<rectangle x1="0.17145" y1="0.74295" x2="0.37465" y2="0.75565" layer="21"/>
<rectangle x1="0.43815" y1="0.74295" x2="0.64135" y2="0.75565" layer="21"/>
<rectangle x1="0.79375" y1="0.74295" x2="1.00965" y2="0.75565" layer="21"/>
<rectangle x1="1.11125" y1="0.74295" x2="1.32715" y2="0.75565" layer="21"/>
<rectangle x1="1.44145" y1="0.74295" x2="1.82245" y2="0.75565" layer="21"/>
<rectangle x1="1.89865" y1="0.74295" x2="2.11455" y2="0.75565" layer="21"/>
<rectangle x1="2.21615" y1="0.74295" x2="2.43205" y2="0.75565" layer="21"/>
<rectangle x1="2.53365" y1="0.74295" x2="2.73685" y2="0.75565" layer="21"/>
<rectangle x1="2.83845" y1="0.74295" x2="3.04165" y2="0.75565" layer="21"/>
<rectangle x1="3.14325" y1="0.74295" x2="3.37185" y2="0.75565" layer="21"/>
<rectangle x1="3.47345" y1="0.74295" x2="3.70205" y2="0.75565" layer="21"/>
<rectangle x1="3.80365" y1="0.74295" x2="4.00685" y2="0.75565" layer="21"/>
<rectangle x1="4.10845" y1="0.74295" x2="4.33705" y2="0.75565" layer="21"/>
<rectangle x1="4.45135" y1="0.74295" x2="4.66725" y2="0.75565" layer="21"/>
<rectangle x1="4.76885" y1="0.74295" x2="4.97205" y2="0.75565" layer="21"/>
<rectangle x1="5.07365" y1="0.74295" x2="5.30225" y2="0.75565" layer="21"/>
<rectangle x1="5.39115" y1="0.74295" x2="5.61975" y2="0.75565" layer="21"/>
<rectangle x1="5.94995" y1="0.74295" x2="6.19125" y2="0.75565" layer="21"/>
<rectangle x1="6.31825" y1="0.74295" x2="6.54685" y2="0.75565" layer="21"/>
<rectangle x1="6.66115" y1="0.74295" x2="6.87705" y2="0.75565" layer="21"/>
<rectangle x1="6.97865" y1="0.74295" x2="7.19455" y2="0.75565" layer="21"/>
<rectangle x1="7.30885" y1="0.74295" x2="7.53745" y2="0.75565" layer="21"/>
<rectangle x1="7.63905" y1="0.74295" x2="7.86765" y2="0.75565" layer="21"/>
<rectangle x1="7.99465" y1="0.74295" x2="8.21055" y2="0.75565" layer="21"/>
<rectangle x1="8.36295" y1="0.74295" x2="8.74395" y2="0.75565" layer="21"/>
<rectangle x1="8.82015" y1="0.74295" x2="9.03605" y2="0.75565" layer="21"/>
<rectangle x1="9.13765" y1="0.74295" x2="9.35355" y2="0.75565" layer="21"/>
<rectangle x1="9.46785" y1="0.74295" x2="9.70915" y2="0.75565" layer="21"/>
<rectangle x1="9.79805" y1="0.74295" x2="10.00125" y2="0.75565" layer="21"/>
<rectangle x1="10.11555" y1="0.74295" x2="10.30605" y2="0.75565" layer="21"/>
<rectangle x1="10.64895" y1="0.74295" x2="10.89025" y2="0.75565" layer="21"/>
<rectangle x1="11.11885" y1="0.74295" x2="11.32205" y2="0.75565" layer="21"/>
<rectangle x1="11.43635" y1="0.74295" x2="11.65225" y2="0.75565" layer="21"/>
<rectangle x1="11.76655" y1="0.74295" x2="11.99515" y2="0.75565" layer="21"/>
<rectangle x1="12.09675" y1="0.74295" x2="12.32535" y2="0.75565" layer="21"/>
<rectangle x1="0.17145" y1="0.75565" x2="0.37465" y2="0.76835" layer="21"/>
<rectangle x1="0.43815" y1="0.75565" x2="0.62865" y2="0.76835" layer="21"/>
<rectangle x1="0.79375" y1="0.75565" x2="1.00965" y2="0.76835" layer="21"/>
<rectangle x1="1.11125" y1="0.75565" x2="1.32715" y2="0.76835" layer="21"/>
<rectangle x1="1.44145" y1="0.75565" x2="1.82245" y2="0.76835" layer="21"/>
<rectangle x1="1.89865" y1="0.75565" x2="2.11455" y2="0.76835" layer="21"/>
<rectangle x1="2.21615" y1="0.75565" x2="2.43205" y2="0.76835" layer="21"/>
<rectangle x1="2.53365" y1="0.75565" x2="2.73685" y2="0.76835" layer="21"/>
<rectangle x1="2.83845" y1="0.75565" x2="3.04165" y2="0.76835" layer="21"/>
<rectangle x1="3.14325" y1="0.75565" x2="3.37185" y2="0.76835" layer="21"/>
<rectangle x1="3.47345" y1="0.75565" x2="3.70205" y2="0.76835" layer="21"/>
<rectangle x1="3.80365" y1="0.75565" x2="4.00685" y2="0.76835" layer="21"/>
<rectangle x1="4.10845" y1="0.75565" x2="4.33705" y2="0.76835" layer="21"/>
<rectangle x1="4.45135" y1="0.75565" x2="4.66725" y2="0.76835" layer="21"/>
<rectangle x1="4.76885" y1="0.75565" x2="4.97205" y2="0.76835" layer="21"/>
<rectangle x1="5.08635" y1="0.75565" x2="5.30225" y2="0.76835" layer="21"/>
<rectangle x1="5.39115" y1="0.75565" x2="5.60705" y2="0.76835" layer="21"/>
<rectangle x1="5.94995" y1="0.75565" x2="6.19125" y2="0.76835" layer="21"/>
<rectangle x1="6.31825" y1="0.75565" x2="6.54685" y2="0.76835" layer="21"/>
<rectangle x1="6.66115" y1="0.75565" x2="6.87705" y2="0.76835" layer="21"/>
<rectangle x1="6.97865" y1="0.75565" x2="7.19455" y2="0.76835" layer="21"/>
<rectangle x1="7.30885" y1="0.75565" x2="7.53745" y2="0.76835" layer="21"/>
<rectangle x1="7.63905" y1="0.75565" x2="7.86765" y2="0.76835" layer="21"/>
<rectangle x1="7.99465" y1="0.75565" x2="8.21055" y2="0.76835" layer="21"/>
<rectangle x1="8.36295" y1="0.75565" x2="8.74395" y2="0.76835" layer="21"/>
<rectangle x1="8.82015" y1="0.75565" x2="9.03605" y2="0.76835" layer="21"/>
<rectangle x1="9.13765" y1="0.75565" x2="9.35355" y2="0.76835" layer="21"/>
<rectangle x1="9.46785" y1="0.75565" x2="9.70915" y2="0.76835" layer="21"/>
<rectangle x1="9.79805" y1="0.75565" x2="10.00125" y2="0.76835" layer="21"/>
<rectangle x1="10.11555" y1="0.75565" x2="10.30605" y2="0.76835" layer="21"/>
<rectangle x1="10.64895" y1="0.75565" x2="10.89025" y2="0.76835" layer="21"/>
<rectangle x1="11.11885" y1="0.75565" x2="11.32205" y2="0.76835" layer="21"/>
<rectangle x1="11.43635" y1="0.75565" x2="11.65225" y2="0.76835" layer="21"/>
<rectangle x1="11.76655" y1="0.75565" x2="11.99515" y2="0.76835" layer="21"/>
<rectangle x1="12.09675" y1="0.75565" x2="12.32535" y2="0.76835" layer="21"/>
<rectangle x1="0.18415" y1="0.76835" x2="0.38735" y2="0.78105" layer="21"/>
<rectangle x1="0.43815" y1="0.76835" x2="0.62865" y2="0.78105" layer="21"/>
<rectangle x1="0.79375" y1="0.76835" x2="1.00965" y2="0.78105" layer="21"/>
<rectangle x1="1.11125" y1="0.76835" x2="1.32715" y2="0.78105" layer="21"/>
<rectangle x1="1.44145" y1="0.76835" x2="1.82245" y2="0.78105" layer="21"/>
<rectangle x1="1.89865" y1="0.76835" x2="2.11455" y2="0.78105" layer="21"/>
<rectangle x1="2.21615" y1="0.76835" x2="2.43205" y2="0.78105" layer="21"/>
<rectangle x1="2.53365" y1="0.76835" x2="2.73685" y2="0.78105" layer="21"/>
<rectangle x1="2.83845" y1="0.76835" x2="3.02895" y2="0.78105" layer="21"/>
<rectangle x1="3.14325" y1="0.76835" x2="3.37185" y2="0.78105" layer="21"/>
<rectangle x1="3.47345" y1="0.76835" x2="3.70205" y2="0.78105" layer="21"/>
<rectangle x1="3.80365" y1="0.76835" x2="4.00685" y2="0.78105" layer="21"/>
<rectangle x1="4.10845" y1="0.76835" x2="4.33705" y2="0.78105" layer="21"/>
<rectangle x1="4.45135" y1="0.76835" x2="4.66725" y2="0.78105" layer="21"/>
<rectangle x1="4.76885" y1="0.76835" x2="4.97205" y2="0.78105" layer="21"/>
<rectangle x1="5.08635" y1="0.76835" x2="5.30225" y2="0.78105" layer="21"/>
<rectangle x1="5.39115" y1="0.76835" x2="5.60705" y2="0.78105" layer="21"/>
<rectangle x1="5.94995" y1="0.76835" x2="6.19125" y2="0.78105" layer="21"/>
<rectangle x1="6.31825" y1="0.76835" x2="6.54685" y2="0.78105" layer="21"/>
<rectangle x1="6.66115" y1="0.76835" x2="6.87705" y2="0.78105" layer="21"/>
<rectangle x1="6.97865" y1="0.76835" x2="7.19455" y2="0.78105" layer="21"/>
<rectangle x1="7.30885" y1="0.76835" x2="7.53745" y2="0.78105" layer="21"/>
<rectangle x1="7.63905" y1="0.76835" x2="7.86765" y2="0.78105" layer="21"/>
<rectangle x1="7.99465" y1="0.76835" x2="8.21055" y2="0.78105" layer="21"/>
<rectangle x1="8.36295" y1="0.76835" x2="8.74395" y2="0.78105" layer="21"/>
<rectangle x1="8.82015" y1="0.76835" x2="9.03605" y2="0.78105" layer="21"/>
<rectangle x1="9.13765" y1="0.76835" x2="9.35355" y2="0.78105" layer="21"/>
<rectangle x1="9.46785" y1="0.76835" x2="9.70915" y2="0.78105" layer="21"/>
<rectangle x1="9.79805" y1="0.76835" x2="10.00125" y2="0.78105" layer="21"/>
<rectangle x1="10.10285" y1="0.76835" x2="10.30605" y2="0.78105" layer="21"/>
<rectangle x1="10.64895" y1="0.76835" x2="10.89025" y2="0.78105" layer="21"/>
<rectangle x1="11.11885" y1="0.76835" x2="11.32205" y2="0.78105" layer="21"/>
<rectangle x1="11.43635" y1="0.76835" x2="11.65225" y2="0.78105" layer="21"/>
<rectangle x1="11.76655" y1="0.76835" x2="11.99515" y2="0.78105" layer="21"/>
<rectangle x1="12.09675" y1="0.76835" x2="12.32535" y2="0.78105" layer="21"/>
<rectangle x1="0.18415" y1="0.78105" x2="0.38735" y2="0.79375" layer="21"/>
<rectangle x1="0.43815" y1="0.78105" x2="0.62865" y2="0.79375" layer="21"/>
<rectangle x1="0.79375" y1="0.78105" x2="1.00965" y2="0.79375" layer="21"/>
<rectangle x1="1.11125" y1="0.78105" x2="1.31445" y2="0.79375" layer="21"/>
<rectangle x1="1.44145" y1="0.78105" x2="1.82245" y2="0.79375" layer="21"/>
<rectangle x1="1.91135" y1="0.78105" x2="2.11455" y2="0.79375" layer="21"/>
<rectangle x1="2.21615" y1="0.78105" x2="2.43205" y2="0.79375" layer="21"/>
<rectangle x1="2.53365" y1="0.78105" x2="2.73685" y2="0.79375" layer="21"/>
<rectangle x1="2.83845" y1="0.78105" x2="3.02895" y2="0.79375" layer="21"/>
<rectangle x1="3.14325" y1="0.78105" x2="3.37185" y2="0.79375" layer="21"/>
<rectangle x1="3.47345" y1="0.78105" x2="3.70205" y2="0.79375" layer="21"/>
<rectangle x1="3.80365" y1="0.78105" x2="4.00685" y2="0.79375" layer="21"/>
<rectangle x1="4.10845" y1="0.78105" x2="4.32435" y2="0.79375" layer="21"/>
<rectangle x1="4.45135" y1="0.78105" x2="4.66725" y2="0.79375" layer="21"/>
<rectangle x1="4.76885" y1="0.78105" x2="4.95935" y2="0.79375" layer="21"/>
<rectangle x1="5.08635" y1="0.78105" x2="5.30225" y2="0.79375" layer="21"/>
<rectangle x1="5.39115" y1="0.78105" x2="5.60705" y2="0.79375" layer="21"/>
<rectangle x1="5.94995" y1="0.78105" x2="6.19125" y2="0.79375" layer="21"/>
<rectangle x1="6.31825" y1="0.78105" x2="6.54685" y2="0.79375" layer="21"/>
<rectangle x1="6.66115" y1="0.78105" x2="6.87705" y2="0.79375" layer="21"/>
<rectangle x1="6.97865" y1="0.78105" x2="7.19455" y2="0.79375" layer="21"/>
<rectangle x1="7.30885" y1="0.78105" x2="7.53745" y2="0.79375" layer="21"/>
<rectangle x1="7.63905" y1="0.78105" x2="7.86765" y2="0.79375" layer="21"/>
<rectangle x1="7.99465" y1="0.78105" x2="8.21055" y2="0.79375" layer="21"/>
<rectangle x1="8.36295" y1="0.78105" x2="8.74395" y2="0.79375" layer="21"/>
<rectangle x1="8.83285" y1="0.78105" x2="9.03605" y2="0.79375" layer="21"/>
<rectangle x1="9.13765" y1="0.78105" x2="9.35355" y2="0.79375" layer="21"/>
<rectangle x1="9.46785" y1="0.78105" x2="9.70915" y2="0.79375" layer="21"/>
<rectangle x1="9.79805" y1="0.78105" x2="10.00125" y2="0.79375" layer="21"/>
<rectangle x1="10.10285" y1="0.78105" x2="10.30605" y2="0.79375" layer="21"/>
<rectangle x1="10.64895" y1="0.78105" x2="10.89025" y2="0.79375" layer="21"/>
<rectangle x1="11.13155" y1="0.78105" x2="11.32205" y2="0.79375" layer="21"/>
<rectangle x1="11.42365" y1="0.78105" x2="11.65225" y2="0.79375" layer="21"/>
<rectangle x1="11.76655" y1="0.78105" x2="11.99515" y2="0.79375" layer="21"/>
<rectangle x1="12.09675" y1="0.78105" x2="12.32535" y2="0.79375" layer="21"/>
<rectangle x1="0.18415" y1="0.79375" x2="0.38735" y2="0.80645" layer="21"/>
<rectangle x1="0.43815" y1="0.79375" x2="0.62865" y2="0.80645" layer="21"/>
<rectangle x1="0.80645" y1="0.79375" x2="1.00965" y2="0.80645" layer="21"/>
<rectangle x1="1.11125" y1="0.79375" x2="1.31445" y2="0.80645" layer="21"/>
<rectangle x1="1.44145" y1="0.79375" x2="1.82245" y2="0.80645" layer="21"/>
<rectangle x1="1.91135" y1="0.79375" x2="2.11455" y2="0.80645" layer="21"/>
<rectangle x1="2.21615" y1="0.79375" x2="2.43205" y2="0.80645" layer="21"/>
<rectangle x1="2.53365" y1="0.79375" x2="2.73685" y2="0.80645" layer="21"/>
<rectangle x1="2.83845" y1="0.79375" x2="3.02895" y2="0.80645" layer="21"/>
<rectangle x1="3.14325" y1="0.79375" x2="3.37185" y2="0.80645" layer="21"/>
<rectangle x1="3.47345" y1="0.79375" x2="3.70205" y2="0.80645" layer="21"/>
<rectangle x1="3.80365" y1="0.79375" x2="4.00685" y2="0.80645" layer="21"/>
<rectangle x1="4.10845" y1="0.79375" x2="4.32435" y2="0.80645" layer="21"/>
<rectangle x1="4.45135" y1="0.79375" x2="4.66725" y2="0.80645" layer="21"/>
<rectangle x1="4.76885" y1="0.79375" x2="4.95935" y2="0.80645" layer="21"/>
<rectangle x1="5.08635" y1="0.79375" x2="5.30225" y2="0.80645" layer="21"/>
<rectangle x1="5.39115" y1="0.79375" x2="5.60705" y2="0.80645" layer="21"/>
<rectangle x1="5.94995" y1="0.79375" x2="6.19125" y2="0.80645" layer="21"/>
<rectangle x1="6.31825" y1="0.79375" x2="6.54685" y2="0.80645" layer="21"/>
<rectangle x1="6.67385" y1="0.79375" x2="6.87705" y2="0.80645" layer="21"/>
<rectangle x1="6.97865" y1="0.79375" x2="7.19455" y2="0.80645" layer="21"/>
<rectangle x1="7.30885" y1="0.79375" x2="7.53745" y2="0.80645" layer="21"/>
<rectangle x1="7.63905" y1="0.79375" x2="7.86765" y2="0.80645" layer="21"/>
<rectangle x1="7.99465" y1="0.79375" x2="8.21055" y2="0.80645" layer="21"/>
<rectangle x1="8.36295" y1="0.79375" x2="8.74395" y2="0.80645" layer="21"/>
<rectangle x1="8.83285" y1="0.79375" x2="9.04875" y2="0.80645" layer="21"/>
<rectangle x1="9.13765" y1="0.79375" x2="9.35355" y2="0.80645" layer="21"/>
<rectangle x1="9.46785" y1="0.79375" x2="9.70915" y2="0.80645" layer="21"/>
<rectangle x1="9.81075" y1="0.79375" x2="10.00125" y2="0.80645" layer="21"/>
<rectangle x1="10.10285" y1="0.79375" x2="10.30605" y2="0.80645" layer="21"/>
<rectangle x1="10.64895" y1="0.79375" x2="10.89025" y2="0.80645" layer="21"/>
<rectangle x1="11.13155" y1="0.79375" x2="11.33475" y2="0.80645" layer="21"/>
<rectangle x1="11.42365" y1="0.79375" x2="11.65225" y2="0.80645" layer="21"/>
<rectangle x1="11.76655" y1="0.79375" x2="11.99515" y2="0.80645" layer="21"/>
<rectangle x1="12.09675" y1="0.79375" x2="12.32535" y2="0.80645" layer="21"/>
<rectangle x1="0.18415" y1="0.80645" x2="0.38735" y2="0.81915" layer="21"/>
<rectangle x1="0.43815" y1="0.80645" x2="0.62865" y2="0.81915" layer="21"/>
<rectangle x1="0.80645" y1="0.80645" x2="1.00965" y2="0.81915" layer="21"/>
<rectangle x1="1.09855" y1="0.80645" x2="1.31445" y2="0.81915" layer="21"/>
<rectangle x1="1.44145" y1="0.80645" x2="1.82245" y2="0.81915" layer="21"/>
<rectangle x1="1.91135" y1="0.80645" x2="2.12725" y2="0.81915" layer="21"/>
<rectangle x1="2.21615" y1="0.80645" x2="2.41935" y2="0.81915" layer="21"/>
<rectangle x1="2.53365" y1="0.80645" x2="2.73685" y2="0.81915" layer="21"/>
<rectangle x1="2.83845" y1="0.80645" x2="3.02895" y2="0.81915" layer="21"/>
<rectangle x1="3.14325" y1="0.80645" x2="3.38455" y2="0.81915" layer="21"/>
<rectangle x1="3.47345" y1="0.80645" x2="3.70205" y2="0.81915" layer="21"/>
<rectangle x1="3.81635" y1="0.80645" x2="4.00685" y2="0.81915" layer="21"/>
<rectangle x1="4.10845" y1="0.80645" x2="4.32435" y2="0.81915" layer="21"/>
<rectangle x1="4.46405" y1="0.80645" x2="4.67995" y2="0.81915" layer="21"/>
<rectangle x1="4.75615" y1="0.80645" x2="4.95935" y2="0.81915" layer="21"/>
<rectangle x1="5.09905" y1="0.80645" x2="5.30225" y2="0.81915" layer="21"/>
<rectangle x1="5.39115" y1="0.80645" x2="5.59435" y2="0.81915" layer="21"/>
<rectangle x1="5.94995" y1="0.80645" x2="6.19125" y2="0.81915" layer="21"/>
<rectangle x1="6.31825" y1="0.80645" x2="6.54685" y2="0.81915" layer="21"/>
<rectangle x1="6.67385" y1="0.80645" x2="6.88975" y2="0.81915" layer="21"/>
<rectangle x1="6.96595" y1="0.80645" x2="7.18185" y2="0.81915" layer="21"/>
<rectangle x1="7.30885" y1="0.80645" x2="7.55015" y2="0.81915" layer="21"/>
<rectangle x1="7.63905" y1="0.80645" x2="7.85495" y2="0.81915" layer="21"/>
<rectangle x1="7.99465" y1="0.80645" x2="8.21055" y2="0.81915" layer="21"/>
<rectangle x1="8.36295" y1="0.80645" x2="8.74395" y2="0.81915" layer="21"/>
<rectangle x1="8.83285" y1="0.80645" x2="9.04875" y2="0.81915" layer="21"/>
<rectangle x1="9.13765" y1="0.80645" x2="9.34085" y2="0.81915" layer="21"/>
<rectangle x1="9.46785" y1="0.80645" x2="9.70915" y2="0.81915" layer="21"/>
<rectangle x1="9.81075" y1="0.80645" x2="10.01395" y2="0.81915" layer="21"/>
<rectangle x1="10.10285" y1="0.80645" x2="10.30605" y2="0.81915" layer="21"/>
<rectangle x1="10.64895" y1="0.80645" x2="10.89025" y2="0.81915" layer="21"/>
<rectangle x1="11.13155" y1="0.80645" x2="11.33475" y2="0.81915" layer="21"/>
<rectangle x1="11.42365" y1="0.80645" x2="11.63955" y2="0.81915" layer="21"/>
<rectangle x1="11.76655" y1="0.80645" x2="11.99515" y2="0.81915" layer="21"/>
<rectangle x1="12.08405" y1="0.80645" x2="12.32535" y2="0.81915" layer="21"/>
<rectangle x1="0.18415" y1="0.81915" x2="0.38735" y2="0.83185" layer="21"/>
<rectangle x1="0.43815" y1="0.81915" x2="0.62865" y2="0.83185" layer="21"/>
<rectangle x1="0.80645" y1="0.81915" x2="1.02235" y2="0.83185" layer="21"/>
<rectangle x1="1.09855" y1="0.81915" x2="1.30175" y2="0.83185" layer="21"/>
<rectangle x1="1.44145" y1="0.81915" x2="1.82245" y2="0.83185" layer="21"/>
<rectangle x1="1.91135" y1="0.81915" x2="2.12725" y2="0.83185" layer="21"/>
<rectangle x1="2.20345" y1="0.81915" x2="2.41935" y2="0.83185" layer="21"/>
<rectangle x1="2.53365" y1="0.81915" x2="2.74955" y2="0.83185" layer="21"/>
<rectangle x1="2.82575" y1="0.81915" x2="3.02895" y2="0.83185" layer="21"/>
<rectangle x1="3.14325" y1="0.81915" x2="3.38455" y2="0.83185" layer="21"/>
<rectangle x1="3.46075" y1="0.81915" x2="3.70205" y2="0.83185" layer="21"/>
<rectangle x1="3.81635" y1="0.81915" x2="4.01955" y2="0.83185" layer="21"/>
<rectangle x1="4.09575" y1="0.81915" x2="4.32435" y2="0.83185" layer="21"/>
<rectangle x1="4.46405" y1="0.81915" x2="4.67995" y2="0.83185" layer="21"/>
<rectangle x1="4.75615" y1="0.81915" x2="4.95935" y2="0.83185" layer="21"/>
<rectangle x1="5.09905" y1="0.81915" x2="5.31495" y2="0.83185" layer="21"/>
<rectangle x1="5.37845" y1="0.81915" x2="5.59435" y2="0.83185" layer="21"/>
<rectangle x1="5.94995" y1="0.81915" x2="6.19125" y2="0.83185" layer="21"/>
<rectangle x1="6.31825" y1="0.81915" x2="6.54685" y2="0.83185" layer="21"/>
<rectangle x1="6.67385" y1="0.81915" x2="6.88975" y2="0.83185" layer="21"/>
<rectangle x1="6.96595" y1="0.81915" x2="7.18185" y2="0.83185" layer="21"/>
<rectangle x1="7.30885" y1="0.81915" x2="7.55015" y2="0.83185" layer="21"/>
<rectangle x1="7.62635" y1="0.81915" x2="7.85495" y2="0.83185" layer="21"/>
<rectangle x1="7.93115" y1="0.81915" x2="8.28675" y2="0.83185" layer="21"/>
<rectangle x1="8.36295" y1="0.81915" x2="8.74395" y2="0.83185" layer="21"/>
<rectangle x1="8.83285" y1="0.81915" x2="9.04875" y2="0.83185" layer="21"/>
<rectangle x1="9.12495" y1="0.81915" x2="9.34085" y2="0.83185" layer="21"/>
<rectangle x1="9.46785" y1="0.81915" x2="9.70915" y2="0.83185" layer="21"/>
<rectangle x1="9.81075" y1="0.81915" x2="10.01395" y2="0.83185" layer="21"/>
<rectangle x1="10.10285" y1="0.81915" x2="10.30605" y2="0.83185" layer="21"/>
<rectangle x1="10.64895" y1="0.81915" x2="10.89025" y2="0.83185" layer="21"/>
<rectangle x1="11.13155" y1="0.81915" x2="11.33475" y2="0.83185" layer="21"/>
<rectangle x1="11.41095" y1="0.81915" x2="11.63955" y2="0.83185" layer="21"/>
<rectangle x1="11.76655" y1="0.81915" x2="12.00785" y2="0.83185" layer="21"/>
<rectangle x1="12.08405" y1="0.81915" x2="12.32535" y2="0.83185" layer="21"/>
<rectangle x1="0.18415" y1="0.83185" x2="0.38735" y2="0.84455" layer="21"/>
<rectangle x1="0.43815" y1="0.83185" x2="0.62865" y2="0.84455" layer="21"/>
<rectangle x1="0.81915" y1="0.83185" x2="1.03505" y2="0.84455" layer="21"/>
<rectangle x1="1.07315" y1="0.83185" x2="1.30175" y2="0.84455" layer="21"/>
<rectangle x1="1.44145" y1="0.83185" x2="1.82245" y2="0.84455" layer="21"/>
<rectangle x1="1.92405" y1="0.83185" x2="2.15265" y2="0.84455" layer="21"/>
<rectangle x1="2.19075" y1="0.83185" x2="2.40665" y2="0.84455" layer="21"/>
<rectangle x1="2.54635" y1="0.83185" x2="2.76225" y2="0.84455" layer="21"/>
<rectangle x1="2.81305" y1="0.83185" x2="3.02895" y2="0.84455" layer="21"/>
<rectangle x1="3.14325" y1="0.83185" x2="3.40995" y2="0.84455" layer="21"/>
<rectangle x1="3.44805" y1="0.83185" x2="3.68935" y2="0.84455" layer="21"/>
<rectangle x1="3.81635" y1="0.83185" x2="4.03225" y2="0.84455" layer="21"/>
<rectangle x1="4.07035" y1="0.83185" x2="4.31165" y2="0.84455" layer="21"/>
<rectangle x1="4.46405" y1="0.83185" x2="4.70535" y2="0.84455" layer="21"/>
<rectangle x1="4.73075" y1="0.83185" x2="4.94665" y2="0.84455" layer="21"/>
<rectangle x1="5.09905" y1="0.83185" x2="5.32765" y2="0.84455" layer="21"/>
<rectangle x1="5.36575" y1="0.83185" x2="5.58165" y2="0.84455" layer="21"/>
<rectangle x1="5.96265" y1="0.83185" x2="6.19125" y2="0.84455" layer="21"/>
<rectangle x1="6.31825" y1="0.83185" x2="6.54685" y2="0.84455" layer="21"/>
<rectangle x1="6.68655" y1="0.83185" x2="6.91515" y2="0.84455" layer="21"/>
<rectangle x1="6.95325" y1="0.83185" x2="7.16915" y2="0.84455" layer="21"/>
<rectangle x1="7.30885" y1="0.83185" x2="7.57555" y2="0.84455" layer="21"/>
<rectangle x1="7.61365" y1="0.83185" x2="7.85495" y2="0.84455" layer="21"/>
<rectangle x1="7.93115" y1="0.83185" x2="8.28675" y2="0.84455" layer="21"/>
<rectangle x1="8.36295" y1="0.83185" x2="8.74395" y2="0.84455" layer="21"/>
<rectangle x1="8.84555" y1="0.83185" x2="9.07415" y2="0.84455" layer="21"/>
<rectangle x1="9.11225" y1="0.83185" x2="9.32815" y2="0.84455" layer="21"/>
<rectangle x1="9.46785" y1="0.83185" x2="9.70915" y2="0.84455" layer="21"/>
<rectangle x1="9.81075" y1="0.83185" x2="10.03935" y2="0.84455" layer="21"/>
<rectangle x1="10.07745" y1="0.83185" x2="10.29335" y2="0.84455" layer="21"/>
<rectangle x1="10.64895" y1="0.83185" x2="10.89025" y2="0.84455" layer="21"/>
<rectangle x1="11.14425" y1="0.83185" x2="11.36015" y2="0.84455" layer="21"/>
<rectangle x1="11.39825" y1="0.83185" x2="11.63955" y2="0.84455" layer="21"/>
<rectangle x1="11.76655" y1="0.83185" x2="12.02055" y2="0.84455" layer="21"/>
<rectangle x1="12.05865" y1="0.83185" x2="12.32535" y2="0.84455" layer="21"/>
<rectangle x1="0.18415" y1="0.84455" x2="0.38735" y2="0.85725" layer="21"/>
<rectangle x1="0.43815" y1="0.84455" x2="0.61595" y2="0.85725" layer="21"/>
<rectangle x1="0.81915" y1="0.84455" x2="1.28905" y2="0.85725" layer="21"/>
<rectangle x1="1.44145" y1="0.84455" x2="1.65735" y2="0.85725" layer="21"/>
<rectangle x1="1.68275" y1="0.84455" x2="1.82245" y2="0.85725" layer="21"/>
<rectangle x1="1.92405" y1="0.84455" x2="2.40665" y2="0.85725" layer="21"/>
<rectangle x1="2.54635" y1="0.84455" x2="3.01625" y2="0.85725" layer="21"/>
<rectangle x1="3.14325" y1="0.84455" x2="3.68935" y2="0.85725" layer="21"/>
<rectangle x1="3.82905" y1="0.84455" x2="4.31165" y2="0.85725" layer="21"/>
<rectangle x1="4.47675" y1="0.84455" x2="4.94665" y2="0.85725" layer="21"/>
<rectangle x1="5.11175" y1="0.84455" x2="5.58165" y2="0.85725" layer="21"/>
<rectangle x1="5.96265" y1="0.84455" x2="6.19125" y2="0.85725" layer="21"/>
<rectangle x1="6.31825" y1="0.84455" x2="6.54685" y2="0.85725" layer="21"/>
<rectangle x1="6.68655" y1="0.84455" x2="7.16915" y2="0.85725" layer="21"/>
<rectangle x1="7.30885" y1="0.84455" x2="7.85495" y2="0.85725" layer="21"/>
<rectangle x1="7.93115" y1="0.84455" x2="8.28675" y2="0.85725" layer="21"/>
<rectangle x1="8.36295" y1="0.84455" x2="8.57885" y2="0.85725" layer="21"/>
<rectangle x1="8.60425" y1="0.84455" x2="8.74395" y2="0.85725" layer="21"/>
<rectangle x1="8.84555" y1="0.84455" x2="9.32815" y2="0.85725" layer="21"/>
<rectangle x1="9.46785" y1="0.84455" x2="9.70915" y2="0.85725" layer="21"/>
<rectangle x1="9.82345" y1="0.84455" x2="10.29335" y2="0.85725" layer="21"/>
<rectangle x1="10.64895" y1="0.84455" x2="10.89025" y2="0.85725" layer="21"/>
<rectangle x1="11.14425" y1="0.84455" x2="11.62685" y2="0.85725" layer="21"/>
<rectangle x1="11.76655" y1="0.84455" x2="12.31265" y2="0.85725" layer="21"/>
<rectangle x1="0.19685" y1="0.85725" x2="0.38735" y2="0.86995" layer="21"/>
<rectangle x1="0.42545" y1="0.85725" x2="0.61595" y2="0.86995" layer="21"/>
<rectangle x1="0.83185" y1="0.85725" x2="1.28905" y2="0.86995" layer="21"/>
<rectangle x1="1.44145" y1="0.85725" x2="1.65735" y2="0.86995" layer="21"/>
<rectangle x1="1.69545" y1="0.85725" x2="1.82245" y2="0.86995" layer="21"/>
<rectangle x1="1.93675" y1="0.85725" x2="2.39395" y2="0.86995" layer="21"/>
<rectangle x1="2.54635" y1="0.85725" x2="3.01625" y2="0.86995" layer="21"/>
<rectangle x1="3.14325" y1="0.85725" x2="3.37185" y2="0.86995" layer="21"/>
<rectangle x1="3.38455" y1="0.85725" x2="3.68935" y2="0.86995" layer="21"/>
<rectangle x1="3.84175" y1="0.85725" x2="4.31165" y2="0.86995" layer="21"/>
<rectangle x1="4.48945" y1="0.85725" x2="4.93395" y2="0.86995" layer="21"/>
<rectangle x1="5.11175" y1="0.85725" x2="5.56895" y2="0.86995" layer="21"/>
<rectangle x1="5.96265" y1="0.85725" x2="6.19125" y2="0.86995" layer="21"/>
<rectangle x1="6.31825" y1="0.85725" x2="6.54685" y2="0.86995" layer="21"/>
<rectangle x1="6.69925" y1="0.85725" x2="7.15645" y2="0.86995" layer="21"/>
<rectangle x1="7.30885" y1="0.85725" x2="7.85495" y2="0.86995" layer="21"/>
<rectangle x1="7.93115" y1="0.85725" x2="8.28675" y2="0.86995" layer="21"/>
<rectangle x1="8.36295" y1="0.85725" x2="8.57885" y2="0.86995" layer="21"/>
<rectangle x1="8.61695" y1="0.85725" x2="8.74395" y2="0.86995" layer="21"/>
<rectangle x1="8.85825" y1="0.85725" x2="9.31545" y2="0.86995" layer="21"/>
<rectangle x1="9.46785" y1="0.85725" x2="9.70915" y2="0.86995" layer="21"/>
<rectangle x1="9.82345" y1="0.85725" x2="10.28065" y2="0.86995" layer="21"/>
<rectangle x1="10.64895" y1="0.85725" x2="10.89025" y2="0.86995" layer="21"/>
<rectangle x1="11.15695" y1="0.85725" x2="11.62685" y2="0.86995" layer="21"/>
<rectangle x1="11.76655" y1="0.85725" x2="12.31265" y2="0.86995" layer="21"/>
<rectangle x1="0.19685" y1="0.86995" x2="0.40005" y2="0.88265" layer="21"/>
<rectangle x1="0.42545" y1="0.86995" x2="0.61595" y2="0.88265" layer="21"/>
<rectangle x1="0.83185" y1="0.86995" x2="1.27635" y2="0.88265" layer="21"/>
<rectangle x1="1.44145" y1="0.86995" x2="1.65735" y2="0.88265" layer="21"/>
<rectangle x1="1.69545" y1="0.86995" x2="1.82245" y2="0.88265" layer="21"/>
<rectangle x1="1.94945" y1="0.86995" x2="2.38125" y2="0.88265" layer="21"/>
<rectangle x1="2.55905" y1="0.86995" x2="3.00355" y2="0.88265" layer="21"/>
<rectangle x1="3.14325" y1="0.86995" x2="3.37185" y2="0.88265" layer="21"/>
<rectangle x1="3.39725" y1="0.86995" x2="3.68935" y2="0.88265" layer="21"/>
<rectangle x1="3.84175" y1="0.86995" x2="4.29895" y2="0.88265" layer="21"/>
<rectangle x1="4.48945" y1="0.86995" x2="4.92125" y2="0.88265" layer="21"/>
<rectangle x1="5.12445" y1="0.86995" x2="5.55625" y2="0.88265" layer="21"/>
<rectangle x1="5.96265" y1="0.86995" x2="6.19125" y2="0.88265" layer="21"/>
<rectangle x1="6.31825" y1="0.86995" x2="6.54685" y2="0.88265" layer="21"/>
<rectangle x1="6.71195" y1="0.86995" x2="7.14375" y2="0.88265" layer="21"/>
<rectangle x1="7.30885" y1="0.86995" x2="7.55015" y2="0.88265" layer="21"/>
<rectangle x1="7.56285" y1="0.86995" x2="7.84225" y2="0.88265" layer="21"/>
<rectangle x1="7.93115" y1="0.86995" x2="8.28675" y2="0.88265" layer="21"/>
<rectangle x1="8.36295" y1="0.86995" x2="8.57885" y2="0.88265" layer="21"/>
<rectangle x1="8.62965" y1="0.86995" x2="8.74395" y2="0.88265" layer="21"/>
<rectangle x1="8.87095" y1="0.86995" x2="9.30275" y2="0.88265" layer="21"/>
<rectangle x1="9.46785" y1="0.86995" x2="9.70915" y2="0.88265" layer="21"/>
<rectangle x1="9.83615" y1="0.86995" x2="10.28065" y2="0.88265" layer="21"/>
<rectangle x1="10.64895" y1="0.86995" x2="10.89025" y2="0.88265" layer="21"/>
<rectangle x1="11.16965" y1="0.86995" x2="11.61415" y2="0.88265" layer="21"/>
<rectangle x1="11.76655" y1="0.86995" x2="11.99515" y2="0.88265" layer="21"/>
<rectangle x1="12.00785" y1="0.86995" x2="12.31265" y2="0.88265" layer="21"/>
<rectangle x1="0.19685" y1="0.88265" x2="0.40005" y2="0.89535" layer="21"/>
<rectangle x1="0.42545" y1="0.88265" x2="0.61595" y2="0.89535" layer="21"/>
<rectangle x1="0.84455" y1="0.88265" x2="1.26365" y2="0.89535" layer="21"/>
<rectangle x1="1.44145" y1="0.88265" x2="1.65735" y2="0.89535" layer="21"/>
<rectangle x1="1.70815" y1="0.88265" x2="1.82245" y2="0.89535" layer="21"/>
<rectangle x1="1.96215" y1="0.88265" x2="2.36855" y2="0.89535" layer="21"/>
<rectangle x1="2.57175" y1="0.88265" x2="2.99085" y2="0.89535" layer="21"/>
<rectangle x1="3.14325" y1="0.88265" x2="3.37185" y2="0.89535" layer="21"/>
<rectangle x1="3.40995" y1="0.88265" x2="3.67665" y2="0.89535" layer="21"/>
<rectangle x1="3.85445" y1="0.88265" x2="4.28625" y2="0.89535" layer="21"/>
<rectangle x1="4.50215" y1="0.88265" x2="4.90855" y2="0.89535" layer="21"/>
<rectangle x1="5.13715" y1="0.88265" x2="5.54355" y2="0.89535" layer="21"/>
<rectangle x1="5.96265" y1="0.88265" x2="6.19125" y2="0.89535" layer="21"/>
<rectangle x1="6.31825" y1="0.88265" x2="6.54685" y2="0.89535" layer="21"/>
<rectangle x1="6.72465" y1="0.88265" x2="7.13105" y2="0.89535" layer="21"/>
<rectangle x1="7.30885" y1="0.88265" x2="7.53745" y2="0.89535" layer="21"/>
<rectangle x1="7.57555" y1="0.88265" x2="7.84225" y2="0.89535" layer="21"/>
<rectangle x1="7.93115" y1="0.88265" x2="8.28675" y2="0.89535" layer="21"/>
<rectangle x1="8.36295" y1="0.88265" x2="8.57885" y2="0.89535" layer="21"/>
<rectangle x1="8.62965" y1="0.88265" x2="8.74395" y2="0.89535" layer="21"/>
<rectangle x1="8.88365" y1="0.88265" x2="9.29005" y2="0.89535" layer="21"/>
<rectangle x1="9.46785" y1="0.88265" x2="9.70915" y2="0.89535" layer="21"/>
<rectangle x1="9.83615" y1="0.88265" x2="10.26795" y2="0.89535" layer="21"/>
<rectangle x1="10.64895" y1="0.88265" x2="10.89025" y2="0.89535" layer="21"/>
<rectangle x1="11.18235" y1="0.88265" x2="11.60145" y2="0.89535" layer="21"/>
<rectangle x1="11.76655" y1="0.88265" x2="11.99515" y2="0.89535" layer="21"/>
<rectangle x1="12.02055" y1="0.88265" x2="12.29995" y2="0.89535" layer="21"/>
<rectangle x1="0.19685" y1="0.89535" x2="0.40005" y2="0.90805" layer="21"/>
<rectangle x1="0.42545" y1="0.89535" x2="0.61595" y2="0.90805" layer="21"/>
<rectangle x1="0.85725" y1="0.89535" x2="1.25095" y2="0.90805" layer="21"/>
<rectangle x1="1.44145" y1="0.89535" x2="1.65735" y2="0.90805" layer="21"/>
<rectangle x1="1.72085" y1="0.89535" x2="1.82245" y2="0.90805" layer="21"/>
<rectangle x1="1.97485" y1="0.89535" x2="2.35585" y2="0.90805" layer="21"/>
<rectangle x1="2.58445" y1="0.89535" x2="2.97815" y2="0.90805" layer="21"/>
<rectangle x1="3.14325" y1="0.89535" x2="3.37185" y2="0.90805" layer="21"/>
<rectangle x1="3.42265" y1="0.89535" x2="3.67665" y2="0.90805" layer="21"/>
<rectangle x1="3.87985" y1="0.89535" x2="4.27355" y2="0.90805" layer="21"/>
<rectangle x1="4.52755" y1="0.89535" x2="4.89585" y2="0.90805" layer="21"/>
<rectangle x1="5.14985" y1="0.89535" x2="5.53085" y2="0.90805" layer="21"/>
<rectangle x1="5.96265" y1="0.89535" x2="6.19125" y2="0.90805" layer="21"/>
<rectangle x1="6.30555" y1="0.89535" x2="6.54685" y2="0.90805" layer="21"/>
<rectangle x1="6.73735" y1="0.89535" x2="7.11835" y2="0.90805" layer="21"/>
<rectangle x1="7.30885" y1="0.89535" x2="7.53745" y2="0.90805" layer="21"/>
<rectangle x1="7.57555" y1="0.89535" x2="7.82955" y2="0.90805" layer="21"/>
<rectangle x1="7.93115" y1="0.89535" x2="8.28675" y2="0.90805" layer="21"/>
<rectangle x1="8.36295" y1="0.89535" x2="8.57885" y2="0.90805" layer="21"/>
<rectangle x1="8.64235" y1="0.89535" x2="8.74395" y2="0.90805" layer="21"/>
<rectangle x1="8.89635" y1="0.89535" x2="9.27735" y2="0.90805" layer="21"/>
<rectangle x1="9.46785" y1="0.89535" x2="9.70915" y2="0.90805" layer="21"/>
<rectangle x1="9.84885" y1="0.89535" x2="10.25525" y2="0.90805" layer="21"/>
<rectangle x1="10.64895" y1="0.89535" x2="10.89025" y2="0.90805" layer="21"/>
<rectangle x1="11.19505" y1="0.89535" x2="11.58875" y2="0.90805" layer="21"/>
<rectangle x1="11.76655" y1="0.89535" x2="11.99515" y2="0.90805" layer="21"/>
<rectangle x1="12.03325" y1="0.89535" x2="12.29995" y2="0.90805" layer="21"/>
<rectangle x1="0.19685" y1="0.90805" x2="0.40005" y2="0.92075" layer="21"/>
<rectangle x1="0.42545" y1="0.90805" x2="0.61595" y2="0.92075" layer="21"/>
<rectangle x1="0.88265" y1="0.90805" x2="1.22555" y2="0.92075" layer="21"/>
<rectangle x1="1.44145" y1="0.90805" x2="1.65735" y2="0.92075" layer="21"/>
<rectangle x1="1.73355" y1="0.90805" x2="1.82245" y2="0.92075" layer="21"/>
<rectangle x1="1.98755" y1="0.90805" x2="2.34315" y2="0.92075" layer="21"/>
<rectangle x1="2.59715" y1="0.90805" x2="2.96545" y2="0.92075" layer="21"/>
<rectangle x1="3.14325" y1="0.90805" x2="3.37185" y2="0.92075" layer="21"/>
<rectangle x1="3.43535" y1="0.90805" x2="3.66395" y2="0.92075" layer="21"/>
<rectangle x1="3.89255" y1="0.90805" x2="4.26085" y2="0.92075" layer="21"/>
<rectangle x1="4.54025" y1="0.90805" x2="4.88315" y2="0.92075" layer="21"/>
<rectangle x1="5.16255" y1="0.90805" x2="5.51815" y2="0.92075" layer="21"/>
<rectangle x1="5.96265" y1="0.90805" x2="6.19125" y2="0.92075" layer="21"/>
<rectangle x1="6.30555" y1="0.90805" x2="6.54685" y2="0.92075" layer="21"/>
<rectangle x1="6.75005" y1="0.90805" x2="7.10565" y2="0.92075" layer="21"/>
<rectangle x1="7.30885" y1="0.90805" x2="7.53745" y2="0.92075" layer="21"/>
<rectangle x1="7.58825" y1="0.90805" x2="7.82955" y2="0.92075" layer="21"/>
<rectangle x1="7.93115" y1="0.90805" x2="8.28675" y2="0.92075" layer="21"/>
<rectangle x1="8.36295" y1="0.90805" x2="8.57885" y2="0.92075" layer="21"/>
<rectangle x1="8.65505" y1="0.90805" x2="8.74395" y2="0.92075" layer="21"/>
<rectangle x1="8.90905" y1="0.90805" x2="9.26465" y2="0.92075" layer="21"/>
<rectangle x1="9.46785" y1="0.90805" x2="9.70915" y2="0.92075" layer="21"/>
<rectangle x1="9.87425" y1="0.90805" x2="10.22985" y2="0.92075" layer="21"/>
<rectangle x1="10.64895" y1="0.90805" x2="10.89025" y2="0.92075" layer="21"/>
<rectangle x1="11.20775" y1="0.90805" x2="11.57605" y2="0.92075" layer="21"/>
<rectangle x1="11.76655" y1="0.90805" x2="11.99515" y2="0.92075" layer="21"/>
<rectangle x1="12.04595" y1="0.90805" x2="12.28725" y2="0.92075" layer="21"/>
<rectangle x1="0.19685" y1="0.92075" x2="0.61595" y2="0.93345" layer="21"/>
<rectangle x1="0.89535" y1="0.92075" x2="1.20015" y2="0.93345" layer="21"/>
<rectangle x1="1.44145" y1="0.92075" x2="1.67005" y2="0.93345" layer="21"/>
<rectangle x1="1.74625" y1="0.92075" x2="1.82245" y2="0.93345" layer="21"/>
<rectangle x1="2.01295" y1="0.92075" x2="2.31775" y2="0.93345" layer="21"/>
<rectangle x1="2.62255" y1="0.92075" x2="2.94005" y2="0.93345" layer="21"/>
<rectangle x1="3.14325" y1="0.92075" x2="3.37185" y2="0.93345" layer="21"/>
<rectangle x1="3.44805" y1="0.92075" x2="3.65125" y2="0.93345" layer="21"/>
<rectangle x1="3.91795" y1="0.92075" x2="4.23545" y2="0.93345" layer="21"/>
<rectangle x1="4.56565" y1="0.92075" x2="4.85775" y2="0.93345" layer="21"/>
<rectangle x1="5.18795" y1="0.92075" x2="5.49275" y2="0.93345" layer="21"/>
<rectangle x1="5.96265" y1="0.92075" x2="6.19125" y2="0.93345" layer="21"/>
<rectangle x1="6.30555" y1="0.92075" x2="6.54685" y2="0.93345" layer="21"/>
<rectangle x1="6.77545" y1="0.92075" x2="7.08025" y2="0.93345" layer="21"/>
<rectangle x1="7.30885" y1="0.92075" x2="7.53745" y2="0.93345" layer="21"/>
<rectangle x1="7.60095" y1="0.92075" x2="7.81685" y2="0.93345" layer="21"/>
<rectangle x1="7.98195" y1="0.92075" x2="8.22325" y2="0.93345" layer="21"/>
<rectangle x1="8.36295" y1="0.92075" x2="8.59155" y2="0.93345" layer="21"/>
<rectangle x1="8.66775" y1="0.92075" x2="8.74395" y2="0.93345" layer="21"/>
<rectangle x1="8.93445" y1="0.92075" x2="9.23925" y2="0.93345" layer="21"/>
<rectangle x1="9.46785" y1="0.92075" x2="9.70915" y2="0.93345" layer="21"/>
<rectangle x1="9.88695" y1="0.92075" x2="10.21715" y2="0.93345" layer="21"/>
<rectangle x1="10.64895" y1="0.92075" x2="10.89025" y2="0.93345" layer="21"/>
<rectangle x1="11.23315" y1="0.92075" x2="11.55065" y2="0.93345" layer="21"/>
<rectangle x1="11.76655" y1="0.92075" x2="11.99515" y2="0.93345" layer="21"/>
<rectangle x1="12.05865" y1="0.92075" x2="12.27455" y2="0.93345" layer="21"/>
<rectangle x1="0.19685" y1="0.93345" x2="0.60325" y2="0.94615" layer="21"/>
<rectangle x1="0.93345" y1="0.93345" x2="1.17475" y2="0.94615" layer="21"/>
<rectangle x1="1.44145" y1="0.93345" x2="1.67005" y2="0.94615" layer="21"/>
<rectangle x1="1.75895" y1="0.93345" x2="1.82245" y2="0.94615" layer="21"/>
<rectangle x1="2.03835" y1="0.93345" x2="2.27965" y2="0.94615" layer="21"/>
<rectangle x1="2.64795" y1="0.93345" x2="2.91465" y2="0.94615" layer="21"/>
<rectangle x1="3.14325" y1="0.93345" x2="3.37185" y2="0.94615" layer="21"/>
<rectangle x1="3.47345" y1="0.93345" x2="3.62585" y2="0.94615" layer="21"/>
<rectangle x1="3.94335" y1="0.93345" x2="4.21005" y2="0.94615" layer="21"/>
<rectangle x1="4.59105" y1="0.93345" x2="4.83235" y2="0.94615" layer="21"/>
<rectangle x1="5.22605" y1="0.93345" x2="5.46735" y2="0.94615" layer="21"/>
<rectangle x1="5.96265" y1="0.93345" x2="6.19125" y2="0.94615" layer="21"/>
<rectangle x1="6.30555" y1="0.93345" x2="6.53415" y2="0.94615" layer="21"/>
<rectangle x1="6.80085" y1="0.93345" x2="7.04215" y2="0.94615" layer="21"/>
<rectangle x1="7.30885" y1="0.93345" x2="7.53745" y2="0.94615" layer="21"/>
<rectangle x1="7.62635" y1="0.93345" x2="7.79145" y2="0.94615" layer="21"/>
<rectangle x1="7.99465" y1="0.93345" x2="8.21055" y2="0.94615" layer="21"/>
<rectangle x1="8.36295" y1="0.93345" x2="8.59155" y2="0.94615" layer="21"/>
<rectangle x1="8.68045" y1="0.93345" x2="8.74395" y2="0.94615" layer="21"/>
<rectangle x1="8.95985" y1="0.93345" x2="9.20115" y2="0.94615" layer="21"/>
<rectangle x1="9.46785" y1="0.93345" x2="9.70915" y2="0.94615" layer="21"/>
<rectangle x1="9.92505" y1="0.93345" x2="10.17905" y2="0.94615" layer="21"/>
<rectangle x1="10.64895" y1="0.93345" x2="10.89025" y2="0.94615" layer="21"/>
<rectangle x1="11.25855" y1="0.93345" x2="11.52525" y2="0.94615" layer="21"/>
<rectangle x1="11.76655" y1="0.93345" x2="11.99515" y2="0.94615" layer="21"/>
<rectangle x1="12.07135" y1="0.93345" x2="12.24915" y2="0.94615" layer="21"/>
<rectangle x1="0.19685" y1="0.94615" x2="0.60325" y2="0.95885" layer="21"/>
<rectangle x1="0.97155" y1="0.94615" x2="1.12395" y2="0.95885" layer="21"/>
<rectangle x1="1.79705" y1="0.94615" x2="1.82245" y2="0.95885" layer="21"/>
<rectangle x1="2.07645" y1="0.94615" x2="2.24155" y2="0.95885" layer="21"/>
<rectangle x1="2.69875" y1="0.94615" x2="2.86385" y2="0.95885" layer="21"/>
<rectangle x1="3.49885" y1="0.94615" x2="3.60045" y2="0.95885" layer="21"/>
<rectangle x1="3.99415" y1="0.94615" x2="4.15925" y2="0.95885" layer="21"/>
<rectangle x1="4.62915" y1="0.94615" x2="4.78155" y2="0.95885" layer="21"/>
<rectangle x1="5.26415" y1="0.94615" x2="5.41655" y2="0.95885" layer="21"/>
<rectangle x1="5.96265" y1="0.94615" x2="6.19125" y2="0.95885" layer="21"/>
<rectangle x1="6.30555" y1="0.94615" x2="6.53415" y2="0.95885" layer="21"/>
<rectangle x1="6.83895" y1="0.94615" x2="7.00405" y2="0.95885" layer="21"/>
<rectangle x1="7.65175" y1="0.94615" x2="7.76605" y2="0.95885" layer="21"/>
<rectangle x1="7.99465" y1="0.94615" x2="8.21055" y2="0.95885" layer="21"/>
<rectangle x1="8.71855" y1="0.94615" x2="8.74395" y2="0.95885" layer="21"/>
<rectangle x1="8.99795" y1="0.94615" x2="9.16305" y2="0.95885" layer="21"/>
<rectangle x1="9.46785" y1="0.94615" x2="9.70915" y2="0.95885" layer="21"/>
<rectangle x1="9.96315" y1="0.94615" x2="10.12825" y2="0.95885" layer="21"/>
<rectangle x1="10.64895" y1="0.94615" x2="10.89025" y2="0.95885" layer="21"/>
<rectangle x1="11.30935" y1="0.94615" x2="11.47445" y2="0.95885" layer="21"/>
<rectangle x1="11.76655" y1="0.94615" x2="11.99515" y2="0.95885" layer="21"/>
<rectangle x1="12.10945" y1="0.94615" x2="12.22375" y2="0.95885" layer="21"/>
<rectangle x1="0.20955" y1="0.95885" x2="0.60325" y2="0.97155" layer="21"/>
<rectangle x1="5.96265" y1="0.95885" x2="6.20395" y2="0.97155" layer="21"/>
<rectangle x1="6.30555" y1="0.95885" x2="6.53415" y2="0.97155" layer="21"/>
<rectangle x1="7.99465" y1="0.95885" x2="8.21055" y2="0.97155" layer="21"/>
<rectangle x1="9.46785" y1="0.95885" x2="9.70915" y2="0.97155" layer="21"/>
<rectangle x1="10.64895" y1="0.95885" x2="10.89025" y2="0.97155" layer="21"/>
<rectangle x1="11.76655" y1="0.95885" x2="11.99515" y2="0.97155" layer="21"/>
<rectangle x1="0.20955" y1="0.97155" x2="0.60325" y2="0.98425" layer="21"/>
<rectangle x1="5.97535" y1="0.97155" x2="6.20395" y2="0.98425" layer="21"/>
<rectangle x1="6.30555" y1="0.97155" x2="6.53415" y2="0.98425" layer="21"/>
<rectangle x1="7.99465" y1="0.97155" x2="8.21055" y2="0.98425" layer="21"/>
<rectangle x1="9.46785" y1="0.97155" x2="9.70915" y2="0.98425" layer="21"/>
<rectangle x1="10.64895" y1="0.97155" x2="10.89025" y2="0.98425" layer="21"/>
<rectangle x1="11.76655" y1="0.97155" x2="11.99515" y2="0.98425" layer="21"/>
<rectangle x1="0.20955" y1="0.98425" x2="0.60325" y2="0.99695" layer="21"/>
<rectangle x1="5.97535" y1="0.98425" x2="6.21665" y2="0.99695" layer="21"/>
<rectangle x1="6.29285" y1="0.98425" x2="6.52145" y2="0.99695" layer="21"/>
<rectangle x1="7.99465" y1="0.98425" x2="8.21055" y2="0.99695" layer="21"/>
<rectangle x1="9.46785" y1="0.98425" x2="9.70915" y2="0.99695" layer="21"/>
<rectangle x1="10.64895" y1="0.98425" x2="10.89025" y2="0.99695" layer="21"/>
<rectangle x1="11.76655" y1="0.98425" x2="11.99515" y2="0.99695" layer="21"/>
<rectangle x1="0.20955" y1="0.99695" x2="0.60325" y2="1.00965" layer="21"/>
<rectangle x1="5.97535" y1="0.99695" x2="6.22935" y2="1.00965" layer="21"/>
<rectangle x1="6.26745" y1="0.99695" x2="6.52145" y2="1.00965" layer="21"/>
<rectangle x1="7.99465" y1="0.99695" x2="8.21055" y2="1.00965" layer="21"/>
<rectangle x1="9.46785" y1="0.99695" x2="9.70915" y2="1.00965" layer="21"/>
<rectangle x1="10.64895" y1="0.99695" x2="10.89025" y2="1.00965" layer="21"/>
<rectangle x1="11.76655" y1="0.99695" x2="11.99515" y2="1.00965" layer="21"/>
<rectangle x1="0.20955" y1="1.00965" x2="0.59055" y2="1.02235" layer="21"/>
<rectangle x1="5.98805" y1="1.00965" x2="6.52145" y2="1.02235" layer="21"/>
<rectangle x1="7.99465" y1="1.00965" x2="8.21055" y2="1.02235" layer="21"/>
<rectangle x1="9.46785" y1="1.00965" x2="9.70915" y2="1.02235" layer="21"/>
<rectangle x1="10.64895" y1="1.00965" x2="10.89025" y2="1.02235" layer="21"/>
<rectangle x1="11.76655" y1="1.00965" x2="11.99515" y2="1.02235" layer="21"/>
<rectangle x1="0.20955" y1="1.02235" x2="0.59055" y2="1.03505" layer="21"/>
<rectangle x1="6.00075" y1="1.02235" x2="6.50875" y2="1.03505" layer="21"/>
<rectangle x1="7.99465" y1="1.02235" x2="8.21055" y2="1.03505" layer="21"/>
<rectangle x1="9.46785" y1="1.02235" x2="9.70915" y2="1.03505" layer="21"/>
<rectangle x1="10.64895" y1="1.02235" x2="10.89025" y2="1.03505" layer="21"/>
<rectangle x1="11.76655" y1="1.02235" x2="11.99515" y2="1.03505" layer="21"/>
<rectangle x1="0.20955" y1="1.03505" x2="0.59055" y2="1.04775" layer="21"/>
<rectangle x1="6.00075" y1="1.03505" x2="6.50875" y2="1.04775" layer="21"/>
<rectangle x1="7.99465" y1="1.03505" x2="8.21055" y2="1.04775" layer="21"/>
<rectangle x1="9.46785" y1="1.03505" x2="9.70915" y2="1.04775" layer="21"/>
<rectangle x1="10.64895" y1="1.03505" x2="10.89025" y2="1.04775" layer="21"/>
<rectangle x1="11.76655" y1="1.03505" x2="11.99515" y2="1.04775" layer="21"/>
<rectangle x1="0.22225" y1="1.04775" x2="0.59055" y2="1.06045" layer="21"/>
<rectangle x1="6.01345" y1="1.04775" x2="6.49605" y2="1.06045" layer="21"/>
<rectangle x1="7.99465" y1="1.04775" x2="8.21055" y2="1.06045" layer="21"/>
<rectangle x1="9.46785" y1="1.04775" x2="9.70915" y2="1.06045" layer="21"/>
<rectangle x1="10.64895" y1="1.04775" x2="10.89025" y2="1.06045" layer="21"/>
<rectangle x1="11.76655" y1="1.04775" x2="11.99515" y2="1.06045" layer="21"/>
<rectangle x1="0.22225" y1="1.06045" x2="0.59055" y2="1.07315" layer="21"/>
<rectangle x1="6.02615" y1="1.06045" x2="6.48335" y2="1.07315" layer="21"/>
<rectangle x1="9.46785" y1="1.06045" x2="9.70915" y2="1.07315" layer="21"/>
<rectangle x1="10.64895" y1="1.06045" x2="10.89025" y2="1.07315" layer="21"/>
<rectangle x1="11.76655" y1="1.06045" x2="11.99515" y2="1.07315" layer="21"/>
<rectangle x1="0.22225" y1="1.07315" x2="0.59055" y2="1.08585" layer="21"/>
<rectangle x1="6.03885" y1="1.07315" x2="6.47065" y2="1.08585" layer="21"/>
<rectangle x1="9.46785" y1="1.07315" x2="9.70915" y2="1.08585" layer="21"/>
<rectangle x1="10.64895" y1="1.07315" x2="10.89025" y2="1.08585" layer="21"/>
<rectangle x1="11.76655" y1="1.07315" x2="11.99515" y2="1.08585" layer="21"/>
<rectangle x1="0.22225" y1="1.08585" x2="0.59055" y2="1.09855" layer="21"/>
<rectangle x1="6.05155" y1="1.08585" x2="6.44525" y2="1.09855" layer="21"/>
<rectangle x1="9.46785" y1="1.08585" x2="9.70915" y2="1.09855" layer="21"/>
<rectangle x1="10.64895" y1="1.08585" x2="10.89025" y2="1.09855" layer="21"/>
<rectangle x1="11.76655" y1="1.08585" x2="11.99515" y2="1.09855" layer="21"/>
<rectangle x1="0.22225" y1="1.09855" x2="0.57785" y2="1.11125" layer="21"/>
<rectangle x1="6.07695" y1="1.09855" x2="6.43255" y2="1.11125" layer="21"/>
<rectangle x1="9.46785" y1="1.09855" x2="9.70915" y2="1.11125" layer="21"/>
<rectangle x1="10.64895" y1="1.09855" x2="10.89025" y2="1.11125" layer="21"/>
<rectangle x1="11.76655" y1="1.09855" x2="11.99515" y2="1.11125" layer="21"/>
<rectangle x1="0.22225" y1="1.11125" x2="0.57785" y2="1.12395" layer="21"/>
<rectangle x1="6.10235" y1="1.11125" x2="6.40715" y2="1.12395" layer="21"/>
<rectangle x1="9.46785" y1="1.11125" x2="9.70915" y2="1.12395" layer="21"/>
<rectangle x1="10.64895" y1="1.11125" x2="10.89025" y2="1.12395" layer="21"/>
<rectangle x1="11.76655" y1="1.11125" x2="11.99515" y2="1.12395" layer="21"/>
<rectangle x1="6.12775" y1="1.12395" x2="6.36905" y2="1.13665" layer="21"/>
<rectangle x1="6.17855" y1="1.13665" x2="6.31825" y2="1.14935" layer="21"/>
<rectangle x1="0.33655" y1="1.64465" x2="3.63855" y2="1.65735" layer="21"/>
<rectangle x1="0.29845" y1="1.65735" x2="3.67665" y2="1.67005" layer="21"/>
<rectangle x1="0.27305" y1="1.67005" x2="3.71475" y2="1.68275" layer="21"/>
<rectangle x1="8.79475" y1="1.67005" x2="12.08405" y2="1.68275" layer="21"/>
<rectangle x1="0.26035" y1="1.68275" x2="3.74015" y2="1.69545" layer="21"/>
<rectangle x1="8.74395" y1="1.68275" x2="12.12215" y2="1.69545" layer="21"/>
<rectangle x1="0.24765" y1="1.69545" x2="3.75285" y2="1.70815" layer="21"/>
<rectangle x1="8.71855" y1="1.69545" x2="12.14755" y2="1.70815" layer="21"/>
<rectangle x1="0.24765" y1="1.70815" x2="3.77825" y2="1.72085" layer="21"/>
<rectangle x1="8.69315" y1="1.70815" x2="12.16025" y2="1.72085" layer="21"/>
<rectangle x1="0.23495" y1="1.72085" x2="3.79095" y2="1.73355" layer="21"/>
<rectangle x1="6.17855" y1="1.72085" x2="6.41985" y2="1.73355" layer="21"/>
<rectangle x1="8.66775" y1="1.72085" x2="12.17295" y2="1.73355" layer="21"/>
<rectangle x1="0.23495" y1="1.73355" x2="3.80365" y2="1.74625" layer="21"/>
<rectangle x1="6.05155" y1="1.73355" x2="6.53415" y2="1.74625" layer="21"/>
<rectangle x1="8.64235" y1="1.73355" x2="12.18565" y2="1.74625" layer="21"/>
<rectangle x1="0.23495" y1="1.74625" x2="3.82905" y2="1.75895" layer="21"/>
<rectangle x1="5.97535" y1="1.74625" x2="6.61035" y2="1.75895" layer="21"/>
<rectangle x1="8.62965" y1="1.74625" x2="12.18565" y2="1.75895" layer="21"/>
<rectangle x1="0.23495" y1="1.75895" x2="3.84175" y2="1.77165" layer="21"/>
<rectangle x1="5.91185" y1="1.75895" x2="6.67385" y2="1.77165" layer="21"/>
<rectangle x1="8.61695" y1="1.75895" x2="12.18565" y2="1.77165" layer="21"/>
<rectangle x1="0.23495" y1="1.77165" x2="3.84175" y2="1.78435" layer="21"/>
<rectangle x1="5.86105" y1="1.77165" x2="6.72465" y2="1.78435" layer="21"/>
<rectangle x1="8.60425" y1="1.77165" x2="12.19835" y2="1.78435" layer="21"/>
<rectangle x1="0.23495" y1="1.78435" x2="3.85445" y2="1.79705" layer="21"/>
<rectangle x1="5.81025" y1="1.78435" x2="6.77545" y2="1.79705" layer="21"/>
<rectangle x1="8.59155" y1="1.78435" x2="12.19835" y2="1.79705" layer="21"/>
<rectangle x1="0.23495" y1="1.79705" x2="3.86715" y2="1.80975" layer="21"/>
<rectangle x1="5.77215" y1="1.79705" x2="6.81355" y2="1.80975" layer="21"/>
<rectangle x1="8.57885" y1="1.79705" x2="12.19835" y2="1.80975" layer="21"/>
<rectangle x1="0.23495" y1="1.80975" x2="3.87985" y2="1.82245" layer="21"/>
<rectangle x1="5.73405" y1="1.80975" x2="6.85165" y2="1.82245" layer="21"/>
<rectangle x1="8.56615" y1="1.80975" x2="12.19835" y2="1.82245" layer="21"/>
<rectangle x1="0.24765" y1="1.82245" x2="3.89255" y2="1.83515" layer="21"/>
<rectangle x1="5.69595" y1="1.82245" x2="6.88975" y2="1.83515" layer="21"/>
<rectangle x1="8.55345" y1="1.82245" x2="12.18565" y2="1.83515" layer="21"/>
<rectangle x1="0.24765" y1="1.83515" x2="3.89255" y2="1.84785" layer="21"/>
<rectangle x1="5.65785" y1="1.83515" x2="6.92785" y2="1.84785" layer="21"/>
<rectangle x1="8.54075" y1="1.83515" x2="12.18565" y2="1.84785" layer="21"/>
<rectangle x1="0.26035" y1="1.84785" x2="3.90525" y2="1.86055" layer="21"/>
<rectangle x1="5.63245" y1="1.84785" x2="6.96595" y2="1.86055" layer="21"/>
<rectangle x1="8.54075" y1="1.84785" x2="12.18565" y2="1.86055" layer="21"/>
<rectangle x1="0.26035" y1="1.86055" x2="3.07975" y2="1.87325" layer="21"/>
<rectangle x1="3.32105" y1="1.86055" x2="3.90525" y2="1.87325" layer="21"/>
<rectangle x1="5.59435" y1="1.86055" x2="6.99135" y2="1.87325" layer="21"/>
<rectangle x1="8.52805" y1="1.86055" x2="12.17295" y2="1.87325" layer="21"/>
<rectangle x1="0.27305" y1="1.87325" x2="3.01625" y2="1.88595" layer="21"/>
<rectangle x1="3.38455" y1="1.87325" x2="3.91795" y2="1.88595" layer="21"/>
<rectangle x1="5.56895" y1="1.87325" x2="7.01675" y2="1.88595" layer="21"/>
<rectangle x1="8.52805" y1="1.87325" x2="12.17295" y2="1.88595" layer="21"/>
<rectangle x1="0.28575" y1="1.88595" x2="2.96545" y2="1.89865" layer="21"/>
<rectangle x1="3.42265" y1="1.88595" x2="3.91795" y2="1.89865" layer="21"/>
<rectangle x1="5.54355" y1="1.88595" x2="7.04215" y2="1.89865" layer="21"/>
<rectangle x1="8.51535" y1="1.88595" x2="12.16025" y2="1.89865" layer="21"/>
<rectangle x1="0.29845" y1="1.89865" x2="2.91465" y2="1.91135" layer="21"/>
<rectangle x1="3.44805" y1="1.89865" x2="3.93065" y2="1.91135" layer="21"/>
<rectangle x1="5.51815" y1="1.89865" x2="7.06755" y2="1.91135" layer="21"/>
<rectangle x1="8.51535" y1="1.89865" x2="12.14755" y2="1.91135" layer="21"/>
<rectangle x1="0.31115" y1="1.91135" x2="2.91465" y2="1.92405" layer="21"/>
<rectangle x1="3.47345" y1="1.91135" x2="3.93065" y2="1.92405" layer="21"/>
<rectangle x1="5.49275" y1="1.91135" x2="7.09295" y2="1.92405" layer="21"/>
<rectangle x1="8.50265" y1="1.91135" x2="12.13485" y2="1.92405" layer="21"/>
<rectangle x1="0.32385" y1="1.92405" x2="2.91465" y2="1.93675" layer="21"/>
<rectangle x1="3.49885" y1="1.92405" x2="3.93065" y2="1.93675" layer="21"/>
<rectangle x1="5.46735" y1="1.92405" x2="7.11835" y2="1.93675" layer="21"/>
<rectangle x1="8.50265" y1="1.92405" x2="12.13485" y2="1.93675" layer="21"/>
<rectangle x1="0.33655" y1="1.93675" x2="2.91465" y2="1.94945" layer="21"/>
<rectangle x1="3.52425" y1="1.93675" x2="3.94335" y2="1.94945" layer="21"/>
<rectangle x1="5.44195" y1="1.93675" x2="7.14375" y2="1.94945" layer="21"/>
<rectangle x1="8.48995" y1="1.93675" x2="12.12215" y2="1.94945" layer="21"/>
<rectangle x1="0.34925" y1="1.94945" x2="2.91465" y2="1.96215" layer="21"/>
<rectangle x1="3.52425" y1="1.94945" x2="3.94335" y2="1.96215" layer="21"/>
<rectangle x1="5.41655" y1="1.94945" x2="7.16915" y2="1.96215" layer="21"/>
<rectangle x1="8.48995" y1="1.94945" x2="12.10945" y2="1.96215" layer="21"/>
<rectangle x1="0.36195" y1="1.96215" x2="2.92735" y2="1.97485" layer="21"/>
<rectangle x1="3.51155" y1="1.96215" x2="3.94335" y2="1.97485" layer="21"/>
<rectangle x1="5.40385" y1="1.96215" x2="7.19455" y2="1.97485" layer="21"/>
<rectangle x1="8.48995" y1="1.96215" x2="12.09675" y2="1.97485" layer="21"/>
<rectangle x1="0.37465" y1="1.97485" x2="2.73685" y2="1.98755" layer="21"/>
<rectangle x1="2.77495" y1="1.97485" x2="2.92735" y2="1.98755" layer="21"/>
<rectangle x1="3.49885" y1="1.97485" x2="3.94335" y2="1.98755" layer="21"/>
<rectangle x1="5.37845" y1="1.97485" x2="7.20725" y2="1.98755" layer="21"/>
<rectangle x1="8.48995" y1="1.97485" x2="12.08405" y2="1.98755" layer="21"/>
<rectangle x1="0.38735" y1="1.98755" x2="2.71145" y2="2.00025" layer="21"/>
<rectangle x1="2.77495" y1="1.98755" x2="2.92735" y2="2.00025" layer="21"/>
<rectangle x1="3.48615" y1="1.98755" x2="3.94335" y2="2.00025" layer="21"/>
<rectangle x1="5.35305" y1="1.98755" x2="7.23265" y2="2.00025" layer="21"/>
<rectangle x1="8.47725" y1="1.98755" x2="12.07135" y2="2.00025" layer="21"/>
<rectangle x1="0.40005" y1="2.00025" x2="2.68605" y2="2.01295" layer="21"/>
<rectangle x1="2.77495" y1="2.00025" x2="2.94005" y2="2.01295" layer="21"/>
<rectangle x1="3.47345" y1="2.00025" x2="3.95605" y2="2.01295" layer="21"/>
<rectangle x1="5.34035" y1="2.00025" x2="7.24535" y2="2.01295" layer="21"/>
<rectangle x1="8.47725" y1="2.00025" x2="12.05865" y2="2.01295" layer="21"/>
<rectangle x1="0.41275" y1="2.01295" x2="2.66065" y2="2.02565" layer="21"/>
<rectangle x1="2.78765" y1="2.01295" x2="2.94005" y2="2.02565" layer="21"/>
<rectangle x1="3.46075" y1="2.01295" x2="3.95605" y2="2.02565" layer="21"/>
<rectangle x1="5.31495" y1="2.01295" x2="7.27075" y2="2.02565" layer="21"/>
<rectangle x1="8.47725" y1="2.01295" x2="12.04595" y2="2.02565" layer="21"/>
<rectangle x1="0.42545" y1="2.02565" x2="2.63525" y2="2.03835" layer="21"/>
<rectangle x1="2.78765" y1="2.02565" x2="2.94005" y2="2.03835" layer="21"/>
<rectangle x1="3.44805" y1="2.02565" x2="3.95605" y2="2.03835" layer="21"/>
<rectangle x1="5.30225" y1="2.02565" x2="7.28345" y2="2.03835" layer="21"/>
<rectangle x1="8.47725" y1="2.02565" x2="12.03325" y2="2.03835" layer="21"/>
<rectangle x1="0.43815" y1="2.03835" x2="2.62255" y2="2.05105" layer="21"/>
<rectangle x1="2.78765" y1="2.03835" x2="2.94005" y2="2.05105" layer="21"/>
<rectangle x1="3.43535" y1="2.03835" x2="3.95605" y2="2.05105" layer="21"/>
<rectangle x1="5.27685" y1="2.03835" x2="7.30885" y2="2.05105" layer="21"/>
<rectangle x1="8.47725" y1="2.03835" x2="12.02055" y2="2.05105" layer="21"/>
<rectangle x1="0.45085" y1="2.05105" x2="2.59715" y2="2.06375" layer="21"/>
<rectangle x1="2.78765" y1="2.05105" x2="2.95275" y2="2.06375" layer="21"/>
<rectangle x1="3.42265" y1="2.05105" x2="3.95605" y2="2.06375" layer="21"/>
<rectangle x1="5.26415" y1="2.05105" x2="7.32155" y2="2.06375" layer="21"/>
<rectangle x1="8.47725" y1="2.05105" x2="12.00785" y2="2.06375" layer="21"/>
<rectangle x1="0.46355" y1="2.06375" x2="2.57175" y2="2.07645" layer="21"/>
<rectangle x1="2.80035" y1="2.06375" x2="2.95275" y2="2.07645" layer="21"/>
<rectangle x1="3.40995" y1="2.06375" x2="3.62585" y2="2.07645" layer="21"/>
<rectangle x1="3.65125" y1="2.06375" x2="3.95605" y2="2.07645" layer="21"/>
<rectangle x1="5.25145" y1="2.06375" x2="7.34695" y2="2.07645" layer="21"/>
<rectangle x1="8.47725" y1="2.06375" x2="9.06145" y2="2.07645" layer="21"/>
<rectangle x1="9.07415" y1="2.06375" x2="11.99515" y2="2.07645" layer="21"/>
<rectangle x1="0.47625" y1="2.07645" x2="2.55905" y2="2.08915" layer="21"/>
<rectangle x1="2.80035" y1="2.07645" x2="2.95275" y2="2.08915" layer="21"/>
<rectangle x1="3.39725" y1="2.07645" x2="3.61315" y2="2.08915" layer="21"/>
<rectangle x1="3.66395" y1="2.07645" x2="3.95605" y2="2.08915" layer="21"/>
<rectangle x1="5.22605" y1="2.07645" x2="7.35965" y2="2.08915" layer="21"/>
<rectangle x1="8.47725" y1="2.07645" x2="9.04875" y2="2.08915" layer="21"/>
<rectangle x1="9.08685" y1="2.07645" x2="11.98245" y2="2.08915" layer="21"/>
<rectangle x1="0.48895" y1="2.08915" x2="2.53365" y2="2.10185" layer="21"/>
<rectangle x1="2.80035" y1="2.08915" x2="2.95275" y2="2.10185" layer="21"/>
<rectangle x1="3.38455" y1="2.08915" x2="3.60045" y2="2.10185" layer="21"/>
<rectangle x1="3.66395" y1="2.08915" x2="3.95605" y2="2.10185" layer="21"/>
<rectangle x1="5.21335" y1="2.08915" x2="7.37235" y2="2.10185" layer="21"/>
<rectangle x1="8.47725" y1="2.08915" x2="9.03605" y2="2.10185" layer="21"/>
<rectangle x1="9.09955" y1="2.08915" x2="11.96975" y2="2.10185" layer="21"/>
<rectangle x1="0.50165" y1="2.10185" x2="2.52095" y2="2.11455" layer="21"/>
<rectangle x1="2.81305" y1="2.10185" x2="2.96545" y2="2.11455" layer="21"/>
<rectangle x1="3.37185" y1="2.10185" x2="3.57505" y2="2.11455" layer="21"/>
<rectangle x1="3.67665" y1="2.10185" x2="3.95605" y2="2.11455" layer="21"/>
<rectangle x1="5.20065" y1="2.10185" x2="7.38505" y2="2.11455" layer="21"/>
<rectangle x1="8.47725" y1="2.10185" x2="9.02335" y2="2.11455" layer="21"/>
<rectangle x1="9.11225" y1="2.10185" x2="11.95705" y2="2.11455" layer="21"/>
<rectangle x1="0.51435" y1="2.11455" x2="2.50825" y2="2.12725" layer="21"/>
<rectangle x1="2.81305" y1="2.11455" x2="2.96545" y2="2.12725" layer="21"/>
<rectangle x1="3.35915" y1="2.11455" x2="3.56235" y2="2.12725" layer="21"/>
<rectangle x1="3.68935" y1="2.11455" x2="3.95605" y2="2.12725" layer="21"/>
<rectangle x1="5.17525" y1="2.11455" x2="7.41045" y2="2.12725" layer="21"/>
<rectangle x1="8.47725" y1="2.11455" x2="9.01065" y2="2.12725" layer="21"/>
<rectangle x1="9.12495" y1="2.11455" x2="11.94435" y2="2.12725" layer="21"/>
<rectangle x1="0.52705" y1="2.12725" x2="2.50825" y2="2.13995" layer="21"/>
<rectangle x1="2.81305" y1="2.12725" x2="2.96545" y2="2.13995" layer="21"/>
<rectangle x1="3.34645" y1="2.12725" x2="3.54965" y2="2.13995" layer="21"/>
<rectangle x1="3.68935" y1="2.12725" x2="3.95605" y2="2.13995" layer="21"/>
<rectangle x1="5.16255" y1="2.12725" x2="7.42315" y2="2.13995" layer="21"/>
<rectangle x1="8.47725" y1="2.12725" x2="8.99795" y2="2.13995" layer="21"/>
<rectangle x1="9.13765" y1="2.12725" x2="11.93165" y2="2.13995" layer="21"/>
<rectangle x1="0.53975" y1="2.13995" x2="2.52095" y2="2.15265" layer="21"/>
<rectangle x1="2.81305" y1="2.13995" x2="2.96545" y2="2.15265" layer="21"/>
<rectangle x1="3.33375" y1="2.13995" x2="3.53695" y2="2.15265" layer="21"/>
<rectangle x1="3.68935" y1="2.13995" x2="3.95605" y2="2.15265" layer="21"/>
<rectangle x1="5.14985" y1="2.13995" x2="7.43585" y2="2.15265" layer="21"/>
<rectangle x1="8.47725" y1="2.13995" x2="8.98525" y2="2.15265" layer="21"/>
<rectangle x1="9.15035" y1="2.13995" x2="9.31545" y2="2.15265" layer="21"/>
<rectangle x1="9.34085" y1="2.13995" x2="11.91895" y2="2.15265" layer="21"/>
<rectangle x1="0.55245" y1="2.15265" x2="2.53365" y2="2.16535" layer="21"/>
<rectangle x1="2.82575" y1="2.15265" x2="2.97815" y2="2.16535" layer="21"/>
<rectangle x1="3.32105" y1="2.15265" x2="3.52425" y2="2.16535" layer="21"/>
<rectangle x1="3.70205" y1="2.15265" x2="3.95605" y2="2.16535" layer="21"/>
<rectangle x1="5.13715" y1="2.15265" x2="7.44855" y2="2.16535" layer="21"/>
<rectangle x1="8.47725" y1="2.15265" x2="8.97255" y2="2.16535" layer="21"/>
<rectangle x1="9.16305" y1="2.15265" x2="9.23925" y2="2.16535" layer="21"/>
<rectangle x1="9.36625" y1="2.15265" x2="11.90625" y2="2.16535" layer="21"/>
<rectangle x1="0.56515" y1="2.16535" x2="2.54635" y2="2.17805" layer="21"/>
<rectangle x1="2.82575" y1="2.16535" x2="2.97815" y2="2.17805" layer="21"/>
<rectangle x1="3.30835" y1="2.16535" x2="3.51155" y2="2.17805" layer="21"/>
<rectangle x1="3.70205" y1="2.16535" x2="3.95605" y2="2.17805" layer="21"/>
<rectangle x1="5.12445" y1="2.16535" x2="7.46125" y2="2.17805" layer="21"/>
<rectangle x1="8.47725" y1="2.16535" x2="8.95985" y2="2.17805" layer="21"/>
<rectangle x1="9.17575" y1="2.16535" x2="9.20115" y2="2.17805" layer="21"/>
<rectangle x1="9.36625" y1="2.16535" x2="9.53135" y2="2.17805" layer="21"/>
<rectangle x1="9.63295" y1="2.16535" x2="11.89355" y2="2.17805" layer="21"/>
<rectangle x1="0.57785" y1="2.17805" x2="2.55905" y2="2.19075" layer="21"/>
<rectangle x1="2.82575" y1="2.17805" x2="2.97815" y2="2.19075" layer="21"/>
<rectangle x1="3.29565" y1="2.17805" x2="3.49885" y2="2.19075" layer="21"/>
<rectangle x1="3.71475" y1="2.17805" x2="3.95605" y2="2.19075" layer="21"/>
<rectangle x1="5.11175" y1="2.17805" x2="7.47395" y2="2.19075" layer="21"/>
<rectangle x1="8.47725" y1="2.17805" x2="8.94715" y2="2.19075" layer="21"/>
<rectangle x1="9.36625" y1="2.17805" x2="9.53135" y2="2.19075" layer="21"/>
<rectangle x1="9.68375" y1="2.17805" x2="11.88085" y2="2.19075" layer="21"/>
<rectangle x1="0.59055" y1="2.19075" x2="2.57175" y2="2.20345" layer="21"/>
<rectangle x1="2.82575" y1="2.19075" x2="2.99085" y2="2.20345" layer="21"/>
<rectangle x1="3.28295" y1="2.19075" x2="3.48615" y2="2.20345" layer="21"/>
<rectangle x1="3.71475" y1="2.19075" x2="3.95605" y2="2.20345" layer="21"/>
<rectangle x1="5.09905" y1="2.19075" x2="7.49935" y2="2.20345" layer="21"/>
<rectangle x1="8.47725" y1="2.19075" x2="8.93445" y2="2.20345" layer="21"/>
<rectangle x1="9.35355" y1="2.19075" x2="9.53135" y2="2.20345" layer="21"/>
<rectangle x1="9.70915" y1="2.19075" x2="11.86815" y2="2.20345" layer="21"/>
<rectangle x1="0.60325" y1="2.20345" x2="2.58445" y2="2.21615" layer="21"/>
<rectangle x1="2.83845" y1="2.20345" x2="2.99085" y2="2.21615" layer="21"/>
<rectangle x1="3.27025" y1="2.20345" x2="3.47345" y2="2.21615" layer="21"/>
<rectangle x1="3.71475" y1="2.20345" x2="3.95605" y2="2.21615" layer="21"/>
<rectangle x1="5.08635" y1="2.20345" x2="7.51205" y2="2.21615" layer="21"/>
<rectangle x1="8.47725" y1="2.20345" x2="8.92175" y2="2.21615" layer="21"/>
<rectangle x1="9.34085" y1="2.20345" x2="9.53135" y2="2.21615" layer="21"/>
<rectangle x1="9.70915" y1="2.20345" x2="11.85545" y2="2.21615" layer="21"/>
<rectangle x1="0.61595" y1="2.21615" x2="2.59715" y2="2.22885" layer="21"/>
<rectangle x1="2.83845" y1="2.21615" x2="2.99085" y2="2.22885" layer="21"/>
<rectangle x1="3.25755" y1="2.21615" x2="3.46075" y2="2.22885" layer="21"/>
<rectangle x1="3.72745" y1="2.21615" x2="3.95605" y2="2.22885" layer="21"/>
<rectangle x1="5.07365" y1="2.21615" x2="7.52475" y2="2.22885" layer="21"/>
<rectangle x1="8.47725" y1="2.21615" x2="8.90905" y2="2.22885" layer="21"/>
<rectangle x1="9.25195" y1="2.21615" x2="9.55675" y2="2.22885" layer="21"/>
<rectangle x1="9.70915" y1="2.21615" x2="11.84275" y2="2.22885" layer="21"/>
<rectangle x1="0.62865" y1="2.22885" x2="2.60985" y2="2.24155" layer="21"/>
<rectangle x1="2.83845" y1="2.22885" x2="2.99085" y2="2.24155" layer="21"/>
<rectangle x1="3.24485" y1="2.22885" x2="3.44805" y2="2.24155" layer="21"/>
<rectangle x1="3.72745" y1="2.22885" x2="3.95605" y2="2.24155" layer="21"/>
<rectangle x1="5.06095" y1="2.22885" x2="7.53745" y2="2.24155" layer="21"/>
<rectangle x1="8.47725" y1="2.22885" x2="8.89635" y2="2.24155" layer="21"/>
<rectangle x1="9.23925" y1="2.22885" x2="9.60755" y2="2.24155" layer="21"/>
<rectangle x1="9.70915" y1="2.22885" x2="11.83005" y2="2.24155" layer="21"/>
<rectangle x1="0.64135" y1="2.24155" x2="2.62255" y2="2.25425" layer="21"/>
<rectangle x1="2.83845" y1="2.24155" x2="3.00355" y2="2.25425" layer="21"/>
<rectangle x1="3.23215" y1="2.24155" x2="3.43535" y2="2.25425" layer="21"/>
<rectangle x1="3.72745" y1="2.24155" x2="3.95605" y2="2.25425" layer="21"/>
<rectangle x1="5.04825" y1="2.24155" x2="7.55015" y2="2.25425" layer="21"/>
<rectangle x1="8.47725" y1="2.24155" x2="8.88365" y2="2.25425" layer="21"/>
<rectangle x1="9.25195" y1="2.24155" x2="9.67105" y2="2.25425" layer="21"/>
<rectangle x1="9.68375" y1="2.24155" x2="11.81735" y2="2.25425" layer="21"/>
<rectangle x1="0.65405" y1="2.25425" x2="2.63525" y2="2.26695" layer="21"/>
<rectangle x1="2.85115" y1="2.25425" x2="3.00355" y2="2.26695" layer="21"/>
<rectangle x1="3.21945" y1="2.25425" x2="3.42265" y2="2.26695" layer="21"/>
<rectangle x1="3.72745" y1="2.25425" x2="3.95605" y2="2.26695" layer="21"/>
<rectangle x1="5.03555" y1="2.25425" x2="7.56285" y2="2.26695" layer="21"/>
<rectangle x1="8.47725" y1="2.25425" x2="8.87095" y2="2.26695" layer="21"/>
<rectangle x1="9.26465" y1="2.25425" x2="9.87425" y2="2.26695" layer="21"/>
<rectangle x1="9.91235" y1="2.25425" x2="11.80465" y2="2.26695" layer="21"/>
<rectangle x1="0.66675" y1="2.26695" x2="2.64795" y2="2.27965" layer="21"/>
<rectangle x1="2.85115" y1="2.26695" x2="3.10515" y2="2.27965" layer="21"/>
<rectangle x1="3.20675" y1="2.26695" x2="3.40995" y2="2.27965" layer="21"/>
<rectangle x1="3.74015" y1="2.26695" x2="3.95605" y2="2.27965" layer="21"/>
<rectangle x1="5.02285" y1="2.26695" x2="7.57555" y2="2.27965" layer="21"/>
<rectangle x1="8.47725" y1="2.26695" x2="8.85825" y2="2.27965" layer="21"/>
<rectangle x1="9.27735" y1="2.26695" x2="9.86155" y2="2.27965" layer="21"/>
<rectangle x1="9.95045" y1="2.26695" x2="11.79195" y2="2.27965" layer="21"/>
<rectangle x1="0.67945" y1="2.27965" x2="2.66065" y2="2.29235" layer="21"/>
<rectangle x1="2.83845" y1="2.27965" x2="3.14325" y2="2.29235" layer="21"/>
<rectangle x1="3.19405" y1="2.27965" x2="3.39725" y2="2.29235" layer="21"/>
<rectangle x1="3.74015" y1="2.27965" x2="3.95605" y2="2.29235" layer="21"/>
<rectangle x1="5.01015" y1="2.27965" x2="7.58825" y2="2.29235" layer="21"/>
<rectangle x1="8.47725" y1="2.27965" x2="8.84555" y2="2.29235" layer="21"/>
<rectangle x1="9.29005" y1="2.27965" x2="9.86155" y2="2.29235" layer="21"/>
<rectangle x1="9.97585" y1="2.27965" x2="11.77925" y2="2.29235" layer="21"/>
<rectangle x1="0.69215" y1="2.29235" x2="2.67335" y2="2.30505" layer="21"/>
<rectangle x1="2.80035" y1="2.29235" x2="3.38455" y2="2.30505" layer="21"/>
<rectangle x1="3.74015" y1="2.29235" x2="3.95605" y2="2.30505" layer="21"/>
<rectangle x1="4.99745" y1="2.29235" x2="7.58825" y2="2.30505" layer="21"/>
<rectangle x1="8.47725" y1="2.29235" x2="8.83285" y2="2.30505" layer="21"/>
<rectangle x1="9.30275" y1="2.29235" x2="9.86155" y2="2.30505" layer="21"/>
<rectangle x1="10.01395" y1="2.29235" x2="11.76655" y2="2.30505" layer="21"/>
<rectangle x1="0.70485" y1="2.30505" x2="2.68605" y2="2.31775" layer="21"/>
<rectangle x1="2.76225" y1="2.30505" x2="3.37185" y2="2.31775" layer="21"/>
<rectangle x1="3.74015" y1="2.30505" x2="3.95605" y2="2.31775" layer="21"/>
<rectangle x1="4.98475" y1="2.30505" x2="7.60095" y2="2.31775" layer="21"/>
<rectangle x1="8.47725" y1="2.30505" x2="8.82015" y2="2.31775" layer="21"/>
<rectangle x1="9.31545" y1="2.30505" x2="9.87425" y2="2.31775" layer="21"/>
<rectangle x1="10.02665" y1="2.30505" x2="11.75385" y2="2.31775" layer="21"/>
<rectangle x1="0.71755" y1="2.31775" x2="2.69875" y2="2.33045" layer="21"/>
<rectangle x1="2.72415" y1="2.31775" x2="3.35915" y2="2.33045" layer="21"/>
<rectangle x1="3.74015" y1="2.31775" x2="3.95605" y2="2.33045" layer="21"/>
<rectangle x1="4.97205" y1="2.31775" x2="7.61365" y2="2.33045" layer="21"/>
<rectangle x1="8.47725" y1="2.31775" x2="8.80745" y2="2.33045" layer="21"/>
<rectangle x1="9.32815" y1="2.31775" x2="9.89965" y2="2.33045" layer="21"/>
<rectangle x1="10.03935" y1="2.31775" x2="11.74115" y2="2.33045" layer="21"/>
<rectangle x1="0.73025" y1="2.33045" x2="3.34645" y2="2.34315" layer="21"/>
<rectangle x1="3.74015" y1="2.33045" x2="3.95605" y2="2.34315" layer="21"/>
<rectangle x1="4.95935" y1="2.33045" x2="7.62635" y2="2.34315" layer="21"/>
<rectangle x1="8.47725" y1="2.33045" x2="8.80745" y2="2.34315" layer="21"/>
<rectangle x1="9.32815" y1="2.33045" x2="9.92505" y2="2.34315" layer="21"/>
<rectangle x1="10.03935" y1="2.33045" x2="11.72845" y2="2.34315" layer="21"/>
<rectangle x1="0.74295" y1="2.34315" x2="3.33375" y2="2.35585" layer="21"/>
<rectangle x1="3.74015" y1="2.34315" x2="3.95605" y2="2.35585" layer="21"/>
<rectangle x1="4.94665" y1="2.34315" x2="7.63905" y2="2.35585" layer="21"/>
<rectangle x1="8.47725" y1="2.34315" x2="8.82015" y2="2.35585" layer="21"/>
<rectangle x1="9.31545" y1="2.34315" x2="9.95045" y2="2.35585" layer="21"/>
<rectangle x1="10.02665" y1="2.34315" x2="11.71575" y2="2.35585" layer="21"/>
<rectangle x1="0.75565" y1="2.35585" x2="3.32105" y2="2.36855" layer="21"/>
<rectangle x1="3.74015" y1="2.35585" x2="3.95605" y2="2.36855" layer="21"/>
<rectangle x1="4.93395" y1="2.35585" x2="7.65175" y2="2.36855" layer="21"/>
<rectangle x1="8.47725" y1="2.35585" x2="8.83285" y2="2.36855" layer="21"/>
<rectangle x1="9.30275" y1="2.35585" x2="9.98855" y2="2.36855" layer="21"/>
<rectangle x1="10.01395" y1="2.35585" x2="11.70305" y2="2.36855" layer="21"/>
<rectangle x1="0.76835" y1="2.36855" x2="3.30835" y2="2.38125" layer="21"/>
<rectangle x1="3.74015" y1="2.36855" x2="3.95605" y2="2.38125" layer="21"/>
<rectangle x1="4.93395" y1="2.36855" x2="7.66445" y2="2.38125" layer="21"/>
<rectangle x1="8.47725" y1="2.36855" x2="8.84555" y2="2.38125" layer="21"/>
<rectangle x1="9.29005" y1="2.36855" x2="11.69035" y2="2.38125" layer="21"/>
<rectangle x1="0.78105" y1="2.38125" x2="3.29565" y2="2.39395" layer="21"/>
<rectangle x1="3.74015" y1="2.38125" x2="3.95605" y2="2.39395" layer="21"/>
<rectangle x1="4.92125" y1="2.38125" x2="7.66445" y2="2.39395" layer="21"/>
<rectangle x1="8.47725" y1="2.38125" x2="8.85825" y2="2.39395" layer="21"/>
<rectangle x1="9.27735" y1="2.38125" x2="11.67765" y2="2.39395" layer="21"/>
<rectangle x1="0.79375" y1="2.39395" x2="3.29565" y2="2.40665" layer="21"/>
<rectangle x1="3.74015" y1="2.39395" x2="3.95605" y2="2.40665" layer="21"/>
<rectangle x1="4.90855" y1="2.39395" x2="6.14045" y2="2.40665" layer="21"/>
<rectangle x1="6.44525" y1="2.39395" x2="7.67715" y2="2.40665" layer="21"/>
<rectangle x1="8.47725" y1="2.39395" x2="8.87095" y2="2.40665" layer="21"/>
<rectangle x1="9.26465" y1="2.39395" x2="10.20445" y2="2.40665" layer="21"/>
<rectangle x1="10.21715" y1="2.39395" x2="11.66495" y2="2.40665" layer="21"/>
<rectangle x1="0.80645" y1="2.40665" x2="3.29565" y2="2.41935" layer="21"/>
<rectangle x1="3.74015" y1="2.40665" x2="3.95605" y2="2.41935" layer="21"/>
<rectangle x1="4.89585" y1="2.40665" x2="6.06425" y2="2.41935" layer="21"/>
<rectangle x1="6.52145" y1="2.40665" x2="7.68985" y2="2.41935" layer="21"/>
<rectangle x1="8.47725" y1="2.40665" x2="8.88365" y2="2.41935" layer="21"/>
<rectangle x1="9.25195" y1="2.40665" x2="10.17905" y2="2.41935" layer="21"/>
<rectangle x1="10.24255" y1="2.40665" x2="11.65225" y2="2.41935" layer="21"/>
<rectangle x1="0.81915" y1="2.41935" x2="3.29565" y2="2.43205" layer="21"/>
<rectangle x1="3.74015" y1="2.41935" x2="3.95605" y2="2.43205" layer="21"/>
<rectangle x1="4.88315" y1="2.41935" x2="6.01345" y2="2.43205" layer="21"/>
<rectangle x1="6.57225" y1="2.41935" x2="7.70255" y2="2.43205" layer="21"/>
<rectangle x1="8.47725" y1="2.41935" x2="8.89635" y2="2.43205" layer="21"/>
<rectangle x1="9.23925" y1="2.41935" x2="10.17905" y2="2.43205" layer="21"/>
<rectangle x1="10.26795" y1="2.41935" x2="11.63955" y2="2.43205" layer="21"/>
<rectangle x1="0.83185" y1="2.43205" x2="3.30835" y2="2.44475" layer="21"/>
<rectangle x1="3.74015" y1="2.43205" x2="3.95605" y2="2.44475" layer="21"/>
<rectangle x1="4.88315" y1="2.43205" x2="5.97535" y2="2.44475" layer="21"/>
<rectangle x1="6.62305" y1="2.43205" x2="7.70255" y2="2.44475" layer="21"/>
<rectangle x1="8.47725" y1="2.43205" x2="8.90905" y2="2.44475" layer="21"/>
<rectangle x1="9.22655" y1="2.43205" x2="10.17905" y2="2.44475" layer="21"/>
<rectangle x1="10.29335" y1="2.43205" x2="11.62685" y2="2.44475" layer="21"/>
<rectangle x1="0.84455" y1="2.44475" x2="3.30835" y2="2.45745" layer="21"/>
<rectangle x1="3.74015" y1="2.44475" x2="3.95605" y2="2.45745" layer="21"/>
<rectangle x1="4.87045" y1="2.44475" x2="5.93725" y2="2.45745" layer="21"/>
<rectangle x1="6.64845" y1="2.44475" x2="7.71525" y2="2.45745" layer="21"/>
<rectangle x1="8.47725" y1="2.44475" x2="8.92175" y2="2.45745" layer="21"/>
<rectangle x1="9.21385" y1="2.44475" x2="10.17905" y2="2.45745" layer="21"/>
<rectangle x1="10.31875" y1="2.44475" x2="11.61415" y2="2.45745" layer="21"/>
<rectangle x1="0.85725" y1="2.45745" x2="3.32105" y2="2.47015" layer="21"/>
<rectangle x1="3.74015" y1="2.45745" x2="3.95605" y2="2.47015" layer="21"/>
<rectangle x1="4.85775" y1="2.45745" x2="5.89915" y2="2.47015" layer="21"/>
<rectangle x1="6.68655" y1="2.45745" x2="7.72795" y2="2.47015" layer="21"/>
<rectangle x1="8.47725" y1="2.45745" x2="8.93445" y2="2.47015" layer="21"/>
<rectangle x1="9.20115" y1="2.45745" x2="10.19175" y2="2.47015" layer="21"/>
<rectangle x1="10.33145" y1="2.45745" x2="11.60145" y2="2.47015" layer="21"/>
<rectangle x1="0.86995" y1="2.47015" x2="3.32105" y2="2.48285" layer="21"/>
<rectangle x1="3.74015" y1="2.47015" x2="3.95605" y2="2.48285" layer="21"/>
<rectangle x1="4.85775" y1="2.47015" x2="5.87375" y2="2.48285" layer="21"/>
<rectangle x1="6.72465" y1="2.47015" x2="7.74065" y2="2.48285" layer="21"/>
<rectangle x1="8.47725" y1="2.47015" x2="8.94715" y2="2.48285" layer="21"/>
<rectangle x1="9.18845" y1="2.47015" x2="10.21715" y2="2.48285" layer="21"/>
<rectangle x1="10.34415" y1="2.47015" x2="10.75055" y2="2.48285" layer="21"/>
<rectangle x1="11.00455" y1="2.47015" x2="11.58875" y2="2.48285" layer="21"/>
<rectangle x1="0.88265" y1="2.48285" x2="3.32105" y2="2.49555" layer="21"/>
<rectangle x1="3.74015" y1="2.48285" x2="3.95605" y2="2.49555" layer="21"/>
<rectangle x1="4.84505" y1="2.48285" x2="5.83565" y2="2.49555" layer="21"/>
<rectangle x1="6.75005" y1="2.48285" x2="7.74065" y2="2.49555" layer="21"/>
<rectangle x1="8.47725" y1="2.48285" x2="8.95985" y2="2.49555" layer="21"/>
<rectangle x1="9.17575" y1="2.48285" x2="10.24255" y2="2.49555" layer="21"/>
<rectangle x1="10.34415" y1="2.48285" x2="10.64895" y2="2.49555" layer="21"/>
<rectangle x1="11.11885" y1="2.48285" x2="11.57605" y2="2.49555" layer="21"/>
<rectangle x1="0.89535" y1="2.49555" x2="3.32105" y2="2.50825" layer="21"/>
<rectangle x1="3.74015" y1="2.49555" x2="3.95605" y2="2.50825" layer="21"/>
<rectangle x1="4.83235" y1="2.49555" x2="5.81025" y2="2.50825" layer="21"/>
<rectangle x1="6.77545" y1="2.49555" x2="7.75335" y2="2.50825" layer="21"/>
<rectangle x1="8.47725" y1="2.49555" x2="8.97255" y2="2.50825" layer="21"/>
<rectangle x1="9.16305" y1="2.49555" x2="10.26795" y2="2.50825" layer="21"/>
<rectangle x1="10.34415" y1="2.49555" x2="10.57275" y2="2.50825" layer="21"/>
<rectangle x1="11.19505" y1="2.49555" x2="11.56335" y2="2.50825" layer="21"/>
<rectangle x1="0.90805" y1="2.50825" x2="3.33375" y2="2.52095" layer="21"/>
<rectangle x1="3.74015" y1="2.50825" x2="3.95605" y2="2.52095" layer="21"/>
<rectangle x1="4.83235" y1="2.50825" x2="5.78485" y2="2.52095" layer="21"/>
<rectangle x1="6.80085" y1="2.50825" x2="7.75335" y2="2.52095" layer="21"/>
<rectangle x1="8.47725" y1="2.50825" x2="8.98525" y2="2.52095" layer="21"/>
<rectangle x1="9.15035" y1="2.50825" x2="10.28065" y2="2.52095" layer="21"/>
<rectangle x1="10.33145" y1="2.50825" x2="10.50925" y2="2.52095" layer="21"/>
<rectangle x1="11.25855" y1="2.50825" x2="11.55065" y2="2.52095" layer="21"/>
<rectangle x1="0.92075" y1="2.52095" x2="3.33375" y2="2.53365" layer="21"/>
<rectangle x1="3.74015" y1="2.52095" x2="3.95605" y2="2.53365" layer="21"/>
<rectangle x1="4.81965" y1="2.52095" x2="5.75945" y2="2.53365" layer="21"/>
<rectangle x1="6.82625" y1="2.52095" x2="7.76605" y2="2.53365" layer="21"/>
<rectangle x1="8.47725" y1="2.52095" x2="8.99795" y2="2.53365" layer="21"/>
<rectangle x1="9.13765" y1="2.52095" x2="10.45845" y2="2.53365" layer="21"/>
<rectangle x1="11.30935" y1="2.52095" x2="11.53795" y2="2.53365" layer="21"/>
<rectangle x1="0.93345" y1="2.53365" x2="3.33375" y2="2.54635" layer="21"/>
<rectangle x1="3.72745" y1="2.53365" x2="3.95605" y2="2.54635" layer="21"/>
<rectangle x1="4.80695" y1="2.53365" x2="5.74675" y2="2.54635" layer="21"/>
<rectangle x1="6.83895" y1="2.53365" x2="7.77875" y2="2.54635" layer="21"/>
<rectangle x1="8.47725" y1="2.53365" x2="9.01065" y2="2.54635" layer="21"/>
<rectangle x1="9.12495" y1="2.53365" x2="10.40765" y2="2.54635" layer="21"/>
<rectangle x1="11.36015" y1="2.53365" x2="11.52525" y2="2.54635" layer="21"/>
<rectangle x1="0.94615" y1="2.54635" x2="3.33375" y2="2.55905" layer="21"/>
<rectangle x1="3.72745" y1="2.54635" x2="3.95605" y2="2.55905" layer="21"/>
<rectangle x1="4.80695" y1="2.54635" x2="5.72135" y2="2.55905" layer="21"/>
<rectangle x1="6.86435" y1="2.54635" x2="7.77875" y2="2.55905" layer="21"/>
<rectangle x1="8.47725" y1="2.54635" x2="9.02335" y2="2.55905" layer="21"/>
<rectangle x1="9.11225" y1="2.54635" x2="10.36955" y2="2.55905" layer="21"/>
<rectangle x1="11.39825" y1="2.54635" x2="11.51255" y2="2.55905" layer="21"/>
<rectangle x1="0.95885" y1="2.55905" x2="3.33375" y2="2.57175" layer="21"/>
<rectangle x1="3.72745" y1="2.55905" x2="3.95605" y2="2.57175" layer="21"/>
<rectangle x1="4.79425" y1="2.55905" x2="5.69595" y2="2.57175" layer="21"/>
<rectangle x1="6.88975" y1="2.55905" x2="7.79145" y2="2.57175" layer="21"/>
<rectangle x1="8.47725" y1="2.55905" x2="9.03605" y2="2.57175" layer="21"/>
<rectangle x1="9.09955" y1="2.55905" x2="10.33145" y2="2.57175" layer="21"/>
<rectangle x1="11.43635" y1="2.55905" x2="11.49985" y2="2.57175" layer="21"/>
<rectangle x1="0.97155" y1="2.57175" x2="3.33375" y2="2.58445" layer="21"/>
<rectangle x1="3.72745" y1="2.57175" x2="3.95605" y2="2.58445" layer="21"/>
<rectangle x1="4.78155" y1="2.57175" x2="5.68325" y2="2.58445" layer="21"/>
<rectangle x1="6.90245" y1="2.57175" x2="7.80415" y2="2.58445" layer="21"/>
<rectangle x1="8.47725" y1="2.57175" x2="9.04875" y2="2.58445" layer="21"/>
<rectangle x1="9.08685" y1="2.57175" x2="10.29335" y2="2.58445" layer="21"/>
<rectangle x1="11.47445" y1="2.57175" x2="11.48715" y2="2.58445" layer="21"/>
<rectangle x1="0.98425" y1="2.58445" x2="3.33375" y2="2.59715" layer="21"/>
<rectangle x1="3.72745" y1="2.58445" x2="3.95605" y2="2.59715" layer="21"/>
<rectangle x1="4.78155" y1="2.58445" x2="5.65785" y2="2.59715" layer="21"/>
<rectangle x1="6.92785" y1="2.58445" x2="7.80415" y2="2.59715" layer="21"/>
<rectangle x1="8.47725" y1="2.58445" x2="9.06145" y2="2.59715" layer="21"/>
<rectangle x1="9.07415" y1="2.58445" x2="10.26795" y2="2.59715" layer="21"/>
<rectangle x1="0.99695" y1="2.59715" x2="3.33375" y2="2.60985" layer="21"/>
<rectangle x1="3.71475" y1="2.59715" x2="3.95605" y2="2.60985" layer="21"/>
<rectangle x1="4.76885" y1="2.59715" x2="5.64515" y2="2.60985" layer="21"/>
<rectangle x1="6.94055" y1="2.59715" x2="7.80415" y2="2.60985" layer="21"/>
<rectangle x1="8.47725" y1="2.59715" x2="10.22985" y2="2.60985" layer="21"/>
<rectangle x1="1.00965" y1="2.60985" x2="3.38455" y2="2.62255" layer="21"/>
<rectangle x1="3.71475" y1="2.60985" x2="3.95605" y2="2.62255" layer="21"/>
<rectangle x1="4.76885" y1="2.60985" x2="5.63245" y2="2.62255" layer="21"/>
<rectangle x1="6.95325" y1="2.60985" x2="7.77875" y2="2.62255" layer="21"/>
<rectangle x1="8.47725" y1="2.60985" x2="10.20445" y2="2.62255" layer="21"/>
<rectangle x1="1.02235" y1="2.62255" x2="3.42265" y2="2.63525" layer="21"/>
<rectangle x1="3.71475" y1="2.62255" x2="3.95605" y2="2.63525" layer="21"/>
<rectangle x1="4.75615" y1="2.62255" x2="5.61975" y2="2.63525" layer="21"/>
<rectangle x1="6.97865" y1="2.62255" x2="7.76605" y2="2.63525" layer="21"/>
<rectangle x1="8.47725" y1="2.62255" x2="8.90905" y2="2.63525" layer="21"/>
<rectangle x1="8.95985" y1="2.62255" x2="10.17905" y2="2.63525" layer="21"/>
<rectangle x1="1.03505" y1="2.63525" x2="3.47345" y2="2.64795" layer="21"/>
<rectangle x1="3.71475" y1="2.63525" x2="3.95605" y2="2.64795" layer="21"/>
<rectangle x1="4.75615" y1="2.63525" x2="5.59435" y2="2.64795" layer="21"/>
<rectangle x1="6.99135" y1="2.63525" x2="7.74065" y2="2.64795" layer="21"/>
<rectangle x1="8.47725" y1="2.63525" x2="8.90905" y2="2.64795" layer="21"/>
<rectangle x1="8.97255" y1="2.63525" x2="10.15365" y2="2.64795" layer="21"/>
<rectangle x1="1.04775" y1="2.64795" x2="3.52425" y2="2.66065" layer="21"/>
<rectangle x1="3.70205" y1="2.64795" x2="3.95605" y2="2.66065" layer="21"/>
<rectangle x1="4.74345" y1="2.64795" x2="5.58165" y2="2.66065" layer="21"/>
<rectangle x1="7.00405" y1="2.64795" x2="7.71525" y2="2.66065" layer="21"/>
<rectangle x1="8.47725" y1="2.64795" x2="8.90905" y2="2.66065" layer="21"/>
<rectangle x1="8.97255" y1="2.64795" x2="10.12825" y2="2.66065" layer="21"/>
<rectangle x1="1.06045" y1="2.66065" x2="3.57505" y2="2.67335" layer="21"/>
<rectangle x1="3.70205" y1="2.66065" x2="3.95605" y2="2.67335" layer="21"/>
<rectangle x1="4.73075" y1="2.66065" x2="5.56895" y2="2.67335" layer="21"/>
<rectangle x1="7.01675" y1="2.66065" x2="7.68985" y2="2.67335" layer="21"/>
<rectangle x1="8.47725" y1="2.66065" x2="8.90905" y2="2.67335" layer="21"/>
<rectangle x1="8.97255" y1="2.66065" x2="10.10285" y2="2.67335" layer="21"/>
<rectangle x1="1.07315" y1="2.67335" x2="3.61315" y2="2.68605" layer="21"/>
<rectangle x1="3.70205" y1="2.67335" x2="3.95605" y2="2.68605" layer="21"/>
<rectangle x1="4.73075" y1="2.67335" x2="5.55625" y2="2.68605" layer="21"/>
<rectangle x1="7.02945" y1="2.67335" x2="7.67715" y2="2.68605" layer="21"/>
<rectangle x1="8.47725" y1="2.67335" x2="8.90905" y2="2.68605" layer="21"/>
<rectangle x1="8.97255" y1="2.67335" x2="10.07745" y2="2.68605" layer="21"/>
<rectangle x1="1.08585" y1="2.68605" x2="3.66395" y2="2.69875" layer="21"/>
<rectangle x1="3.68935" y1="2.68605" x2="3.95605" y2="2.69875" layer="21"/>
<rectangle x1="4.71805" y1="2.68605" x2="5.54355" y2="2.69875" layer="21"/>
<rectangle x1="7.04215" y1="2.68605" x2="7.65175" y2="2.69875" layer="21"/>
<rectangle x1="8.47725" y1="2.68605" x2="8.90905" y2="2.69875" layer="21"/>
<rectangle x1="8.97255" y1="2.68605" x2="10.05205" y2="2.69875" layer="21"/>
<rectangle x1="1.09855" y1="2.69875" x2="3.95605" y2="2.71145" layer="21"/>
<rectangle x1="4.71805" y1="2.69875" x2="5.53085" y2="2.71145" layer="21"/>
<rectangle x1="7.05485" y1="2.69875" x2="7.62635" y2="2.71145" layer="21"/>
<rectangle x1="8.47725" y1="2.69875" x2="8.90905" y2="2.71145" layer="21"/>
<rectangle x1="8.97255" y1="2.69875" x2="10.02665" y2="2.71145" layer="21"/>
<rectangle x1="1.11125" y1="2.71145" x2="3.95605" y2="2.72415" layer="21"/>
<rectangle x1="4.70535" y1="2.71145" x2="5.51815" y2="2.72415" layer="21"/>
<rectangle x1="7.06755" y1="2.71145" x2="7.61365" y2="2.72415" layer="21"/>
<rectangle x1="8.47725" y1="2.71145" x2="8.90905" y2="2.72415" layer="21"/>
<rectangle x1="8.97255" y1="2.71145" x2="10.01395" y2="2.72415" layer="21"/>
<rectangle x1="1.12395" y1="2.72415" x2="3.95605" y2="2.73685" layer="21"/>
<rectangle x1="4.70535" y1="2.72415" x2="5.50545" y2="2.73685" layer="21"/>
<rectangle x1="7.08025" y1="2.72415" x2="7.58825" y2="2.73685" layer="21"/>
<rectangle x1="8.47725" y1="2.72415" x2="8.90905" y2="2.73685" layer="21"/>
<rectangle x1="8.97255" y1="2.72415" x2="9.98855" y2="2.73685" layer="21"/>
<rectangle x1="1.13665" y1="2.73685" x2="3.95605" y2="2.74955" layer="21"/>
<rectangle x1="4.70535" y1="2.73685" x2="5.49275" y2="2.74955" layer="21"/>
<rectangle x1="7.09295" y1="2.73685" x2="7.56285" y2="2.74955" layer="21"/>
<rectangle x1="8.47725" y1="2.73685" x2="8.90905" y2="2.74955" layer="21"/>
<rectangle x1="8.97255" y1="2.73685" x2="9.97585" y2="2.74955" layer="21"/>
<rectangle x1="1.14935" y1="2.74955" x2="3.30835" y2="2.76225" layer="21"/>
<rectangle x1="3.33375" y1="2.74955" x2="3.95605" y2="2.76225" layer="21"/>
<rectangle x1="4.69265" y1="2.74955" x2="5.48005" y2="2.76225" layer="21"/>
<rectangle x1="7.10565" y1="2.74955" x2="7.53745" y2="2.76225" layer="21"/>
<rectangle x1="8.47725" y1="2.74955" x2="8.90905" y2="2.76225" layer="21"/>
<rectangle x1="8.98525" y1="2.74955" x2="9.95045" y2="2.76225" layer="21"/>
<rectangle x1="1.16205" y1="2.76225" x2="3.30835" y2="2.77495" layer="21"/>
<rectangle x1="3.37185" y1="2.76225" x2="3.95605" y2="2.77495" layer="21"/>
<rectangle x1="4.69265" y1="2.76225" x2="5.46735" y2="2.77495" layer="21"/>
<rectangle x1="7.11835" y1="2.76225" x2="7.52475" y2="2.77495" layer="21"/>
<rectangle x1="8.47725" y1="2.76225" x2="8.90905" y2="2.77495" layer="21"/>
<rectangle x1="8.98525" y1="2.76225" x2="9.93775" y2="2.77495" layer="21"/>
<rectangle x1="1.17475" y1="2.77495" x2="3.30835" y2="2.78765" layer="21"/>
<rectangle x1="3.42265" y1="2.77495" x2="3.95605" y2="2.78765" layer="21"/>
<rectangle x1="4.67995" y1="2.77495" x2="5.45465" y2="2.78765" layer="21"/>
<rectangle x1="7.13105" y1="2.77495" x2="7.49935" y2="2.78765" layer="21"/>
<rectangle x1="8.47725" y1="2.77495" x2="8.92175" y2="2.78765" layer="21"/>
<rectangle x1="8.97255" y1="2.77495" x2="9.91235" y2="2.78765" layer="21"/>
<rectangle x1="1.18745" y1="2.78765" x2="3.29565" y2="2.80035" layer="21"/>
<rectangle x1="3.47345" y1="2.78765" x2="3.95605" y2="2.80035" layer="21"/>
<rectangle x1="4.67995" y1="2.78765" x2="5.44195" y2="2.80035" layer="21"/>
<rectangle x1="7.14375" y1="2.78765" x2="7.47395" y2="2.80035" layer="21"/>
<rectangle x1="8.47725" y1="2.78765" x2="8.93445" y2="2.80035" layer="21"/>
<rectangle x1="8.95985" y1="2.78765" x2="9.89965" y2="2.80035" layer="21"/>
<rectangle x1="1.20015" y1="2.80035" x2="3.29565" y2="2.81305" layer="21"/>
<rectangle x1="3.51155" y1="2.80035" x2="3.95605" y2="2.81305" layer="21"/>
<rectangle x1="4.66725" y1="2.80035" x2="5.44195" y2="2.81305" layer="21"/>
<rectangle x1="7.15645" y1="2.80035" x2="7.46125" y2="2.81305" layer="21"/>
<rectangle x1="8.47725" y1="2.80035" x2="9.88695" y2="2.81305" layer="21"/>
<rectangle x1="1.21285" y1="2.81305" x2="3.29565" y2="2.82575" layer="21"/>
<rectangle x1="3.56235" y1="2.81305" x2="3.95605" y2="2.82575" layer="21"/>
<rectangle x1="4.66725" y1="2.81305" x2="5.42925" y2="2.82575" layer="21"/>
<rectangle x1="7.15645" y1="2.81305" x2="7.43585" y2="2.82575" layer="21"/>
<rectangle x1="8.47725" y1="2.81305" x2="9.86155" y2="2.82575" layer="21"/>
<rectangle x1="1.22555" y1="2.82575" x2="3.29565" y2="2.83845" layer="21"/>
<rectangle x1="3.61315" y1="2.82575" x2="3.95605" y2="2.83845" layer="21"/>
<rectangle x1="4.66725" y1="2.82575" x2="5.41655" y2="2.83845" layer="21"/>
<rectangle x1="7.16915" y1="2.82575" x2="7.41045" y2="2.83845" layer="21"/>
<rectangle x1="8.47725" y1="2.82575" x2="9.84885" y2="2.83845" layer="21"/>
<rectangle x1="1.23825" y1="2.83845" x2="3.28295" y2="2.85115" layer="21"/>
<rectangle x1="3.62585" y1="2.83845" x2="3.95605" y2="2.85115" layer="21"/>
<rectangle x1="4.65455" y1="2.83845" x2="5.40385" y2="2.85115" layer="21"/>
<rectangle x1="7.18185" y1="2.83845" x2="7.38505" y2="2.85115" layer="21"/>
<rectangle x1="8.47725" y1="2.83845" x2="9.83615" y2="2.85115" layer="21"/>
<rectangle x1="1.25095" y1="2.85115" x2="3.28295" y2="2.86385" layer="21"/>
<rectangle x1="3.62585" y1="2.85115" x2="3.95605" y2="2.86385" layer="21"/>
<rectangle x1="4.65455" y1="2.85115" x2="5.40385" y2="2.86385" layer="21"/>
<rectangle x1="7.18185" y1="2.85115" x2="7.37235" y2="2.86385" layer="21"/>
<rectangle x1="8.47725" y1="2.85115" x2="9.82345" y2="2.86385" layer="21"/>
<rectangle x1="1.26365" y1="2.86385" x2="3.27025" y2="2.87655" layer="21"/>
<rectangle x1="3.62585" y1="2.86385" x2="3.95605" y2="2.87655" layer="21"/>
<rectangle x1="4.64185" y1="2.86385" x2="5.39115" y2="2.87655" layer="21"/>
<rectangle x1="7.19455" y1="2.86385" x2="7.34695" y2="2.87655" layer="21"/>
<rectangle x1="8.47725" y1="2.86385" x2="9.79805" y2="2.87655" layer="21"/>
<rectangle x1="1.27635" y1="2.87655" x2="3.27025" y2="2.88925" layer="21"/>
<rectangle x1="3.61315" y1="2.87655" x2="3.95605" y2="2.88925" layer="21"/>
<rectangle x1="4.64185" y1="2.87655" x2="5.37845" y2="2.88925" layer="21"/>
<rectangle x1="7.20725" y1="2.87655" x2="7.32155" y2="2.88925" layer="21"/>
<rectangle x1="8.47725" y1="2.87655" x2="9.78535" y2="2.88925" layer="21"/>
<rectangle x1="1.28905" y1="2.88925" x2="3.27025" y2="2.90195" layer="21"/>
<rectangle x1="3.61315" y1="2.88925" x2="3.95605" y2="2.90195" layer="21"/>
<rectangle x1="4.64185" y1="2.88925" x2="5.37845" y2="2.90195" layer="21"/>
<rectangle x1="7.20725" y1="2.88925" x2="7.29615" y2="2.90195" layer="21"/>
<rectangle x1="8.47725" y1="2.88925" x2="9.77265" y2="2.90195" layer="21"/>
<rectangle x1="1.30175" y1="2.90195" x2="3.28295" y2="2.91465" layer="21"/>
<rectangle x1="3.60045" y1="2.90195" x2="3.95605" y2="2.91465" layer="21"/>
<rectangle x1="4.62915" y1="2.90195" x2="5.36575" y2="2.91465" layer="21"/>
<rectangle x1="7.21995" y1="2.90195" x2="7.28345" y2="2.91465" layer="21"/>
<rectangle x1="8.47725" y1="2.90195" x2="9.75995" y2="2.91465" layer="21"/>
<rectangle x1="1.31445" y1="2.91465" x2="3.29565" y2="2.92735" layer="21"/>
<rectangle x1="3.60045" y1="2.91465" x2="3.95605" y2="2.92735" layer="21"/>
<rectangle x1="4.62915" y1="2.91465" x2="5.35305" y2="2.92735" layer="21"/>
<rectangle x1="7.23265" y1="2.91465" x2="7.25805" y2="2.92735" layer="21"/>
<rectangle x1="8.47725" y1="2.91465" x2="9.74725" y2="2.92735" layer="21"/>
<rectangle x1="1.32715" y1="2.92735" x2="3.30835" y2="2.94005" layer="21"/>
<rectangle x1="3.58775" y1="2.92735" x2="3.95605" y2="2.94005" layer="21"/>
<rectangle x1="4.62915" y1="2.92735" x2="5.35305" y2="2.94005" layer="21"/>
<rectangle x1="8.47725" y1="2.92735" x2="9.73455" y2="2.94005" layer="21"/>
<rectangle x1="1.33985" y1="2.94005" x2="3.32105" y2="2.95275" layer="21"/>
<rectangle x1="3.57505" y1="2.94005" x2="3.95605" y2="2.95275" layer="21"/>
<rectangle x1="4.61645" y1="2.94005" x2="5.34035" y2="2.95275" layer="21"/>
<rectangle x1="8.47725" y1="2.94005" x2="9.72185" y2="2.95275" layer="21"/>
<rectangle x1="1.35255" y1="2.95275" x2="3.33375" y2="2.96545" layer="21"/>
<rectangle x1="3.57505" y1="2.95275" x2="3.95605" y2="2.96545" layer="21"/>
<rectangle x1="4.61645" y1="2.95275" x2="5.34035" y2="2.96545" layer="21"/>
<rectangle x1="8.47725" y1="2.95275" x2="8.98525" y2="2.96545" layer="21"/>
<rectangle x1="9.01065" y1="2.95275" x2="9.70915" y2="2.96545" layer="21"/>
<rectangle x1="1.36525" y1="2.96545" x2="3.34645" y2="2.97815" layer="21"/>
<rectangle x1="3.56235" y1="2.96545" x2="3.95605" y2="2.97815" layer="21"/>
<rectangle x1="4.61645" y1="2.96545" x2="5.32765" y2="2.97815" layer="21"/>
<rectangle x1="8.47725" y1="2.96545" x2="8.97255" y2="2.97815" layer="21"/>
<rectangle x1="9.02335" y1="2.96545" x2="9.69645" y2="2.97815" layer="21"/>
<rectangle x1="1.37795" y1="2.97815" x2="3.35915" y2="2.99085" layer="21"/>
<rectangle x1="3.56235" y1="2.97815" x2="3.95605" y2="2.99085" layer="21"/>
<rectangle x1="4.60375" y1="2.97815" x2="5.32765" y2="2.99085" layer="21"/>
<rectangle x1="8.47725" y1="2.97815" x2="8.97255" y2="2.99085" layer="21"/>
<rectangle x1="9.03605" y1="2.97815" x2="9.68375" y2="2.99085" layer="21"/>
<rectangle x1="1.39065" y1="2.99085" x2="3.37185" y2="3.00355" layer="21"/>
<rectangle x1="3.54965" y1="2.99085" x2="3.95605" y2="3.00355" layer="21"/>
<rectangle x1="4.60375" y1="2.99085" x2="5.31495" y2="3.00355" layer="21"/>
<rectangle x1="8.47725" y1="2.99085" x2="8.97255" y2="3.00355" layer="21"/>
<rectangle x1="9.03605" y1="2.99085" x2="9.67105" y2="3.00355" layer="21"/>
<rectangle x1="1.40335" y1="3.00355" x2="3.38455" y2="3.01625" layer="21"/>
<rectangle x1="3.53695" y1="3.00355" x2="3.95605" y2="3.01625" layer="21"/>
<rectangle x1="4.60375" y1="3.00355" x2="5.31495" y2="3.01625" layer="21"/>
<rectangle x1="8.47725" y1="3.00355" x2="8.97255" y2="3.01625" layer="21"/>
<rectangle x1="9.03605" y1="3.00355" x2="9.65835" y2="3.01625" layer="21"/>
<rectangle x1="1.41605" y1="3.01625" x2="3.39725" y2="3.02895" layer="21"/>
<rectangle x1="3.53695" y1="3.01625" x2="3.95605" y2="3.02895" layer="21"/>
<rectangle x1="4.60375" y1="3.01625" x2="5.30225" y2="3.02895" layer="21"/>
<rectangle x1="8.47725" y1="3.01625" x2="8.97255" y2="3.02895" layer="21"/>
<rectangle x1="9.04875" y1="3.01625" x2="9.64565" y2="3.02895" layer="21"/>
<rectangle x1="1.42875" y1="3.02895" x2="3.40995" y2="3.04165" layer="21"/>
<rectangle x1="3.52425" y1="3.02895" x2="3.95605" y2="3.04165" layer="21"/>
<rectangle x1="4.59105" y1="3.02895" x2="5.30225" y2="3.04165" layer="21"/>
<rectangle x1="8.47725" y1="3.02895" x2="8.97255" y2="3.04165" layer="21"/>
<rectangle x1="9.04875" y1="3.02895" x2="9.63295" y2="3.04165" layer="21"/>
<rectangle x1="1.44145" y1="3.04165" x2="3.42265" y2="3.05435" layer="21"/>
<rectangle x1="3.51155" y1="3.04165" x2="3.95605" y2="3.05435" layer="21"/>
<rectangle x1="4.59105" y1="3.04165" x2="5.28955" y2="3.05435" layer="21"/>
<rectangle x1="8.47725" y1="3.04165" x2="8.98525" y2="3.05435" layer="21"/>
<rectangle x1="9.04875" y1="3.04165" x2="9.62025" y2="3.05435" layer="21"/>
<rectangle x1="1.45415" y1="3.05435" x2="3.43535" y2="3.06705" layer="21"/>
<rectangle x1="3.51155" y1="3.05435" x2="3.95605" y2="3.06705" layer="21"/>
<rectangle x1="4.59105" y1="3.05435" x2="5.28955" y2="3.06705" layer="21"/>
<rectangle x1="8.47725" y1="3.05435" x2="8.98525" y2="3.06705" layer="21"/>
<rectangle x1="9.06145" y1="3.05435" x2="9.60755" y2="3.06705" layer="21"/>
<rectangle x1="1.46685" y1="3.06705" x2="3.44805" y2="3.07975" layer="21"/>
<rectangle x1="3.49885" y1="3.06705" x2="3.95605" y2="3.07975" layer="21"/>
<rectangle x1="4.59105" y1="3.06705" x2="5.28955" y2="3.07975" layer="21"/>
<rectangle x1="8.47725" y1="3.06705" x2="8.98525" y2="3.07975" layer="21"/>
<rectangle x1="9.06145" y1="3.06705" x2="9.60755" y2="3.07975" layer="21"/>
<rectangle x1="1.47955" y1="3.07975" x2="3.46075" y2="3.09245" layer="21"/>
<rectangle x1="3.48615" y1="3.07975" x2="3.95605" y2="3.09245" layer="21"/>
<rectangle x1="4.57835" y1="3.07975" x2="5.27685" y2="3.09245" layer="21"/>
<rectangle x1="8.47725" y1="3.07975" x2="8.99795" y2="3.09245" layer="21"/>
<rectangle x1="9.06145" y1="3.07975" x2="9.59485" y2="3.09245" layer="21"/>
<rectangle x1="1.49225" y1="3.09245" x2="3.95605" y2="3.10515" layer="21"/>
<rectangle x1="4.57835" y1="3.09245" x2="5.27685" y2="3.10515" layer="21"/>
<rectangle x1="8.47725" y1="3.09245" x2="8.99795" y2="3.10515" layer="21"/>
<rectangle x1="9.07415" y1="3.09245" x2="9.58215" y2="3.10515" layer="21"/>
<rectangle x1="1.50495" y1="3.10515" x2="3.95605" y2="3.11785" layer="21"/>
<rectangle x1="4.57835" y1="3.10515" x2="5.26415" y2="3.11785" layer="21"/>
<rectangle x1="8.47725" y1="3.10515" x2="9.01065" y2="3.11785" layer="21"/>
<rectangle x1="9.06145" y1="3.10515" x2="9.56945" y2="3.11785" layer="21"/>
<rectangle x1="1.51765" y1="3.11785" x2="3.95605" y2="3.13055" layer="21"/>
<rectangle x1="4.57835" y1="3.11785" x2="5.26415" y2="3.13055" layer="21"/>
<rectangle x1="8.47725" y1="3.11785" x2="9.01065" y2="3.13055" layer="21"/>
<rectangle x1="9.06145" y1="3.11785" x2="9.55675" y2="3.13055" layer="21"/>
<rectangle x1="1.53035" y1="3.13055" x2="3.95605" y2="3.14325" layer="21"/>
<rectangle x1="4.57835" y1="3.13055" x2="5.26415" y2="3.14325" layer="21"/>
<rectangle x1="8.47725" y1="3.13055" x2="9.55675" y2="3.14325" layer="21"/>
<rectangle x1="1.54305" y1="3.14325" x2="3.95605" y2="3.15595" layer="21"/>
<rectangle x1="4.56565" y1="3.14325" x2="5.25145" y2="3.15595" layer="21"/>
<rectangle x1="8.47725" y1="3.14325" x2="9.54405" y2="3.15595" layer="21"/>
<rectangle x1="1.55575" y1="3.15595" x2="3.95605" y2="3.16865" layer="21"/>
<rectangle x1="4.56565" y1="3.15595" x2="5.25145" y2="3.16865" layer="21"/>
<rectangle x1="8.47725" y1="3.15595" x2="9.53135" y2="3.16865" layer="21"/>
<rectangle x1="1.56845" y1="3.16865" x2="3.95605" y2="3.18135" layer="21"/>
<rectangle x1="4.56565" y1="3.16865" x2="5.25145" y2="3.18135" layer="21"/>
<rectangle x1="8.47725" y1="3.16865" x2="9.51865" y2="3.18135" layer="21"/>
<rectangle x1="1.58115" y1="3.18135" x2="3.95605" y2="3.19405" layer="21"/>
<rectangle x1="4.56565" y1="3.18135" x2="5.23875" y2="3.19405" layer="21"/>
<rectangle x1="8.47725" y1="3.18135" x2="9.51865" y2="3.19405" layer="21"/>
<rectangle x1="1.59385" y1="3.19405" x2="3.95605" y2="3.20675" layer="21"/>
<rectangle x1="4.56565" y1="3.19405" x2="5.23875" y2="3.20675" layer="21"/>
<rectangle x1="8.47725" y1="3.19405" x2="9.50595" y2="3.20675" layer="21"/>
<rectangle x1="1.60655" y1="3.20675" x2="3.95605" y2="3.21945" layer="21"/>
<rectangle x1="4.55295" y1="3.20675" x2="5.23875" y2="3.21945" layer="21"/>
<rectangle x1="8.47725" y1="3.20675" x2="9.49325" y2="3.21945" layer="21"/>
<rectangle x1="1.61925" y1="3.21945" x2="3.95605" y2="3.23215" layer="21"/>
<rectangle x1="4.55295" y1="3.21945" x2="5.23875" y2="3.23215" layer="21"/>
<rectangle x1="8.47725" y1="3.21945" x2="9.49325" y2="3.23215" layer="21"/>
<rectangle x1="1.63195" y1="3.23215" x2="3.95605" y2="3.24485" layer="21"/>
<rectangle x1="4.55295" y1="3.23215" x2="5.22605" y2="3.24485" layer="21"/>
<rectangle x1="8.47725" y1="3.23215" x2="9.48055" y2="3.24485" layer="21"/>
<rectangle x1="1.64465" y1="3.24485" x2="3.95605" y2="3.25755" layer="21"/>
<rectangle x1="4.55295" y1="3.24485" x2="5.22605" y2="3.25755" layer="21"/>
<rectangle x1="8.47725" y1="3.24485" x2="9.46785" y2="3.25755" layer="21"/>
<rectangle x1="1.65735" y1="3.25755" x2="3.95605" y2="3.27025" layer="21"/>
<rectangle x1="4.55295" y1="3.25755" x2="5.22605" y2="3.27025" layer="21"/>
<rectangle x1="8.47725" y1="3.25755" x2="9.46785" y2="3.27025" layer="21"/>
<rectangle x1="1.67005" y1="3.27025" x2="3.95605" y2="3.28295" layer="21"/>
<rectangle x1="4.55295" y1="3.27025" x2="5.22605" y2="3.28295" layer="21"/>
<rectangle x1="8.47725" y1="3.27025" x2="9.45515" y2="3.28295" layer="21"/>
<rectangle x1="1.68275" y1="3.28295" x2="3.95605" y2="3.29565" layer="21"/>
<rectangle x1="4.55295" y1="3.28295" x2="5.22605" y2="3.29565" layer="21"/>
<rectangle x1="8.47725" y1="3.28295" x2="9.09955" y2="3.29565" layer="21"/>
<rectangle x1="9.15035" y1="3.28295" x2="9.44245" y2="3.29565" layer="21"/>
<rectangle x1="1.69545" y1="3.29565" x2="3.95605" y2="3.30835" layer="21"/>
<rectangle x1="4.55295" y1="3.29565" x2="5.21335" y2="3.30835" layer="21"/>
<rectangle x1="8.47725" y1="3.29565" x2="9.09955" y2="3.30835" layer="21"/>
<rectangle x1="9.16305" y1="3.29565" x2="9.44245" y2="3.30835" layer="21"/>
<rectangle x1="1.70815" y1="3.30835" x2="3.95605" y2="3.32105" layer="21"/>
<rectangle x1="4.55295" y1="3.30835" x2="5.21335" y2="3.32105" layer="21"/>
<rectangle x1="8.47725" y1="3.30835" x2="9.08685" y2="3.32105" layer="21"/>
<rectangle x1="9.16305" y1="3.30835" x2="9.42975" y2="3.32105" layer="21"/>
<rectangle x1="1.72085" y1="3.32105" x2="3.95605" y2="3.33375" layer="21"/>
<rectangle x1="4.55295" y1="3.32105" x2="5.21335" y2="3.33375" layer="21"/>
<rectangle x1="8.47725" y1="3.32105" x2="9.09955" y2="3.33375" layer="21"/>
<rectangle x1="9.17575" y1="3.32105" x2="9.42975" y2="3.33375" layer="21"/>
<rectangle x1="1.73355" y1="3.33375" x2="3.95605" y2="3.34645" layer="21"/>
<rectangle x1="4.54025" y1="3.33375" x2="5.21335" y2="3.34645" layer="21"/>
<rectangle x1="8.47725" y1="3.33375" x2="9.09955" y2="3.34645" layer="21"/>
<rectangle x1="9.17575" y1="3.33375" x2="9.41705" y2="3.34645" layer="21"/>
<rectangle x1="1.74625" y1="3.34645" x2="3.95605" y2="3.35915" layer="21"/>
<rectangle x1="4.54025" y1="3.34645" x2="5.21335" y2="3.35915" layer="21"/>
<rectangle x1="8.47725" y1="3.34645" x2="9.11225" y2="3.35915" layer="21"/>
<rectangle x1="9.18845" y1="3.34645" x2="9.41705" y2="3.35915" layer="21"/>
<rectangle x1="1.75895" y1="3.35915" x2="3.95605" y2="3.37185" layer="21"/>
<rectangle x1="4.54025" y1="3.35915" x2="5.21335" y2="3.37185" layer="21"/>
<rectangle x1="8.47725" y1="3.35915" x2="9.11225" y2="3.37185" layer="21"/>
<rectangle x1="9.18845" y1="3.35915" x2="9.40435" y2="3.37185" layer="21"/>
<rectangle x1="1.77165" y1="3.37185" x2="3.95605" y2="3.38455" layer="21"/>
<rectangle x1="4.54025" y1="3.37185" x2="5.21335" y2="3.38455" layer="21"/>
<rectangle x1="8.47725" y1="3.37185" x2="9.12495" y2="3.38455" layer="21"/>
<rectangle x1="9.20115" y1="3.37185" x2="9.40435" y2="3.38455" layer="21"/>
<rectangle x1="1.78435" y1="3.38455" x2="3.95605" y2="3.39725" layer="21"/>
<rectangle x1="4.54025" y1="3.38455" x2="5.21335" y2="3.39725" layer="21"/>
<rectangle x1="8.47725" y1="3.38455" x2="9.12495" y2="3.39725" layer="21"/>
<rectangle x1="9.20115" y1="3.38455" x2="9.39165" y2="3.39725" layer="21"/>
<rectangle x1="1.79705" y1="3.39725" x2="3.95605" y2="3.40995" layer="21"/>
<rectangle x1="4.54025" y1="3.39725" x2="5.21335" y2="3.40995" layer="21"/>
<rectangle x1="8.47725" y1="3.39725" x2="9.13765" y2="3.40995" layer="21"/>
<rectangle x1="9.21385" y1="3.39725" x2="9.39165" y2="3.40995" layer="21"/>
<rectangle x1="1.80975" y1="3.40995" x2="3.95605" y2="3.42265" layer="21"/>
<rectangle x1="4.54025" y1="3.40995" x2="5.21335" y2="3.42265" layer="21"/>
<rectangle x1="8.47725" y1="3.40995" x2="9.13765" y2="3.42265" layer="21"/>
<rectangle x1="9.21385" y1="3.40995" x2="9.37895" y2="3.42265" layer="21"/>
<rectangle x1="1.82245" y1="3.42265" x2="3.95605" y2="3.43535" layer="21"/>
<rectangle x1="4.54025" y1="3.42265" x2="5.21335" y2="3.43535" layer="21"/>
<rectangle x1="8.47725" y1="3.42265" x2="9.15035" y2="3.43535" layer="21"/>
<rectangle x1="9.21385" y1="3.42265" x2="9.37895" y2="3.43535" layer="21"/>
<rectangle x1="1.83515" y1="3.43535" x2="3.95605" y2="3.44805" layer="21"/>
<rectangle x1="4.54025" y1="3.43535" x2="5.21335" y2="3.44805" layer="21"/>
<rectangle x1="8.47725" y1="3.43535" x2="9.15035" y2="3.44805" layer="21"/>
<rectangle x1="9.20115" y1="3.43535" x2="9.36625" y2="3.44805" layer="21"/>
<rectangle x1="1.84785" y1="3.44805" x2="3.95605" y2="3.46075" layer="21"/>
<rectangle x1="4.54025" y1="3.44805" x2="5.21335" y2="3.46075" layer="21"/>
<rectangle x1="8.47725" y1="3.44805" x2="9.36625" y2="3.46075" layer="21"/>
<rectangle x1="1.86055" y1="3.46075" x2="3.95605" y2="3.47345" layer="21"/>
<rectangle x1="4.54025" y1="3.46075" x2="5.21335" y2="3.47345" layer="21"/>
<rectangle x1="8.47725" y1="3.46075" x2="9.35355" y2="3.47345" layer="21"/>
<rectangle x1="1.87325" y1="3.47345" x2="3.95605" y2="3.48615" layer="21"/>
<rectangle x1="4.54025" y1="3.47345" x2="5.21335" y2="3.48615" layer="21"/>
<rectangle x1="8.47725" y1="3.47345" x2="9.35355" y2="3.48615" layer="21"/>
<rectangle x1="1.88595" y1="3.48615" x2="3.95605" y2="3.49885" layer="21"/>
<rectangle x1="4.54025" y1="3.48615" x2="5.21335" y2="3.49885" layer="21"/>
<rectangle x1="8.47725" y1="3.48615" x2="9.34085" y2="3.49885" layer="21"/>
<rectangle x1="1.89865" y1="3.49885" x2="3.95605" y2="3.51155" layer="21"/>
<rectangle x1="4.54025" y1="3.49885" x2="5.21335" y2="3.51155" layer="21"/>
<rectangle x1="8.47725" y1="3.49885" x2="9.34085" y2="3.51155" layer="21"/>
<rectangle x1="1.91135" y1="3.51155" x2="3.95605" y2="3.52425" layer="21"/>
<rectangle x1="4.54025" y1="3.51155" x2="5.21335" y2="3.52425" layer="21"/>
<rectangle x1="8.47725" y1="3.51155" x2="9.34085" y2="3.52425" layer="21"/>
<rectangle x1="1.92405" y1="3.52425" x2="3.95605" y2="3.53695" layer="21"/>
<rectangle x1="4.54025" y1="3.52425" x2="5.21335" y2="3.53695" layer="21"/>
<rectangle x1="8.47725" y1="3.52425" x2="9.32815" y2="3.53695" layer="21"/>
<rectangle x1="1.93675" y1="3.53695" x2="3.95605" y2="3.54965" layer="21"/>
<rectangle x1="4.54025" y1="3.53695" x2="5.21335" y2="3.54965" layer="21"/>
<rectangle x1="8.47725" y1="3.53695" x2="9.32815" y2="3.54965" layer="21"/>
<rectangle x1="1.94945" y1="3.54965" x2="3.95605" y2="3.56235" layer="21"/>
<rectangle x1="4.54025" y1="3.54965" x2="5.21335" y2="3.56235" layer="21"/>
<rectangle x1="8.47725" y1="3.54965" x2="9.31545" y2="3.56235" layer="21"/>
<rectangle x1="1.96215" y1="3.56235" x2="3.95605" y2="3.57505" layer="21"/>
<rectangle x1="4.54025" y1="3.56235" x2="5.21335" y2="3.57505" layer="21"/>
<rectangle x1="8.47725" y1="3.56235" x2="9.31545" y2="3.57505" layer="21"/>
<rectangle x1="1.97485" y1="3.57505" x2="3.95605" y2="3.58775" layer="21"/>
<rectangle x1="4.54025" y1="3.57505" x2="5.21335" y2="3.58775" layer="21"/>
<rectangle x1="8.47725" y1="3.57505" x2="9.31545" y2="3.58775" layer="21"/>
<rectangle x1="1.98755" y1="3.58775" x2="3.95605" y2="3.60045" layer="21"/>
<rectangle x1="4.54025" y1="3.58775" x2="5.21335" y2="3.60045" layer="21"/>
<rectangle x1="8.47725" y1="3.58775" x2="9.26465" y2="3.60045" layer="21"/>
<rectangle x1="2.00025" y1="3.60045" x2="3.95605" y2="3.61315" layer="21"/>
<rectangle x1="4.54025" y1="3.60045" x2="5.21335" y2="3.61315" layer="21"/>
<rectangle x1="8.47725" y1="3.60045" x2="9.26465" y2="3.61315" layer="21"/>
<rectangle x1="2.01295" y1="3.61315" x2="3.95605" y2="3.62585" layer="21"/>
<rectangle x1="4.55295" y1="3.61315" x2="5.21335" y2="3.62585" layer="21"/>
<rectangle x1="8.47725" y1="3.61315" x2="9.25195" y2="3.62585" layer="21"/>
<rectangle x1="2.02565" y1="3.62585" x2="3.95605" y2="3.63855" layer="21"/>
<rectangle x1="4.55295" y1="3.62585" x2="5.21335" y2="3.63855" layer="21"/>
<rectangle x1="8.47725" y1="3.62585" x2="9.26465" y2="3.63855" layer="21"/>
<rectangle x1="2.03835" y1="3.63855" x2="3.95605" y2="3.65125" layer="21"/>
<rectangle x1="4.55295" y1="3.63855" x2="5.22605" y2="3.65125" layer="21"/>
<rectangle x1="8.47725" y1="3.63855" x2="9.26465" y2="3.65125" layer="21"/>
<rectangle x1="2.05105" y1="3.65125" x2="3.95605" y2="3.66395" layer="21"/>
<rectangle x1="4.55295" y1="3.65125" x2="5.22605" y2="3.66395" layer="21"/>
<rectangle x1="8.47725" y1="3.65125" x2="9.27735" y2="3.66395" layer="21"/>
<rectangle x1="2.06375" y1="3.66395" x2="3.95605" y2="3.67665" layer="21"/>
<rectangle x1="4.55295" y1="3.66395" x2="5.22605" y2="3.67665" layer="21"/>
<rectangle x1="8.47725" y1="3.66395" x2="9.27735" y2="3.67665" layer="21"/>
<rectangle x1="2.07645" y1="3.67665" x2="3.95605" y2="3.68935" layer="21"/>
<rectangle x1="4.55295" y1="3.67665" x2="5.22605" y2="3.68935" layer="21"/>
<rectangle x1="8.47725" y1="3.67665" x2="9.27735" y2="3.68935" layer="21"/>
<rectangle x1="2.08915" y1="3.68935" x2="3.95605" y2="3.70205" layer="21"/>
<rectangle x1="4.55295" y1="3.68935" x2="5.22605" y2="3.70205" layer="21"/>
<rectangle x1="7.94385" y1="3.68935" x2="7.98195" y2="3.70205" layer="21"/>
<rectangle x1="8.47725" y1="3.68935" x2="9.27735" y2="3.70205" layer="21"/>
<rectangle x1="2.10185" y1="3.70205" x2="3.95605" y2="3.71475" layer="21"/>
<rectangle x1="4.55295" y1="3.70205" x2="5.22605" y2="3.71475" layer="21"/>
<rectangle x1="7.90575" y1="3.70205" x2="7.98195" y2="3.71475" layer="21"/>
<rectangle x1="8.47725" y1="3.70205" x2="9.27735" y2="3.71475" layer="21"/>
<rectangle x1="2.11455" y1="3.71475" x2="3.95605" y2="3.72745" layer="21"/>
<rectangle x1="4.55295" y1="3.71475" x2="5.23875" y2="3.72745" layer="21"/>
<rectangle x1="7.85495" y1="3.71475" x2="7.98195" y2="3.72745" layer="21"/>
<rectangle x1="8.47725" y1="3.71475" x2="9.26465" y2="3.72745" layer="21"/>
<rectangle x1="2.12725" y1="3.72745" x2="3.95605" y2="3.74015" layer="21"/>
<rectangle x1="4.56565" y1="3.72745" x2="5.23875" y2="3.74015" layer="21"/>
<rectangle x1="7.81685" y1="3.72745" x2="7.96925" y2="3.74015" layer="21"/>
<rectangle x1="8.47725" y1="3.72745" x2="9.26465" y2="3.74015" layer="21"/>
<rectangle x1="2.13995" y1="3.74015" x2="3.95605" y2="3.75285" layer="21"/>
<rectangle x1="4.56565" y1="3.74015" x2="5.23875" y2="3.75285" layer="21"/>
<rectangle x1="7.77875" y1="3.74015" x2="7.96925" y2="3.75285" layer="21"/>
<rectangle x1="8.47725" y1="3.74015" x2="9.26465" y2="3.75285" layer="21"/>
<rectangle x1="2.15265" y1="3.75285" x2="3.95605" y2="3.76555" layer="21"/>
<rectangle x1="4.56565" y1="3.75285" x2="5.23875" y2="3.76555" layer="21"/>
<rectangle x1="7.72795" y1="3.75285" x2="7.96925" y2="3.76555" layer="21"/>
<rectangle x1="8.47725" y1="3.75285" x2="9.25195" y2="3.76555" layer="21"/>
<rectangle x1="2.16535" y1="3.76555" x2="3.95605" y2="3.77825" layer="21"/>
<rectangle x1="4.56565" y1="3.76555" x2="5.25145" y2="3.77825" layer="21"/>
<rectangle x1="7.68985" y1="3.76555" x2="7.96925" y2="3.77825" layer="21"/>
<rectangle x1="8.47725" y1="3.76555" x2="9.25195" y2="3.77825" layer="21"/>
<rectangle x1="2.17805" y1="3.77825" x2="3.95605" y2="3.79095" layer="21"/>
<rectangle x1="4.56565" y1="3.77825" x2="5.25145" y2="3.79095" layer="21"/>
<rectangle x1="7.65175" y1="3.77825" x2="7.95655" y2="3.79095" layer="21"/>
<rectangle x1="8.47725" y1="3.77825" x2="9.25195" y2="3.79095" layer="21"/>
<rectangle x1="2.19075" y1="3.79095" x2="3.95605" y2="3.80365" layer="21"/>
<rectangle x1="4.56565" y1="3.79095" x2="5.25145" y2="3.80365" layer="21"/>
<rectangle x1="7.61365" y1="3.79095" x2="7.95655" y2="3.80365" layer="21"/>
<rectangle x1="8.47725" y1="3.79095" x2="9.25195" y2="3.80365" layer="21"/>
<rectangle x1="2.20345" y1="3.80365" x2="3.95605" y2="3.81635" layer="21"/>
<rectangle x1="4.57835" y1="3.80365" x2="5.26415" y2="3.81635" layer="21"/>
<rectangle x1="7.57555" y1="3.80365" x2="7.95655" y2="3.81635" layer="21"/>
<rectangle x1="8.47725" y1="3.80365" x2="9.25195" y2="3.81635" layer="21"/>
<rectangle x1="2.21615" y1="3.81635" x2="3.95605" y2="3.82905" layer="21"/>
<rectangle x1="4.57835" y1="3.81635" x2="5.26415" y2="3.82905" layer="21"/>
<rectangle x1="7.52475" y1="3.81635" x2="7.95655" y2="3.82905" layer="21"/>
<rectangle x1="8.47725" y1="3.81635" x2="9.23925" y2="3.82905" layer="21"/>
<rectangle x1="2.22885" y1="3.82905" x2="3.95605" y2="3.84175" layer="21"/>
<rectangle x1="4.57835" y1="3.82905" x2="5.26415" y2="3.84175" layer="21"/>
<rectangle x1="7.48665" y1="3.82905" x2="7.95655" y2="3.84175" layer="21"/>
<rectangle x1="8.47725" y1="3.82905" x2="9.23925" y2="3.84175" layer="21"/>
<rectangle x1="2.24155" y1="3.84175" x2="3.95605" y2="3.85445" layer="21"/>
<rectangle x1="4.57835" y1="3.84175" x2="5.27685" y2="3.85445" layer="21"/>
<rectangle x1="7.44855" y1="3.84175" x2="7.94385" y2="3.85445" layer="21"/>
<rectangle x1="8.47725" y1="3.84175" x2="9.23925" y2="3.85445" layer="21"/>
<rectangle x1="2.25425" y1="3.85445" x2="3.95605" y2="3.86715" layer="21"/>
<rectangle x1="4.57835" y1="3.85445" x2="5.27685" y2="3.86715" layer="21"/>
<rectangle x1="7.39775" y1="3.85445" x2="7.94385" y2="3.86715" layer="21"/>
<rectangle x1="8.47725" y1="3.85445" x2="9.23925" y2="3.86715" layer="21"/>
<rectangle x1="2.26695" y1="3.86715" x2="3.95605" y2="3.87985" layer="21"/>
<rectangle x1="4.59105" y1="3.86715" x2="5.27685" y2="3.87985" layer="21"/>
<rectangle x1="7.35965" y1="3.86715" x2="7.94385" y2="3.87985" layer="21"/>
<rectangle x1="8.47725" y1="3.86715" x2="9.23925" y2="3.87985" layer="21"/>
<rectangle x1="2.27965" y1="3.87985" x2="3.95605" y2="3.89255" layer="21"/>
<rectangle x1="4.59105" y1="3.87985" x2="5.28955" y2="3.89255" layer="21"/>
<rectangle x1="7.32155" y1="3.87985" x2="7.94385" y2="3.89255" layer="21"/>
<rectangle x1="8.47725" y1="3.87985" x2="9.23925" y2="3.89255" layer="21"/>
<rectangle x1="2.29235" y1="3.89255" x2="3.95605" y2="3.90525" layer="21"/>
<rectangle x1="4.59105" y1="3.89255" x2="5.28955" y2="3.90525" layer="21"/>
<rectangle x1="7.28345" y1="3.89255" x2="7.93115" y2="3.90525" layer="21"/>
<rectangle x1="8.47725" y1="3.89255" x2="9.22655" y2="3.90525" layer="21"/>
<rectangle x1="2.30505" y1="3.90525" x2="3.95605" y2="3.91795" layer="21"/>
<rectangle x1="4.59105" y1="3.90525" x2="5.30225" y2="3.91795" layer="21"/>
<rectangle x1="7.23265" y1="3.90525" x2="7.93115" y2="3.91795" layer="21"/>
<rectangle x1="8.47725" y1="3.90525" x2="9.22655" y2="3.91795" layer="21"/>
<rectangle x1="2.31775" y1="3.91795" x2="3.95605" y2="3.93065" layer="21"/>
<rectangle x1="4.60375" y1="3.91795" x2="5.30225" y2="3.93065" layer="21"/>
<rectangle x1="7.19455" y1="3.91795" x2="7.93115" y2="3.93065" layer="21"/>
<rectangle x1="8.47725" y1="3.91795" x2="9.22655" y2="3.93065" layer="21"/>
<rectangle x1="2.33045" y1="3.93065" x2="3.95605" y2="3.94335" layer="21"/>
<rectangle x1="4.60375" y1="3.93065" x2="5.31495" y2="3.94335" layer="21"/>
<rectangle x1="7.15645" y1="3.93065" x2="7.93115" y2="3.94335" layer="21"/>
<rectangle x1="8.47725" y1="3.93065" x2="9.22655" y2="3.94335" layer="21"/>
<rectangle x1="2.34315" y1="3.94335" x2="3.95605" y2="3.95605" layer="21"/>
<rectangle x1="4.60375" y1="3.94335" x2="5.31495" y2="3.95605" layer="21"/>
<rectangle x1="7.11835" y1="3.94335" x2="7.93115" y2="3.95605" layer="21"/>
<rectangle x1="8.47725" y1="3.94335" x2="9.22655" y2="3.95605" layer="21"/>
<rectangle x1="2.35585" y1="3.95605" x2="3.95605" y2="3.96875" layer="21"/>
<rectangle x1="4.60375" y1="3.95605" x2="5.32765" y2="3.96875" layer="21"/>
<rectangle x1="7.06755" y1="3.95605" x2="7.91845" y2="3.96875" layer="21"/>
<rectangle x1="8.47725" y1="3.95605" x2="9.22655" y2="3.96875" layer="21"/>
<rectangle x1="2.36855" y1="3.96875" x2="3.95605" y2="3.98145" layer="21"/>
<rectangle x1="4.61645" y1="3.96875" x2="5.32765" y2="3.98145" layer="21"/>
<rectangle x1="7.02945" y1="3.96875" x2="7.91845" y2="3.98145" layer="21"/>
<rectangle x1="8.47725" y1="3.96875" x2="9.22655" y2="3.98145" layer="21"/>
<rectangle x1="2.38125" y1="3.98145" x2="3.95605" y2="3.99415" layer="21"/>
<rectangle x1="4.61645" y1="3.98145" x2="5.34035" y2="3.99415" layer="21"/>
<rectangle x1="6.99135" y1="3.98145" x2="7.91845" y2="3.99415" layer="21"/>
<rectangle x1="8.47725" y1="3.98145" x2="9.22655" y2="3.99415" layer="21"/>
<rectangle x1="2.39395" y1="3.99415" x2="3.95605" y2="4.00685" layer="21"/>
<rectangle x1="4.61645" y1="3.99415" x2="5.34035" y2="4.00685" layer="21"/>
<rectangle x1="6.95325" y1="3.99415" x2="7.91845" y2="4.00685" layer="21"/>
<rectangle x1="8.47725" y1="3.99415" x2="9.21385" y2="4.00685" layer="21"/>
<rectangle x1="2.40665" y1="4.00685" x2="3.95605" y2="4.01955" layer="21"/>
<rectangle x1="4.62915" y1="4.00685" x2="5.35305" y2="4.01955" layer="21"/>
<rectangle x1="6.90245" y1="4.00685" x2="7.90575" y2="4.01955" layer="21"/>
<rectangle x1="8.47725" y1="4.00685" x2="9.21385" y2="4.01955" layer="21"/>
<rectangle x1="2.41935" y1="4.01955" x2="3.95605" y2="4.03225" layer="21"/>
<rectangle x1="4.62915" y1="4.01955" x2="5.35305" y2="4.03225" layer="21"/>
<rectangle x1="6.86435" y1="4.01955" x2="7.90575" y2="4.03225" layer="21"/>
<rectangle x1="8.47725" y1="4.01955" x2="9.21385" y2="4.03225" layer="21"/>
<rectangle x1="2.43205" y1="4.03225" x2="3.95605" y2="4.04495" layer="21"/>
<rectangle x1="4.62915" y1="4.03225" x2="5.36575" y2="4.04495" layer="21"/>
<rectangle x1="6.83895" y1="4.03225" x2="7.90575" y2="4.04495" layer="21"/>
<rectangle x1="8.47725" y1="4.03225" x2="9.21385" y2="4.04495" layer="21"/>
<rectangle x1="2.44475" y1="4.04495" x2="3.95605" y2="4.05765" layer="21"/>
<rectangle x1="4.64185" y1="4.04495" x2="5.36575" y2="4.05765" layer="21"/>
<rectangle x1="6.80085" y1="4.04495" x2="7.90575" y2="4.05765" layer="21"/>
<rectangle x1="8.47725" y1="4.04495" x2="9.21385" y2="4.05765" layer="21"/>
<rectangle x1="2.45745" y1="4.05765" x2="3.95605" y2="4.07035" layer="21"/>
<rectangle x1="4.64185" y1="4.05765" x2="5.37845" y2="4.07035" layer="21"/>
<rectangle x1="6.75005" y1="4.05765" x2="7.90575" y2="4.07035" layer="21"/>
<rectangle x1="8.47725" y1="4.05765" x2="9.21385" y2="4.07035" layer="21"/>
<rectangle x1="2.47015" y1="4.07035" x2="3.95605" y2="4.08305" layer="21"/>
<rectangle x1="4.64185" y1="4.07035" x2="5.39115" y2="4.08305" layer="21"/>
<rectangle x1="6.71195" y1="4.07035" x2="7.89305" y2="4.08305" layer="21"/>
<rectangle x1="8.47725" y1="4.07035" x2="9.21385" y2="4.08305" layer="21"/>
<rectangle x1="2.48285" y1="4.08305" x2="3.95605" y2="4.09575" layer="21"/>
<rectangle x1="4.65455" y1="4.08305" x2="5.39115" y2="4.09575" layer="21"/>
<rectangle x1="6.69925" y1="4.08305" x2="7.89305" y2="4.09575" layer="21"/>
<rectangle x1="8.47725" y1="4.08305" x2="9.21385" y2="4.09575" layer="21"/>
<rectangle x1="2.49555" y1="4.09575" x2="3.95605" y2="4.10845" layer="21"/>
<rectangle x1="4.65455" y1="4.09575" x2="5.40385" y2="4.10845" layer="21"/>
<rectangle x1="6.71195" y1="4.09575" x2="7.89305" y2="4.10845" layer="21"/>
<rectangle x1="8.47725" y1="4.09575" x2="9.21385" y2="4.10845" layer="21"/>
<rectangle x1="2.50825" y1="4.10845" x2="3.95605" y2="4.12115" layer="21"/>
<rectangle x1="4.65455" y1="4.10845" x2="5.41655" y2="4.12115" layer="21"/>
<rectangle x1="6.72465" y1="4.10845" x2="7.89305" y2="4.12115" layer="21"/>
<rectangle x1="8.47725" y1="4.10845" x2="9.21385" y2="4.12115" layer="21"/>
<rectangle x1="2.52095" y1="4.12115" x2="3.95605" y2="4.13385" layer="21"/>
<rectangle x1="4.66725" y1="4.12115" x2="5.42925" y2="4.13385" layer="21"/>
<rectangle x1="6.73735" y1="4.12115" x2="7.88035" y2="4.13385" layer="21"/>
<rectangle x1="8.47725" y1="4.12115" x2="9.21385" y2="4.13385" layer="21"/>
<rectangle x1="2.53365" y1="4.13385" x2="3.95605" y2="4.14655" layer="21"/>
<rectangle x1="4.66725" y1="4.13385" x2="5.42925" y2="4.14655" layer="21"/>
<rectangle x1="6.75005" y1="4.13385" x2="7.88035" y2="4.14655" layer="21"/>
<rectangle x1="8.47725" y1="4.13385" x2="9.21385" y2="4.14655" layer="21"/>
<rectangle x1="2.54635" y1="4.14655" x2="3.95605" y2="4.15925" layer="21"/>
<rectangle x1="4.67995" y1="4.14655" x2="5.44195" y2="4.15925" layer="21"/>
<rectangle x1="6.76275" y1="4.14655" x2="7.88035" y2="4.15925" layer="21"/>
<rectangle x1="8.47725" y1="4.14655" x2="9.21385" y2="4.15925" layer="21"/>
<rectangle x1="2.55905" y1="4.15925" x2="3.95605" y2="4.17195" layer="21"/>
<rectangle x1="4.67995" y1="4.15925" x2="5.45465" y2="4.17195" layer="21"/>
<rectangle x1="6.77545" y1="4.15925" x2="7.88035" y2="4.17195" layer="21"/>
<rectangle x1="8.47725" y1="4.15925" x2="9.21385" y2="4.17195" layer="21"/>
<rectangle x1="2.57175" y1="4.17195" x2="3.95605" y2="4.18465" layer="21"/>
<rectangle x1="4.67995" y1="4.17195" x2="5.46735" y2="4.18465" layer="21"/>
<rectangle x1="6.78815" y1="4.17195" x2="7.86765" y2="4.18465" layer="21"/>
<rectangle x1="8.47725" y1="4.17195" x2="9.21385" y2="4.18465" layer="21"/>
<rectangle x1="2.58445" y1="4.18465" x2="3.95605" y2="4.19735" layer="21"/>
<rectangle x1="4.69265" y1="4.18465" x2="5.48005" y2="4.19735" layer="21"/>
<rectangle x1="6.81355" y1="4.18465" x2="7.86765" y2="4.19735" layer="21"/>
<rectangle x1="8.47725" y1="4.18465" x2="9.21385" y2="4.19735" layer="21"/>
<rectangle x1="2.59715" y1="4.19735" x2="3.95605" y2="4.21005" layer="21"/>
<rectangle x1="4.69265" y1="4.19735" x2="5.49275" y2="4.21005" layer="21"/>
<rectangle x1="6.82625" y1="4.19735" x2="7.86765" y2="4.21005" layer="21"/>
<rectangle x1="8.47725" y1="4.19735" x2="9.21385" y2="4.21005" layer="21"/>
<rectangle x1="2.60985" y1="4.21005" x2="3.95605" y2="4.22275" layer="21"/>
<rectangle x1="4.70535" y1="4.21005" x2="5.49275" y2="4.22275" layer="21"/>
<rectangle x1="6.83895" y1="4.21005" x2="7.86765" y2="4.22275" layer="21"/>
<rectangle x1="8.47725" y1="4.21005" x2="9.21385" y2="4.22275" layer="21"/>
<rectangle x1="2.62255" y1="4.22275" x2="3.95605" y2="4.23545" layer="21"/>
<rectangle x1="4.70535" y1="4.22275" x2="5.50545" y2="4.23545" layer="21"/>
<rectangle x1="6.85165" y1="4.22275" x2="7.86765" y2="4.23545" layer="21"/>
<rectangle x1="8.47725" y1="4.22275" x2="9.21385" y2="4.23545" layer="21"/>
<rectangle x1="2.63525" y1="4.23545" x2="3.95605" y2="4.24815" layer="21"/>
<rectangle x1="4.71805" y1="4.23545" x2="5.51815" y2="4.24815" layer="21"/>
<rectangle x1="6.86435" y1="4.23545" x2="7.85495" y2="4.24815" layer="21"/>
<rectangle x1="8.47725" y1="4.23545" x2="9.21385" y2="4.24815" layer="21"/>
<rectangle x1="2.64795" y1="4.24815" x2="3.95605" y2="4.26085" layer="21"/>
<rectangle x1="4.71805" y1="4.24815" x2="5.53085" y2="4.26085" layer="21"/>
<rectangle x1="6.87705" y1="4.24815" x2="7.85495" y2="4.26085" layer="21"/>
<rectangle x1="8.47725" y1="4.24815" x2="9.21385" y2="4.26085" layer="21"/>
<rectangle x1="2.66065" y1="4.26085" x2="3.95605" y2="4.27355" layer="21"/>
<rectangle x1="4.73075" y1="4.26085" x2="5.54355" y2="4.27355" layer="21"/>
<rectangle x1="6.88975" y1="4.26085" x2="7.85495" y2="4.27355" layer="21"/>
<rectangle x1="8.47725" y1="4.26085" x2="9.21385" y2="4.27355" layer="21"/>
<rectangle x1="2.67335" y1="4.27355" x2="3.95605" y2="4.28625" layer="21"/>
<rectangle x1="4.73075" y1="4.27355" x2="5.55625" y2="4.28625" layer="21"/>
<rectangle x1="6.90245" y1="4.27355" x2="7.85495" y2="4.28625" layer="21"/>
<rectangle x1="8.47725" y1="4.27355" x2="9.21385" y2="4.28625" layer="21"/>
<rectangle x1="2.68605" y1="4.28625" x2="3.95605" y2="4.29895" layer="21"/>
<rectangle x1="4.74345" y1="4.28625" x2="5.58165" y2="4.29895" layer="21"/>
<rectangle x1="6.91515" y1="4.28625" x2="7.84225" y2="4.29895" layer="21"/>
<rectangle x1="8.47725" y1="4.28625" x2="9.22655" y2="4.29895" layer="21"/>
<rectangle x1="2.69875" y1="4.29895" x2="3.95605" y2="4.31165" layer="21"/>
<rectangle x1="4.74345" y1="4.29895" x2="5.59435" y2="4.31165" layer="21"/>
<rectangle x1="6.92785" y1="4.29895" x2="7.84225" y2="4.31165" layer="21"/>
<rectangle x1="8.47725" y1="4.29895" x2="9.22655" y2="4.31165" layer="21"/>
<rectangle x1="2.71145" y1="4.31165" x2="3.95605" y2="4.32435" layer="21"/>
<rectangle x1="4.75615" y1="4.31165" x2="5.60705" y2="4.32435" layer="21"/>
<rectangle x1="6.94055" y1="4.31165" x2="7.84225" y2="4.32435" layer="21"/>
<rectangle x1="8.47725" y1="4.31165" x2="9.22655" y2="4.32435" layer="21"/>
<rectangle x1="2.72415" y1="4.32435" x2="3.95605" y2="4.33705" layer="21"/>
<rectangle x1="4.75615" y1="4.32435" x2="5.61975" y2="4.33705" layer="21"/>
<rectangle x1="6.95325" y1="4.32435" x2="7.84225" y2="4.33705" layer="21"/>
<rectangle x1="8.47725" y1="4.32435" x2="9.22655" y2="4.33705" layer="21"/>
<rectangle x1="2.73685" y1="4.33705" x2="3.95605" y2="4.34975" layer="21"/>
<rectangle x1="4.76885" y1="4.33705" x2="5.63245" y2="4.34975" layer="21"/>
<rectangle x1="6.95325" y1="4.33705" x2="7.84225" y2="4.34975" layer="21"/>
<rectangle x1="8.47725" y1="4.33705" x2="9.22655" y2="4.34975" layer="21"/>
<rectangle x1="2.74955" y1="4.34975" x2="3.95605" y2="4.36245" layer="21"/>
<rectangle x1="4.78155" y1="4.34975" x2="5.65785" y2="4.36245" layer="21"/>
<rectangle x1="6.92785" y1="4.34975" x2="7.82955" y2="4.36245" layer="21"/>
<rectangle x1="8.47725" y1="4.34975" x2="9.22655" y2="4.36245" layer="21"/>
<rectangle x1="2.76225" y1="4.36245" x2="3.95605" y2="4.37515" layer="21"/>
<rectangle x1="4.78155" y1="4.36245" x2="5.67055" y2="4.37515" layer="21"/>
<rectangle x1="6.91515" y1="4.36245" x2="7.82955" y2="4.37515" layer="21"/>
<rectangle x1="8.47725" y1="4.36245" x2="9.22655" y2="4.37515" layer="21"/>
<rectangle x1="2.77495" y1="4.37515" x2="3.95605" y2="4.38785" layer="21"/>
<rectangle x1="4.79425" y1="4.37515" x2="5.69595" y2="4.38785" layer="21"/>
<rectangle x1="6.90245" y1="4.37515" x2="7.82955" y2="4.38785" layer="21"/>
<rectangle x1="8.47725" y1="4.37515" x2="9.22655" y2="4.38785" layer="21"/>
<rectangle x1="2.78765" y1="4.38785" x2="3.95605" y2="4.40055" layer="21"/>
<rectangle x1="4.79425" y1="4.38785" x2="5.70865" y2="4.40055" layer="21"/>
<rectangle x1="6.87705" y1="4.38785" x2="7.82955" y2="4.40055" layer="21"/>
<rectangle x1="8.47725" y1="4.38785" x2="9.22655" y2="4.40055" layer="21"/>
<rectangle x1="2.80035" y1="4.40055" x2="3.95605" y2="4.41325" layer="21"/>
<rectangle x1="4.80695" y1="4.40055" x2="5.73405" y2="4.41325" layer="21"/>
<rectangle x1="6.86435" y1="4.40055" x2="7.81685" y2="4.41325" layer="21"/>
<rectangle x1="8.47725" y1="4.40055" x2="9.23925" y2="4.41325" layer="21"/>
<rectangle x1="2.81305" y1="4.41325" x2="3.95605" y2="4.42595" layer="21"/>
<rectangle x1="4.81965" y1="4.41325" x2="5.74675" y2="4.42595" layer="21"/>
<rectangle x1="6.83895" y1="4.41325" x2="7.81685" y2="4.42595" layer="21"/>
<rectangle x1="8.47725" y1="4.41325" x2="9.23925" y2="4.42595" layer="21"/>
<rectangle x1="2.82575" y1="4.42595" x2="3.95605" y2="4.43865" layer="21"/>
<rectangle x1="4.81965" y1="4.42595" x2="5.77215" y2="4.43865" layer="21"/>
<rectangle x1="6.81355" y1="4.42595" x2="7.81685" y2="4.43865" layer="21"/>
<rectangle x1="8.47725" y1="4.42595" x2="9.23925" y2="4.43865" layer="21"/>
<rectangle x1="2.83845" y1="4.43865" x2="3.95605" y2="4.45135" layer="21"/>
<rectangle x1="4.83235" y1="4.43865" x2="5.79755" y2="4.45135" layer="21"/>
<rectangle x1="6.78815" y1="4.43865" x2="7.81685" y2="4.45135" layer="21"/>
<rectangle x1="8.47725" y1="4.43865" x2="9.23925" y2="4.45135" layer="21"/>
<rectangle x1="2.85115" y1="4.45135" x2="3.95605" y2="4.46405" layer="21"/>
<rectangle x1="4.84505" y1="4.45135" x2="5.82295" y2="4.46405" layer="21"/>
<rectangle x1="6.76275" y1="4.45135" x2="7.80415" y2="4.46405" layer="21"/>
<rectangle x1="8.47725" y1="4.45135" x2="9.23925" y2="4.46405" layer="21"/>
<rectangle x1="2.86385" y1="4.46405" x2="3.95605" y2="4.47675" layer="21"/>
<rectangle x1="4.84505" y1="4.46405" x2="5.84835" y2="4.47675" layer="21"/>
<rectangle x1="6.73735" y1="4.46405" x2="7.80415" y2="4.47675" layer="21"/>
<rectangle x1="8.47725" y1="4.46405" x2="9.25195" y2="4.47675" layer="21"/>
<rectangle x1="2.87655" y1="4.47675" x2="3.95605" y2="4.48945" layer="21"/>
<rectangle x1="4.85775" y1="4.47675" x2="5.88645" y2="4.48945" layer="21"/>
<rectangle x1="6.71195" y1="4.47675" x2="7.80415" y2="4.48945" layer="21"/>
<rectangle x1="8.47725" y1="4.47675" x2="9.25195" y2="4.48945" layer="21"/>
<rectangle x1="2.88925" y1="4.48945" x2="3.95605" y2="4.50215" layer="21"/>
<rectangle x1="4.87045" y1="4.48945" x2="5.91185" y2="4.50215" layer="21"/>
<rectangle x1="6.67385" y1="4.48945" x2="7.80415" y2="4.50215" layer="21"/>
<rectangle x1="8.47725" y1="4.48945" x2="9.25195" y2="4.50215" layer="21"/>
<rectangle x1="2.90195" y1="4.50215" x2="3.95605" y2="4.51485" layer="21"/>
<rectangle x1="4.87045" y1="4.50215" x2="5.94995" y2="4.51485" layer="21"/>
<rectangle x1="6.63575" y1="4.50215" x2="7.80415" y2="4.51485" layer="21"/>
<rectangle x1="8.47725" y1="4.50215" x2="9.25195" y2="4.51485" layer="21"/>
<rectangle x1="2.91465" y1="4.51485" x2="3.95605" y2="4.52755" layer="21"/>
<rectangle x1="4.88315" y1="4.51485" x2="5.98805" y2="4.52755" layer="21"/>
<rectangle x1="6.59765" y1="4.51485" x2="7.79145" y2="4.52755" layer="21"/>
<rectangle x1="8.47725" y1="4.51485" x2="9.25195" y2="4.52755" layer="21"/>
<rectangle x1="2.92735" y1="4.52755" x2="3.95605" y2="4.54025" layer="21"/>
<rectangle x1="4.89585" y1="4.52755" x2="6.03885" y2="4.54025" layer="21"/>
<rectangle x1="6.55955" y1="4.52755" x2="7.79145" y2="4.54025" layer="21"/>
<rectangle x1="8.47725" y1="4.52755" x2="9.26465" y2="4.54025" layer="21"/>
<rectangle x1="2.94005" y1="4.54025" x2="3.95605" y2="4.55295" layer="21"/>
<rectangle x1="4.90855" y1="4.54025" x2="6.08965" y2="4.55295" layer="21"/>
<rectangle x1="6.49605" y1="4.54025" x2="7.79145" y2="4.55295" layer="21"/>
<rectangle x1="8.47725" y1="4.54025" x2="9.26465" y2="4.55295" layer="21"/>
<rectangle x1="2.95275" y1="4.55295" x2="3.95605" y2="4.56565" layer="21"/>
<rectangle x1="4.90855" y1="4.55295" x2="6.17855" y2="4.56565" layer="21"/>
<rectangle x1="6.40715" y1="4.55295" x2="7.79145" y2="4.56565" layer="21"/>
<rectangle x1="8.47725" y1="4.55295" x2="9.26465" y2="4.56565" layer="21"/>
<rectangle x1="2.96545" y1="4.56565" x2="3.95605" y2="4.57835" layer="21"/>
<rectangle x1="4.92125" y1="4.56565" x2="7.77875" y2="4.57835" layer="21"/>
<rectangle x1="8.47725" y1="4.56565" x2="9.26465" y2="4.57835" layer="21"/>
<rectangle x1="2.97815" y1="4.57835" x2="3.95605" y2="4.59105" layer="21"/>
<rectangle x1="4.93395" y1="4.57835" x2="7.77875" y2="4.59105" layer="21"/>
<rectangle x1="8.47725" y1="4.57835" x2="9.27735" y2="4.59105" layer="21"/>
<rectangle x1="2.99085" y1="4.59105" x2="3.95605" y2="4.60375" layer="21"/>
<rectangle x1="4.94665" y1="4.59105" x2="7.77875" y2="4.60375" layer="21"/>
<rectangle x1="8.47725" y1="4.59105" x2="9.27735" y2="4.60375" layer="21"/>
<rectangle x1="3.00355" y1="4.60375" x2="3.95605" y2="4.61645" layer="21"/>
<rectangle x1="4.95935" y1="4.60375" x2="7.77875" y2="4.61645" layer="21"/>
<rectangle x1="8.47725" y1="4.60375" x2="9.27735" y2="4.61645" layer="21"/>
<rectangle x1="3.01625" y1="4.61645" x2="3.95605" y2="4.62915" layer="21"/>
<rectangle x1="4.95935" y1="4.61645" x2="7.77875" y2="4.62915" layer="21"/>
<rectangle x1="8.47725" y1="4.61645" x2="9.29005" y2="4.62915" layer="21"/>
<rectangle x1="3.02895" y1="4.62915" x2="3.95605" y2="4.64185" layer="21"/>
<rectangle x1="4.97205" y1="4.62915" x2="7.76605" y2="4.64185" layer="21"/>
<rectangle x1="8.47725" y1="4.62915" x2="9.29005" y2="4.64185" layer="21"/>
<rectangle x1="3.04165" y1="4.64185" x2="3.95605" y2="4.65455" layer="21"/>
<rectangle x1="4.98475" y1="4.64185" x2="7.76605" y2="4.65455" layer="21"/>
<rectangle x1="8.47725" y1="4.64185" x2="9.29005" y2="4.65455" layer="21"/>
<rectangle x1="3.05435" y1="4.65455" x2="3.95605" y2="4.66725" layer="21"/>
<rectangle x1="4.99745" y1="4.65455" x2="7.76605" y2="4.66725" layer="21"/>
<rectangle x1="8.47725" y1="4.65455" x2="9.30275" y2="4.66725" layer="21"/>
<rectangle x1="3.06705" y1="4.66725" x2="3.95605" y2="4.67995" layer="21"/>
<rectangle x1="5.01015" y1="4.66725" x2="7.76605" y2="4.67995" layer="21"/>
<rectangle x1="8.47725" y1="4.66725" x2="9.30275" y2="4.67995" layer="21"/>
<rectangle x1="3.07975" y1="4.67995" x2="3.95605" y2="4.69265" layer="21"/>
<rectangle x1="5.02285" y1="4.67995" x2="7.75335" y2="4.69265" layer="21"/>
<rectangle x1="8.47725" y1="4.67995" x2="9.30275" y2="4.69265" layer="21"/>
<rectangle x1="3.09245" y1="4.69265" x2="3.95605" y2="4.70535" layer="21"/>
<rectangle x1="5.03555" y1="4.69265" x2="7.75335" y2="4.70535" layer="21"/>
<rectangle x1="8.47725" y1="4.69265" x2="9.31545" y2="4.70535" layer="21"/>
<rectangle x1="3.10515" y1="4.70535" x2="3.95605" y2="4.71805" layer="21"/>
<rectangle x1="5.04825" y1="4.70535" x2="7.75335" y2="4.71805" layer="21"/>
<rectangle x1="8.47725" y1="4.70535" x2="9.31545" y2="4.71805" layer="21"/>
<rectangle x1="3.11785" y1="4.71805" x2="3.95605" y2="4.73075" layer="21"/>
<rectangle x1="5.06095" y1="4.71805" x2="7.75335" y2="4.73075" layer="21"/>
<rectangle x1="8.47725" y1="4.71805" x2="9.31545" y2="4.73075" layer="21"/>
<rectangle x1="3.13055" y1="4.73075" x2="3.95605" y2="4.74345" layer="21"/>
<rectangle x1="5.07365" y1="4.73075" x2="7.74065" y2="4.74345" layer="21"/>
<rectangle x1="8.47725" y1="4.73075" x2="9.31545" y2="4.74345" layer="21"/>
<rectangle x1="3.14325" y1="4.74345" x2="3.95605" y2="4.75615" layer="21"/>
<rectangle x1="5.08635" y1="4.74345" x2="7.74065" y2="4.75615" layer="21"/>
<rectangle x1="8.47725" y1="4.74345" x2="9.31545" y2="4.75615" layer="21"/>
<rectangle x1="3.15595" y1="4.75615" x2="3.95605" y2="4.76885" layer="21"/>
<rectangle x1="5.09905" y1="4.75615" x2="7.74065" y2="4.76885" layer="21"/>
<rectangle x1="8.47725" y1="4.75615" x2="9.30275" y2="4.76885" layer="21"/>
<rectangle x1="3.16865" y1="4.76885" x2="3.95605" y2="4.78155" layer="21"/>
<rectangle x1="5.11175" y1="4.76885" x2="7.74065" y2="4.78155" layer="21"/>
<rectangle x1="8.47725" y1="4.76885" x2="9.29005" y2="4.78155" layer="21"/>
<rectangle x1="3.18135" y1="4.78155" x2="3.95605" y2="4.79425" layer="21"/>
<rectangle x1="5.12445" y1="4.78155" x2="7.74065" y2="4.79425" layer="21"/>
<rectangle x1="8.47725" y1="4.78155" x2="9.27735" y2="4.79425" layer="21"/>
<rectangle x1="3.19405" y1="4.79425" x2="3.95605" y2="4.80695" layer="21"/>
<rectangle x1="5.13715" y1="4.79425" x2="7.44855" y2="4.80695" layer="21"/>
<rectangle x1="7.47395" y1="4.79425" x2="7.72795" y2="4.80695" layer="21"/>
<rectangle x1="8.47725" y1="4.79425" x2="9.26465" y2="4.80695" layer="21"/>
<rectangle x1="3.20675" y1="4.80695" x2="3.95605" y2="4.81965" layer="21"/>
<rectangle x1="5.16255" y1="4.80695" x2="7.43585" y2="4.81965" layer="21"/>
<rectangle x1="7.48665" y1="4.80695" x2="7.72795" y2="4.81965" layer="21"/>
<rectangle x1="8.47725" y1="4.80695" x2="9.25195" y2="4.81965" layer="21"/>
<rectangle x1="3.21945" y1="4.81965" x2="3.95605" y2="4.83235" layer="21"/>
<rectangle x1="5.17525" y1="4.81965" x2="7.41045" y2="4.83235" layer="21"/>
<rectangle x1="7.49935" y1="4.81965" x2="7.72795" y2="4.83235" layer="21"/>
<rectangle x1="8.47725" y1="4.81965" x2="9.23925" y2="4.83235" layer="21"/>
<rectangle x1="3.23215" y1="4.83235" x2="3.95605" y2="4.84505" layer="21"/>
<rectangle x1="5.18795" y1="4.83235" x2="7.39775" y2="4.84505" layer="21"/>
<rectangle x1="7.51205" y1="4.83235" x2="7.72795" y2="4.84505" layer="21"/>
<rectangle x1="8.47725" y1="4.83235" x2="9.22655" y2="4.84505" layer="21"/>
<rectangle x1="3.24485" y1="4.84505" x2="3.95605" y2="4.85775" layer="21"/>
<rectangle x1="5.20065" y1="4.84505" x2="7.38505" y2="4.85775" layer="21"/>
<rectangle x1="7.52475" y1="4.84505" x2="7.71525" y2="4.85775" layer="21"/>
<rectangle x1="8.47725" y1="4.84505" x2="9.21385" y2="4.85775" layer="21"/>
<rectangle x1="3.25755" y1="4.85775" x2="3.95605" y2="4.87045" layer="21"/>
<rectangle x1="5.21335" y1="4.85775" x2="7.37235" y2="4.87045" layer="21"/>
<rectangle x1="7.53745" y1="4.85775" x2="7.71525" y2="4.87045" layer="21"/>
<rectangle x1="8.47725" y1="4.85775" x2="9.20115" y2="4.87045" layer="21"/>
<rectangle x1="3.27025" y1="4.87045" x2="3.95605" y2="4.88315" layer="21"/>
<rectangle x1="5.23875" y1="4.87045" x2="7.34695" y2="4.88315" layer="21"/>
<rectangle x1="7.55015" y1="4.87045" x2="7.71525" y2="4.88315" layer="21"/>
<rectangle x1="8.47725" y1="4.87045" x2="9.18845" y2="4.88315" layer="21"/>
<rectangle x1="3.28295" y1="4.88315" x2="3.95605" y2="4.89585" layer="21"/>
<rectangle x1="5.25145" y1="4.88315" x2="7.33425" y2="4.89585" layer="21"/>
<rectangle x1="7.56285" y1="4.88315" x2="7.71525" y2="4.89585" layer="21"/>
<rectangle x1="8.47725" y1="4.88315" x2="9.17575" y2="4.89585" layer="21"/>
<rectangle x1="3.29565" y1="4.89585" x2="3.95605" y2="4.90855" layer="21"/>
<rectangle x1="5.26415" y1="4.89585" x2="7.32155" y2="4.90855" layer="21"/>
<rectangle x1="7.57555" y1="4.89585" x2="7.71525" y2="4.90855" layer="21"/>
<rectangle x1="8.47725" y1="4.89585" x2="9.16305" y2="4.90855" layer="21"/>
<rectangle x1="3.30835" y1="4.90855" x2="3.95605" y2="4.92125" layer="21"/>
<rectangle x1="5.28955" y1="4.90855" x2="7.29615" y2="4.92125" layer="21"/>
<rectangle x1="7.58825" y1="4.90855" x2="7.70255" y2="4.92125" layer="21"/>
<rectangle x1="8.47725" y1="4.90855" x2="9.15035" y2="4.92125" layer="21"/>
<rectangle x1="3.32105" y1="4.92125" x2="3.95605" y2="4.93395" layer="21"/>
<rectangle x1="5.30225" y1="4.92125" x2="7.28345" y2="4.93395" layer="21"/>
<rectangle x1="7.60095" y1="4.92125" x2="7.70255" y2="4.93395" layer="21"/>
<rectangle x1="8.47725" y1="4.92125" x2="9.13765" y2="4.93395" layer="21"/>
<rectangle x1="3.33375" y1="4.93395" x2="3.95605" y2="4.94665" layer="21"/>
<rectangle x1="5.32765" y1="4.93395" x2="7.25805" y2="4.94665" layer="21"/>
<rectangle x1="7.62635" y1="4.93395" x2="7.70255" y2="4.94665" layer="21"/>
<rectangle x1="8.47725" y1="4.93395" x2="9.12495" y2="4.94665" layer="21"/>
<rectangle x1="3.34645" y1="4.94665" x2="3.95605" y2="4.95935" layer="21"/>
<rectangle x1="5.34035" y1="4.94665" x2="7.24535" y2="4.95935" layer="21"/>
<rectangle x1="7.63905" y1="4.94665" x2="7.70255" y2="4.95935" layer="21"/>
<rectangle x1="8.47725" y1="4.94665" x2="9.11225" y2="4.95935" layer="21"/>
<rectangle x1="3.35915" y1="4.95935" x2="3.95605" y2="4.97205" layer="21"/>
<rectangle x1="5.36575" y1="4.95935" x2="7.21995" y2="4.97205" layer="21"/>
<rectangle x1="7.65175" y1="4.95935" x2="7.68985" y2="4.97205" layer="21"/>
<rectangle x1="8.47725" y1="4.95935" x2="9.09955" y2="4.97205" layer="21"/>
<rectangle x1="3.37185" y1="4.97205" x2="3.95605" y2="4.98475" layer="21"/>
<rectangle x1="5.39115" y1="4.97205" x2="7.20725" y2="4.98475" layer="21"/>
<rectangle x1="7.66445" y1="4.97205" x2="7.68985" y2="4.98475" layer="21"/>
<rectangle x1="8.47725" y1="4.97205" x2="9.08685" y2="4.98475" layer="21"/>
<rectangle x1="3.38455" y1="4.98475" x2="3.95605" y2="4.99745" layer="21"/>
<rectangle x1="5.40385" y1="4.98475" x2="7.18185" y2="4.99745" layer="21"/>
<rectangle x1="7.67715" y1="4.98475" x2="7.68985" y2="4.99745" layer="21"/>
<rectangle x1="8.47725" y1="4.98475" x2="9.07415" y2="4.99745" layer="21"/>
<rectangle x1="3.39725" y1="4.99745" x2="3.95605" y2="5.01015" layer="21"/>
<rectangle x1="5.42925" y1="4.99745" x2="7.15645" y2="5.01015" layer="21"/>
<rectangle x1="8.47725" y1="4.99745" x2="9.06145" y2="5.01015" layer="21"/>
<rectangle x1="3.40995" y1="5.01015" x2="3.95605" y2="5.02285" layer="21"/>
<rectangle x1="5.45465" y1="5.01015" x2="7.13105" y2="5.02285" layer="21"/>
<rectangle x1="8.47725" y1="5.01015" x2="9.04875" y2="5.02285" layer="21"/>
<rectangle x1="3.42265" y1="5.02285" x2="3.95605" y2="5.03555" layer="21"/>
<rectangle x1="5.48005" y1="5.02285" x2="7.11835" y2="5.03555" layer="21"/>
<rectangle x1="8.47725" y1="5.02285" x2="9.03605" y2="5.03555" layer="21"/>
<rectangle x1="3.43535" y1="5.03555" x2="3.95605" y2="5.04825" layer="21"/>
<rectangle x1="5.49275" y1="5.03555" x2="7.09295" y2="5.04825" layer="21"/>
<rectangle x1="8.47725" y1="5.03555" x2="9.02335" y2="5.04825" layer="21"/>
<rectangle x1="3.44805" y1="5.04825" x2="3.95605" y2="5.06095" layer="21"/>
<rectangle x1="5.51815" y1="5.04825" x2="7.06755" y2="5.06095" layer="21"/>
<rectangle x1="8.47725" y1="5.04825" x2="9.01065" y2="5.06095" layer="21"/>
<rectangle x1="3.46075" y1="5.06095" x2="3.95605" y2="5.07365" layer="21"/>
<rectangle x1="5.55625" y1="5.06095" x2="7.04215" y2="5.07365" layer="21"/>
<rectangle x1="8.47725" y1="5.06095" x2="8.99795" y2="5.07365" layer="21"/>
<rectangle x1="3.47345" y1="5.07365" x2="3.95605" y2="5.08635" layer="21"/>
<rectangle x1="5.58165" y1="5.07365" x2="7.00405" y2="5.08635" layer="21"/>
<rectangle x1="8.47725" y1="5.07365" x2="8.98525" y2="5.08635" layer="21"/>
<rectangle x1="3.48615" y1="5.08635" x2="3.95605" y2="5.09905" layer="21"/>
<rectangle x1="5.60705" y1="5.08635" x2="6.97865" y2="5.09905" layer="21"/>
<rectangle x1="8.47725" y1="5.08635" x2="8.97255" y2="5.09905" layer="21"/>
<rectangle x1="3.49885" y1="5.09905" x2="3.95605" y2="5.11175" layer="21"/>
<rectangle x1="5.63245" y1="5.09905" x2="6.95325" y2="5.11175" layer="21"/>
<rectangle x1="8.47725" y1="5.09905" x2="8.95985" y2="5.11175" layer="21"/>
<rectangle x1="3.51155" y1="5.11175" x2="3.95605" y2="5.12445" layer="21"/>
<rectangle x1="5.67055" y1="5.11175" x2="6.91515" y2="5.12445" layer="21"/>
<rectangle x1="8.47725" y1="5.11175" x2="8.94715" y2="5.12445" layer="21"/>
<rectangle x1="3.52425" y1="5.12445" x2="3.95605" y2="5.13715" layer="21"/>
<rectangle x1="5.70865" y1="5.12445" x2="6.87705" y2="5.13715" layer="21"/>
<rectangle x1="8.47725" y1="5.12445" x2="8.93445" y2="5.13715" layer="21"/>
<rectangle x1="3.53695" y1="5.13715" x2="3.95605" y2="5.14985" layer="21"/>
<rectangle x1="5.74675" y1="5.13715" x2="6.85165" y2="5.14985" layer="21"/>
<rectangle x1="8.47725" y1="5.13715" x2="8.92175" y2="5.14985" layer="21"/>
<rectangle x1="3.54965" y1="5.14985" x2="3.94335" y2="5.16255" layer="21"/>
<rectangle x1="5.78485" y1="5.14985" x2="6.80085" y2="5.16255" layer="21"/>
<rectangle x1="8.47725" y1="5.14985" x2="8.90905" y2="5.16255" layer="21"/>
<rectangle x1="3.56235" y1="5.16255" x2="3.94335" y2="5.17525" layer="21"/>
<rectangle x1="5.82295" y1="5.16255" x2="6.76275" y2="5.17525" layer="21"/>
<rectangle x1="8.47725" y1="5.16255" x2="8.89635" y2="5.17525" layer="21"/>
<rectangle x1="3.57505" y1="5.17525" x2="3.94335" y2="5.18795" layer="21"/>
<rectangle x1="5.87375" y1="5.17525" x2="6.71195" y2="5.18795" layer="21"/>
<rectangle x1="8.47725" y1="5.17525" x2="8.88365" y2="5.18795" layer="21"/>
<rectangle x1="3.58775" y1="5.18795" x2="3.94335" y2="5.20065" layer="21"/>
<rectangle x1="5.93725" y1="5.18795" x2="6.66115" y2="5.20065" layer="21"/>
<rectangle x1="8.47725" y1="5.18795" x2="8.87095" y2="5.20065" layer="21"/>
<rectangle x1="3.60045" y1="5.20065" x2="3.94335" y2="5.21335" layer="21"/>
<rectangle x1="6.00075" y1="5.20065" x2="6.58495" y2="5.21335" layer="21"/>
<rectangle x1="8.47725" y1="5.20065" x2="8.85825" y2="5.21335" layer="21"/>
<rectangle x1="3.61315" y1="5.21335" x2="3.94335" y2="5.22605" layer="21"/>
<rectangle x1="6.08965" y1="5.21335" x2="6.49605" y2="5.22605" layer="21"/>
<rectangle x1="8.47725" y1="5.21335" x2="8.84555" y2="5.22605" layer="21"/>
<rectangle x1="3.62585" y1="5.22605" x2="3.94335" y2="5.23875" layer="21"/>
<rectangle x1="8.47725" y1="5.22605" x2="8.83285" y2="5.23875" layer="21"/>
<rectangle x1="3.63855" y1="5.23875" x2="3.94335" y2="5.25145" layer="21"/>
<rectangle x1="8.47725" y1="5.23875" x2="8.82015" y2="5.25145" layer="21"/>
<rectangle x1="3.65125" y1="5.25145" x2="3.94335" y2="5.26415" layer="21"/>
<rectangle x1="8.47725" y1="5.25145" x2="8.80745" y2="5.26415" layer="21"/>
<rectangle x1="3.66395" y1="5.26415" x2="3.93065" y2="5.27685" layer="21"/>
<rectangle x1="8.47725" y1="5.26415" x2="8.79475" y2="5.27685" layer="21"/>
<rectangle x1="3.67665" y1="5.27685" x2="3.93065" y2="5.28955" layer="21"/>
<rectangle x1="8.48995" y1="5.27685" x2="8.78205" y2="5.28955" layer="21"/>
<rectangle x1="3.68935" y1="5.28955" x2="3.91795" y2="5.30225" layer="21"/>
<rectangle x1="8.48995" y1="5.28955" x2="8.76935" y2="5.30225" layer="21"/>
<rectangle x1="3.70205" y1="5.30225" x2="3.91795" y2="5.31495" layer="21"/>
<rectangle x1="8.48995" y1="5.30225" x2="8.75665" y2="5.31495" layer="21"/>
<rectangle x1="3.71475" y1="5.31495" x2="3.90525" y2="5.32765" layer="21"/>
<rectangle x1="8.50265" y1="5.31495" x2="8.74395" y2="5.32765" layer="21"/>
<rectangle x1="3.74015" y1="5.32765" x2="3.90525" y2="5.34035" layer="21"/>
<rectangle x1="8.50265" y1="5.32765" x2="8.71855" y2="5.34035" layer="21"/>
<rectangle x1="3.76555" y1="5.34035" x2="3.87985" y2="5.35305" layer="21"/>
<rectangle x1="8.51535" y1="5.34035" x2="8.70585" y2="5.35305" layer="21"/>
<rectangle x1="3.80365" y1="5.35305" x2="3.85445" y2="5.36575" layer="21"/>
<rectangle x1="8.52805" y1="5.35305" x2="8.69315" y2="5.36575" layer="21"/>
<rectangle x1="8.54075" y1="5.36575" x2="8.66775" y2="5.37845" layer="21"/>
<rectangle x1="8.56615" y1="5.37845" x2="8.62965" y2="5.39115" layer="21"/>
<text x="0.635" y="-1.905" size="1.4224" layer="21" font="vector" ratio="18">acl.mit.edu</text>
</package>
<package name="ACL-LOGO-BOTTOM">
<rectangle x1="-2.94005" y1="0.03175" x2="-2.71145" y2="0.04445" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.04445" x2="-2.71145" y2="0.05715" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.05715" x2="-2.71145" y2="0.06985" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.06985" x2="-2.71145" y2="0.08255" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.08255" x2="-2.71145" y2="0.09525" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.09525" x2="-2.71145" y2="0.10795" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.10795" x2="-2.71145" y2="0.12065" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.12065" x2="-2.71145" y2="0.13335" layer="21" rot="R180"/>
<rectangle x1="-5.86105" y1="0.12065" x2="-5.78485" y2="0.13335" layer="21" rot="R180"/>
<rectangle x1="-0.70485" y1="0.13335" x2="-0.56515" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-1.80975" y1="0.13335" x2="-1.64465" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-2.44475" y1="0.13335" x2="-2.29235" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.13335" x2="-2.71145" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-3.15595" y1="0.13335" x2="-3.06705" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.13335" x2="-3.48615" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-4.34975" y1="0.13335" x2="-4.19735" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="0.13335" x2="-4.84505" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-5.92455" y1="0.13335" x2="-5.70865" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-6.57225" y1="0.13335" x2="-6.40715" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-8.73125" y1="0.13335" x2="-8.56615" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-9.72185" y1="0.13335" x2="-9.56945" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.13335" x2="-10.80135" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-11.77925" y1="0.13335" x2="-11.69035" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.14605" x2="0.33655" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-0.29845" y1="0.14605" x2="-0.05715" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-0.75565" y1="0.14605" x2="-0.51435" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.14605" x2="-1.00965" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-1.86055" y1="0.14605" x2="-1.59385" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="0.14605" x2="-2.24155" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.14605" x2="-2.71145" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-3.19405" y1="0.14605" x2="-3.04165" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-3.60045" y1="0.14605" x2="-3.44805" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.14605" x2="-3.68935" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-4.40055" y1="0.14605" x2="-4.15925" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-5.04825" y1="0.14605" x2="-4.79425" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-5.96265" y1="0.14605" x2="-5.67055" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-6.62305" y1="0.14605" x2="-6.35635" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.14605" x2="-6.87705" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.14605" x2="-7.20725" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.14605" x2="-7.66445" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.14605" x2="-7.93115" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="0.14605" x2="-8.51535" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.14605" x2="-9.03605" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-9.77265" y1="0.14605" x2="-9.51865" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.14605" x2="-10.21715" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-10.92835" y1="0.14605" x2="-10.76325" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.14605" x2="-11.00455" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="0.14605" x2="-11.33475" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-11.80465" y1="0.14605" x2="-11.65225" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.15875" x2="0.33655" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-0.29845" y1="0.15875" x2="-0.05715" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-0.78105" y1="0.15875" x2="-0.47625" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.15875" x2="-1.00965" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-1.88595" y1="0.15875" x2="-1.56845" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="0.15875" x2="-2.21615" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.15875" x2="-2.71145" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-3.20675" y1="0.15875" x2="-3.01625" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-3.62585" y1="0.15875" x2="-3.42265" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.15875" x2="-3.68935" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-4.42595" y1="0.15875" x2="-4.12115" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-5.07365" y1="0.15875" x2="-4.76885" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-5.98805" y1="0.15875" x2="-5.64515" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-6.64845" y1="0.15875" x2="-6.33095" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.15875" x2="-6.87705" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.15875" x2="-7.20725" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.15875" x2="-7.62635" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.15875" x2="-7.93115" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="0.15875" x2="-8.48995" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.15875" x2="-9.03605" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="0.15875" x2="-9.48055" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.15875" x2="-10.21715" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-10.94105" y1="0.15875" x2="-10.73785" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.15875" x2="-11.00455" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="0.15875" x2="-11.33475" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-11.83005" y1="0.15875" x2="-11.62685" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.17145" x2="0.33655" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.17145" x2="-0.05715" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-0.80645" y1="0.17145" x2="-0.45085" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.17145" x2="-1.00965" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-1.91135" y1="0.17145" x2="-1.54305" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="0.17145" x2="-2.19075" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.17145" x2="-2.71145" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="0.17145" x2="-3.00355" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-3.63855" y1="0.17145" x2="-3.40995" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.17145" x2="-3.68935" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-4.45135" y1="0.17145" x2="-4.10845" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="0.17145" x2="-4.74345" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-6.01345" y1="0.17145" x2="-5.61975" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-6.67385" y1="0.17145" x2="-6.30555" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.17145" x2="-6.87705" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.17145" x2="-7.20725" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.17145" x2="-7.60095" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.17145" x2="-7.93115" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="0.17145" x2="-8.46455" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.17145" x2="-9.03605" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-9.82345" y1="0.17145" x2="-9.45515" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.17145" x2="-10.21715" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-10.95375" y1="0.17145" x2="-10.72515" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.17145" x2="-11.00455" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="0.17145" x2="-11.33475" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-11.84275" y1="0.17145" x2="-11.61415" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.18415" x2="0.33655" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.18415" x2="-0.05715" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-0.81915" y1="0.18415" x2="-0.43815" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.18415" x2="-1.00965" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-1.92405" y1="0.18415" x2="-1.53035" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="0.18415" x2="-2.16535" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.18415" x2="-2.71145" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="0.18415" x2="-2.99085" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-3.65125" y1="0.18415" x2="-3.39725" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.18415" x2="-3.68935" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-4.46405" y1="0.18415" x2="-4.08305" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="0.18415" x2="-4.73075" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-6.02615" y1="0.18415" x2="-5.60705" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="0.18415" x2="-6.29285" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.18415" x2="-6.87705" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.18415" x2="-7.20725" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.18415" x2="-7.58825" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.18415" x2="-7.93115" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="0.18415" x2="-8.45185" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.18415" x2="-9.03605" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="0.18415" x2="-9.44245" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.18415" x2="-10.21715" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-10.96645" y1="0.18415" x2="-10.71245" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.18415" x2="-11.00455" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.18415" x2="-11.33475" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-11.85545" y1="0.18415" x2="-11.60145" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.19685" x2="0.32385" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.19685" x2="-0.05715" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-0.83185" y1="0.19685" x2="-0.42545" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.19685" x2="-1.00965" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-1.93675" y1="0.19685" x2="-1.51765" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="0.19685" x2="-2.15265" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.19685" x2="-2.71145" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.19685" x2="-2.97815" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-3.65125" y1="0.19685" x2="-3.38455" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.19685" x2="-3.68935" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-4.47675" y1="0.19685" x2="-4.07035" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="0.19685" x2="-4.70535" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-6.03885" y1="0.19685" x2="-5.59435" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-6.69925" y1="0.19685" x2="-6.28015" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.19685" x2="-6.87705" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.19685" x2="-7.20725" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.19685" x2="-7.57555" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.19685" x2="-7.93115" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="0.19685" x2="-8.43915" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.19685" x2="-9.03605" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.19685" x2="-9.42975" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.19685" x2="-10.21715" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-10.97915" y1="0.19685" x2="-10.71245" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.19685" x2="-11.00455" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.19685" x2="-11.33475" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.19685" x2="-11.58875" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.20955" x2="0.32385" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.20955" x2="-0.04445" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-0.84455" y1="0.20955" x2="-0.41275" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.20955" x2="-1.00965" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-1.94945" y1="0.20955" x2="-1.50495" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.20955" x2="-2.13995" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.20955" x2="-2.71145" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.20955" x2="-2.96545" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-3.66395" y1="0.20955" x2="-3.38455" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.20955" x2="-3.67665" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-4.48945" y1="0.20955" x2="-4.07035" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-5.13715" y1="0.20955" x2="-4.69265" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-6.05155" y1="0.20955" x2="-5.58165" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-6.71195" y1="0.20955" x2="-6.26745" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.20955" x2="-6.87705" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.20955" x2="-7.20725" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.20955" x2="-7.57555" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.20955" x2="-7.93115" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="0.20955" x2="-8.42645" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.20955" x2="-9.03605" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.20955" x2="-9.41705" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.20955" x2="-10.21715" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.20955" x2="-10.69975" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.20955" x2="-11.33475" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.20955" x2="-11.57605" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.22225" x2="0.32385" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.22225" x2="-0.04445" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-0.85725" y1="0.22225" x2="-0.40005" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.22225" x2="-1.00965" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-1.96215" y1="0.22225" x2="-1.49225" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.22225" x2="-2.13995" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.22225" x2="-2.71145" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.22225" x2="-2.95275" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.22225" x2="-3.38455" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-4.50215" y1="0.22225" x2="-4.05765" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.22225" x2="-4.69265" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-6.06425" y1="0.22225" x2="-5.56895" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-6.72465" y1="0.22225" x2="-6.25475" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.22225" x2="-6.87705" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.22225" x2="-7.20725" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.22225" x2="-7.56285" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.22225" x2="-7.93115" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="0.22225" x2="-8.41375" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.22225" x2="-9.03605" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.22225" x2="-9.40435" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.22225" x2="-10.21715" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.22225" x2="-10.69975" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.22225" x2="-11.33475" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.23495" x2="0.32385" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.23495" x2="-0.04445" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.23495" x2="-0.38735" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.23495" x2="-1.00965" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.23495" x2="-1.49225" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.23495" x2="-2.12725" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.23495" x2="-2.71145" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.23495" x2="-3.37185" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-4.50215" y1="0.23495" x2="-4.04495" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.23495" x2="-4.67995" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="0.23495" x2="-5.56895" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.23495" x2="-6.25475" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.23495" x2="-6.87705" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.23495" x2="-7.20725" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.23495" x2="-7.56285" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.23495" x2="-7.93115" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.23495" x2="-8.41375" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.23495" x2="-9.03605" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.23495" x2="-9.40435" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.23495" x2="-10.21715" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.23495" x2="-10.69975" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.23495" x2="-11.33475" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.24765" x2="0.32385" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.24765" x2="-0.04445" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-0.61595" y1="0.24765" x2="-0.38735" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.24765" x2="-0.64135" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.24765" x2="-1.00965" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-1.72085" y1="0.24765" x2="-1.47955" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.24765" x2="-1.74625" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-2.34315" y1="0.24765" x2="-2.12725" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.24765" x2="-2.36855" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="0.24765" x2="-2.71145" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.24765" x2="-3.00355" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-3.61315" y1="0.24765" x2="-3.37185" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.24765" x2="-3.63855" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-4.28625" y1="0.24765" x2="-4.04495" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.24765" x2="-4.31165" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="0.24765" x2="-4.66725" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.24765" x2="-4.93395" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="0.24765" x2="-5.55625" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-6.48335" y1="0.24765" x2="-6.24205" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.24765" x2="-6.50875" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.24765" x2="-6.87705" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.24765" x2="-7.20725" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.24765" x2="-7.56285" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.24765" x2="-7.93115" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-8.64235" y1="0.24765" x2="-8.40105" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.24765" x2="-8.66775" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.24765" x2="-9.03605" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-9.60755" y1="0.24765" x2="-9.39165" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.24765" x2="-9.64565" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.24765" x2="-10.21715" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-10.94105" y1="0.24765" x2="-10.68705" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.24765" x2="-10.96645" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-11.60145" y1="0.24765" x2="-11.33475" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.24765" x2="-11.62685" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.26035" x2="0.32385" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.26035" x2="-0.04445" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-0.59055" y1="0.26035" x2="-0.37465" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.26035" x2="-0.66675" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.26035" x2="-1.00965" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.26035" x2="-1.47955" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.26035" x2="-1.77165" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-2.31775" y1="0.26035" x2="-2.11455" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.26035" x2="-2.40665" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.26035" x2="-2.71145" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.26035" x2="-3.02895" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.26035" x2="-3.37185" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.26035" x2="-3.66395" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-4.26085" y1="0.26035" x2="-4.03225" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.26035" x2="-4.32435" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="0.26035" x2="-4.66725" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.26035" x2="-4.95935" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.26035" x2="-5.55625" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.26035" x2="-6.24205" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.26035" x2="-6.53415" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.26035" x2="-6.87705" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.26035" x2="-7.20725" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.26035" x2="-7.56285" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.26035" x2="-7.93115" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.26035" x2="-8.40105" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.26035" x2="-8.69315" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.26035" x2="-9.03605" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-9.59485" y1="0.26035" x2="-9.39165" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.26035" x2="-9.67105" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.26035" x2="-10.21715" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-10.91565" y1="0.26035" x2="-10.68705" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.26035" x2="-10.99185" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="0.26035" x2="-11.33475" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.26035" x2="-11.65225" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.27305" x2="0.32385" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.27305" x2="-0.04445" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-0.59055" y1="0.27305" x2="-0.37465" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.27305" x2="-0.67945" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.27305" x2="-1.00965" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.27305" x2="-1.47955" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.27305" x2="-1.77165" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.27305" x2="-2.11455" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.27305" x2="-2.40665" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.27305" x2="-2.71145" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.27305" x2="-3.04165" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.27305" x2="-3.37185" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.27305" x2="-3.67665" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.27305" x2="-4.03225" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.27305" x2="-4.33705" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.27305" x2="-4.66725" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.27305" x2="-4.97205" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-5.78485" y1="0.27305" x2="-5.54355" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.27305" x2="-5.86105" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.27305" x2="-6.24205" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.27305" x2="-6.53415" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.27305" x2="-6.87705" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.27305" x2="-7.20725" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.27305" x2="-7.56285" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.27305" x2="-7.93115" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.27305" x2="-8.40105" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.27305" x2="-8.70585" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.27305" x2="-9.03605" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.27305" x2="-9.39165" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.27305" x2="-9.68375" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.27305" x2="-10.21715" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.27305" x2="-10.68705" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.27305" x2="-10.99185" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="0.27305" x2="-11.33475" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.27305" x2="-11.65225" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.28575" x2="0.31115" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.28575" x2="-0.04445" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.28575" x2="-0.37465" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.28575" x2="-0.67945" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.28575" x2="-1.00965" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.28575" x2="-1.46685" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.28575" x2="-1.78435" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.28575" x2="-2.11455" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.28575" x2="-2.41935" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.28575" x2="-2.71145" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.28575" x2="-3.04165" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.28575" x2="-3.37185" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.28575" x2="-3.67665" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.28575" x2="-4.03225" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.28575" x2="-4.33705" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.28575" x2="-4.65455" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.28575" x2="-4.97205" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.28575" x2="-5.54355" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.28575" x2="-5.87375" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.28575" x2="-6.22935" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.28575" x2="-6.54685" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.28575" x2="-6.87705" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.28575" x2="-7.20725" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.28575" x2="-7.56285" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.28575" x2="-7.93115" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.28575" x2="-8.40105" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.28575" x2="-8.70585" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.28575" x2="-9.03605" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.28575" x2="-9.37895" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.28575" x2="-9.68375" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.28575" x2="-10.21715" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.28575" x2="-10.68705" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.28575" x2="-10.99185" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.28575" x2="-11.33475" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.28575" x2="-11.66495" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.29845" x2="0.31115" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.29845" x2="-0.04445" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.29845" x2="-0.36195" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.29845" x2="-0.69215" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.29845" x2="-1.00965" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.29845" x2="-1.46685" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.29845" x2="-1.78435" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.29845" x2="-2.11455" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.29845" x2="-2.41935" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.29845" x2="-2.71145" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.29845" x2="-3.04165" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.29845" x2="-3.37185" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.29845" x2="-3.67665" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.29845" x2="-4.01955" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.29845" x2="-4.34975" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.29845" x2="-4.65455" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.29845" x2="-4.97205" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.29845" x2="-5.54355" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.29845" x2="-5.87375" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.29845" x2="-6.22935" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.29845" x2="-6.54685" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.29845" x2="-6.87705" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.29845" x2="-7.20725" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.29845" x2="-7.56285" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.29845" x2="-7.93115" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.29845" x2="-8.38835" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.29845" x2="-8.70585" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.29845" x2="-9.03605" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.29845" x2="-9.37895" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.29845" x2="-9.68375" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.29845" x2="-10.21715" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.29845" x2="-10.68705" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.29845" x2="-11.00455" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.29845" x2="-11.33475" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.29845" x2="-11.66495" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.31115" x2="0.31115" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.31115" x2="-0.04445" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.31115" x2="-0.36195" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.31115" x2="-0.69215" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.31115" x2="-1.00965" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.31115" x2="-1.46685" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.31115" x2="-1.78435" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.31115" x2="-2.11455" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.31115" x2="-2.41935" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.31115" x2="-2.71145" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.31115" x2="-3.04165" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.31115" x2="-3.37185" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.31115" x2="-3.67665" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.31115" x2="-4.01955" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.31115" x2="-4.34975" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.31115" x2="-4.65455" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.31115" x2="-4.97205" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.31115" x2="-5.54355" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.31115" x2="-5.87375" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.31115" x2="-6.22935" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.31115" x2="-6.54685" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.31115" x2="-6.87705" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.31115" x2="-7.20725" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.31115" x2="-7.56285" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.31115" x2="-7.93115" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.31115" x2="-8.38835" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.31115" x2="-8.70585" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.31115" x2="-9.03605" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.31115" x2="-9.37895" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.31115" x2="-9.68375" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.31115" x2="-10.21715" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.31115" x2="-10.68705" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.31115" x2="-11.00455" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.31115" x2="-11.33475" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.31115" x2="-11.66495" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.32385" x2="0.31115" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.32385" x2="-0.36195" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.32385" x2="-0.69215" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.32385" x2="-1.00965" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.32385" x2="-1.46685" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.32385" x2="-1.78435" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.32385" x2="-2.10185" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.32385" x2="-2.41935" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.32385" x2="-2.71145" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.32385" x2="-3.04165" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.32385" x2="-3.37185" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.32385" x2="-3.67665" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.32385" x2="-4.01955" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.32385" x2="-4.34975" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.32385" x2="-4.65455" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.32385" x2="-4.97205" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.32385" x2="-5.53085" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.32385" x2="-5.87375" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.32385" x2="-6.22935" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.32385" x2="-6.54685" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.32385" x2="-6.87705" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.32385" x2="-7.20725" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.32385" x2="-7.56285" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.32385" x2="-7.93115" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.32385" x2="-8.38835" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.32385" x2="-8.70585" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.32385" x2="-9.03605" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.32385" x2="-9.37895" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.32385" x2="-9.68375" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.32385" x2="-10.21715" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.32385" x2="-10.68705" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.32385" x2="-11.00455" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.32385" x2="-11.33475" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.32385" x2="-11.66495" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.33655" x2="0.31115" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.33655" x2="-0.36195" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.33655" x2="-0.69215" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.33655" x2="-1.00965" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.33655" x2="-1.46685" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.33655" x2="-1.78435" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.33655" x2="-2.10185" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.33655" x2="-2.41935" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.33655" x2="-2.71145" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.33655" x2="-3.04165" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.33655" x2="-3.37185" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.33655" x2="-3.67665" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.33655" x2="-4.01955" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.33655" x2="-4.34975" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.33655" x2="-4.64185" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.33655" x2="-4.97205" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.33655" x2="-5.53085" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.33655" x2="-5.87375" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.33655" x2="-6.22935" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.33655" x2="-6.54685" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.33655" x2="-6.87705" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.33655" x2="-7.20725" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.33655" x2="-7.56285" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.33655" x2="-7.93115" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.33655" x2="-8.38835" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.33655" x2="-8.70585" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.33655" x2="-9.03605" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.33655" x2="-9.37895" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.33655" x2="-9.68375" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.33655" x2="-10.21715" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.33655" x2="-10.68705" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.33655" x2="-11.00455" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.33655" x2="-11.33475" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.33655" x2="-11.66495" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.34925" x2="0.31115" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.34925" x2="-0.36195" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.34925" x2="-0.69215" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.34925" x2="-1.00965" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.34925" x2="-1.46685" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.34925" x2="-1.78435" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.34925" x2="-2.10185" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.34925" x2="-2.41935" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.34925" x2="-2.71145" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.34925" x2="-3.04165" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.34925" x2="-3.37185" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.34925" x2="-3.67665" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.34925" x2="-4.01955" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.34925" x2="-4.34975" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.34925" x2="-4.64185" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.34925" x2="-4.97205" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.34925" x2="-5.53085" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.34925" x2="-5.87375" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.34925" x2="-6.22935" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.34925" x2="-6.54685" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.34925" x2="-6.87705" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.34925" x2="-7.20725" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.34925" x2="-7.56285" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.34925" x2="-7.93115" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.34925" x2="-8.38835" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.34925" x2="-8.70585" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.34925" x2="-9.03605" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.34925" x2="-9.37895" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.34925" x2="-9.68375" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.34925" x2="-10.21715" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.34925" x2="-10.68705" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.34925" x2="-11.00455" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.34925" x2="-11.33475" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.34925" x2="-11.66495" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.36195" x2="0.31115" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.36195" x2="-0.34925" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.36195" x2="-0.69215" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.36195" x2="-1.00965" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.36195" x2="-1.46685" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.36195" x2="-1.78435" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.36195" x2="-2.10185" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.36195" x2="-2.41935" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.36195" x2="-2.71145" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.36195" x2="-3.04165" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.36195" x2="-3.37185" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.36195" x2="-3.67665" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.36195" x2="-4.01955" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.36195" x2="-4.34975" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.36195" x2="-4.64185" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.36195" x2="-4.97205" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.36195" x2="-5.53085" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.36195" x2="-5.87375" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.36195" x2="-6.22935" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.36195" x2="-6.54685" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.36195" x2="-6.87705" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.36195" x2="-7.20725" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.36195" x2="-7.56285" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.36195" x2="-7.93115" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.36195" x2="-8.38835" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.36195" x2="-8.70585" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.36195" x2="-9.03605" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.36195" x2="-9.37895" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.36195" x2="-9.68375" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.36195" x2="-10.21715" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.36195" x2="-10.68705" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.36195" x2="-11.00455" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.36195" x2="-11.33475" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.36195" x2="-11.66495" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.37465" x2="0.31115" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.37465" x2="-0.34925" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.37465" x2="-0.69215" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.37465" x2="-1.00965" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.37465" x2="-1.46685" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.37465" x2="-1.78435" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.37465" x2="-2.10185" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.37465" x2="-2.41935" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.37465" x2="-2.71145" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.37465" x2="-3.04165" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.37465" x2="-3.37185" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.37465" x2="-3.67665" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.37465" x2="-4.01955" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.37465" x2="-4.34975" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.37465" x2="-4.64185" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.37465" x2="-4.97205" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.37465" x2="-5.53085" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.37465" x2="-5.87375" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.37465" x2="-6.22935" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.37465" x2="-6.54685" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.37465" x2="-6.87705" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.37465" x2="-7.20725" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.37465" x2="-7.56285" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.37465" x2="-7.93115" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.37465" x2="-8.38835" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.37465" x2="-8.70585" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.37465" x2="-9.03605" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.37465" x2="-9.37895" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.37465" x2="-9.68375" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.37465" x2="-10.21715" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.37465" x2="-10.68705" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.37465" x2="-11.00455" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.37465" x2="-11.33475" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.37465" x2="-11.66495" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.38735" x2="0.29845" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.38735" x2="-0.34925" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.38735" x2="-0.69215" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.38735" x2="-1.00965" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.38735" x2="-1.45415" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.38735" x2="-1.78435" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.38735" x2="-2.10185" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.38735" x2="-2.41935" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.38735" x2="-2.71145" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.38735" x2="-3.04165" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.38735" x2="-3.37185" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.38735" x2="-3.67665" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.38735" x2="-4.01955" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.38735" x2="-4.34975" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.38735" x2="-4.64185" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.38735" x2="-4.97205" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.38735" x2="-5.53085" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.38735" x2="-5.88645" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.38735" x2="-6.21665" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.38735" x2="-6.54685" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.38735" x2="-6.87705" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.38735" x2="-7.20725" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.38735" x2="-7.56285" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.38735" x2="-7.93115" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.38735" x2="-8.38835" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.38735" x2="-8.70585" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.38735" x2="-9.03605" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.38735" x2="-9.37895" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.38735" x2="-9.68375" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.38735" x2="-10.21715" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.38735" x2="-10.68705" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.38735" x2="-11.00455" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.38735" x2="-11.33475" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.38735" x2="-11.66495" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.40005" x2="0.29845" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.40005" x2="-0.34925" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.40005" x2="-0.69215" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.40005" x2="-1.00965" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.40005" x2="-1.45415" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.40005" x2="-1.78435" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.40005" x2="-2.10185" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.40005" x2="-2.40665" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.40005" x2="-2.71145" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.40005" x2="-3.04165" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.40005" x2="-3.37185" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.40005" x2="-3.67665" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.40005" x2="-4.00685" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.40005" x2="-4.34975" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.40005" x2="-4.64185" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.40005" x2="-4.97205" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.40005" x2="-5.53085" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.40005" x2="-5.88645" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.40005" x2="-6.21665" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.40005" x2="-6.54685" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.40005" x2="-6.87705" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.40005" x2="-7.20725" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.40005" x2="-7.56285" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.40005" x2="-7.93115" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.40005" x2="-8.38835" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.40005" x2="-8.70585" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.40005" x2="-9.03605" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.40005" x2="-9.37895" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.40005" x2="-9.68375" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.40005" x2="-10.21715" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.40005" x2="-10.68705" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.40005" x2="-11.00455" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.40005" x2="-11.33475" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.40005" x2="-11.66495" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.41275" x2="0.29845" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.41275" x2="-0.34925" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.41275" x2="-0.69215" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.41275" x2="-1.00965" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.41275" x2="-1.45415" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.41275" x2="-1.78435" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.41275" x2="-2.40665" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.41275" x2="-2.71145" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.41275" x2="-3.04165" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.41275" x2="-3.37185" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.41275" x2="-3.67665" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.41275" x2="-4.00685" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.41275" x2="-4.34975" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.41275" x2="-4.64185" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.41275" x2="-4.97205" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.41275" x2="-5.53085" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.41275" x2="-5.88645" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.41275" x2="-6.21665" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.41275" x2="-6.54685" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.41275" x2="-6.87705" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.41275" x2="-7.20725" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.41275" x2="-7.56285" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.41275" x2="-7.93115" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.41275" x2="-8.37565" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.41275" x2="-8.70585" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.41275" x2="-9.03605" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.41275" x2="-9.67105" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.41275" x2="-10.21715" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.41275" x2="-10.68705" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.41275" x2="-11.00455" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.41275" x2="-11.33475" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.41275" x2="-11.66495" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.42545" x2="0.29845" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.42545" x2="-0.34925" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.42545" x2="-0.69215" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.42545" x2="-1.00965" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.42545" x2="-1.45415" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.42545" x2="-1.78435" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.42545" x2="-2.38125" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.42545" x2="-2.71145" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.42545" x2="-3.04165" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.42545" x2="-3.37185" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.42545" x2="-3.67665" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.42545" x2="-4.00685" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.42545" x2="-4.34975" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.42545" x2="-4.64185" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.42545" x2="-4.97205" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.42545" x2="-5.53085" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.42545" x2="-5.88645" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.42545" x2="-6.21665" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.42545" x2="-6.54685" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.42545" x2="-6.87705" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.42545" x2="-7.20725" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.42545" x2="-7.56285" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.42545" x2="-7.93115" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.42545" x2="-8.37565" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.42545" x2="-8.70585" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.42545" x2="-9.03605" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.42545" x2="-9.65835" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.42545" x2="-10.21715" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.42545" x2="-10.68705" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.42545" x2="-11.00455" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.42545" x2="-11.33475" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.42545" x2="-11.66495" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.43815" x2="0.29845" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.43815" x2="-0.34925" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.43815" x2="-0.69215" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.43815" x2="-1.00965" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.43815" x2="-1.45415" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.43815" x2="-1.78435" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.43815" x2="-2.36855" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.43815" x2="-2.71145" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.43815" x2="-3.04165" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.43815" x2="-3.37185" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.43815" x2="-3.67665" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.43815" x2="-4.00685" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.43815" x2="-4.34975" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.43815" x2="-4.64185" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.43815" x2="-4.97205" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.43815" x2="-5.53085" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.43815" x2="-5.88645" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.43815" x2="-6.21665" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.43815" x2="-6.54685" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.43815" x2="-6.87705" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.43815" x2="-7.20725" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.43815" x2="-7.56285" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.43815" x2="-7.93115" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.43815" x2="-8.37565" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.43815" x2="-8.70585" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.43815" x2="-9.03605" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.43815" x2="-9.63295" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.43815" x2="-10.21715" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.43815" x2="-10.68705" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.43815" x2="-11.00455" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.43815" x2="-11.33475" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.43815" x2="-11.66495" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.45085" x2="0.29845" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.45085" x2="-0.34925" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.45085" x2="-0.69215" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.45085" x2="-1.00965" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.45085" x2="-1.45415" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.45085" x2="-1.78435" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.45085" x2="-2.34315" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.45085" x2="-2.71145" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.45085" x2="-3.04165" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.45085" x2="-3.37185" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.45085" x2="-3.67665" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.45085" x2="-4.00685" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-4.55295" y1="0.45085" x2="-4.34975" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.45085" x2="-4.64185" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.45085" x2="-4.97205" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.45085" x2="-5.51815" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.45085" x2="-5.88645" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.45085" x2="-6.21665" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.45085" x2="-6.54685" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.45085" x2="-6.87705" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.45085" x2="-7.20725" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.45085" x2="-7.56285" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.45085" x2="-7.93115" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.45085" x2="-8.37565" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.45085" x2="-8.70585" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.45085" x2="-9.03605" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.45085" x2="-9.62025" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.45085" x2="-10.21715" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.45085" x2="-10.68705" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.45085" x2="-11.00455" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.45085" x2="-11.33475" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.45085" x2="-11.66495" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.46355" x2="0.29845" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.46355" x2="-0.34925" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.46355" x2="-1.00965" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.46355" x2="-1.45415" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.46355" x2="-1.78435" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.46355" x2="-2.31775" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.46355" x2="-2.71145" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.46355" x2="-3.04165" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.46355" x2="-3.37185" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.46355" x2="-3.67665" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.46355" x2="-4.00685" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.46355" x2="-4.64185" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.46355" x2="-5.51815" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.46355" x2="-5.88645" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.46355" x2="-6.21665" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.46355" x2="-6.54685" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.46355" x2="-6.87705" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.46355" x2="-7.20725" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.46355" x2="-7.56285" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.46355" x2="-7.93115" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.46355" x2="-8.37565" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.46355" x2="-8.70585" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.46355" x2="-9.03605" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.46355" x2="-9.59485" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.46355" x2="-10.21715" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-10.91565" y1="0.46355" x2="-10.69975" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.46355" x2="-11.00455" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.46355" x2="-11.33475" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.46355" x2="-11.66495" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.47625" x2="0.28575" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.47625" x2="-0.03175" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.47625" x2="-0.34925" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.47625" x2="-1.00965" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.47625" x2="-1.45415" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.47625" x2="-1.78435" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.47625" x2="-2.30505" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.47625" x2="-2.71145" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.47625" x2="-3.04165" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-3.60045" y1="0.47625" x2="-3.38455" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.47625" x2="-3.67665" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.47625" x2="-4.00685" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.47625" x2="-4.64185" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.47625" x2="-5.51815" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.47625" x2="-5.88645" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.47625" x2="-6.21665" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.47625" x2="-6.54685" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.47625" x2="-6.87705" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.47625" x2="-7.20725" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.47625" x2="-7.56285" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.47625" x2="-7.93115" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.47625" x2="-8.37565" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.47625" x2="-8.70585" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.47625" x2="-9.03605" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.47625" x2="-9.56945" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.47625" x2="-10.21715" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-10.91565" y1="0.47625" x2="-10.69975" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.47625" x2="-11.00455" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.47625" x2="-11.33475" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.47625" x2="-11.66495" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.48895" x2="0.28575" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.48895" x2="-0.03175" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.48895" x2="-0.34925" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.48895" x2="-1.00965" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.48895" x2="-1.45415" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.48895" x2="-1.78435" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.48895" x2="-2.27965" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.48895" x2="-2.71145" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.48895" x2="-3.04165" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-3.61315" y1="0.48895" x2="-3.38455" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.48895" x2="-3.67665" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.48895" x2="-4.00685" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.48895" x2="-4.64185" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.48895" x2="-5.51815" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.48895" x2="-5.88645" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.48895" x2="-6.21665" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.48895" x2="-6.54685" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.48895" x2="-6.87705" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.48895" x2="-7.20725" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.48895" x2="-7.56285" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.48895" x2="-7.93115" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.48895" x2="-8.37565" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.48895" x2="-8.70585" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.48895" x2="-9.03605" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.48895" x2="-9.54405" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.48895" x2="-10.21715" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-10.92835" y1="0.48895" x2="-10.71245" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.48895" x2="-11.00455" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.48895" x2="-11.33475" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.48895" x2="-11.66495" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.50165" x2="0.28575" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.50165" x2="-0.03175" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.50165" x2="-0.34925" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.50165" x2="-1.00965" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.50165" x2="-1.45415" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.50165" x2="-1.78435" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.50165" x2="-2.25425" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.50165" x2="-2.71145" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.50165" x2="-3.04165" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-3.62585" y1="0.50165" x2="-3.39725" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.50165" x2="-3.67665" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.50165" x2="-4.00685" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.50165" x2="-4.64185" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.50165" x2="-5.51815" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.50165" x2="-5.88645" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.50165" x2="-6.21665" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.50165" x2="-6.54685" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.50165" x2="-6.87705" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.50165" x2="-7.20725" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.50165" x2="-7.56285" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.50165" x2="-7.93115" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.50165" x2="-8.37565" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.50165" x2="-8.70585" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.50165" x2="-9.03605" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.50165" x2="-9.53135" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.50165" x2="-10.21715" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-10.94105" y1="0.50165" x2="-10.71245" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.50165" x2="-11.00455" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.50165" x2="-11.33475" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.50165" x2="-11.66495" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.51435" x2="0.28575" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.51435" x2="-0.03175" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.51435" x2="-0.34925" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.51435" x2="-1.00965" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.51435" x2="-1.45415" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.51435" x2="-1.78435" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.51435" x2="-2.22885" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.51435" x2="-2.71145" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.51435" x2="-3.04165" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-3.63855" y1="0.51435" x2="-3.40995" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.51435" x2="-3.67665" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.51435" x2="-4.00685" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.51435" x2="-4.64185" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.51435" x2="-5.51815" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.51435" x2="-5.88645" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.51435" x2="-6.21665" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.51435" x2="-6.54685" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.51435" x2="-6.87705" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.51435" x2="-7.20725" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.51435" x2="-7.56285" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.51435" x2="-7.93115" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.51435" x2="-8.37565" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.51435" x2="-8.70585" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.51435" x2="-9.03605" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.51435" x2="-9.50595" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.51435" x2="-10.21715" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-10.95375" y1="0.51435" x2="-10.73785" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.51435" x2="-11.00455" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.51435" x2="-11.33475" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.51435" x2="-11.66495" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.52705" x2="0.28575" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.52705" x2="-0.03175" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.52705" x2="-0.34925" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.52705" x2="-1.00965" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.52705" x2="-1.45415" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.52705" x2="-1.78435" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="0.52705" x2="-2.21615" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.52705" x2="-2.71145" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.52705" x2="-3.04165" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-3.65125" y1="0.52705" x2="-3.43535" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.52705" x2="-3.67665" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.52705" x2="-4.00685" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.52705" x2="-4.64185" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.52705" x2="-5.51815" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.52705" x2="-6.21665" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.52705" x2="-6.54685" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.52705" x2="-6.87705" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.52705" x2="-7.20725" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.52705" x2="-7.56285" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.52705" x2="-7.93115" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.52705" x2="-8.37565" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.52705" x2="-8.70585" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.52705" x2="-9.03605" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="0.52705" x2="-9.48055" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.52705" x2="-10.21715" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-10.97915" y1="0.52705" x2="-10.75055" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.52705" x2="-11.00455" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.52705" x2="-11.33475" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.52705" x2="-11.66495" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.53975" x2="0.28575" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.53975" x2="-0.03175" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.53975" x2="-0.34925" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.53975" x2="-1.00965" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.53975" x2="-1.45415" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.53975" x2="-1.78435" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="0.53975" x2="-2.19075" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.53975" x2="-2.71145" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.53975" x2="-3.04165" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.53975" x2="-3.46075" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.53975" x2="-4.00685" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.53975" x2="-4.64185" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.53975" x2="-5.51815" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.53975" x2="-6.21665" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.53975" x2="-6.54685" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.53975" x2="-6.87705" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.53975" x2="-7.20725" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.53975" x2="-7.56285" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.53975" x2="-7.93115" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.53975" x2="-8.37565" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.53975" x2="-8.70585" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.53975" x2="-9.03605" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-9.82345" y1="0.53975" x2="-9.46785" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.53975" x2="-10.21715" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.53975" x2="-10.77595" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.53975" x2="-11.33475" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.53975" x2="-11.66495" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.55245" x2="0.28575" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.55245" x2="-0.03175" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.55245" x2="-0.34925" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.55245" x2="-1.00965" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.55245" x2="-1.45415" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.55245" x2="-1.78435" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="0.55245" x2="-2.17805" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.55245" x2="-2.71145" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.55245" x2="-3.04165" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.55245" x2="-3.48615" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.55245" x2="-4.00685" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.55245" x2="-4.64185" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.55245" x2="-5.51815" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.55245" x2="-6.21665" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.55245" x2="-6.54685" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.55245" x2="-6.87705" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.55245" x2="-7.20725" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.55245" x2="-7.56285" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.55245" x2="-7.93115" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.55245" x2="-8.37565" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.55245" x2="-8.70585" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.55245" x2="-9.03605" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="0.55245" x2="-9.44245" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.55245" x2="-10.21715" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.55245" x2="-10.80135" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.55245" x2="-11.33475" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.55245" x2="-11.66495" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.56515" x2="0.28575" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.56515" x2="-0.03175" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.56515" x2="-0.34925" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.56515" x2="-1.00965" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.56515" x2="-1.45415" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.56515" x2="-1.78435" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="0.56515" x2="-2.16535" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.56515" x2="-2.71145" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.56515" x2="-3.04165" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.56515" x2="-3.52425" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.56515" x2="-4.00685" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.56515" x2="-4.64185" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.56515" x2="-5.51815" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.56515" x2="-6.21665" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.56515" x2="-6.54685" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.56515" x2="-6.87705" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.56515" x2="-7.20725" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.56515" x2="-7.56285" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.56515" x2="-7.93115" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.56515" x2="-8.37565" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.56515" x2="-8.70585" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.56515" x2="-9.03605" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-9.78535" y1="0.56515" x2="-9.42975" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.56515" x2="-10.21715" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.56515" x2="-10.83945" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.56515" x2="-11.33475" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.56515" x2="-11.66495" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.57785" x2="0.27305" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.57785" x2="-0.01905" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.57785" x2="-0.34925" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.57785" x2="-1.00965" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.57785" x2="-1.45415" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.57785" x2="-1.78435" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="0.57785" x2="-2.13995" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.57785" x2="-2.71145" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.57785" x2="-3.04165" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.57785" x2="-3.54965" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.57785" x2="-4.00685" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.57785" x2="-4.64185" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.57785" x2="-5.51815" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.57785" x2="-6.21665" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.57785" x2="-6.54685" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.57785" x2="-6.87705" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.57785" x2="-7.20725" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.57785" x2="-7.56285" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.57785" x2="-7.93115" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.57785" x2="-8.37565" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.57785" x2="-8.70585" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.57785" x2="-9.03605" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-9.75995" y1="0.57785" x2="-9.41705" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.57785" x2="-10.21715" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.57785" x2="-10.86485" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.57785" x2="-11.33475" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.57785" x2="-11.66495" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.59055" x2="0.27305" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.59055" x2="-0.01905" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.59055" x2="-0.34925" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.59055" x2="-1.00965" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.59055" x2="-1.45415" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.59055" x2="-1.78435" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-2.47015" y1="0.59055" x2="-2.13995" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.59055" x2="-2.71145" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.59055" x2="-3.04165" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.59055" x2="-3.58775" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.59055" x2="-4.00685" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.59055" x2="-4.64185" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.59055" x2="-5.51815" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.59055" x2="-6.21665" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.59055" x2="-6.54685" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.59055" x2="-6.87705" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.59055" x2="-7.20725" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.59055" x2="-7.56285" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.59055" x2="-7.93115" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.59055" x2="-8.37565" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.59055" x2="-8.70585" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.59055" x2="-9.03605" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-9.73455" y1="0.59055" x2="-9.40435" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.59055" x2="-10.21715" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.59055" x2="-10.90295" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.59055" x2="-11.33475" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.59055" x2="-11.66495" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.60325" x2="0.27305" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.60325" x2="-0.01905" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.60325" x2="-0.34925" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.60325" x2="-1.00965" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.60325" x2="-1.45415" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.60325" x2="-1.78435" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-2.44475" y1="0.60325" x2="-2.12725" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.60325" x2="-2.71145" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.60325" x2="-3.04165" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.60325" x2="-3.61315" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.60325" x2="-4.00685" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.60325" x2="-4.64185" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.60325" x2="-5.51815" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.60325" x2="-6.21665" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.60325" x2="-6.54685" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.60325" x2="-6.87705" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.60325" x2="-7.20725" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.60325" x2="-7.56285" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.60325" x2="-7.93115" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.60325" x2="-8.37565" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.60325" x2="-8.70585" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.60325" x2="-9.03605" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-9.70915" y1="0.60325" x2="-9.39165" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.60325" x2="-10.21715" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.60325" x2="-10.92835" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.60325" x2="-11.33475" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.60325" x2="-11.66495" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.61595" x2="0.27305" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.61595" x2="-0.01905" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.61595" x2="-0.34925" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-1.25095" y1="0.61595" x2="-1.00965" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.61595" x2="-1.45415" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.61595" x2="-1.78435" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-2.41935" y1="0.61595" x2="-2.11455" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.61595" x2="-2.71145" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.61595" x2="-3.04165" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.61595" x2="-3.63855" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.61595" x2="-4.00685" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.61595" x2="-4.64185" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.61595" x2="-5.51815" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.61595" x2="-6.21665" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.61595" x2="-6.54685" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.61595" x2="-6.87705" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.61595" x2="-7.20725" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.61595" x2="-7.56285" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-8.17245" y1="0.61595" x2="-7.93115" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.61595" x2="-8.37565" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.61595" x2="-8.70585" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.61595" x2="-9.03605" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-9.68375" y1="0.61595" x2="-9.39165" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.61595" x2="-10.21715" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.61595" x2="-10.95375" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.61595" x2="-11.33475" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.61595" x2="-11.66495" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.62865" x2="0.27305" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.62865" x2="-0.01905" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.62865" x2="-0.34925" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-1.25095" y1="0.62865" x2="-1.00965" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.62865" x2="-1.45415" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.62865" x2="-1.78435" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-2.39395" y1="0.62865" x2="-2.11455" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.62865" x2="-2.71145" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.62865" x2="-3.04165" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.62865" x2="-3.66395" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.62865" x2="-4.00685" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.62865" x2="-4.64185" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.62865" x2="-5.51815" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.62865" x2="-6.21665" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.62865" x2="-6.54685" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.62865" x2="-6.87705" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.62865" x2="-7.20725" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.62865" x2="-7.56285" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-8.17245" y1="0.62865" x2="-7.93115" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.62865" x2="-8.37565" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.62865" x2="-8.70585" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.62865" x2="-9.03605" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-9.65835" y1="0.62865" x2="-9.39165" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.62865" x2="-10.21715" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.62865" x2="-10.97915" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.62865" x2="-11.33475" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.62865" x2="-11.66495" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.64135" x2="0.27305" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.64135" x2="-0.01905" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.64135" x2="-0.34925" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-1.26365" y1="0.64135" x2="-1.00965" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.64135" x2="-1.45415" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.64135" x2="-1.78435" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-2.36855" y1="0.64135" x2="-2.11455" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.64135" x2="-2.71145" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.64135" x2="-3.04165" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.64135" x2="-3.67665" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.64135" x2="-4.00685" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.64135" x2="-4.64185" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.64135" x2="-5.51815" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.64135" x2="-6.21665" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.64135" x2="-6.54685" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.64135" x2="-6.87705" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.64135" x2="-7.20725" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.64135" x2="-7.56285" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-8.18515" y1="0.64135" x2="-7.93115" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.64135" x2="-8.37565" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.64135" x2="-8.70585" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.64135" x2="-9.03605" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-9.63295" y1="0.64135" x2="-9.37895" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.64135" x2="-10.21715" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.64135" x2="-10.99185" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.64135" x2="-11.33475" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.64135" x2="-11.66495" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.65405" x2="0.27305" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.65405" x2="-0.01905" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.65405" x2="-0.34925" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.65405" x2="-0.66675" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-1.26365" y1="0.65405" x2="-1.00965" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.65405" x2="-1.45415" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.65405" x2="-1.78435" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-2.34315" y1="0.65405" x2="-2.10185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.65405" x2="-2.71145" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.65405" x2="-3.04165" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.65405" x2="-3.37185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.65405" x2="-3.67665" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.65405" x2="-4.00685" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.65405" x2="-4.64185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.65405" x2="-4.95935" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.65405" x2="-5.51815" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.65405" x2="-6.21665" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.65405" x2="-6.54685" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.65405" x2="-6.87705" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.65405" x2="-7.20725" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.65405" x2="-7.56285" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-8.18515" y1="0.65405" x2="-7.93115" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.65405" x2="-8.37565" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.65405" x2="-8.70585" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.65405" x2="-9.03605" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-9.62025" y1="0.65405" x2="-9.37895" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.65405" x2="-10.21715" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.65405" x2="-10.68705" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.65405" x2="-10.99185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.65405" x2="-11.33475" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.65405" x2="-11.66495" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.66675" x2="0.26035" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.66675" x2="-0.01905" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.66675" x2="-0.34925" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.66675" x2="-0.67945" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-1.28905" y1="0.66675" x2="-1.00965" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.66675" x2="-1.45415" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.66675" x2="-1.78435" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-2.33045" y1="0.66675" x2="-2.10185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.66675" x2="-2.71145" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.66675" x2="-3.04165" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.66675" x2="-3.37185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.66675" x2="-3.67665" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.66675" x2="-4.00685" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.66675" x2="-4.33705" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.66675" x2="-4.64185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.66675" x2="-4.95935" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.66675" x2="-5.51815" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.66675" x2="-6.21665" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.66675" x2="-6.54685" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.66675" x2="-6.87705" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.66675" x2="-7.20725" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.66675" x2="-7.56285" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-8.21055" y1="0.66675" x2="-7.93115" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.66675" x2="-8.38835" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.66675" x2="-8.70585" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.66675" x2="-9.03605" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-9.60755" y1="0.66675" x2="-9.37895" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.66675" x2="-10.21715" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.66675" x2="-10.68705" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.66675" x2="-10.99185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.66675" x2="-11.33475" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.66675" x2="-11.66495" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.67945" x2="0.26035" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.67945" x2="-0.01905" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.67945" x2="-0.34925" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.67945" x2="-0.67945" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-1.31445" y1="0.67945" x2="-1.00965" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.67945" x2="-1.45415" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.67945" x2="-1.78435" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-2.31775" y1="0.67945" x2="-2.10185" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.67945" x2="-2.71145" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.67945" x2="-3.04165" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.67945" x2="-3.37185" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.67945" x2="-3.67665" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.67945" x2="-4.00685" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.67945" x2="-4.33705" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.67945" x2="-4.64185" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.67945" x2="-4.95935" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.67945" x2="-5.51815" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.67945" x2="-6.21665" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.67945" x2="-6.54685" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.67945" x2="-6.87705" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.67945" x2="-7.20725" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.67945" x2="-7.56285" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-8.23595" y1="0.67945" x2="-7.93115" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.67945" x2="-8.38835" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.67945" x2="-8.70585" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.67945" x2="-9.03605" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-9.59485" y1="0.67945" x2="-9.36625" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.67945" x2="-10.21715" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.67945" x2="-10.68705" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.67945" x2="-11.00455" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.67945" x2="-11.33475" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.67945" x2="-11.66495" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.69215" x2="0.26035" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.69215" x2="-0.01905" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.69215" x2="-0.34925" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.69215" x2="-0.67945" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.69215" x2="-1.00965" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.69215" x2="-1.46685" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.69215" x2="-1.78435" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.69215" x2="-2.10185" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.69215" x2="-2.71145" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.69215" x2="-3.04165" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.69215" x2="-3.37185" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.69215" x2="-3.67665" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.69215" x2="-4.00685" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.69215" x2="-4.33705" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.69215" x2="-4.64185" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.69215" x2="-4.95935" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.69215" x2="-5.51815" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.69215" x2="-6.22935" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.69215" x2="-6.54685" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.69215" x2="-6.87705" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.69215" x2="-7.20725" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.69215" x2="-7.56285" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.69215" x2="-7.93115" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.69215" x2="-8.38835" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.69215" x2="-8.70585" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.69215" x2="-9.03605" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.69215" x2="-9.36625" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.69215" x2="-10.21715" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.69215" x2="-10.68705" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.69215" x2="-11.00455" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.69215" x2="-11.33475" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.69215" x2="-11.66495" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.70485" x2="0.26035" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.70485" x2="-0.01905" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.70485" x2="-0.34925" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.70485" x2="-0.67945" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.70485" x2="-1.00965" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.70485" x2="-1.46685" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.70485" x2="-1.78435" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.70485" x2="-2.10185" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.70485" x2="-2.40665" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.70485" x2="-2.71145" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.70485" x2="-3.04165" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.70485" x2="-3.37185" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.70485" x2="-3.67665" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.70485" x2="-4.00685" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.70485" x2="-4.33705" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.70485" x2="-4.64185" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.70485" x2="-4.95935" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.70485" x2="-5.51815" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.70485" x2="-6.22935" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.70485" x2="-6.54685" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.70485" x2="-6.87705" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.70485" x2="-7.20725" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.70485" x2="-7.56285" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.70485" x2="-7.93115" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.70485" x2="-8.38835" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.70485" x2="-8.70585" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.70485" x2="-9.03605" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.70485" x2="-9.36625" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.70485" x2="-9.68375" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.70485" x2="-10.21715" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.70485" x2="-10.68705" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.70485" x2="-11.00455" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.70485" x2="-11.33475" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.70485" x2="-11.66495" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.71755" x2="0.26035" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.71755" x2="-0.00635" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.71755" x2="-0.34925" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.71755" x2="-0.67945" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.71755" x2="-1.00965" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.71755" x2="-1.46685" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.71755" x2="-1.78435" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.71755" x2="-2.10185" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.71755" x2="-2.40665" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.71755" x2="-2.71145" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.71755" x2="-3.04165" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.71755" x2="-3.37185" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.71755" x2="-3.67665" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.71755" x2="-4.01955" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.71755" x2="-4.33705" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.71755" x2="-4.64185" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.71755" x2="-4.95935" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.71755" x2="-5.51815" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.71755" x2="-5.88645" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.71755" x2="-6.22935" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.71755" x2="-6.54685" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.71755" x2="-6.87705" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.71755" x2="-7.20725" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.71755" x2="-7.56285" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.71755" x2="-7.93115" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.71755" x2="-8.38835" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.71755" x2="-8.70585" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.71755" x2="-9.03605" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.71755" x2="-9.36625" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.71755" x2="-9.68375" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.71755" x2="-10.21715" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.71755" x2="-10.68705" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.71755" x2="-11.00455" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.71755" x2="-11.33475" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.71755" x2="-11.66495" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.73025" x2="0.26035" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.73025" x2="-0.00635" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.73025" x2="-0.36195" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.73025" x2="-0.67945" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.73025" x2="-1.00965" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.73025" x2="-1.46685" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.73025" x2="-1.78435" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.73025" x2="-2.10185" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.73025" x2="-2.40665" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.73025" x2="-2.71145" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.73025" x2="-3.04165" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.73025" x2="-3.37185" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.73025" x2="-3.67665" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.73025" x2="-4.01955" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.73025" x2="-4.33705" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.73025" x2="-4.64185" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.73025" x2="-4.95935" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.73025" x2="-5.51815" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.73025" x2="-5.88645" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.73025" x2="-6.22935" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.73025" x2="-6.54685" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.73025" x2="-6.87705" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.73025" x2="-7.20725" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.73025" x2="-7.56285" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.73025" x2="-7.93115" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.73025" x2="-8.38835" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.73025" x2="-8.70585" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.73025" x2="-9.03605" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.73025" x2="-9.36625" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.73025" x2="-9.68375" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.73025" x2="-10.21715" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.73025" x2="-10.68705" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.73025" x2="-11.00455" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.73025" x2="-11.33475" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.73025" x2="-11.66495" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.74295" x2="0.26035" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.74295" x2="-0.00635" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.74295" x2="-0.36195" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.74295" x2="-0.67945" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.74295" x2="-1.00965" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.74295" x2="-1.46685" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.74295" x2="-1.78435" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.74295" x2="-2.10185" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.74295" x2="-2.40665" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.74295" x2="-2.71145" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.74295" x2="-3.04165" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.74295" x2="-3.37185" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.74295" x2="-3.67665" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.74295" x2="-4.01955" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.74295" x2="-4.33705" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.74295" x2="-4.64185" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.74295" x2="-4.95935" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.74295" x2="-5.51815" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.74295" x2="-5.88645" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.74295" x2="-6.22935" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.74295" x2="-6.54685" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.74295" x2="-6.87705" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.74295" x2="-7.20725" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.74295" x2="-7.56285" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.74295" x2="-7.93115" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.74295" x2="-8.38835" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.74295" x2="-8.70585" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.74295" x2="-9.03605" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.74295" x2="-9.36625" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.74295" x2="-9.68375" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.74295" x2="-10.21715" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.74295" x2="-10.68705" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.74295" x2="-11.00455" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.74295" x2="-11.33475" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.74295" x2="-11.66495" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.75565" x2="0.26035" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.75565" x2="-0.00635" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.75565" x2="-0.36195" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.75565" x2="-0.67945" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.75565" x2="-1.00965" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.75565" x2="-1.46685" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.75565" x2="-1.78435" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.75565" x2="-2.10185" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.75565" x2="-2.40665" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.75565" x2="-2.71145" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.75565" x2="-3.04165" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.75565" x2="-3.37185" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.75565" x2="-3.67665" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.75565" x2="-4.01955" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.75565" x2="-4.33705" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.75565" x2="-4.65455" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.75565" x2="-4.95935" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.75565" x2="-5.51815" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.75565" x2="-5.88645" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.75565" x2="-6.22935" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.75565" x2="-6.54685" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.75565" x2="-6.87705" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.75565" x2="-7.20725" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.75565" x2="-7.56285" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.75565" x2="-7.93115" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.75565" x2="-8.38835" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.75565" x2="-8.70585" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.75565" x2="-9.03605" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.75565" x2="-9.36625" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.75565" x2="-9.68375" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.75565" x2="-10.21715" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.75565" x2="-10.68705" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.75565" x2="-11.00455" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.75565" x2="-11.33475" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.75565" x2="-11.66495" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.76835" x2="0.24765" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.76835" x2="-0.00635" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.76835" x2="-0.36195" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.76835" x2="-0.67945" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.76835" x2="-1.00965" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.76835" x2="-1.46685" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.76835" x2="-1.78435" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.76835" x2="-2.10185" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.76835" x2="-2.40665" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.76835" x2="-2.71145" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.76835" x2="-3.04165" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.76835" x2="-3.37185" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.76835" x2="-3.67665" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.76835" x2="-4.01955" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.76835" x2="-4.33705" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.76835" x2="-4.65455" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.76835" x2="-4.95935" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.76835" x2="-5.51815" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.76835" x2="-5.88645" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.76835" x2="-6.22935" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.76835" x2="-6.54685" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.76835" x2="-6.87705" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.76835" x2="-7.20725" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.76835" x2="-7.56285" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.76835" x2="-7.93115" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.76835" x2="-8.38835" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.76835" x2="-8.70585" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.76835" x2="-9.03605" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.76835" x2="-9.36625" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.76835" x2="-9.67105" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.76835" x2="-10.21715" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.76835" x2="-10.68705" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.76835" x2="-11.00455" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.76835" x2="-11.33475" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.76835" x2="-11.66495" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.78105" x2="0.24765" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.78105" x2="-0.00635" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.78105" x2="-0.36195" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.78105" x2="-0.67945" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.78105" x2="-1.00965" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.78105" x2="-1.47955" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.78105" x2="-1.78435" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.78105" x2="-2.10185" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.78105" x2="-2.40665" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.78105" x2="-2.71145" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.78105" x2="-3.04165" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.78105" x2="-3.37185" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.78105" x2="-3.67665" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.78105" x2="-4.01955" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.78105" x2="-4.33705" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.78105" x2="-4.65455" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.78105" x2="-4.95935" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.78105" x2="-5.51815" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.78105" x2="-5.88645" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.78105" x2="-6.22935" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.78105" x2="-6.54685" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.78105" x2="-6.87705" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.78105" x2="-7.20725" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.78105" x2="-7.56285" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.78105" x2="-7.93115" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.78105" x2="-8.40105" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.78105" x2="-8.70585" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.78105" x2="-9.03605" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.78105" x2="-9.36625" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.78105" x2="-9.67105" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.78105" x2="-10.21715" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.78105" x2="-10.69975" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.78105" x2="-10.99185" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.78105" x2="-11.33475" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.78105" x2="-11.66495" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.79375" x2="0.24765" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.79375" x2="-0.00635" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.79375" x2="-0.37465" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.79375" x2="-0.67945" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.79375" x2="-1.00965" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.79375" x2="-1.47955" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.79375" x2="-1.78435" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.79375" x2="-2.10185" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.79375" x2="-2.40665" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.79375" x2="-2.71145" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.79375" x2="-3.04165" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.79375" x2="-3.37185" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.79375" x2="-3.67665" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.79375" x2="-4.01955" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.79375" x2="-4.33705" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.79375" x2="-4.65455" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.79375" x2="-4.95935" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.79375" x2="-5.51815" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.79375" x2="-5.88645" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.79375" x2="-6.24205" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.79375" x2="-6.54685" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.79375" x2="-6.87705" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.79375" x2="-7.20725" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.79375" x2="-7.56285" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.79375" x2="-7.93115" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.79375" x2="-8.40105" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.79375" x2="-8.70585" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.79375" x2="-9.03605" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.79375" x2="-9.37895" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.79375" x2="-9.67105" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.79375" x2="-10.21715" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.79375" x2="-10.69975" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.79375" x2="-10.99185" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.79375" x2="-11.33475" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.79375" x2="-11.66495" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.80645" x2="0.24765" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.80645" x2="-0.00635" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.80645" x2="-0.37465" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.80645" x2="-0.66675" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.80645" x2="-1.00965" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.80645" x2="-1.47955" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.80645" x2="-1.78435" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.80645" x2="-2.10185" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.80645" x2="-2.40665" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.80645" x2="-2.71145" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.80645" x2="-3.04165" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.80645" x2="-3.38455" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.80645" x2="-3.67665" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.80645" x2="-4.03225" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.80645" x2="-4.32435" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.80645" x2="-4.66725" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.80645" x2="-4.95935" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.80645" x2="-5.51815" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.80645" x2="-5.88645" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.80645" x2="-6.24205" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.80645" x2="-6.53415" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="0.80645" x2="-6.87705" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.80645" x2="-7.20725" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.80645" x2="-7.56285" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.80645" x2="-7.93115" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.80645" x2="-8.40105" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.80645" x2="-8.70585" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.80645" x2="-9.03605" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.80645" x2="-9.37895" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.80645" x2="-9.67105" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.80645" x2="-10.21715" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.80645" x2="-10.69975" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="0.80645" x2="-10.99185" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.80645" x2="-11.33475" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.80645" x2="-11.65225" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.81915" x2="0.24765" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.81915" x2="-0.00635" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-0.59055" y1="0.81915" x2="-0.37465" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.81915" x2="-0.66675" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.81915" x2="-1.00965" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.81915" x2="-1.47955" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.81915" x2="-1.77165" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-2.31775" y1="0.81915" x2="-2.10185" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.81915" x2="-2.39395" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.81915" x2="-2.71145" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.81915" x2="-3.02895" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.81915" x2="-3.38455" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.81915" x2="-3.66395" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.81915" x2="-4.03225" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.81915" x2="-4.32435" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="0.81915" x2="-4.66725" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.81915" x2="-4.94665" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.81915" x2="-5.51815" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.81915" x2="-5.88645" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.81915" x2="-6.24205" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.81915" x2="-6.53415" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="0.81915" x2="-6.87705" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.81915" x2="-7.19455" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.81915" x2="-7.49935" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.81915" x2="-7.93115" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.81915" x2="-8.40105" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.81915" x2="-8.69315" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.81915" x2="-9.03605" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.81915" x2="-9.37895" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.81915" x2="-9.67105" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.81915" x2="-10.21715" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.81915" x2="-10.69975" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="0.81915" x2="-10.97915" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="0.81915" x2="-11.33475" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.81915" x2="-11.65225" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.83185" x2="0.24765" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.83185" x2="-0.00635" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-0.60325" y1="0.83185" x2="-0.38735" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.83185" x2="-0.64135" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.83185" x2="-1.00965" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-1.72085" y1="0.83185" x2="-1.49225" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.83185" x2="-1.75895" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-2.33045" y1="0.83185" x2="-2.11455" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.83185" x2="-2.38125" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="0.83185" x2="-2.71145" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.83185" x2="-3.01625" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-3.60045" y1="0.83185" x2="-3.38455" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-3.87985" y1="0.83185" x2="-3.63855" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-4.27355" y1="0.83185" x2="-4.03225" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.83185" x2="-4.29895" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="0.83185" x2="-4.66725" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.83185" x2="-4.93395" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.83185" x2="-5.53085" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.83185" x2="-5.88645" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-6.48335" y1="0.83185" x2="-6.25475" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.83185" x2="-6.52145" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-7.14375" y1="0.83185" x2="-6.87705" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.83185" x2="-7.18185" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.83185" x2="-7.49935" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.83185" x2="-7.93115" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-8.64235" y1="0.83185" x2="-8.41375" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.83185" x2="-8.68045" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.83185" x2="-9.03605" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-9.60755" y1="0.83185" x2="-9.37895" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.83185" x2="-9.64565" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.83185" x2="-10.21715" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-10.92835" y1="0.83185" x2="-10.71245" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="0.83185" x2="-10.96645" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-11.58875" y1="0.83185" x2="-11.33475" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.83185" x2="-11.62685" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.84455" x2="0.24765" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.84455" x2="-0.00635" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-0.85725" y1="0.84455" x2="-0.38735" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.84455" x2="-1.00965" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.84455" x2="-1.25095" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.84455" x2="-1.49225" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.84455" x2="-2.11455" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.84455" x2="-2.71145" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-3.87985" y1="0.84455" x2="-3.39725" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.84455" x2="-4.04495" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.84455" x2="-4.67995" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.84455" x2="-5.53085" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.84455" x2="-5.88645" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.84455" x2="-6.25475" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.84455" x2="-6.87705" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.84455" x2="-7.49935" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.84455" x2="-7.93115" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.84455" x2="-8.17245" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.84455" x2="-8.41375" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.84455" x2="-9.03605" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.84455" x2="-9.39165" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.84455" x2="-10.21715" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-11.19505" y1="0.84455" x2="-10.71245" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.84455" x2="-11.33475" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.85725" x2="0.23495" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.85725" x2="0.00635" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-0.85725" y1="0.85725" x2="-0.40005" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.85725" x2="-1.00965" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.85725" x2="-1.26365" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-1.96215" y1="0.85725" x2="-1.50495" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.85725" x2="-2.11455" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.85725" x2="-2.71145" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.85725" x2="-2.95275" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-3.87985" y1="0.85725" x2="-3.40995" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-4.50215" y1="0.85725" x2="-4.05765" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-5.13715" y1="0.85725" x2="-4.67995" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.85725" x2="-5.53085" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.85725" x2="-5.88645" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-6.72465" y1="0.85725" x2="-6.26745" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.85725" x2="-6.87705" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.85725" x2="-7.49935" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.85725" x2="-7.93115" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.85725" x2="-8.18515" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="0.85725" x2="-8.42645" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.85725" x2="-9.03605" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.85725" x2="-9.39165" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.85725" x2="-10.21715" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-11.19505" y1="0.85725" x2="-10.72515" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.85725" x2="-11.33475" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.86995" x2="0.23495" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.86995" x2="0.00635" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-0.84455" y1="0.86995" x2="-0.40005" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.86995" x2="-1.00965" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.86995" x2="-1.26365" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-1.94945" y1="0.86995" x2="-1.51765" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="0.86995" x2="-2.12725" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.86995" x2="-2.71145" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.86995" x2="-2.96545" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-3.86715" y1="0.86995" x2="-3.40995" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-4.48945" y1="0.86995" x2="-4.05765" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="0.86995" x2="-4.69265" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.86995" x2="-5.53085" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.86995" x2="-5.88645" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-6.71195" y1="0.86995" x2="-6.28015" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="0.86995" x2="-6.87705" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="0.86995" x2="-7.13105" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.86995" x2="-7.49935" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.86995" x2="-7.93115" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.86995" x2="-8.19785" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="0.86995" x2="-8.43915" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.86995" x2="-9.03605" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.86995" x2="-9.40435" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.86995" x2="-10.21715" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-11.18235" y1="0.86995" x2="-10.73785" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.86995" x2="-11.33475" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.86995" x2="-11.57605" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.88265" x2="0.23495" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.88265" x2="0.00635" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-0.83185" y1="0.88265" x2="-0.41275" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.88265" x2="-1.00965" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.88265" x2="-1.27635" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-1.93675" y1="0.88265" x2="-1.53035" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="0.88265" x2="-2.13995" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.88265" x2="-2.71145" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.88265" x2="-2.97815" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-3.85445" y1="0.88265" x2="-3.42265" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-4.47675" y1="0.88265" x2="-4.07035" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="0.88265" x2="-4.70535" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.88265" x2="-5.53085" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.88265" x2="-5.88645" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-6.69925" y1="0.88265" x2="-6.29285" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.88265" x2="-6.87705" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="0.88265" x2="-7.14375" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.88265" x2="-7.49935" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.88265" x2="-7.93115" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.88265" x2="-8.19785" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="0.88265" x2="-8.45185" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.88265" x2="-9.03605" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="0.88265" x2="-9.40435" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.88265" x2="-10.21715" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-11.16965" y1="0.88265" x2="-10.75055" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.88265" x2="-11.33475" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.88265" x2="-11.58875" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.89535" x2="0.23495" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.89535" x2="0.00635" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-0.81915" y1="0.89535" x2="-0.42545" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.89535" x2="-1.00965" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.89535" x2="-1.28905" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-1.92405" y1="0.89535" x2="-1.54305" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="0.89535" x2="-2.15265" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.89535" x2="-2.71145" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.89535" x2="-2.99085" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-3.84175" y1="0.89535" x2="-3.44805" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-4.46405" y1="0.89535" x2="-4.09575" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="0.89535" x2="-4.71805" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.89535" x2="-5.53085" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.89535" x2="-5.87375" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="0.89535" x2="-6.30555" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.89535" x2="-6.87705" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="0.89535" x2="-7.14375" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.89535" x2="-7.49935" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.89535" x2="-7.93115" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.89535" x2="-8.21055" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="0.89535" x2="-8.46455" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.89535" x2="-9.03605" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-9.82345" y1="0.89535" x2="-9.41705" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.89535" x2="-10.21715" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-11.15695" y1="0.89535" x2="-10.76325" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.89535" x2="-11.33475" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.89535" x2="-11.60145" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.90805" x2="0.23495" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.90805" x2="0.00635" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-0.79375" y1="0.90805" x2="-0.45085" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.90805" x2="-1.00965" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.90805" x2="-1.30175" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-1.91135" y1="0.90805" x2="-1.55575" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="0.90805" x2="-2.16535" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.90805" x2="-2.71145" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="0.90805" x2="-3.00355" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-3.82905" y1="0.90805" x2="-3.46075" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-4.45135" y1="0.90805" x2="-4.10845" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-5.08635" y1="0.90805" x2="-4.73075" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.90805" x2="-5.53085" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.90805" x2="-5.87375" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-6.67385" y1="0.90805" x2="-6.31825" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.90805" x2="-6.87705" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="0.90805" x2="-7.15645" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.90805" x2="-7.49935" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.90805" x2="-7.93115" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.90805" x2="-8.22325" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="0.90805" x2="-8.47725" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.90805" x2="-9.03605" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="0.90805" x2="-9.44245" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.90805" x2="-10.21715" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-11.14425" y1="0.90805" x2="-10.77595" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.90805" x2="-11.33475" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-11.85545" y1="0.90805" x2="-11.61415" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.92075" x2="0.23495" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-0.76835" y1="0.92075" x2="-0.46355" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.92075" x2="-1.00965" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.92075" x2="-1.31445" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-1.88595" y1="0.92075" x2="-1.58115" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="0.92075" x2="-2.19075" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.92075" x2="-2.71145" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-3.21945" y1="0.92075" x2="-3.01625" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-3.80365" y1="0.92075" x2="-3.48615" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-4.42595" y1="0.92075" x2="-4.13385" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="0.92075" x2="-4.75615" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.92075" x2="-5.53085" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.92075" x2="-5.87375" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-6.64845" y1="0.92075" x2="-6.34365" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.92075" x2="-6.87705" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="0.92075" x2="-7.16915" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.92075" x2="-7.55015" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.92075" x2="-7.93115" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.92075" x2="-8.23595" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="0.92075" x2="-8.50265" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.92075" x2="-9.03605" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-9.78535" y1="0.92075" x2="-9.45515" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.92075" x2="-10.21715" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-11.11885" y1="0.92075" x2="-10.80135" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.92075" x2="-11.33475" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-11.84275" y1="0.92075" x2="-11.62685" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.93345" x2="0.23495" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-0.74295" y1="0.93345" x2="-0.50165" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.93345" x2="-1.00965" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.93345" x2="-1.32715" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-1.84785" y1="0.93345" x2="-1.60655" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="0.93345" x2="-2.21615" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.93345" x2="-2.71145" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-3.19405" y1="0.93345" x2="-3.04165" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-3.77825" y1="0.93345" x2="-3.51155" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-4.40055" y1="0.93345" x2="-4.15925" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-5.03555" y1="0.93345" x2="-4.79425" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.93345" x2="-5.53085" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.93345" x2="-5.87375" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-6.61035" y1="0.93345" x2="-6.36905" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.93345" x2="-6.87705" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="0.93345" x2="-7.19455" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.93345" x2="-7.56285" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.93345" x2="-7.93115" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.93345" x2="-8.24865" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-8.76935" y1="0.93345" x2="-8.52805" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.93345" x2="-9.03605" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="0.93345" x2="-9.49325" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.93345" x2="-10.21715" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-11.09345" y1="0.93345" x2="-10.82675" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.93345" x2="-11.33475" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-11.81735" y1="0.93345" x2="-11.63955" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.94615" x2="0.23495" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-0.69215" y1="0.94615" x2="-0.53975" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.94615" x2="-1.36525" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-1.80975" y1="0.94615" x2="-1.64465" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-2.43205" y1="0.94615" x2="-2.26695" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-3.16865" y1="0.94615" x2="-3.06705" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-3.72745" y1="0.94615" x2="-3.56235" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-4.34975" y1="0.94615" x2="-4.19735" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-4.98475" y1="0.94615" x2="-4.83235" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.94615" x2="-5.53085" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.94615" x2="-5.87375" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-6.57225" y1="0.94615" x2="-6.40715" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="0.94615" x2="-7.21995" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.94615" x2="-7.56285" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.94615" x2="-8.28675" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-8.73125" y1="0.94615" x2="-8.56615" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.94615" x2="-9.03605" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-9.69645" y1="0.94615" x2="-9.53135" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.94615" x2="-10.21715" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-11.04265" y1="0.94615" x2="-10.87755" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.94615" x2="-11.33475" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-11.79195" y1="0.94615" x2="-11.67765" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.95885" x2="0.22225" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.95885" x2="-5.53085" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.95885" x2="-5.87375" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.95885" x2="-7.56285" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.95885" x2="-9.03605" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.95885" x2="-10.21715" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.95885" x2="-11.33475" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.97155" x2="0.22225" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.97155" x2="-5.54355" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.97155" x2="-5.87375" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.97155" x2="-7.56285" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.97155" x2="-9.03605" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.97155" x2="-10.21715" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.97155" x2="-11.33475" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.98425" x2="0.22225" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-5.78485" y1="0.98425" x2="-5.54355" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.98425" x2="-5.86105" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.98425" x2="-7.56285" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.98425" x2="-9.03605" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.98425" x2="-10.21715" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.98425" x2="-11.33475" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.99695" x2="0.22225" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-5.79755" y1="0.99695" x2="-5.54355" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.99695" x2="-5.83565" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.99695" x2="-7.56285" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.99695" x2="-9.03605" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.99695" x2="-10.21715" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.99695" x2="-11.33475" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.00965" x2="0.22225" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="1.00965" x2="-5.55625" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.00965" x2="-7.56285" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.00965" x2="-9.03605" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.00965" x2="-10.21715" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.00965" x2="-11.33475" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.02235" x2="0.22225" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="1.02235" x2="-5.56895" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.02235" x2="-7.56285" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.02235" x2="-9.03605" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.02235" x2="-10.21715" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.02235" x2="-11.33475" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.03505" x2="0.22225" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="1.03505" x2="-5.56895" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.03505" x2="-7.56285" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.03505" x2="-9.03605" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.03505" x2="-10.21715" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.03505" x2="-11.33475" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.04775" x2="0.20955" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-6.06425" y1="1.04775" x2="-5.58165" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.04775" x2="-7.56285" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.04775" x2="-9.03605" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.04775" x2="-10.21715" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.04775" x2="-11.33475" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.06045" x2="0.20955" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-6.05155" y1="1.06045" x2="-5.59435" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.06045" x2="-9.03605" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.06045" x2="-10.21715" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.06045" x2="-11.33475" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.07315" x2="0.20955" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-6.03885" y1="1.07315" x2="-5.60705" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.07315" x2="-9.03605" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.07315" x2="-10.21715" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.07315" x2="-11.33475" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.08585" x2="0.20955" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-6.01345" y1="1.08585" x2="-5.61975" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.08585" x2="-9.03605" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.08585" x2="-10.21715" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.08585" x2="-11.33475" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-0.14605" y1="1.09855" x2="0.20955" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-6.00075" y1="1.09855" x2="-5.64515" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.09855" x2="-9.03605" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.09855" x2="-10.21715" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.09855" x2="-11.33475" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-0.14605" y1="1.11125" x2="0.20955" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-5.97535" y1="1.11125" x2="-5.67055" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.11125" x2="-9.03605" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.11125" x2="-10.21715" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.11125" x2="-11.33475" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-5.93725" y1="1.12395" x2="-5.69595" y2="1.13665" layer="21" rot="R180"/>
<rectangle x1="-5.88645" y1="1.13665" x2="-5.74675" y2="1.14935" layer="21" rot="R180"/>
<rectangle x1="-3.20675" y1="1.64465" x2="0.09525" y2="1.65735" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="1.65735" x2="0.13335" y2="1.67005" layer="21" rot="R180"/>
<rectangle x1="-3.28295" y1="1.67005" x2="0.15875" y2="1.68275" layer="21" rot="R180"/>
<rectangle x1="-11.65225" y1="1.67005" x2="-8.36295" y2="1.68275" layer="21" rot="R180"/>
<rectangle x1="-3.30835" y1="1.68275" x2="0.17145" y2="1.69545" layer="21" rot="R180"/>
<rectangle x1="-11.69035" y1="1.68275" x2="-8.31215" y2="1.69545" layer="21" rot="R180"/>
<rectangle x1="-3.32105" y1="1.69545" x2="0.18415" y2="1.70815" layer="21" rot="R180"/>
<rectangle x1="-11.71575" y1="1.69545" x2="-8.28675" y2="1.70815" layer="21" rot="R180"/>
<rectangle x1="-3.34645" y1="1.70815" x2="0.18415" y2="1.72085" layer="21" rot="R180"/>
<rectangle x1="-11.72845" y1="1.70815" x2="-8.26135" y2="1.72085" layer="21" rot="R180"/>
<rectangle x1="-3.35915" y1="1.72085" x2="0.19685" y2="1.73355" layer="21" rot="R180"/>
<rectangle x1="-5.98805" y1="1.72085" x2="-5.74675" y2="1.73355" layer="21" rot="R180"/>
<rectangle x1="-11.74115" y1="1.72085" x2="-8.23595" y2="1.73355" layer="21" rot="R180"/>
<rectangle x1="-3.37185" y1="1.73355" x2="0.19685" y2="1.74625" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="1.73355" x2="-5.61975" y2="1.74625" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.73355" x2="-8.21055" y2="1.74625" layer="21" rot="R180"/>
<rectangle x1="-3.39725" y1="1.74625" x2="0.19685" y2="1.75895" layer="21" rot="R180"/>
<rectangle x1="-6.17855" y1="1.74625" x2="-5.54355" y2="1.75895" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.74625" x2="-8.19785" y2="1.75895" layer="21" rot="R180"/>
<rectangle x1="-3.40995" y1="1.75895" x2="0.19685" y2="1.77165" layer="21" rot="R180"/>
<rectangle x1="-6.24205" y1="1.75895" x2="-5.48005" y2="1.77165" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.75895" x2="-8.18515" y2="1.77165" layer="21" rot="R180"/>
<rectangle x1="-3.40995" y1="1.77165" x2="0.19685" y2="1.78435" layer="21" rot="R180"/>
<rectangle x1="-6.29285" y1="1.77165" x2="-5.42925" y2="1.78435" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.77165" x2="-8.17245" y2="1.78435" layer="21" rot="R180"/>
<rectangle x1="-3.42265" y1="1.78435" x2="0.19685" y2="1.79705" layer="21" rot="R180"/>
<rectangle x1="-6.34365" y1="1.78435" x2="-5.37845" y2="1.79705" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.78435" x2="-8.15975" y2="1.79705" layer="21" rot="R180"/>
<rectangle x1="-3.43535" y1="1.79705" x2="0.19685" y2="1.80975" layer="21" rot="R180"/>
<rectangle x1="-6.38175" y1="1.79705" x2="-5.34035" y2="1.80975" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.79705" x2="-8.14705" y2="1.80975" layer="21" rot="R180"/>
<rectangle x1="-3.44805" y1="1.80975" x2="0.19685" y2="1.82245" layer="21" rot="R180"/>
<rectangle x1="-6.41985" y1="1.80975" x2="-5.30225" y2="1.82245" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.80975" x2="-8.13435" y2="1.82245" layer="21" rot="R180"/>
<rectangle x1="-3.46075" y1="1.82245" x2="0.18415" y2="1.83515" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="1.82245" x2="-5.26415" y2="1.83515" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.82245" x2="-8.12165" y2="1.83515" layer="21" rot="R180"/>
<rectangle x1="-3.46075" y1="1.83515" x2="0.18415" y2="1.84785" layer="21" rot="R180"/>
<rectangle x1="-6.49605" y1="1.83515" x2="-5.22605" y2="1.84785" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.83515" x2="-8.10895" y2="1.84785" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="1.84785" x2="0.17145" y2="1.86055" layer="21" rot="R180"/>
<rectangle x1="-6.53415" y1="1.84785" x2="-5.20065" y2="1.86055" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.84785" x2="-8.10895" y2="1.86055" layer="21" rot="R180"/>
<rectangle x1="-2.64795" y1="1.86055" x2="0.17145" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="1.86055" x2="-2.88925" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-6.55955" y1="1.86055" x2="-5.16255" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-11.74115" y1="1.86055" x2="-8.09625" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="1.87325" x2="0.15875" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="1.87325" x2="-2.95275" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-6.58495" y1="1.87325" x2="-5.13715" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-11.74115" y1="1.87325" x2="-8.09625" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="1.88595" x2="0.14605" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="1.88595" x2="-2.99085" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-6.61035" y1="1.88595" x2="-5.11175" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-11.72845" y1="1.88595" x2="-8.08355" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.89865" x2="0.13335" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="1.89865" x2="-3.01625" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-6.63575" y1="1.89865" x2="-5.08635" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-11.71575" y1="1.89865" x2="-8.08355" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.91135" x2="0.12065" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="1.91135" x2="-3.04165" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-6.66115" y1="1.91135" x2="-5.06095" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-11.70305" y1="1.91135" x2="-8.07085" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.92405" x2="0.10795" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="1.92405" x2="-3.06705" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="1.92405" x2="-5.03555" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-11.70305" y1="1.92405" x2="-8.07085" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.93675" x2="0.09525" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.93675" x2="-3.09245" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-6.71195" y1="1.93675" x2="-5.01015" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-11.69035" y1="1.93675" x2="-8.05815" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.94945" x2="0.08255" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.94945" x2="-3.09245" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="1.94945" x2="-4.98475" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-11.67765" y1="1.94945" x2="-8.05815" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="1.96215" x2="0.06985" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.96215" x2="-3.07975" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="1.96215" x2="-4.97205" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-11.66495" y1="1.96215" x2="-8.05815" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="1.97485" x2="0.05715" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="1.97485" x2="-2.34315" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.97485" x2="-3.06705" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="1.97485" x2="-4.94665" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-11.65225" y1="1.97485" x2="-8.05815" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-2.27965" y1="1.98755" x2="0.04445" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="1.98755" x2="-2.34315" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.98755" x2="-3.05435" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-6.80085" y1="1.98755" x2="-4.92125" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-11.63955" y1="1.98755" x2="-8.04545" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-2.25425" y1="2.00025" x2="0.03175" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.00025" x2="-2.34315" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.00025" x2="-3.04165" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-6.81355" y1="2.00025" x2="-4.90855" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-11.62685" y1="2.00025" x2="-8.04545" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-2.22885" y1="2.01295" x2="0.01905" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.01295" x2="-2.35585" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.01295" x2="-3.02895" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-6.83895" y1="2.01295" x2="-4.88315" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-11.61415" y1="2.01295" x2="-8.04545" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-2.20345" y1="2.02565" x2="0.00635" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.02565" x2="-2.35585" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.02565" x2="-3.01625" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-6.85165" y1="2.02565" x2="-4.87045" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-11.60145" y1="2.02565" x2="-8.04545" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-2.19075" y1="2.03835" x2="-0.00635" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.03835" x2="-2.35585" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.03835" x2="-3.00355" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-6.87705" y1="2.03835" x2="-4.84505" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-11.58875" y1="2.03835" x2="-8.04545" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-2.16535" y1="2.05105" x2="-0.01905" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.05105" x2="-2.35585" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.05105" x2="-2.99085" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-6.88975" y1="2.05105" x2="-4.83235" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="2.05105" x2="-8.04545" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-2.13995" y1="2.06375" x2="-0.03175" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.06375" x2="-2.36855" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-3.19405" y1="2.06375" x2="-2.97815" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.06375" x2="-3.21945" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-6.91515" y1="2.06375" x2="-4.81965" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-8.62965" y1="2.06375" x2="-8.04545" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="2.06375" x2="-8.64235" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-2.12725" y1="2.07645" x2="-0.04445" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.07645" x2="-2.36855" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-3.18135" y1="2.07645" x2="-2.96545" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.07645" x2="-3.23215" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-6.92785" y1="2.07645" x2="-4.79425" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="2.07645" x2="-8.04545" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="2.07645" x2="-8.65505" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-2.10185" y1="2.08915" x2="-0.05715" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.08915" x2="-2.36855" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-3.16865" y1="2.08915" x2="-2.95275" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.08915" x2="-3.23215" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-6.94055" y1="2.08915" x2="-4.78155" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="2.08915" x2="-8.04545" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-11.53795" y1="2.08915" x2="-8.66775" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-2.08915" y1="2.10185" x2="-0.06985" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.10185" x2="-2.38125" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-3.14325" y1="2.10185" x2="-2.94005" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.10185" x2="-3.24485" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-6.95325" y1="2.10185" x2="-4.76885" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-8.59155" y1="2.10185" x2="-8.04545" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-11.52525" y1="2.10185" x2="-8.68045" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-2.07645" y1="2.11455" x2="-0.08255" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.11455" x2="-2.38125" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-3.13055" y1="2.11455" x2="-2.92735" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.11455" x2="-3.25755" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-6.97865" y1="2.11455" x2="-4.74345" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="2.11455" x2="-8.04545" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-11.51255" y1="2.11455" x2="-8.69315" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-2.07645" y1="2.12725" x2="-0.09525" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.12725" x2="-2.38125" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-3.11785" y1="2.12725" x2="-2.91465" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.12725" x2="-3.25755" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-6.99135" y1="2.12725" x2="-4.73075" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="2.12725" x2="-8.04545" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-11.49985" y1="2.12725" x2="-8.70585" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-2.08915" y1="2.13995" x2="-0.10795" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.13995" x2="-2.38125" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-3.10515" y1="2.13995" x2="-2.90195" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.13995" x2="-3.25755" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-7.00405" y1="2.13995" x2="-4.71805" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="2.13995" x2="-8.04545" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="2.13995" x2="-8.71855" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-11.48715" y1="2.13995" x2="-8.90905" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-2.10185" y1="2.15265" x2="-0.12065" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="2.15265" x2="-2.39395" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-3.09245" y1="2.15265" x2="-2.88925" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.15265" x2="-3.27025" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-7.01675" y1="2.15265" x2="-4.70535" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.15265" x2="-8.04545" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="2.15265" x2="-8.73125" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-11.47445" y1="2.15265" x2="-8.93445" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-2.11455" y1="2.16535" x2="-0.13335" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="2.16535" x2="-2.39395" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-3.07975" y1="2.16535" x2="-2.87655" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.16535" x2="-3.27025" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-7.02945" y1="2.16535" x2="-4.69265" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-8.52805" y1="2.16535" x2="-8.04545" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-8.76935" y1="2.16535" x2="-8.74395" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.16535" x2="-8.93445" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-11.46175" y1="2.16535" x2="-9.20115" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-2.12725" y1="2.17805" x2="-0.14605" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="2.17805" x2="-2.39395" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-3.06705" y1="2.17805" x2="-2.86385" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.17805" x2="-3.28295" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-7.04215" y1="2.17805" x2="-4.67995" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-8.51535" y1="2.17805" x2="-8.04545" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.17805" x2="-8.93445" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-11.44905" y1="2.17805" x2="-9.25195" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-2.13995" y1="2.19075" x2="-0.15875" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.19075" x2="-2.39395" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-3.05435" y1="2.19075" x2="-2.85115" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.19075" x2="-3.28295" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-7.06755" y1="2.19075" x2="-4.66725" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="2.19075" x2="-8.04545" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.19075" x2="-8.92175" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-11.43635" y1="2.19075" x2="-9.27735" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-2.15265" y1="2.20345" x2="-0.17145" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.20345" x2="-2.40665" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-3.04165" y1="2.20345" x2="-2.83845" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.20345" x2="-3.28295" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-7.08025" y1="2.20345" x2="-4.65455" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="2.20345" x2="-8.04545" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.20345" x2="-8.90905" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-11.42365" y1="2.20345" x2="-9.27735" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-2.16535" y1="2.21615" x2="-0.18415" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.21615" x2="-2.40665" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-3.02895" y1="2.21615" x2="-2.82575" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.21615" x2="-3.29565" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-7.09295" y1="2.21615" x2="-4.64185" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.21615" x2="-8.04545" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-9.12495" y1="2.21615" x2="-8.82015" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-11.41095" y1="2.21615" x2="-9.27735" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-2.17805" y1="2.22885" x2="-0.19685" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.22885" x2="-2.40665" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-3.01625" y1="2.22885" x2="-2.81305" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.22885" x2="-3.29565" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="2.22885" x2="-4.62915" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-8.46455" y1="2.22885" x2="-8.04545" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-9.17575" y1="2.22885" x2="-8.80745" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-11.39825" y1="2.22885" x2="-9.27735" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-2.19075" y1="2.24155" x2="-0.20955" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="2.24155" x2="-2.40665" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-3.00355" y1="2.24155" x2="-2.80035" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.24155" x2="-3.29565" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="2.24155" x2="-4.61645" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-8.45185" y1="2.24155" x2="-8.04545" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-9.23925" y1="2.24155" x2="-8.82015" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-11.38555" y1="2.24155" x2="-9.25195" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-2.20345" y1="2.25425" x2="-0.22225" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="2.25425" x2="-2.41935" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-2.99085" y1="2.25425" x2="-2.78765" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.25425" x2="-3.29565" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-7.13105" y1="2.25425" x2="-4.60375" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-8.43915" y1="2.25425" x2="-8.04545" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-9.44245" y1="2.25425" x2="-8.83285" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-11.37285" y1="2.25425" x2="-9.48055" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-2.21615" y1="2.26695" x2="-0.23495" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-2.67335" y1="2.26695" x2="-2.41935" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="2.26695" x2="-2.77495" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.26695" x2="-3.30835" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-7.14375" y1="2.26695" x2="-4.59105" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-8.42645" y1="2.26695" x2="-8.04545" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.26695" x2="-8.84555" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-11.36015" y1="2.26695" x2="-9.51865" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-2.22885" y1="2.27965" x2="-0.24765" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-2.71145" y1="2.27965" x2="-2.40665" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-2.96545" y1="2.27965" x2="-2.76225" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.27965" x2="-3.30835" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-7.15645" y1="2.27965" x2="-4.57835" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-8.41375" y1="2.27965" x2="-8.04545" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.27965" x2="-8.85825" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-11.34745" y1="2.27965" x2="-9.54405" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-2.24155" y1="2.29235" x2="-0.26035" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="2.29235" x2="-2.36855" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.29235" x2="-3.30835" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-7.15645" y1="2.29235" x2="-4.56565" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-8.40105" y1="2.29235" x2="-8.04545" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.29235" x2="-8.87095" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-11.33475" y1="2.29235" x2="-9.58215" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-2.25425" y1="2.30505" x2="-0.27305" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="2.30505" x2="-2.33045" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.30505" x2="-3.30835" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-7.16915" y1="2.30505" x2="-4.55295" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-8.38835" y1="2.30505" x2="-8.04545" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-9.44245" y1="2.30505" x2="-8.88365" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-11.32205" y1="2.30505" x2="-9.59485" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-2.26695" y1="2.31775" x2="-0.28575" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-2.92735" y1="2.31775" x2="-2.29235" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.31775" x2="-3.30835" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-7.18185" y1="2.31775" x2="-4.54025" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-8.37565" y1="2.31775" x2="-8.04545" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-9.46785" y1="2.31775" x2="-8.89635" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-11.30935" y1="2.31775" x2="-9.60755" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-2.91465" y1="2.33045" x2="-0.29845" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.33045" x2="-3.30835" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-7.19455" y1="2.33045" x2="-4.52755" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-8.37565" y1="2.33045" x2="-8.04545" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-9.49325" y1="2.33045" x2="-8.89635" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-11.29665" y1="2.33045" x2="-9.60755" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.34315" x2="-0.31115" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.34315" x2="-3.30835" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-7.20725" y1="2.34315" x2="-4.51485" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-8.38835" y1="2.34315" x2="-8.04545" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-9.51865" y1="2.34315" x2="-8.88365" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-11.28395" y1="2.34315" x2="-9.59485" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.35585" x2="-0.32385" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.35585" x2="-3.30835" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-7.21995" y1="2.35585" x2="-4.50215" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-8.40105" y1="2.35585" x2="-8.04545" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-9.55675" y1="2.35585" x2="-8.87095" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-11.27125" y1="2.35585" x2="-9.58215" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.36855" x2="-0.33655" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.36855" x2="-3.30835" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-7.23265" y1="2.36855" x2="-4.50215" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-8.41375" y1="2.36855" x2="-8.04545" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-11.25855" y1="2.36855" x2="-8.85825" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.38125" x2="-0.34925" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.38125" x2="-3.30835" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-7.23265" y1="2.38125" x2="-4.48945" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-8.42645" y1="2.38125" x2="-8.04545" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-11.24585" y1="2.38125" x2="-8.84555" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.39395" x2="-0.36195" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.39395" x2="-3.30835" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-5.70865" y1="2.39395" x2="-4.47675" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-7.24535" y1="2.39395" x2="-6.01345" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-8.43915" y1="2.39395" x2="-8.04545" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-9.77265" y1="2.39395" x2="-8.83285" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-11.23315" y1="2.39395" x2="-9.78535" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.40665" x2="-0.37465" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.40665" x2="-3.30835" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-5.63245" y1="2.40665" x2="-4.46405" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="2.40665" x2="-6.08965" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-8.45185" y1="2.40665" x2="-8.04545" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.40665" x2="-8.82015" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="2.40665" x2="-9.81075" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.41935" x2="-0.38735" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.41935" x2="-3.30835" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-5.58165" y1="2.41935" x2="-4.45135" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="2.41935" x2="-6.14045" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-8.46455" y1="2.41935" x2="-8.04545" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.41935" x2="-8.80745" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="2.41935" x2="-9.83615" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.43205" x2="-0.40005" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.43205" x2="-3.30835" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-5.54355" y1="2.43205" x2="-4.45135" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="2.43205" x2="-6.19125" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.43205" x2="-8.04545" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.43205" x2="-8.79475" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-11.19505" y1="2.43205" x2="-9.86155" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.44475" x2="-0.41275" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.44475" x2="-3.30835" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-5.50545" y1="2.44475" x2="-4.43865" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="2.44475" x2="-6.21665" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="2.44475" x2="-8.04545" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.44475" x2="-8.78205" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-11.18235" y1="2.44475" x2="-9.88695" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.45745" x2="-0.42545" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.45745" x2="-3.30835" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-5.46735" y1="2.45745" x2="-4.42595" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="2.45745" x2="-6.25475" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="2.45745" x2="-8.04545" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-9.75995" y1="2.45745" x2="-8.76935" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-11.16965" y1="2.45745" x2="-9.89965" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.47015" x2="-0.43815" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.47015" x2="-3.30835" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-5.44195" y1="2.47015" x2="-4.42595" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="2.47015" x2="-6.29285" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-8.51535" y1="2.47015" x2="-8.04545" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-9.78535" y1="2.47015" x2="-8.75665" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-10.31875" y1="2.47015" x2="-9.91235" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-11.15695" y1="2.47015" x2="-10.57275" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.48285" x2="-0.45085" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.48285" x2="-3.30835" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-5.40385" y1="2.48285" x2="-4.41325" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="2.48285" x2="-6.31825" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-8.52805" y1="2.48285" x2="-8.04545" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-9.81075" y1="2.48285" x2="-8.74395" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-10.21715" y1="2.48285" x2="-9.91235" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-11.14425" y1="2.48285" x2="-10.68705" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.49555" x2="-0.46355" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.49555" x2="-3.30835" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-5.37845" y1="2.49555" x2="-4.40055" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="2.49555" x2="-6.34365" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.49555" x2="-8.04545" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="2.49555" x2="-8.73125" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-10.14095" y1="2.49555" x2="-9.91235" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-11.13155" y1="2.49555" x2="-10.76325" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.50825" x2="-0.47625" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.50825" x2="-3.30835" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-5.35305" y1="2.50825" x2="-4.40055" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="2.50825" x2="-6.36905" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="2.50825" x2="-8.04545" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="2.50825" x2="-8.71855" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-10.07745" y1="2.50825" x2="-9.89965" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-11.11885" y1="2.50825" x2="-10.82675" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.52095" x2="-0.48895" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.52095" x2="-3.30835" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-5.32765" y1="2.52095" x2="-4.38785" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="2.52095" x2="-6.39445" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="2.52095" x2="-8.04545" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-10.02665" y1="2.52095" x2="-8.70585" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-11.10615" y1="2.52095" x2="-10.87755" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.53365" x2="-0.50165" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.53365" x2="-3.29565" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-5.31495" y1="2.53365" x2="-4.37515" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="2.53365" x2="-6.40715" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="2.53365" x2="-8.04545" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-9.97585" y1="2.53365" x2="-8.69315" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-11.09345" y1="2.53365" x2="-10.92835" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.54635" x2="-0.51435" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.54635" x2="-3.29565" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-5.28955" y1="2.54635" x2="-4.37515" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="2.54635" x2="-6.43255" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-8.59155" y1="2.54635" x2="-8.04545" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-9.93775" y1="2.54635" x2="-8.68045" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-11.08075" y1="2.54635" x2="-10.96645" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.55905" x2="-0.52705" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.55905" x2="-3.29565" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-5.26415" y1="2.55905" x2="-4.36245" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="2.55905" x2="-6.45795" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="2.55905" x2="-8.04545" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="2.55905" x2="-8.66775" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-11.06805" y1="2.55905" x2="-11.00455" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.57175" x2="-0.53975" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.57175" x2="-3.29565" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-5.25145" y1="2.57175" x2="-4.34975" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="2.57175" x2="-6.47065" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="2.57175" x2="-8.04545" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="2.57175" x2="-8.65505" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-11.05535" y1="2.57175" x2="-11.04265" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.58445" x2="-0.55245" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.58445" x2="-3.29565" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-5.22605" y1="2.58445" x2="-4.34975" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="2.58445" x2="-6.49605" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-8.62965" y1="2.58445" x2="-8.04545" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="2.58445" x2="-8.64235" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.59715" x2="-0.56515" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.59715" x2="-3.28295" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-5.21335" y1="2.59715" x2="-4.33705" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="2.59715" x2="-6.50875" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="2.59715" x2="-8.04545" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="2.60985" x2="-0.57785" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.60985" x2="-3.28295" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-5.20065" y1="2.60985" x2="-4.33705" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="2.60985" x2="-6.52145" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-9.77265" y1="2.60985" x2="-8.04545" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-2.99085" y1="2.62255" x2="-0.59055" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.62255" x2="-3.28295" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="2.62255" x2="-4.32435" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="2.62255" x2="-6.54685" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.62255" x2="-8.04545" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.62255" x2="-8.52805" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-3.04165" y1="2.63525" x2="-0.60325" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.63525" x2="-3.28295" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="2.63525" x2="-4.32435" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="2.63525" x2="-6.55955" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.63525" x2="-8.04545" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-9.72185" y1="2.63525" x2="-8.54075" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-3.09245" y1="2.64795" x2="-0.61595" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.64795" x2="-3.27025" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="2.64795" x2="-4.31165" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="2.64795" x2="-6.57225" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.64795" x2="-8.04545" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-9.69645" y1="2.64795" x2="-8.54075" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-3.14325" y1="2.66065" x2="-0.62865" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.66065" x2="-3.27025" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-5.13715" y1="2.66065" x2="-4.29895" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="2.66065" x2="-6.58495" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.66065" x2="-8.04545" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-9.67105" y1="2.66065" x2="-8.54075" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-3.18135" y1="2.67335" x2="-0.64135" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.67335" x2="-3.27025" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="2.67335" x2="-4.29895" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-7.24535" y1="2.67335" x2="-6.59765" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.67335" x2="-8.04545" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-9.64565" y1="2.67335" x2="-8.54075" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="2.68605" x2="-0.65405" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.68605" x2="-3.25755" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="2.68605" x2="-4.28625" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-7.21995" y1="2.68605" x2="-6.61035" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.68605" x2="-8.04545" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-9.62025" y1="2.68605" x2="-8.54075" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.69875" x2="-0.66675" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="2.69875" x2="-4.28625" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-7.19455" y1="2.69875" x2="-6.62305" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.69875" x2="-8.04545" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-9.59485" y1="2.69875" x2="-8.54075" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.71145" x2="-0.67945" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-5.08635" y1="2.71145" x2="-4.27355" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-7.18185" y1="2.71145" x2="-6.63575" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.71145" x2="-8.04545" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="2.71145" x2="-8.54075" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.72415" x2="-0.69215" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-5.07365" y1="2.72415" x2="-4.27355" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-7.15645" y1="2.72415" x2="-6.64845" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.72415" x2="-8.04545" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-9.55675" y1="2.72415" x2="-8.54075" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.73685" x2="-0.70485" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="2.73685" x2="-4.27355" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-7.13105" y1="2.73685" x2="-6.66115" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.73685" x2="-8.04545" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-9.54405" y1="2.73685" x2="-8.54075" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.74955" x2="-0.71755" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.74955" x2="-2.90195" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-5.04825" y1="2.74955" x2="-4.26085" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="2.74955" x2="-6.67385" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.74955" x2="-8.04545" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-9.51865" y1="2.74955" x2="-8.55345" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.76225" x2="-0.73025" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.76225" x2="-2.94005" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-5.03555" y1="2.76225" x2="-4.26085" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-7.09295" y1="2.76225" x2="-6.68655" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.76225" x2="-8.04545" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-9.50595" y1="2.76225" x2="-8.55345" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.77495" x2="-0.74295" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.77495" x2="-2.99085" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-5.02285" y1="2.77495" x2="-4.24815" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-7.06755" y1="2.77495" x2="-6.69925" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="2.77495" x2="-8.04545" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-9.48055" y1="2.77495" x2="-8.54075" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.78765" x2="-0.75565" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.78765" x2="-3.04165" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-5.01015" y1="2.78765" x2="-4.24815" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-7.04215" y1="2.78765" x2="-6.71195" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="2.78765" x2="-8.04545" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-9.46785" y1="2.78765" x2="-8.52805" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.80035" x2="-0.76835" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.80035" x2="-3.07975" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-5.01015" y1="2.80035" x2="-4.23545" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-7.02945" y1="2.80035" x2="-6.72465" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-9.45515" y1="2.80035" x2="-8.04545" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.81305" x2="-0.78105" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.81305" x2="-3.13055" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="2.81305" x2="-4.23545" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-7.00405" y1="2.81305" x2="-6.72465" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.81305" x2="-8.04545" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.82575" x2="-0.79375" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.82575" x2="-3.18135" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-4.98475" y1="2.82575" x2="-4.23545" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-6.97865" y1="2.82575" x2="-6.73735" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-9.41705" y1="2.82575" x2="-8.04545" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-2.85115" y1="2.83845" x2="-0.80645" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.83845" x2="-3.19405" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-4.97205" y1="2.83845" x2="-4.22275" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-6.95325" y1="2.83845" x2="-6.75005" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-9.40435" y1="2.83845" x2="-8.04545" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-2.85115" y1="2.85115" x2="-0.81915" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.85115" x2="-3.19405" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-4.97205" y1="2.85115" x2="-4.22275" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-6.94055" y1="2.85115" x2="-6.75005" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-9.39165" y1="2.85115" x2="-8.04545" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-2.83845" y1="2.86385" x2="-0.83185" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.86385" x2="-3.19405" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-4.95935" y1="2.86385" x2="-4.21005" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-6.91515" y1="2.86385" x2="-6.76275" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-9.36625" y1="2.86385" x2="-8.04545" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-2.83845" y1="2.87655" x2="-0.84455" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.87655" x2="-3.18135" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-4.94665" y1="2.87655" x2="-4.21005" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-6.88975" y1="2.87655" x2="-6.77545" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-9.35355" y1="2.87655" x2="-8.04545" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-2.83845" y1="2.88925" x2="-0.85725" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.88925" x2="-3.18135" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-4.94665" y1="2.88925" x2="-4.21005" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-6.86435" y1="2.88925" x2="-6.77545" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-9.34085" y1="2.88925" x2="-8.04545" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-2.85115" y1="2.90195" x2="-0.86995" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.90195" x2="-3.16865" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-4.93395" y1="2.90195" x2="-4.19735" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-6.85165" y1="2.90195" x2="-6.78815" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-9.32815" y1="2.90195" x2="-8.04545" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.91465" x2="-0.88265" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.91465" x2="-3.16865" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="2.91465" x2="-4.19735" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-6.82625" y1="2.91465" x2="-6.80085" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-9.31545" y1="2.91465" x2="-8.04545" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.92735" x2="-0.89535" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.92735" x2="-3.15595" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="2.92735" x2="-4.19735" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-9.30275" y1="2.92735" x2="-8.04545" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.94005" x2="-0.90805" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.94005" x2="-3.14325" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="2.94005" x2="-4.18465" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-9.29005" y1="2.94005" x2="-8.04545" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.95275" x2="-0.92075" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.95275" x2="-3.14325" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="2.95275" x2="-4.18465" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="2.95275" x2="-8.04545" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="2.95275" x2="-8.57885" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-2.91465" y1="2.96545" x2="-0.93345" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.96545" x2="-3.13055" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="2.96545" x2="-4.18465" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.96545" x2="-8.04545" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-9.26465" y1="2.96545" x2="-8.59155" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-2.92735" y1="2.97815" x2="-0.94615" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.97815" x2="-3.13055" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="2.97815" x2="-4.17195" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.97815" x2="-8.04545" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-9.25195" y1="2.97815" x2="-8.60425" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="2.99085" x2="-0.95885" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.99085" x2="-3.11785" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="2.99085" x2="-4.17195" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.99085" x2="-8.04545" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-9.23925" y1="2.99085" x2="-8.60425" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="3.00355" x2="-0.97155" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.00355" x2="-3.10515" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="3.00355" x2="-4.17195" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="3.00355" x2="-8.04545" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-9.22655" y1="3.00355" x2="-8.60425" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-2.96545" y1="3.01625" x2="-0.98425" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.01625" x2="-3.10515" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.01625" x2="-4.17195" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="3.01625" x2="-8.04545" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-9.21385" y1="3.01625" x2="-8.61695" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="3.02895" x2="-0.99695" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.02895" x2="-3.09245" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.02895" x2="-4.15925" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="3.02895" x2="-8.04545" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-9.20115" y1="3.02895" x2="-8.61695" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-2.99085" y1="3.04165" x2="-1.00965" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.04165" x2="-3.07975" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.04165" x2="-4.15925" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="3.04165" x2="-8.04545" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-9.18845" y1="3.04165" x2="-8.61695" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-3.00355" y1="3.05435" x2="-1.02235" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.05435" x2="-3.07975" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.05435" x2="-4.15925" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="3.05435" x2="-8.04545" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-9.17575" y1="3.05435" x2="-8.62965" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-3.01625" y1="3.06705" x2="-1.03505" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.06705" x2="-3.06705" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.06705" x2="-4.15925" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="3.06705" x2="-8.04545" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-9.17575" y1="3.06705" x2="-8.62965" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-3.02895" y1="3.07975" x2="-1.04775" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.07975" x2="-3.05435" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.07975" x2="-4.14655" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="3.07975" x2="-8.04545" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-9.16305" y1="3.07975" x2="-8.62965" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.09245" x2="-1.06045" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.09245" x2="-4.14655" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="3.09245" x2="-8.04545" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-9.15035" y1="3.09245" x2="-8.64235" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.10515" x2="-1.07315" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.10515" x2="-4.14655" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="3.10515" x2="-8.04545" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-9.13765" y1="3.10515" x2="-8.62965" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.11785" x2="-1.08585" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.11785" x2="-4.14655" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="3.11785" x2="-8.04545" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-9.12495" y1="3.11785" x2="-8.62965" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.13055" x2="-1.09855" y2="3.14325" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.13055" x2="-4.14655" y2="3.14325" layer="21" rot="R180"/>
<rectangle x1="-9.12495" y1="3.13055" x2="-8.04545" y2="3.14325" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.14325" x2="-1.11125" y2="3.15595" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.14325" x2="-4.13385" y2="3.15595" layer="21" rot="R180"/>
<rectangle x1="-9.11225" y1="3.14325" x2="-8.04545" y2="3.15595" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.15595" x2="-1.12395" y2="3.16865" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.15595" x2="-4.13385" y2="3.16865" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="3.15595" x2="-8.04545" y2="3.16865" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.16865" x2="-1.13665" y2="3.18135" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.16865" x2="-4.13385" y2="3.18135" layer="21" rot="R180"/>
<rectangle x1="-9.08685" y1="3.16865" x2="-8.04545" y2="3.18135" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.18135" x2="-1.14935" y2="3.19405" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.18135" x2="-4.13385" y2="3.19405" layer="21" rot="R180"/>
<rectangle x1="-9.08685" y1="3.18135" x2="-8.04545" y2="3.19405" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.19405" x2="-1.16205" y2="3.20675" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.19405" x2="-4.13385" y2="3.20675" layer="21" rot="R180"/>
<rectangle x1="-9.07415" y1="3.19405" x2="-8.04545" y2="3.20675" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.20675" x2="-1.17475" y2="3.21945" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.20675" x2="-4.12115" y2="3.21945" layer="21" rot="R180"/>
<rectangle x1="-9.06145" y1="3.20675" x2="-8.04545" y2="3.21945" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.21945" x2="-1.18745" y2="3.23215" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.21945" x2="-4.12115" y2="3.23215" layer="21" rot="R180"/>
<rectangle x1="-9.06145" y1="3.21945" x2="-8.04545" y2="3.23215" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.23215" x2="-1.20015" y2="3.24485" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.23215" x2="-4.12115" y2="3.24485" layer="21" rot="R180"/>
<rectangle x1="-9.04875" y1="3.23215" x2="-8.04545" y2="3.24485" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.24485" x2="-1.21285" y2="3.25755" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.24485" x2="-4.12115" y2="3.25755" layer="21" rot="R180"/>
<rectangle x1="-9.03605" y1="3.24485" x2="-8.04545" y2="3.25755" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.25755" x2="-1.22555" y2="3.27025" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.25755" x2="-4.12115" y2="3.27025" layer="21" rot="R180"/>
<rectangle x1="-9.03605" y1="3.25755" x2="-8.04545" y2="3.27025" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.27025" x2="-1.23825" y2="3.28295" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.27025" x2="-4.12115" y2="3.28295" layer="21" rot="R180"/>
<rectangle x1="-9.02335" y1="3.27025" x2="-8.04545" y2="3.28295" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.28295" x2="-1.25095" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.28295" x2="-4.12115" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.28295" x2="-8.04545" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-9.01065" y1="3.28295" x2="-8.71855" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.29565" x2="-1.26365" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.29565" x2="-4.12115" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.29565" x2="-8.04545" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-9.01065" y1="3.29565" x2="-8.73125" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.30835" x2="-1.27635" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.30835" x2="-4.12115" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-8.65505" y1="3.30835" x2="-8.04545" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-8.99795" y1="3.30835" x2="-8.73125" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.32105" x2="-1.28905" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.32105" x2="-4.12115" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.32105" x2="-8.04545" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-8.99795" y1="3.32105" x2="-8.74395" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.33375" x2="-1.30175" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.33375" x2="-4.10845" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.33375" x2="-8.04545" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-8.98525" y1="3.33375" x2="-8.74395" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.34645" x2="-1.31445" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.34645" x2="-4.10845" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-8.68045" y1="3.34645" x2="-8.04545" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-8.98525" y1="3.34645" x2="-8.75665" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.35915" x2="-1.32715" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.35915" x2="-4.10845" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-8.68045" y1="3.35915" x2="-8.04545" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-8.97255" y1="3.35915" x2="-8.75665" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.37185" x2="-1.33985" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.37185" x2="-4.10845" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-8.69315" y1="3.37185" x2="-8.04545" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-8.97255" y1="3.37185" x2="-8.76935" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.38455" x2="-1.35255" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.38455" x2="-4.10845" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-8.69315" y1="3.38455" x2="-8.04545" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-8.95985" y1="3.38455" x2="-8.76935" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.39725" x2="-1.36525" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.39725" x2="-4.10845" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-8.70585" y1="3.39725" x2="-8.04545" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-8.95985" y1="3.39725" x2="-8.78205" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.40995" x2="-1.37795" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.40995" x2="-4.10845" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-8.70585" y1="3.40995" x2="-8.04545" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-8.94715" y1="3.40995" x2="-8.78205" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.42265" x2="-1.39065" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.42265" x2="-4.10845" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-8.71855" y1="3.42265" x2="-8.04545" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-8.94715" y1="3.42265" x2="-8.78205" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.43535" x2="-1.40335" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.43535" x2="-4.10845" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-8.71855" y1="3.43535" x2="-8.04545" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="3.43535" x2="-8.76935" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.44805" x2="-1.41605" y2="3.46075" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.44805" x2="-4.10845" y2="3.46075" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="3.44805" x2="-8.04545" y2="3.46075" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.46075" x2="-1.42875" y2="3.47345" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.46075" x2="-4.10845" y2="3.47345" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="3.46075" x2="-8.04545" y2="3.47345" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.47345" x2="-1.44145" y2="3.48615" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.47345" x2="-4.10845" y2="3.48615" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="3.47345" x2="-8.04545" y2="3.48615" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.48615" x2="-1.45415" y2="3.49885" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.48615" x2="-4.10845" y2="3.49885" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="3.48615" x2="-8.04545" y2="3.49885" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.49885" x2="-1.46685" y2="3.51155" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.49885" x2="-4.10845" y2="3.51155" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="3.49885" x2="-8.04545" y2="3.51155" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.51155" x2="-1.47955" y2="3.52425" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.51155" x2="-4.10845" y2="3.52425" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="3.51155" x2="-8.04545" y2="3.52425" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.52425" x2="-1.49225" y2="3.53695" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.52425" x2="-4.10845" y2="3.53695" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="3.52425" x2="-8.04545" y2="3.53695" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.53695" x2="-1.50495" y2="3.54965" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.53695" x2="-4.10845" y2="3.54965" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="3.53695" x2="-8.04545" y2="3.54965" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.54965" x2="-1.51765" y2="3.56235" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.54965" x2="-4.10845" y2="3.56235" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="3.54965" x2="-8.04545" y2="3.56235" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.56235" x2="-1.53035" y2="3.57505" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.56235" x2="-4.10845" y2="3.57505" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="3.56235" x2="-8.04545" y2="3.57505" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.57505" x2="-1.54305" y2="3.58775" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.57505" x2="-4.10845" y2="3.58775" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="3.57505" x2="-8.04545" y2="3.58775" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.58775" x2="-1.55575" y2="3.60045" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.58775" x2="-4.10845" y2="3.60045" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.58775" x2="-8.04545" y2="3.60045" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.60045" x2="-1.56845" y2="3.61315" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.60045" x2="-4.10845" y2="3.61315" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.60045" x2="-8.04545" y2="3.61315" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.61315" x2="-1.58115" y2="3.62585" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.61315" x2="-4.12115" y2="3.62585" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.61315" x2="-8.04545" y2="3.62585" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.62585" x2="-1.59385" y2="3.63855" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.62585" x2="-4.12115" y2="3.63855" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.62585" x2="-8.04545" y2="3.63855" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.63855" x2="-1.60655" y2="3.65125" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.63855" x2="-4.12115" y2="3.65125" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.63855" x2="-8.04545" y2="3.65125" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.65125" x2="-1.61925" y2="3.66395" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.65125" x2="-4.12115" y2="3.66395" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.65125" x2="-8.04545" y2="3.66395" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.66395" x2="-1.63195" y2="3.67665" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.66395" x2="-4.12115" y2="3.67665" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.66395" x2="-8.04545" y2="3.67665" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.67665" x2="-1.64465" y2="3.68935" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.67665" x2="-4.12115" y2="3.68935" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.67665" x2="-8.04545" y2="3.68935" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.68935" x2="-1.65735" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.68935" x2="-4.12115" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-7.55015" y1="3.68935" x2="-7.51205" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.68935" x2="-8.04545" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.70205" x2="-1.67005" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.70205" x2="-4.12115" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-7.55015" y1="3.70205" x2="-7.47395" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.70205" x2="-8.04545" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.71475" x2="-1.68275" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.71475" x2="-4.12115" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-7.55015" y1="3.71475" x2="-7.42315" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.71475" x2="-8.04545" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.72745" x2="-1.69545" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.72745" x2="-4.13385" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.72745" x2="-7.38505" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.72745" x2="-8.04545" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.74015" x2="-1.70815" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.74015" x2="-4.13385" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.74015" x2="-7.34695" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.74015" x2="-8.04545" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.75285" x2="-1.72085" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.75285" x2="-4.13385" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.75285" x2="-7.29615" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.75285" x2="-8.04545" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.76555" x2="-1.73355" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.76555" x2="-4.13385" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.76555" x2="-7.25805" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.76555" x2="-8.04545" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.77825" x2="-1.74625" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.77825" x2="-4.13385" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.77825" x2="-7.21995" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.77825" x2="-8.04545" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.79095" x2="-1.75895" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.79095" x2="-4.13385" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.79095" x2="-7.18185" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.79095" x2="-8.04545" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.80365" x2="-1.77165" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.80365" x2="-4.14655" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.80365" x2="-7.14375" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.80365" x2="-8.04545" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.81635" x2="-1.78435" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.81635" x2="-4.14655" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.81635" x2="-7.09295" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.81635" x2="-8.04545" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.82905" x2="-1.79705" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.82905" x2="-4.14655" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.82905" x2="-7.05485" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.82905" x2="-8.04545" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.84175" x2="-1.80975" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.84175" x2="-4.14655" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.84175" x2="-7.01675" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.84175" x2="-8.04545" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.85445" x2="-1.82245" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.85445" x2="-4.14655" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.85445" x2="-6.96595" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.85445" x2="-8.04545" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.86715" x2="-1.83515" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.86715" x2="-4.15925" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.86715" x2="-6.92785" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.86715" x2="-8.04545" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.87985" x2="-1.84785" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.87985" x2="-4.15925" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.87985" x2="-6.88975" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.87985" x2="-8.04545" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.89255" x2="-1.86055" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.89255" x2="-4.15925" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.89255" x2="-6.85165" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.89255" x2="-8.04545" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.90525" x2="-1.87325" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.90525" x2="-4.15925" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.90525" x2="-6.80085" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.90525" x2="-8.04545" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.91795" x2="-1.88595" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.91795" x2="-4.17195" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.91795" x2="-6.76275" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.91795" x2="-8.04545" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.93065" x2="-1.89865" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="3.93065" x2="-4.17195" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.93065" x2="-6.72465" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.93065" x2="-8.04545" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.94335" x2="-1.91135" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="3.94335" x2="-4.17195" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.94335" x2="-6.68655" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.94335" x2="-8.04545" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.95605" x2="-1.92405" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="3.95605" x2="-4.17195" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.95605" x2="-6.63575" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.95605" x2="-8.04545" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.96875" x2="-1.93675" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="3.96875" x2="-4.18465" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.96875" x2="-6.59765" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.96875" x2="-8.04545" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.98145" x2="-1.94945" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="3.98145" x2="-4.18465" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.98145" x2="-6.55955" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.98145" x2="-8.04545" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.99415" x2="-1.96215" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="3.99415" x2="-4.18465" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.99415" x2="-6.52145" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="3.99415" x2="-8.04545" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.00685" x2="-1.97485" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="4.00685" x2="-4.19735" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.00685" x2="-6.47065" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.00685" x2="-8.04545" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.01955" x2="-1.98755" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="4.01955" x2="-4.19735" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.01955" x2="-6.43255" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.01955" x2="-8.04545" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.03225" x2="-2.00025" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-4.93395" y1="4.03225" x2="-4.19735" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.03225" x2="-6.40715" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.03225" x2="-8.04545" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.04495" x2="-2.01295" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-4.93395" y1="4.04495" x2="-4.21005" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.04495" x2="-6.36905" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.04495" x2="-8.04545" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.05765" x2="-2.02565" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-4.94665" y1="4.05765" x2="-4.21005" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.05765" x2="-6.31825" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.05765" x2="-8.04545" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.07035" x2="-2.03835" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-4.95935" y1="4.07035" x2="-4.21005" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.07035" x2="-6.28015" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.07035" x2="-8.04545" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.08305" x2="-2.05105" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-4.95935" y1="4.08305" x2="-4.22275" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.08305" x2="-6.26745" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.08305" x2="-8.04545" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.09575" x2="-2.06375" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-4.97205" y1="4.09575" x2="-4.22275" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.09575" x2="-6.28015" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.09575" x2="-8.04545" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.10845" x2="-2.07645" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-4.98475" y1="4.10845" x2="-4.22275" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.10845" x2="-6.29285" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.10845" x2="-8.04545" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.12115" x2="-2.08915" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="4.12115" x2="-4.23545" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.12115" x2="-6.30555" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.12115" x2="-8.04545" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.13385" x2="-2.10185" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="4.13385" x2="-4.23545" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.13385" x2="-6.31825" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.13385" x2="-8.04545" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.14655" x2="-2.11455" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-5.01015" y1="4.14655" x2="-4.24815" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.14655" x2="-6.33095" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.14655" x2="-8.04545" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.15925" x2="-2.12725" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-5.02285" y1="4.15925" x2="-4.24815" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.15925" x2="-6.34365" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.15925" x2="-8.04545" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.17195" x2="-2.13995" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-5.03555" y1="4.17195" x2="-4.24815" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.17195" x2="-6.35635" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.17195" x2="-8.04545" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.18465" x2="-2.15265" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-5.04825" y1="4.18465" x2="-4.26085" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.18465" x2="-6.38175" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.18465" x2="-8.04545" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.19735" x2="-2.16535" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="4.19735" x2="-4.26085" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.19735" x2="-6.39445" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.19735" x2="-8.04545" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.21005" x2="-2.17805" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="4.21005" x2="-4.27355" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.21005" x2="-6.40715" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.21005" x2="-8.04545" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.22275" x2="-2.19075" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-5.07365" y1="4.22275" x2="-4.27355" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.22275" x2="-6.41985" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.22275" x2="-8.04545" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.23545" x2="-2.20345" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-5.08635" y1="4.23545" x2="-4.28625" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.23545" x2="-6.43255" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.23545" x2="-8.04545" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.24815" x2="-2.21615" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="4.24815" x2="-4.28625" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.24815" x2="-6.44525" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.24815" x2="-8.04545" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.26085" x2="-2.22885" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="4.26085" x2="-4.29895" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.26085" x2="-6.45795" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.26085" x2="-8.04545" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.27355" x2="-2.24155" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="4.27355" x2="-4.29895" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.27355" x2="-6.47065" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.27355" x2="-8.04545" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.28625" x2="-2.25425" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="4.28625" x2="-4.31165" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.28625" x2="-6.48335" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.28625" x2="-8.04545" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.29895" x2="-2.26695" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="4.29895" x2="-4.31165" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.29895" x2="-6.49605" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.29895" x2="-8.04545" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.31165" x2="-2.27965" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="4.31165" x2="-4.32435" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.31165" x2="-6.50875" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.31165" x2="-8.04545" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.32435" x2="-2.29235" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="4.32435" x2="-4.32435" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.32435" x2="-6.52145" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.32435" x2="-8.04545" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.33705" x2="-2.30505" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-5.20065" y1="4.33705" x2="-4.33705" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.33705" x2="-6.52145" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.33705" x2="-8.04545" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.34975" x2="-2.31775" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-5.22605" y1="4.34975" x2="-4.34975" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.34975" x2="-6.49605" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.34975" x2="-8.04545" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.36245" x2="-2.33045" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-5.23875" y1="4.36245" x2="-4.34975" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.36245" x2="-6.48335" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.36245" x2="-8.04545" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.37515" x2="-2.34315" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-5.26415" y1="4.37515" x2="-4.36245" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.37515" x2="-6.47065" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.37515" x2="-8.04545" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.38785" x2="-2.35585" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-5.27685" y1="4.38785" x2="-4.36245" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.38785" x2="-6.44525" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.38785" x2="-8.04545" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.40055" x2="-2.36855" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-5.30225" y1="4.40055" x2="-4.37515" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.40055" x2="-6.43255" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.40055" x2="-8.04545" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.41325" x2="-2.38125" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-5.31495" y1="4.41325" x2="-4.38785" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.41325" x2="-6.40715" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.41325" x2="-8.04545" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.42595" x2="-2.39395" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-5.34035" y1="4.42595" x2="-4.38785" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.42595" x2="-6.38175" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.42595" x2="-8.04545" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.43865" x2="-2.40665" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-5.36575" y1="4.43865" x2="-4.40055" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.43865" x2="-6.35635" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.43865" x2="-8.04545" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.45135" x2="-2.41935" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-5.39115" y1="4.45135" x2="-4.41325" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.45135" x2="-6.33095" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.45135" x2="-8.04545" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.46405" x2="-2.43205" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-5.41655" y1="4.46405" x2="-4.41325" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.46405" x2="-6.30555" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.46405" x2="-8.04545" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.47675" x2="-2.44475" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-5.45465" y1="4.47675" x2="-4.42595" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.47675" x2="-6.28015" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.47675" x2="-8.04545" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.48945" x2="-2.45745" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-5.48005" y1="4.48945" x2="-4.43865" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.48945" x2="-6.24205" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.48945" x2="-8.04545" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.50215" x2="-2.47015" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-5.51815" y1="4.50215" x2="-4.43865" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.50215" x2="-6.20395" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.50215" x2="-8.04545" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.51485" x2="-2.48285" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-5.55625" y1="4.51485" x2="-4.45135" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.51485" x2="-6.16585" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.51485" x2="-8.04545" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.52755" x2="-2.49555" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-5.60705" y1="4.52755" x2="-4.46405" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.52755" x2="-6.12775" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.52755" x2="-8.04545" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.54025" x2="-2.50825" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-5.65785" y1="4.54025" x2="-4.47675" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.54025" x2="-6.06425" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.54025" x2="-8.04545" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.55295" x2="-2.52095" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-5.74675" y1="4.55295" x2="-4.47675" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.55295" x2="-5.97535" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.55295" x2="-8.04545" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.56565" x2="-2.53365" y2="4.57835" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.56565" x2="-4.48945" y2="4.57835" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.56565" x2="-8.04545" y2="4.57835" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.57835" x2="-2.54635" y2="4.59105" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.57835" x2="-4.50215" y2="4.59105" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.57835" x2="-8.04545" y2="4.59105" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.59105" x2="-2.55905" y2="4.60375" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.59105" x2="-4.51485" y2="4.60375" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.59105" x2="-8.04545" y2="4.60375" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.60375" x2="-2.57175" y2="4.61645" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.60375" x2="-4.52755" y2="4.61645" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.60375" x2="-8.04545" y2="4.61645" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.61645" x2="-2.58445" y2="4.62915" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.61645" x2="-4.52755" y2="4.62915" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.61645" x2="-8.04545" y2="4.62915" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.62915" x2="-2.59715" y2="4.64185" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.62915" x2="-4.54025" y2="4.64185" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.62915" x2="-8.04545" y2="4.64185" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.64185" x2="-2.60985" y2="4.65455" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.64185" x2="-4.55295" y2="4.65455" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.64185" x2="-8.04545" y2="4.65455" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.65455" x2="-2.62255" y2="4.66725" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.65455" x2="-4.56565" y2="4.66725" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.65455" x2="-8.04545" y2="4.66725" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.66725" x2="-2.63525" y2="4.67995" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.66725" x2="-4.57835" y2="4.67995" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.66725" x2="-8.04545" y2="4.67995" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.67995" x2="-2.64795" y2="4.69265" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.67995" x2="-4.59105" y2="4.69265" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.67995" x2="-8.04545" y2="4.69265" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.69265" x2="-2.66065" y2="4.70535" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.69265" x2="-4.60375" y2="4.70535" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.69265" x2="-8.04545" y2="4.70535" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.70535" x2="-2.67335" y2="4.71805" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.70535" x2="-4.61645" y2="4.71805" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.70535" x2="-8.04545" y2="4.71805" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.71805" x2="-2.68605" y2="4.73075" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.71805" x2="-4.62915" y2="4.73075" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.71805" x2="-8.04545" y2="4.73075" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.73075" x2="-2.69875" y2="4.74345" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.73075" x2="-4.64185" y2="4.74345" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.73075" x2="-8.04545" y2="4.74345" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.74345" x2="-2.71145" y2="4.75615" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.74345" x2="-4.65455" y2="4.75615" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.74345" x2="-8.04545" y2="4.75615" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.75615" x2="-2.72415" y2="4.76885" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.75615" x2="-4.66725" y2="4.76885" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.75615" x2="-8.04545" y2="4.76885" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.76885" x2="-2.73685" y2="4.78155" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.76885" x2="-4.67995" y2="4.78155" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.76885" x2="-8.04545" y2="4.78155" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.78155" x2="-2.74955" y2="4.79425" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.78155" x2="-4.69265" y2="4.79425" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.78155" x2="-8.04545" y2="4.79425" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.79425" x2="-2.76225" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-7.01675" y1="4.79425" x2="-4.70535" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.79425" x2="-7.04215" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.79425" x2="-8.04545" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.80695" x2="-2.77495" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-7.00405" y1="4.80695" x2="-4.73075" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.80695" x2="-7.05485" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.80695" x2="-8.04545" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.81965" x2="-2.78765" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-6.97865" y1="4.81965" x2="-4.74345" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.81965" x2="-7.06755" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.81965" x2="-8.04545" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.83235" x2="-2.80035" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-6.96595" y1="4.83235" x2="-4.75615" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.83235" x2="-7.08025" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.83235" x2="-8.04545" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.84505" x2="-2.81305" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-6.95325" y1="4.84505" x2="-4.76885" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.84505" x2="-7.09295" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.84505" x2="-8.04545" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.85775" x2="-2.82575" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-6.94055" y1="4.85775" x2="-4.78155" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.85775" x2="-7.10565" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-8.76935" y1="4.85775" x2="-8.04545" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.87045" x2="-2.83845" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-6.91515" y1="4.87045" x2="-4.80695" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.87045" x2="-7.11835" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-8.75665" y1="4.87045" x2="-8.04545" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.88315" x2="-2.85115" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-6.90245" y1="4.88315" x2="-4.81965" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.88315" x2="-7.13105" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-8.74395" y1="4.88315" x2="-8.04545" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.89585" x2="-2.86385" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-6.88975" y1="4.89585" x2="-4.83235" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.89585" x2="-7.14375" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-8.73125" y1="4.89585" x2="-8.04545" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.90855" x2="-2.87655" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-6.86435" y1="4.90855" x2="-4.85775" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.90855" x2="-7.15645" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-8.71855" y1="4.90855" x2="-8.04545" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.92125" x2="-2.88925" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-6.85165" y1="4.92125" x2="-4.87045" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.92125" x2="-7.16915" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-8.70585" y1="4.92125" x2="-8.04545" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.93395" x2="-2.90195" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-6.82625" y1="4.93395" x2="-4.89585" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.93395" x2="-7.19455" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-8.69315" y1="4.93395" x2="-8.04545" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.94665" x2="-2.91465" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-6.81355" y1="4.94665" x2="-4.90855" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.94665" x2="-7.20725" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-8.68045" y1="4.94665" x2="-8.04545" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.95935" x2="-2.92735" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-6.78815" y1="4.95935" x2="-4.93395" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="4.95935" x2="-7.21995" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="4.95935" x2="-8.04545" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.97205" x2="-2.94005" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="4.97205" x2="-4.95935" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="4.97205" x2="-7.23265" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-8.65505" y1="4.97205" x2="-8.04545" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.98475" x2="-2.95275" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="4.98475" x2="-4.97205" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="4.98475" x2="-7.24535" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-8.64235" y1="4.98475" x2="-8.04545" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.99745" x2="-2.96545" y2="5.01015" layer="21" rot="R180"/>
<rectangle x1="-6.72465" y1="4.99745" x2="-4.99745" y2="5.01015" layer="21" rot="R180"/>
<rectangle x1="-8.62965" y1="4.99745" x2="-8.04545" y2="5.01015" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.01015" x2="-2.97815" y2="5.02285" layer="21" rot="R180"/>
<rectangle x1="-6.69925" y1="5.01015" x2="-5.02285" y2="5.02285" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="5.01015" x2="-8.04545" y2="5.02285" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.02285" x2="-2.99085" y2="5.03555" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="5.02285" x2="-5.04825" y2="5.03555" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="5.02285" x2="-8.04545" y2="5.03555" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.03555" x2="-3.00355" y2="5.04825" layer="21" rot="R180"/>
<rectangle x1="-6.66115" y1="5.03555" x2="-5.06095" y2="5.04825" layer="21" rot="R180"/>
<rectangle x1="-8.59155" y1="5.03555" x2="-8.04545" y2="5.04825" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.04825" x2="-3.01625" y2="5.06095" layer="21" rot="R180"/>
<rectangle x1="-6.63575" y1="5.04825" x2="-5.08635" y2="5.06095" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="5.04825" x2="-8.04545" y2="5.06095" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.06095" x2="-3.02895" y2="5.07365" layer="21" rot="R180"/>
<rectangle x1="-6.61035" y1="5.06095" x2="-5.12445" y2="5.07365" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="5.06095" x2="-8.04545" y2="5.07365" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.07365" x2="-3.04165" y2="5.08635" layer="21" rot="R180"/>
<rectangle x1="-6.57225" y1="5.07365" x2="-5.14985" y2="5.08635" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="5.07365" x2="-8.04545" y2="5.08635" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.08635" x2="-3.05435" y2="5.09905" layer="21" rot="R180"/>
<rectangle x1="-6.54685" y1="5.08635" x2="-5.17525" y2="5.09905" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="5.08635" x2="-8.04545" y2="5.09905" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.09905" x2="-3.06705" y2="5.11175" layer="21" rot="R180"/>
<rectangle x1="-6.52145" y1="5.09905" x2="-5.20065" y2="5.11175" layer="21" rot="R180"/>
<rectangle x1="-8.52805" y1="5.09905" x2="-8.04545" y2="5.11175" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.11175" x2="-3.07975" y2="5.12445" layer="21" rot="R180"/>
<rectangle x1="-6.48335" y1="5.11175" x2="-5.23875" y2="5.12445" layer="21" rot="R180"/>
<rectangle x1="-8.51535" y1="5.11175" x2="-8.04545" y2="5.12445" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.12445" x2="-3.09245" y2="5.13715" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="5.12445" x2="-5.27685" y2="5.13715" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="5.12445" x2="-8.04545" y2="5.13715" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.13715" x2="-3.10515" y2="5.14985" layer="21" rot="R180"/>
<rectangle x1="-6.41985" y1="5.13715" x2="-5.31495" y2="5.14985" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="5.13715" x2="-8.04545" y2="5.14985" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.14985" x2="-3.11785" y2="5.16255" layer="21" rot="R180"/>
<rectangle x1="-6.36905" y1="5.14985" x2="-5.35305" y2="5.16255" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="5.14985" x2="-8.04545" y2="5.16255" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.16255" x2="-3.13055" y2="5.17525" layer="21" rot="R180"/>
<rectangle x1="-6.33095" y1="5.16255" x2="-5.39115" y2="5.17525" layer="21" rot="R180"/>
<rectangle x1="-8.46455" y1="5.16255" x2="-8.04545" y2="5.17525" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.17525" x2="-3.14325" y2="5.18795" layer="21" rot="R180"/>
<rectangle x1="-6.28015" y1="5.17525" x2="-5.44195" y2="5.18795" layer="21" rot="R180"/>
<rectangle x1="-8.45185" y1="5.17525" x2="-8.04545" y2="5.18795" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.18795" x2="-3.15595" y2="5.20065" layer="21" rot="R180"/>
<rectangle x1="-6.22935" y1="5.18795" x2="-5.50545" y2="5.20065" layer="21" rot="R180"/>
<rectangle x1="-8.43915" y1="5.18795" x2="-8.04545" y2="5.20065" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.20065" x2="-3.16865" y2="5.21335" layer="21" rot="R180"/>
<rectangle x1="-6.15315" y1="5.20065" x2="-5.56895" y2="5.21335" layer="21" rot="R180"/>
<rectangle x1="-8.42645" y1="5.20065" x2="-8.04545" y2="5.21335" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.21335" x2="-3.18135" y2="5.22605" layer="21" rot="R180"/>
<rectangle x1="-6.06425" y1="5.21335" x2="-5.65785" y2="5.22605" layer="21" rot="R180"/>
<rectangle x1="-8.41375" y1="5.21335" x2="-8.04545" y2="5.22605" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.22605" x2="-3.19405" y2="5.23875" layer="21" rot="R180"/>
<rectangle x1="-8.40105" y1="5.22605" x2="-8.04545" y2="5.23875" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.23875" x2="-3.20675" y2="5.25145" layer="21" rot="R180"/>
<rectangle x1="-8.38835" y1="5.23875" x2="-8.04545" y2="5.25145" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.25145" x2="-3.21945" y2="5.26415" layer="21" rot="R180"/>
<rectangle x1="-8.37565" y1="5.25145" x2="-8.04545" y2="5.26415" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="5.26415" x2="-3.23215" y2="5.27685" layer="21" rot="R180"/>
<rectangle x1="-8.36295" y1="5.26415" x2="-8.04545" y2="5.27685" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="5.27685" x2="-3.24485" y2="5.28955" layer="21" rot="R180"/>
<rectangle x1="-8.35025" y1="5.27685" x2="-8.05815" y2="5.28955" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="5.28955" x2="-3.25755" y2="5.30225" layer="21" rot="R180"/>
<rectangle x1="-8.33755" y1="5.28955" x2="-8.05815" y2="5.30225" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="5.30225" x2="-3.27025" y2="5.31495" layer="21" rot="R180"/>
<rectangle x1="-8.32485" y1="5.30225" x2="-8.05815" y2="5.31495" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="5.31495" x2="-3.28295" y2="5.32765" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="5.31495" x2="-8.07085" y2="5.32765" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="5.32765" x2="-3.30835" y2="5.34035" layer="21" rot="R180"/>
<rectangle x1="-8.28675" y1="5.32765" x2="-8.07085" y2="5.34035" layer="21" rot="R180"/>
<rectangle x1="-3.44805" y1="5.34035" x2="-3.33375" y2="5.35305" layer="21" rot="R180"/>
<rectangle x1="-8.27405" y1="5.34035" x2="-8.08355" y2="5.35305" layer="21" rot="R180"/>
<rectangle x1="-3.42265" y1="5.35305" x2="-3.37185" y2="5.36575" layer="21" rot="R180"/>
<rectangle x1="-8.26135" y1="5.35305" x2="-8.09625" y2="5.36575" layer="21" rot="R180"/>
<rectangle x1="-8.23595" y1="5.36575" x2="-8.10895" y2="5.37845" layer="21" rot="R180"/>
<rectangle x1="-8.19785" y1="5.37845" x2="-8.13435" y2="5.39115" layer="21" rot="R180"/>
<text x="-2.8956" y="-0.9652" size="0.8128" layer="22" font="vector" rot="MR0">acl.mit.edu</text>
</package>
<package name="MINIQUADBATT">
<pad name="+" x="1.778" y="0" drill="1.6" diameter="2.54" shape="square"/>
<pad name="-" x="-1.524" y="0" drill="1.6" diameter="2.54"/>
<wire x1="-2.921" y1="1.397" x2="3.175" y2="1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.397" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="-2.921" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-2.921" y1="-1.397" x2="-2.921" y2="1.397" width="0.2032" layer="21"/>
</package>
<package name="7-PIN-LGA">
<smd name="6" x="1.5" y="-1.7" dx="0.6" dy="1" layer="1"/>
<smd name="5" x="0" y="-1.7" dx="0.6" dy="1" layer="1"/>
<smd name="4" x="-1.5" y="-1.7" dx="0.6" dy="1" layer="1"/>
<smd name="3" x="-1.5" y="1.7" dx="0.6" dy="1" layer="1"/>
<smd name="2" x="0" y="1.7" dx="0.6" dy="1" layer="1"/>
<smd name="1" x="1.5" y="1.7" dx="0.6" dy="1" layer="1"/>
<smd name="7" x="1.75" y="0" dx="1" dy="0.6" layer="1"/>
<wire x1="-1.9" y1="1.8" x2="1.9" y2="1.8" width="0.127" layer="51"/>
<wire x1="1.9" y1="1.8" x2="1.9" y2="-1.8" width="0.127" layer="51"/>
<wire x1="1.9" y1="-1.8" x2="-1.9" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-1.9" y1="-1.8" x2="-1.9" y2="1.8" width="0.127" layer="51"/>
<wire x1="1.935" y1="2" x2="2.1" y2="2" width="0.127" layer="21"/>
<wire x1="2.1" y1="2" x2="2.1" y2="1.3" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="2.1" y2="-2" width="0.127" layer="21"/>
<wire x1="2.1" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="-2.1" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-2" x2="-2.1" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.3" x2="-2.1" y2="2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<circle x="-1.3" y="0.6" radius="0.3" width="0.127" layer="21"/>
<text x="-3" y="2.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BATT">
<pad name="P$1" x="3.175" y="0" drill="2.2" diameter="4.572" shape="square"/>
<pad name="P$2" x="-2.54" y="0" drill="2.2" diameter="4.572"/>
<wire x1="-5.08" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="5.715" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-5.08" x2="-5.08" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="3.81" width="0.2032" layer="21"/>
<text x="2.54" y="-5.08" size="2.54" layer="21" ratio="18">+</text>
<text x="-3.81" y="-5.08" size="2.54" layer="21" ratio="18">-</text>
<text x="-1.27" y="2.54" size="1.016" layer="21" font="vector" ratio="15">BATT</text>
</package>
<package name="ESC">
<pad name="+" x="1.778" y="0" drill="1.5" diameter="2.54" shape="square"/>
<pad name="-" x="-1.778" y="0" drill="1.6" diameter="2.54"/>
<wire x1="-3.175" y1="1.905" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="-3.175" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.905" x2="-3.175" y2="1.905" width="0.2032" layer="21"/>
<text x="1.27" y="1.905" size="2.1844" layer="21" font="vector" ratio="14">+</text>
<text x="-1.905" y="1.905" size="2.1844" layer="21" font="vector" ratio="14">-</text>
</package>
<package name="PQFP144">
<description>&lt;b&gt;Platic Quad Flat Package&lt;/b&gt;&lt;p&gt;
PGE (S-PQFP-G144)</description>
<wire x1="-9.75" y1="9" x2="-9" y2="9.75" width="0.254" layer="21"/>
<wire x1="-9" y1="9.75" x2="9.5" y2="9.75" width="0.254" layer="21"/>
<wire x1="9.5" y1="9.75" x2="9.75" y2="9.5" width="0.254" layer="21"/>
<wire x1="9.75" y1="9.5" x2="9.75" y2="-9.5" width="0.254" layer="21"/>
<wire x1="9.75" y1="-9.5" x2="9.5" y2="-9.75" width="0.254" layer="21"/>
<wire x1="9.5" y1="-9.75" x2="-9.5" y2="-9.75" width="0.254" layer="21"/>
<wire x1="-9.5" y1="-9.75" x2="-9.75" y2="-9.5" width="0.254" layer="21"/>
<wire x1="-9.75" y1="-9.5" x2="-9.75" y2="9" width="0.254" layer="21"/>
<wire x1="1.03" y1="0.34" x2="0.95" y2="0.37" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.37" x2="0.85" y2="0.39" width="0.1016" layer="51"/>
<wire x1="0.85" y1="0.39" x2="0.79" y2="0.39" width="0.1016" layer="51"/>
<wire x1="0.79" y1="0.39" x2="0.71" y2="0.38" width="0.1016" layer="51"/>
<wire x1="0.71" y1="0.38" x2="0.66" y2="0.22" width="0.1016" layer="51"/>
<wire x1="0.66" y1="0.22" x2="0.78" y2="0.22" width="0.1016" layer="51"/>
<wire x1="0.78" y1="0.22" x2="0.72" y2="-0.07" width="0.1016" layer="51"/>
<wire x1="0.72" y1="-0.07" x2="0.61" y2="-0.08" width="0.1016" layer="51"/>
<wire x1="0.61" y1="-0.08" x2="0.51" y2="-0.46" width="0.1016" layer="51"/>
<wire x1="0.51" y1="-0.46" x2="0.59" y2="-0.43" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-0.43" x2="1.09" y2="-0.43" width="0.1016" layer="51"/>
<wire x1="1.09" y1="-0.43" x2="1.13" y2="-0.38" width="0.1016" layer="51"/>
<wire x1="1.13" y1="-0.38" x2="1.14" y2="-0.34" width="0.1016" layer="51"/>
<wire x1="1.14" y1="-0.34" x2="1.14" y2="-0.12" width="0.1016" layer="51"/>
<wire x1="1.14" y1="-0.12" x2="1.11" y2="-0.06" width="0.1016" layer="51"/>
<wire x1="1.11" y1="-0.06" x2="1.07" y2="-0.01" width="0.1016" layer="51"/>
<wire x1="1.07" y1="-0.01" x2="1.04" y2="0.04" width="0.1016" layer="51"/>
<wire x1="1.04" y1="0.04" x2="1.02" y2="0.11" width="0.1016" layer="51"/>
<wire x1="1.02" y1="0.11" x2="1.02" y2="0.16" width="0.1016" layer="51"/>
<wire x1="1.02" y1="0.16" x2="1.03" y2="0.34" width="0.1016" layer="51"/>
<wire x1="0.61" y1="-0.46" x2="0.61" y2="-0.48" width="0.1016" layer="51"/>
<wire x1="0.61" y1="-0.48" x2="0.59" y2="-0.58" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-0.58" x2="0.58" y2="-0.61" width="0.1016" layer="51"/>
<wire x1="0.58" y1="-0.61" x2="0.57" y2="-0.63" width="0.1016" layer="51"/>
<wire x1="0.57" y1="-0.63" x2="0.46" y2="-0.67" width="0.1016" layer="51"/>
<wire x1="0.46" y1="-0.67" x2="0.42" y2="-0.68" width="0.1016" layer="51"/>
<wire x1="0.42" y1="-0.68" x2="0.36" y2="-0.68" width="0.1016" layer="51"/>
<wire x1="0.36" y1="-0.68" x2="0.29" y2="-0.67" width="0.1016" layer="51"/>
<wire x1="0.29" y1="-0.67" x2="0.23" y2="-0.65" width="0.1016" layer="51"/>
<wire x1="0.23" y1="-0.65" x2="0.18" y2="-0.62" width="0.1016" layer="51"/>
<wire x1="0.18" y1="-0.62" x2="0.15" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="0.15" y1="-0.6" x2="0.11" y2="-0.56" width="0.1016" layer="51"/>
<wire x1="0.11" y1="-0.56" x2="0.09" y2="-0.52" width="0.1016" layer="51"/>
<wire x1="0.09" y1="-0.52" x2="0.08" y2="-0.51" width="0.1016" layer="51"/>
<wire x1="0.08" y1="-0.51" x2="0.06" y2="-0.48" width="0.1016" layer="51"/>
<wire x1="0.06" y1="-0.48" x2="0.06" y2="-0.43" width="0.1016" layer="51"/>
<wire x1="0.06" y1="-0.43" x2="-0.09" y2="-0.43" width="0.1016" layer="51"/>
<wire x1="-0.09" y1="-0.43" x2="-0.09" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="-0.09" y1="-0.45" x2="-0.06" y2="-0.48" width="0.1016" layer="51"/>
<wire x1="-0.06" y1="-0.48" x2="-0.02" y2="-0.54" width="0.1016" layer="51"/>
<wire x1="-0.02" y1="-0.54" x2="0.03" y2="-0.61" width="0.1016" layer="51"/>
<wire x1="0.03" y1="-0.61" x2="0.11" y2="-0.77" width="0.1016" layer="51"/>
<wire x1="0.11" y1="-0.77" x2="0.19" y2="-0.91" width="0.1016" layer="51"/>
<wire x1="0.19" y1="-0.91" x2="0.25" y2="-0.98" width="0.1016" layer="51"/>
<wire x1="0.25" y1="-0.98" x2="0.31" y2="-1.03" width="0.1016" layer="51"/>
<wire x1="0.31" y1="-1.03" x2="0.37" y2="-1.07" width="0.1016" layer="51"/>
<wire x1="0.37" y1="-1.07" x2="0.44" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.44" y1="-1.1" x2="0.49" y2="-1.12" width="0.1016" layer="51"/>
<wire x1="0.49" y1="-1.12" x2="0.55" y2="-1.13" width="0.1016" layer="51"/>
<wire x1="0.55" y1="-1.13" x2="0.59" y2="-1.12" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-1.12" x2="0.62" y2="-1.11" width="0.1016" layer="51"/>
<wire x1="0.62" y1="-1.11" x2="0.59" y2="-1.03" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-1.03" x2="0.58" y2="-0.97" width="0.1016" layer="51"/>
<wire x1="0.58" y1="-0.97" x2="0.58" y2="-0.85" width="0.1016" layer="51"/>
<wire x1="0.58" y1="-0.85" x2="0.6" y2="-0.78" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.78" x2="0.64" y2="-0.71" width="0.1016" layer="51"/>
<wire x1="0.64" y1="-0.71" x2="0.7" y2="-0.65" width="0.1016" layer="51"/>
<wire x1="0.7" y1="-0.65" x2="0.82" y2="-0.56" width="0.1016" layer="51"/>
<wire x1="0.82" y1="-0.56" x2="0.95" y2="-0.51" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.51" x2="1.08" y2="-0.44" width="0.1016" layer="51"/>
<wire x1="1.08" y1="-0.44" x2="0.61" y2="-0.46" width="0.1016" layer="51"/>
<wire x1="0.1" y1="0.8" x2="-0.33" y2="0.8" width="0.1016" layer="51"/>
<wire x1="-0.33" y1="0.8" x2="-0.33" y2="0.02" width="0.1016" layer="51"/>
<wire x1="-0.33" y1="0.02" x2="-0.83" y2="0.02" width="0.1016" layer="51"/>
<wire x1="-0.83" y1="0.02" x2="-0.82" y2="-0.03" width="0.1016" layer="51"/>
<wire x1="-0.82" y1="-0.03" x2="-0.79" y2="-0.09" width="0.1016" layer="51"/>
<wire x1="-0.79" y1="-0.09" x2="-0.74" y2="-0.15" width="0.1016" layer="51"/>
<wire x1="-0.74" y1="-0.15" x2="-0.65" y2="-0.23" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="-0.23" x2="-0.6" y2="-0.29" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.29" x2="-0.56" y2="-0.36" width="0.1016" layer="51"/>
<wire x1="-0.56" y1="-0.36" x2="-0.54" y2="-0.39" width="0.1016" layer="51"/>
<wire x1="-0.54" y1="-0.39" x2="-0.52" y2="-0.44" width="0.1016" layer="51"/>
<wire x1="-0.52" y1="-0.44" x2="-0.48" y2="-0.48" width="0.1016" layer="51"/>
<wire x1="-0.48" y1="-0.48" x2="-0.42" y2="-0.52" width="0.1016" layer="51"/>
<wire x1="-0.42" y1="-0.52" x2="-0.39" y2="-0.53" width="0.1016" layer="51"/>
<wire x1="-0.39" y1="-0.53" x2="-0.34" y2="-0.53" width="0.1016" layer="51"/>
<wire x1="-0.34" y1="-0.53" x2="-0.26" y2="-0.46" width="0.1016" layer="51"/>
<wire x1="-0.26" y1="-0.46" x2="-0.2" y2="-0.43" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.43" x2="-0.12" y2="-0.43" width="0.1016" layer="51"/>
<wire x1="-0.12" y1="-0.43" x2="0.06" y2="-0.38" width="0.1016" layer="51"/>
<wire x1="0.06" y1="-0.38" x2="0.14" y2="-0.07" width="0.1016" layer="51"/>
<wire x1="0.14" y1="-0.07" x2="-0.04" y2="-0.07" width="0.1016" layer="51"/>
<wire x1="-0.04" y1="-0.07" x2="-0.04" y2="-0.03" width="0.1016" layer="51"/>
<wire x1="-0.04" y1="-0.03" x2="0.01" y2="0.21" width="0.1016" layer="51"/>
<wire x1="0.01" y1="0.21" x2="0.2" y2="0.21" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.21" x2="0.26" y2="0.42" width="0.1016" layer="51"/>
<wire x1="0.26" y1="0.42" x2="0.14" y2="0.48" width="0.1016" layer="51"/>
<wire x1="0.14" y1="0.48" x2="0.1" y2="0.52" width="0.1016" layer="51"/>
<wire x1="0.1" y1="0.52" x2="0.1" y2="0.8" width="0.1016" layer="51"/>
<wire x1="0.53" y1="0.3" x2="0.39" y2="0.3" width="0.1016" layer="51"/>
<wire x1="0.39" y1="0.3" x2="0.24" y2="-0.38" width="0.1016" layer="51"/>
<wire x1="0.24" y1="-0.38" x2="0.37" y2="-0.38" width="0.1016" layer="51"/>
<wire x1="0.37" y1="-0.38" x2="0.53" y2="0.3" width="0.1016" layer="51"/>
<circle x="-8.75" y="8.75" radius="0.2499" width="0.254" layer="21"/>
<circle x="0.5" y="0.5" radius="0.12" width="0" layer="51"/>
<smd name="1" x="-10.75" y="8.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="2" x="-10.75" y="8.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="3" x="-10.75" y="7.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="4" x="-10.75" y="7.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="5" x="-10.75" y="6.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="6" x="-10.75" y="6.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="7" x="-10.75" y="5.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="8" x="-10.75" y="5.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="9" x="-10.75" y="4.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="10" x="-10.75" y="4.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="11" x="-10.75" y="3.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="12" x="-10.75" y="3.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="13" x="-10.75" y="2.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="14" x="-10.75" y="2.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="15" x="-10.75" y="1.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="16" x="-10.75" y="1.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="17" x="-10.75" y="0.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="18" x="-10.75" y="0.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="19" x="-10.75" y="-0.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="20" x="-10.75" y="-0.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="21" x="-10.75" y="-1.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="22" x="-10.75" y="-1.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="23" x="-10.75" y="-2.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="24" x="-10.75" y="-2.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="25" x="-10.75" y="-3.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="26" x="-10.75" y="-3.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="27" x="-10.75" y="-4.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="28" x="-10.75" y="-4.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="29" x="-10.75" y="-5.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="30" x="-10.75" y="-5.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="31" x="-10.75" y="-6.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="32" x="-10.75" y="-6.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="33" x="-10.75" y="-7.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="34" x="-10.75" y="-7.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="35" x="-10.75" y="-8.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="36" x="-10.75" y="-8.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="37" x="-8.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="38" x="-8.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="39" x="-7.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="40" x="-7.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="41" x="-6.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="42" x="-6.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="43" x="-5.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="44" x="-5.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="45" x="-4.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="46" x="-4.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="47" x="-3.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="48" x="-3.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="49" x="-2.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="50" x="-2.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="51" x="-1.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="52" x="-1.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="53" x="-0.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="54" x="-0.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="55" x="0.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="56" x="0.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="57" x="1.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="58" x="1.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="59" x="2.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="60" x="2.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="61" x="3.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="62" x="3.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="63" x="4.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="64" x="4.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="65" x="5.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="66" x="5.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="67" x="6.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="68" x="6.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="69" x="7.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="70" x="7.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="71" x="8.25" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="72" x="8.75" y="-10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="73" x="10.75" y="-8.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="74" x="10.75" y="-8.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="75" x="10.75" y="-7.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="76" x="10.75" y="-7.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="77" x="10.75" y="-6.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="78" x="10.75" y="-6.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="79" x="10.75" y="-5.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="80" x="10.75" y="-5.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="81" x="10.75" y="-4.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="82" x="10.75" y="-4.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="83" x="10.75" y="-3.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="84" x="10.75" y="-3.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="85" x="10.75" y="-2.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="86" x="10.75" y="-2.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="87" x="10.75" y="-1.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="88" x="10.75" y="-1.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="89" x="10.75" y="-0.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="90" x="10.75" y="-0.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="91" x="10.75" y="0.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="92" x="10.75" y="0.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="93" x="10.75" y="1.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="94" x="10.75" y="1.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="95" x="10.75" y="2.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="96" x="10.75" y="2.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="97" x="10.75" y="3.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="98" x="10.75" y="3.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="99" x="10.75" y="4.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="100" x="10.75" y="4.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="101" x="10.75" y="5.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="102" x="10.75" y="5.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="103" x="10.75" y="6.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="104" x="10.75" y="6.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="105" x="10.75" y="7.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="106" x="10.75" y="7.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="107" x="10.75" y="8.25" dx="1.2" dy="0.33" layer="1"/>
<smd name="108" x="10.75" y="8.75" dx="1.2" dy="0.33" layer="1"/>
<smd name="109" x="8.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="110" x="8.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="111" x="7.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="112" x="7.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="113" x="6.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="114" x="6.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="115" x="5.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="116" x="5.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="117" x="4.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="118" x="4.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="119" x="3.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="120" x="3.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="121" x="2.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="122" x="2.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="123" x="1.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="124" x="1.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="125" x="0.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="126" x="0.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="127" x="-0.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="128" x="-0.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="129" x="-1.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="130" x="-1.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="131" x="-2.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="132" x="-2.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="133" x="-3.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="134" x="-3.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="135" x="-4.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="136" x="-4.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="137" x="-5.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="138" x="-5.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="139" x="-6.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="140" x="-6.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="141" x="-7.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="142" x="-7.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="143" x="-8.25" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<smd name="144" x="-8.75" y="10.75" dx="0.33" dy="1.2" layer="1"/>
<text x="-6.7201" y="4.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.7799" y="-5.74" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="-8.2301" size="0.6096" layer="51">PGE(S-PQFP-G144)</text>
<rectangle x1="-10.9499" y1="8.5999" x2="-9.8999" y2="8.8999" layer="51"/>
<rectangle x1="-10.9499" y1="8.1001" x2="-9.8999" y2="8.4" layer="51"/>
<rectangle x1="-10.9499" y1="7.5999" x2="-9.8999" y2="7.8999" layer="51"/>
<rectangle x1="-10.9499" y1="7.1001" x2="-9.8999" y2="7.4" layer="51"/>
<rectangle x1="-10.9499" y1="6.5999" x2="-9.8999" y2="6.8999" layer="51"/>
<rectangle x1="-10.9499" y1="6.1001" x2="-9.8999" y2="6.4" layer="51"/>
<rectangle x1="-10.9499" y1="5.5999" x2="-9.8999" y2="5.8999" layer="51"/>
<rectangle x1="-10.9499" y1="5.1001" x2="-9.8999" y2="5.4" layer="51"/>
<rectangle x1="-10.9499" y1="4.5999" x2="-9.8999" y2="4.8999" layer="51"/>
<rectangle x1="-10.9499" y1="4.1001" x2="-9.8999" y2="4.4" layer="51"/>
<rectangle x1="-10.9499" y1="3.5999" x2="-9.8999" y2="3.8999" layer="51"/>
<rectangle x1="-10.9499" y1="3.1001" x2="-9.8999" y2="3.4" layer="51"/>
<rectangle x1="-10.9499" y1="2.5999" x2="-9.8999" y2="2.8999" layer="51"/>
<rectangle x1="-10.9499" y1="2.1001" x2="-9.8999" y2="2.4" layer="51"/>
<rectangle x1="-10.9499" y1="1.5999" x2="-9.8999" y2="1.8999" layer="51"/>
<rectangle x1="-10.9499" y1="1.1001" x2="-9.8999" y2="1.4" layer="51"/>
<rectangle x1="-10.9499" y1="0.5999" x2="-9.8999" y2="0.8999" layer="51"/>
<rectangle x1="-10.9499" y1="0.1001" x2="-9.8999" y2="0.4001" layer="51"/>
<rectangle x1="-10.9499" y1="-0.4001" x2="-9.8999" y2="-0.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-0.8999" x2="-9.8999" y2="-0.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-1.4" x2="-9.8999" y2="-1.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-1.8999" x2="-9.8999" y2="-1.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-2.4" x2="-9.8999" y2="-2.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-2.8999" x2="-9.8999" y2="-2.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-3.4" x2="-9.8999" y2="-3.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-3.8999" x2="-9.8999" y2="-3.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-4.4" x2="-9.8999" y2="-4.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-4.8999" x2="-9.8999" y2="-4.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-5.4" x2="-9.8999" y2="-5.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-5.8999" x2="-9.8999" y2="-5.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-6.4" x2="-9.8999" y2="-6.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-6.8999" x2="-9.8999" y2="-6.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-7.4" x2="-9.8999" y2="-7.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-7.8999" x2="-9.8999" y2="-7.5999" layer="51"/>
<rectangle x1="-10.9499" y1="-8.4" x2="-9.8999" y2="-8.1001" layer="51"/>
<rectangle x1="-10.9499" y1="-8.8999" x2="-9.8999" y2="-8.5999" layer="51"/>
<rectangle x1="-8.8999" y1="-10.9499" x2="-8.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-8.4" y1="-10.9499" x2="-8.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-7.8999" y1="-10.9499" x2="-7.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-7.4" y1="-10.9499" x2="-7.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-6.8999" y1="-10.9499" x2="-6.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-6.4" y1="-10.9499" x2="-6.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-5.8999" y1="-10.9499" x2="-5.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-5.4" y1="-10.9499" x2="-5.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-4.8999" y1="-10.9499" x2="-4.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-4.4" y1="-10.9499" x2="-4.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-3.8999" y1="-10.9499" x2="-3.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-3.4" y1="-10.9499" x2="-3.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-2.8999" y1="-10.9499" x2="-2.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-2.4" y1="-10.9499" x2="-2.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-1.8999" y1="-10.9499" x2="-1.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-1.4" y1="-10.9499" x2="-1.1001" y2="-9.8999" layer="51"/>
<rectangle x1="-0.8999" y1="-10.9499" x2="-0.5999" y2="-9.8999" layer="51"/>
<rectangle x1="-0.4001" y1="-10.9499" x2="-0.1001" y2="-9.8999" layer="51"/>
<rectangle x1="0.1001" y1="-10.9499" x2="0.4001" y2="-9.8999" layer="51"/>
<rectangle x1="0.5999" y1="-10.9499" x2="0.8999" y2="-9.8999" layer="51"/>
<rectangle x1="1.1001" y1="-10.9499" x2="1.4" y2="-9.8999" layer="51"/>
<rectangle x1="1.5999" y1="-10.9499" x2="1.8999" y2="-9.8999" layer="51"/>
<rectangle x1="2.1001" y1="-10.9499" x2="2.4" y2="-9.8999" layer="51"/>
<rectangle x1="2.5999" y1="-10.9499" x2="2.8999" y2="-9.8999" layer="51"/>
<rectangle x1="3.1001" y1="-10.9499" x2="3.4" y2="-9.8999" layer="51"/>
<rectangle x1="3.5999" y1="-10.9499" x2="3.8999" y2="-9.8999" layer="51"/>
<rectangle x1="4.1001" y1="-10.9499" x2="4.4" y2="-9.8999" layer="51"/>
<rectangle x1="4.5999" y1="-10.9499" x2="4.8999" y2="-9.8999" layer="51"/>
<rectangle x1="5.1001" y1="-10.9499" x2="5.4" y2="-9.8999" layer="51"/>
<rectangle x1="5.5999" y1="-10.9499" x2="5.8999" y2="-9.8999" layer="51"/>
<rectangle x1="6.1001" y1="-10.9499" x2="6.4" y2="-9.8999" layer="51"/>
<rectangle x1="6.5999" y1="-10.9499" x2="6.8999" y2="-9.8999" layer="51"/>
<rectangle x1="7.1001" y1="-10.9499" x2="7.4" y2="-9.8999" layer="51"/>
<rectangle x1="7.5999" y1="-10.9499" x2="7.8999" y2="-9.8999" layer="51"/>
<rectangle x1="8.1001" y1="-10.9499" x2="8.4" y2="-9.8999" layer="51"/>
<rectangle x1="8.5999" y1="-10.9499" x2="8.8999" y2="-9.8999" layer="51"/>
<rectangle x1="9.8999" y1="-8.8999" x2="10.9499" y2="-8.5999" layer="51"/>
<rectangle x1="9.8999" y1="-8.4" x2="10.9499" y2="-8.1001" layer="51"/>
<rectangle x1="9.8999" y1="-7.8999" x2="10.9499" y2="-7.5999" layer="51"/>
<rectangle x1="9.8999" y1="-7.4" x2="10.9499" y2="-7.1001" layer="51"/>
<rectangle x1="9.8999" y1="-6.8999" x2="10.9499" y2="-6.5999" layer="51"/>
<rectangle x1="9.8999" y1="-6.4" x2="10.9499" y2="-6.1001" layer="51"/>
<rectangle x1="9.8999" y1="-5.8999" x2="10.9499" y2="-5.5999" layer="51"/>
<rectangle x1="9.8999" y1="-5.4" x2="10.9499" y2="-5.1001" layer="51"/>
<rectangle x1="9.8999" y1="-4.8999" x2="10.9499" y2="-4.5999" layer="51"/>
<rectangle x1="9.8999" y1="-4.4" x2="10.9499" y2="-4.1001" layer="51"/>
<rectangle x1="9.8999" y1="-3.8999" x2="10.9499" y2="-3.5999" layer="51"/>
<rectangle x1="9.8999" y1="-3.4" x2="10.9499" y2="-3.1001" layer="51"/>
<rectangle x1="9.8999" y1="-2.8999" x2="10.9499" y2="-2.5999" layer="51"/>
<rectangle x1="9.8999" y1="-2.4" x2="10.9499" y2="-2.1001" layer="51"/>
<rectangle x1="9.8999" y1="-1.8999" x2="10.9499" y2="-1.5999" layer="51"/>
<rectangle x1="9.8999" y1="-1.4" x2="10.9499" y2="-1.1001" layer="51"/>
<rectangle x1="9.8999" y1="-0.8999" x2="10.9499" y2="-0.5999" layer="51"/>
<rectangle x1="9.8999" y1="-0.4001" x2="10.9499" y2="-0.1001" layer="51"/>
<rectangle x1="9.8999" y1="0.1001" x2="10.9499" y2="0.4001" layer="51"/>
<rectangle x1="9.8999" y1="0.5999" x2="10.9499" y2="0.8999" layer="51"/>
<rectangle x1="9.8999" y1="1.1001" x2="10.9499" y2="1.4" layer="51"/>
<rectangle x1="9.8999" y1="1.5999" x2="10.9499" y2="1.8999" layer="51"/>
<rectangle x1="9.8999" y1="2.1001" x2="10.9499" y2="2.4" layer="51"/>
<rectangle x1="9.8999" y1="2.5999" x2="10.9499" y2="2.8999" layer="51"/>
<rectangle x1="9.8999" y1="3.1001" x2="10.9499" y2="3.4" layer="51"/>
<rectangle x1="9.8999" y1="3.5999" x2="10.9499" y2="3.8999" layer="51"/>
<rectangle x1="9.8999" y1="4.1001" x2="10.9499" y2="4.4" layer="51"/>
<rectangle x1="9.8999" y1="4.5999" x2="10.9499" y2="4.8999" layer="51"/>
<rectangle x1="9.8999" y1="5.1001" x2="10.9499" y2="5.4" layer="51"/>
<rectangle x1="9.8999" y1="5.5999" x2="10.9499" y2="5.8999" layer="51"/>
<rectangle x1="9.8999" y1="6.1001" x2="10.9499" y2="6.4" layer="51"/>
<rectangle x1="9.8999" y1="6.5999" x2="10.9499" y2="6.8999" layer="51"/>
<rectangle x1="9.8999" y1="7.1001" x2="10.9499" y2="7.4" layer="51"/>
<rectangle x1="9.8999" y1="7.5999" x2="10.9499" y2="7.8999" layer="51"/>
<rectangle x1="9.8999" y1="8.1001" x2="10.9499" y2="8.4" layer="51"/>
<rectangle x1="9.8999" y1="8.5999" x2="10.9499" y2="8.8999" layer="51"/>
<rectangle x1="8.5999" y1="9.8999" x2="8.8999" y2="10.9499" layer="51"/>
<rectangle x1="8.1001" y1="9.8999" x2="8.4" y2="10.9499" layer="51"/>
<rectangle x1="7.5999" y1="9.8999" x2="7.8999" y2="10.9499" layer="51"/>
<rectangle x1="7.1001" y1="9.8999" x2="7.4" y2="10.9499" layer="51"/>
<rectangle x1="6.5999" y1="9.8999" x2="6.8999" y2="10.9499" layer="51"/>
<rectangle x1="6.1001" y1="9.8999" x2="6.4" y2="10.9499" layer="51"/>
<rectangle x1="5.5999" y1="9.8999" x2="5.8999" y2="10.9499" layer="51"/>
<rectangle x1="5.1001" y1="9.8999" x2="5.4" y2="10.9499" layer="51"/>
<rectangle x1="4.5999" y1="9.8999" x2="4.8999" y2="10.9499" layer="51"/>
<rectangle x1="4.1001" y1="9.8999" x2="4.4" y2="10.9499" layer="51"/>
<rectangle x1="3.5999" y1="9.8999" x2="3.8999" y2="10.9499" layer="51"/>
<rectangle x1="3.1001" y1="9.8999" x2="3.4" y2="10.9499" layer="51"/>
<rectangle x1="2.5999" y1="9.8999" x2="2.8999" y2="10.9499" layer="51"/>
<rectangle x1="2.1001" y1="9.8999" x2="2.4" y2="10.9499" layer="51"/>
<rectangle x1="1.5999" y1="9.8999" x2="1.8999" y2="10.9499" layer="51"/>
<rectangle x1="1.1001" y1="9.8999" x2="1.4" y2="10.9499" layer="51"/>
<rectangle x1="0.5999" y1="9.8999" x2="0.8999" y2="10.9499" layer="51"/>
<rectangle x1="0.1001" y1="9.8999" x2="0.4001" y2="10.9499" layer="51"/>
<rectangle x1="-0.4001" y1="9.8999" x2="-0.1001" y2="10.9499" layer="51"/>
<rectangle x1="-0.8999" y1="9.8999" x2="-0.5999" y2="10.9499" layer="51"/>
<rectangle x1="-1.4" y1="9.8999" x2="-1.1001" y2="10.9499" layer="51"/>
<rectangle x1="-1.8999" y1="9.8999" x2="-1.5999" y2="10.9499" layer="51"/>
<rectangle x1="-2.4" y1="9.8999" x2="-2.1001" y2="10.9499" layer="51"/>
<rectangle x1="-2.8999" y1="9.8999" x2="-2.5999" y2="10.9499" layer="51"/>
<rectangle x1="-3.4" y1="9.8999" x2="-3.1001" y2="10.9499" layer="51"/>
<rectangle x1="-3.8999" y1="9.8999" x2="-3.5999" y2="10.9499" layer="51"/>
<rectangle x1="-4.4" y1="9.8999" x2="-4.1001" y2="10.9499" layer="51"/>
<rectangle x1="-4.8999" y1="9.8999" x2="-4.5999" y2="10.9499" layer="51"/>
<rectangle x1="-5.4" y1="9.8999" x2="-5.1001" y2="10.9499" layer="51"/>
<rectangle x1="-5.8999" y1="9.8999" x2="-5.5999" y2="10.9499" layer="51"/>
<rectangle x1="-6.4" y1="9.8999" x2="-6.1001" y2="10.9499" layer="51"/>
<rectangle x1="-6.8999" y1="9.8999" x2="-6.5999" y2="10.9499" layer="51"/>
<rectangle x1="-7.4" y1="9.8999" x2="-7.1001" y2="10.9499" layer="51"/>
<rectangle x1="-7.8999" y1="9.8999" x2="-7.5999" y2="10.9499" layer="51"/>
<rectangle x1="-8.4" y1="9.8999" x2="-8.1001" y2="10.9499" layer="51"/>
<rectangle x1="-8.8999" y1="9.8999" x2="-8.5999" y2="10.9499" layer="51"/>
<polygon width="0.1" layer="51">
<vertex x="1.03" y="0.34"/>
<vertex x="0.95" y="0.37"/>
<vertex x="0.85" y="0.39"/>
<vertex x="0.79" y="0.39"/>
<vertex x="0.71" y="0.38"/>
<vertex x="0.66" y="0.22"/>
<vertex x="0.78" y="0.22"/>
<vertex x="0.72" y="-0.07"/>
<vertex x="0.61" y="-0.08"/>
<vertex x="0.51" y="-0.46"/>
<vertex x="0.59" y="-0.43"/>
<vertex x="1.09" y="-0.43"/>
<vertex x="1.13" y="-0.38"/>
<vertex x="1.14" y="-0.34"/>
<vertex x="1.14" y="-0.12"/>
<vertex x="1.11" y="-0.06"/>
<vertex x="1.07" y="-0.01"/>
<vertex x="1.04" y="0.04"/>
<vertex x="1.02" y="0.11"/>
<vertex x="1.02" y="0.16"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="0.61" y="-0.46"/>
<vertex x="0.61" y="-0.48"/>
<vertex x="0.59" y="-0.58"/>
<vertex x="0.58" y="-0.61"/>
<vertex x="0.57" y="-0.63"/>
<vertex x="0.46" y="-0.67"/>
<vertex x="0.42" y="-0.68"/>
<vertex x="0.36" y="-0.68"/>
<vertex x="0.29" y="-0.67"/>
<vertex x="0.23" y="-0.65"/>
<vertex x="0.18" y="-0.62"/>
<vertex x="0.15" y="-0.6"/>
<vertex x="0.11" y="-0.56"/>
<vertex x="0.09" y="-0.52"/>
<vertex x="0.08" y="-0.51"/>
<vertex x="0.06" y="-0.48"/>
<vertex x="0.06" y="-0.43"/>
<vertex x="-0.09" y="-0.43"/>
<vertex x="-0.09" y="-0.45"/>
<vertex x="-0.06" y="-0.48"/>
<vertex x="-0.02" y="-0.54"/>
<vertex x="0.03" y="-0.61"/>
<vertex x="0.11" y="-0.77"/>
<vertex x="0.19" y="-0.91"/>
<vertex x="0.25" y="-0.98"/>
<vertex x="0.31" y="-1.03"/>
<vertex x="0.37" y="-1.07"/>
<vertex x="0.44" y="-1.1"/>
<vertex x="0.49" y="-1.12"/>
<vertex x="0.55" y="-1.13"/>
<vertex x="0.59" y="-1.12"/>
<vertex x="0.62" y="-1.11"/>
<vertex x="0.59" y="-1.03"/>
<vertex x="0.58" y="-0.97"/>
<vertex x="0.58" y="-0.85"/>
<vertex x="0.6" y="-0.78"/>
<vertex x="0.64" y="-0.71"/>
<vertex x="0.7" y="-0.65"/>
<vertex x="0.82" y="-0.56"/>
<vertex x="0.95" y="-0.51"/>
<vertex x="1.08" y="-0.44"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="0.1" y="0.8"/>
<vertex x="-0.33" y="0.8"/>
<vertex x="-0.33" y="0.02"/>
<vertex x="-0.83" y="0.02"/>
<vertex x="-0.82" y="-0.03"/>
<vertex x="-0.79" y="-0.09"/>
<vertex x="-0.74" y="-0.15"/>
<vertex x="-0.65" y="-0.23"/>
<vertex x="-0.6" y="-0.29"/>
<vertex x="-0.56" y="-0.36"/>
<vertex x="-0.54" y="-0.39"/>
<vertex x="-0.52" y="-0.44"/>
<vertex x="-0.48" y="-0.48"/>
<vertex x="-0.42" y="-0.52"/>
<vertex x="-0.39" y="-0.53"/>
<vertex x="-0.34" y="-0.53"/>
<vertex x="-0.26" y="-0.46"/>
<vertex x="-0.2" y="-0.43"/>
<vertex x="-0.12" y="-0.43"/>
<vertex x="0.06" y="-0.38"/>
<vertex x="0.14" y="-0.07"/>
<vertex x="-0.04" y="-0.07"/>
<vertex x="-0.04" y="-0.03"/>
<vertex x="0.01" y="0.21"/>
<vertex x="0.2" y="0.21"/>
<vertex x="0.26" y="0.42"/>
<vertex x="0.14" y="0.48"/>
<vertex x="0.1" y="0.52"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="0.53" y="0.3"/>
<vertex x="0.39" y="0.3"/>
<vertex x="0.24" y="-0.38"/>
<vertex x="0.37" y="-0.38"/>
</polygon>
<smd name="GND" x="0" y="1.26" dx="6.4" dy="9" layer="1"/>
</package>
<package name="TO252-4">
<description>DPAK 5, center lead crop, TO252-4, http://www.diodes.com/datasheets/ap02001.pdf</description>
<wire x1="3.26" y1="-3.13" x2="-3.24" y2="-3.13" width="0.127" layer="51"/>
<wire x1="3.26" y1="-3.13" x2="3.26" y2="3.66" width="0.127" layer="51"/>
<wire x1="3.26" y1="3.66" x2="-2.21" y2="3.66" width="0.127" layer="51"/>
<wire x1="-2.21" y1="3.66" x2="-3.24" y2="3.67" width="0.127" layer="51"/>
<wire x1="-3.24" y1="3.67" x2="-3.24" y2="-3.13" width="0.127" layer="51"/>
<wire x1="-2.21" y1="3.66" x2="-2.21" y2="4" width="0.127" layer="51"/>
<wire x1="-2.21" y1="4" x2="-1.96" y2="4.35" width="0.127" layer="51"/>
<wire x1="2.29" y1="3.68" x2="2.29" y2="4.1" width="0.127" layer="51"/>
<wire x1="2.29" y1="4.1" x2="2.04" y2="4.35" width="0.127" layer="51"/>
<wire x1="2.04" y1="4.35" x2="-1.96" y2="4.35" width="0.127" layer="51"/>
<wire x1="-3" y1="3.03" x2="-3.48" y2="3.03" width="0.127" layer="21"/>
<wire x1="-3.48" y1="3.03" x2="-3.48" y2="-3.38" width="0.127" layer="21"/>
<wire x1="-3.48" y1="-3.38" x2="-3" y2="-3.38" width="0.127" layer="21"/>
<wire x1="2.98" y1="-3.38" x2="3.46" y2="-3.38" width="0.127" layer="21"/>
<wire x1="3.46" y1="-3.38" x2="3.46" y2="3.03" width="0.127" layer="21"/>
<wire x1="3.46" y1="3.03" x2="2.98" y2="3.03" width="0.127" layer="21"/>
<circle x="-2.57" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<circle x="-1.26" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<circle x="1.22" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<circle x="2.53" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<smd name="GND" x="0" y="1.64" dx="5.73" dy="6.17" layer="1" rot="R90"/>
<smd name="NC" x="1.27" y="-5.3" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="VOUT" x="2.54" y="-5.3" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="VIN" x="-2.54" y="-5.3" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="INHIBIT" x="-1.27" y="-5.3" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-3" y="3.7" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3" y="-3" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.92" y1="-5.8" x2="-2.22" y2="-3.1" layer="51"/>
<rectangle x1="-1.59" y1="-5.8" x2="-0.89" y2="-3.1" layer="51"/>
<rectangle x1="0.89" y1="-5.79" x2="1.59" y2="-3.09" layer="51"/>
<rectangle x1="2.22" y1="-5.79" x2="2.92" y2="-3.09" layer="51"/>
</package>
<package name="CRYSTAL-SMD-ABM3B">
<wire x1="-0.6" y1="1.6" x2="0.6" y2="1.6" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.3" x2="2.5" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.6" x2="-0.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="0.3" x2="-2.5" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-2" y="-1.2" dx="1.8" dy="1.2" layer="1"/>
<smd name="3" x="2" y="1.2" dx="1.8" dy="1.2" layer="1"/>
<smd name="4" x="-2" y="1.2" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="2" y="-1.2" dx="1.8" dy="1.2" layer="1"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SC-70-6">
<wire x1="1" y1="0.725" x2="1" y2="-0.725" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.725" x2="-1" y2="0.725" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.725" x2="1" y2="-0.725" width="0.127" layer="51"/>
<wire x1="-1" y1="0.725" x2="1" y2="0.725" width="0.127" layer="51"/>
<smd name="1" x="-0.65" y="-1.1" dx="0.4" dy="0.9" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="0.4" dy="0.9" layer="1"/>
<smd name="3" x="0.65" y="-1.1" dx="0.4" dy="0.9" layer="1"/>
<smd name="4" x="0.65" y="1.1" dx="0.4" dy="0.9" layer="1"/>
<smd name="6" x="-0.65" y="1.1" dx="0.4" dy="0.9" layer="1"/>
<smd name="5" x="0" y="1.1" dx="0.4" dy="0.9" layer="1"/>
<text x="-0.939" y="1.674" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.254" size="0.4064" layer="27">&gt;VALUE</text>
<circle x="-0.508" y="-0.254" radius="0.254" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="0.508" x2="-1.016" y2="-0.508" width="0.2032" layer="21"/>
</package>
<package name="QFN-24">
<wire x1="1.65" y1="-2" x2="2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.65" x2="2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2" x2="-2" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="2" y1="1.65" x2="2" y2="2" width="0.2032" layer="21"/>
<wire x1="2" y1="2" x2="1.65" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="2" x2="-2" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="0.508" x2="-1.016" y2="-0.889" width="0.0762" layer="51"/>
<wire x1="-1.016" y1="-0.889" x2="1.016" y2="-0.889" width="0.0762" layer="51"/>
<wire x1="1.143" y1="1.143" x2="0.635" y2="1.143" width="0.0762" layer="51" curve="-270"/>
<wire x1="1.143" y1="1.143" x2="1.0668" y2="0.9144" width="0.0762" layer="51"/>
<wire x1="1.143" y1="1.143" x2="1.397" y2="1.0414" width="0.0762" layer="51"/>
<wire x1="-0.7874" y1="-0.3048" x2="-0.7874" y2="0.0762" width="0.0762" layer="51" curve="-280.388858"/>
<wire x1="-0.7874" y1="0.2794" x2="-0.7874" y2="0.0762" width="0.0762" layer="51"/>
<wire x1="-0.7874" y1="0.0762" x2="-0.889" y2="-0.0254" width="0.0762" layer="51"/>
<wire x1="0.5334" y1="-1.1176" x2="0.508" y2="-0.635" width="0.0762" layer="51" curve="-248.760689"/>
<wire x1="0.381" y1="-1.016" x2="0.5334" y2="-1.1176" width="0.0762" layer="51"/>
<wire x1="0.5334" y1="-1.1176" x2="0.4826" y2="-1.2954" width="0.0762" layer="51"/>
<smd name="1" x="-2" y="1.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="2" x="-2" y="0.75" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="3" x="-2" y="0.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="4" x="-2" y="-0.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="5" x="-2" y="-0.75" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="6" x="-2" y="-1.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="7" x="-1.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-0.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="0.75" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="1.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="2" y="-1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="14" x="2" y="-0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="15" x="2" y="-0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="16" x="2" y="0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="17" x="2" y="0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="18" x="2" y="1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="19" x="1.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="20" x="0.75" y="2" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="21" x="0.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="22" x="-0.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="23" x="-0.75" y="2" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="24" x="-1.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<text x="-2.45" y="2.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-4.15" size="1.27" layer="27">&gt;VALUE</text>
<text x="1.1176" y="-1.1938" size="0.4064" layer="51">X</text>
<text x="-1.2192" y="0.6604" size="0.4064" layer="51">Y</text>
<text x="0" y="0.8636" size="0.4064" layer="51">Z</text>
<rectangle x1="-0.5" y1="-0.5" x2="0.5" y2="0.5" layer="1"/>
<rectangle x1="-0.5842" y1="-0.5842" x2="0.5842" y2="0.5842" layer="29"/>
</package>
<package name="XBEE-SMD">
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="21.25" width="0.2032" layer="51"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-9.75" y2="21.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="0.75" x2="-9.75" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="20.6" x2="-9.75" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="20.6" width="0.2032" layer="21"/>
<wire x1="9.75" y1="0.75" x2="9.75" y2="21.25" width="0.2032" layer="51"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="51"/>
<wire x1="9.75" y1="0.75" x2="12.25" y2="0.75" width="0.2032" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="51"/>
<wire x1="9.75" y1="0.75" x2="9.75" y2="1.3" width="0.2032" layer="21"/>
<wire x1="9.75" y1="0.75" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="1.3" width="0.2032" layer="21"/>
<wire x1="12.25" y1="20.6" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="9.75" y2="21.25" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="20.6" width="0.2032" layer="21"/>
<smd name="10" x="-9.5" y="2" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="9" x="-12.5" y="4" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="8" x="-9.5" y="6" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="7" x="-12.5" y="8" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="6" x="-9.5" y="10" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="5" x="-12.5" y="12" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="4" x="-9.5" y="14" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="3" x="-12.5" y="16" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="2" x="-9.5" y="18" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="1" x="-12.5" y="20" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="11" x="12.5" y="2" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="12" x="9.5" y="4" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="13" x="12.5" y="6" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="14" x="9.5" y="8" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="15" x="12.5" y="10" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="16" x="9.5" y="12" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="17" x="12.5" y="14" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="18" x="9.5" y="16" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="19" x="12.5" y="18" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<smd name="20" x="9.5" y="20" dx="0.85" dy="1.7" layer="1" rot="R90"/>
<text x="-8.89" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-8.89" y="1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="XBEE-1">
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<pad name="10" x="-11" y="2" drill="0.8" diameter="1.524"/>
<pad name="9" x="-11" y="4" drill="0.8" diameter="1.524"/>
<pad name="8" x="-11" y="6" drill="0.8" diameter="1.524"/>
<pad name="7" x="-11" y="8" drill="0.8" diameter="1.524"/>
<pad name="6" x="-11" y="10" drill="0.8" diameter="1.524"/>
<pad name="5" x="-11" y="12" drill="0.8" diameter="1.524"/>
<pad name="4" x="-11" y="14" drill="0.8" diameter="1.524"/>
<pad name="3" x="-11" y="16" drill="0.8" diameter="1.524"/>
<pad name="2" x="-11" y="18" drill="0.8" diameter="1.524"/>
<pad name="1" x="-11" y="20" drill="0.8" diameter="1.524"/>
<pad name="11" x="11" y="2" drill="0.8" diameter="1.524"/>
<pad name="12" x="11" y="4" drill="0.8" diameter="1.524"/>
<pad name="13" x="11" y="6" drill="0.8" diameter="1.524"/>
<pad name="14" x="11" y="8" drill="0.8" diameter="1.524"/>
<pad name="15" x="11" y="10" drill="0.8" diameter="1.524"/>
<pad name="16" x="11" y="12" drill="0.8" diameter="1.524"/>
<pad name="17" x="11" y="14" drill="0.8" diameter="1.524"/>
<pad name="18" x="11" y="16" drill="0.8" diameter="1.524"/>
<pad name="19" x="11" y="18" drill="0.8" diameter="1.524"/>
<pad name="20" x="11" y="20" drill="0.8" diameter="1.524"/>
<text x="-8.89" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-8.89" y="1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="XBEE-SILK">
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-5" y1="27.6" x2="-5.7" y2="27" width="0.2032" layer="21"/>
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="21"/>
<wire x1="5" y1="27.6" x2="5.7" y2="27" width="0.2032" layer="21"/>
<pad name="10" x="-11" y="2" drill="0.8" diameter="1.524"/>
<pad name="9" x="-11" y="4" drill="0.8" diameter="1.524"/>
<pad name="8" x="-11" y="6" drill="0.8" diameter="1.524"/>
<pad name="7" x="-11" y="8" drill="0.8" diameter="1.524"/>
<pad name="6" x="-11" y="10" drill="0.8" diameter="1.524"/>
<pad name="5" x="-11" y="12" drill="0.8" diameter="1.524"/>
<pad name="4" x="-11" y="14" drill="0.8" diameter="1.524"/>
<pad name="3" x="-11" y="16" drill="0.8" diameter="1.524"/>
<pad name="2" x="-11" y="18" drill="0.8" diameter="1.524"/>
<pad name="1" x="-11" y="20" drill="0.8" diameter="1.524"/>
<pad name="11" x="11" y="2" drill="0.8" diameter="1.524"/>
<pad name="12" x="11" y="4" drill="0.8" diameter="1.524"/>
<pad name="13" x="11" y="6" drill="0.8" diameter="1.524"/>
<pad name="14" x="11" y="8" drill="0.8" diameter="1.524"/>
<pad name="15" x="11" y="10" drill="0.8" diameter="1.524"/>
<pad name="16" x="11" y="12" drill="0.8" diameter="1.524"/>
<pad name="17" x="11" y="14" drill="0.8" diameter="1.524"/>
<pad name="18" x="11" y="16" drill="0.8" diameter="1.524"/>
<pad name="19" x="11" y="18" drill="0.8" diameter="1.524"/>
<pad name="20" x="11" y="20" drill="0.8" diameter="1.524"/>
<text x="-8.89" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-8.89" y="1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="XBEE-1_LOCK">
<wire x1="-5" y1="27.6" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="21.25" x2="-5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="12.25" y1="21.25" x2="5" y2="27.6" width="0.2032" layer="51"/>
<wire x1="9.75" y1="21.25" x2="12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="12.25" y1="21.25" x2="12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="12.25" y1="0.75" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="9.75" y1="21.25" x2="9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-12.25" y2="21.25" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="21.25" x2="-12.25" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-9.75" y1="21.25" x2="-9.75" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-12.25" y1="0.75" x2="-12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="12.25" y1="0.75" x2="12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="12.25" y2="0" width="0.2032" layer="51"/>
<wire x1="12.25" y1="0" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="0" x2="-12.25" y2="-6.25" width="0.2032" layer="51"/>
<wire x1="-12.25" y1="-6.25" x2="12.25" y2="-6.25" width="0.2032" layer="51"/>
<pad name="10" x="-10.8222" y="2" drill="0.7366" diameter="1.524"/>
<pad name="9" x="-11.1778" y="4" drill="0.7366" diameter="1.524"/>
<pad name="8" x="-10.8222" y="6" drill="0.7366" diameter="1.524"/>
<pad name="7" x="-11.1778" y="8" drill="0.7366" diameter="1.524"/>
<pad name="6" x="-10.8222" y="10" drill="0.7366" diameter="1.524"/>
<pad name="5" x="-11.1778" y="12" drill="0.7366" diameter="1.524"/>
<pad name="4" x="-10.8222" y="14" drill="0.7366" diameter="1.524"/>
<pad name="3" x="-11.1778" y="16" drill="0.7366" diameter="1.524"/>
<pad name="2" x="-10.8222" y="18" drill="0.7366" diameter="1.524"/>
<pad name="1" x="-11.1778" y="20" drill="0.7366" diameter="1.524"/>
<pad name="11" x="11.1778" y="2" drill="0.7366" diameter="1.524"/>
<pad name="12" x="10.8222" y="4" drill="0.7366" diameter="1.524"/>
<pad name="13" x="11.1778" y="6" drill="0.7366" diameter="1.524"/>
<pad name="14" x="10.8222" y="8" drill="0.7366" diameter="1.524"/>
<pad name="15" x="11.1778" y="10" drill="0.7366" diameter="1.524"/>
<pad name="16" x="10.8222" y="12" drill="0.7366" diameter="1.524"/>
<pad name="17" x="11.1778" y="14" drill="0.7366" diameter="1.524"/>
<pad name="18" x="10.8222" y="16" drill="0.7366" diameter="1.524"/>
<pad name="19" x="11.1778" y="18" drill="0.7366" diameter="1.524"/>
<pad name="20" x="10.8222" y="20" drill="0.7366" diameter="1.524"/>
<text x="-8.89" y="2.54" size="0.4064" layer="25">&gt;Name</text>
<text x="-8.89" y="1.27" size="0.4064" layer="27">&gt;Value</text>
<rectangle x1="-11.0998" y1="1.7272" x2="-10.8966" y2="2.2352" layer="51"/>
<rectangle x1="-11.0998" y1="3.7338" x2="-10.8966" y2="4.2418" layer="51"/>
<rectangle x1="-11.0998" y1="5.715" x2="-10.8966" y2="6.223" layer="51"/>
<rectangle x1="-11.0998" y1="7.7216" x2="-10.8966" y2="8.2296" layer="51"/>
<rectangle x1="-11.0998" y1="9.7282" x2="-10.8966" y2="10.2362" layer="51"/>
<rectangle x1="-11.0998" y1="11.7602" x2="-10.8966" y2="12.2682" layer="51"/>
<rectangle x1="-11.0998" y1="13.7414" x2="-10.8966" y2="14.2494" layer="51"/>
<rectangle x1="-11.0998" y1="15.7226" x2="-10.8966" y2="16.2306" layer="51"/>
<rectangle x1="-11.0998" y1="17.7292" x2="-10.8966" y2="18.2372" layer="51"/>
<rectangle x1="-11.0998" y1="19.7358" x2="-10.8966" y2="20.2438" layer="51"/>
<rectangle x1="10.8966" y1="1.7272" x2="11.0998" y2="2.2352" layer="51"/>
<rectangle x1="10.8966" y1="3.7338" x2="11.0998" y2="4.2418" layer="51"/>
<rectangle x1="10.8966" y1="5.715" x2="11.0998" y2="6.223" layer="51"/>
<rectangle x1="10.8966" y1="7.7216" x2="11.0998" y2="8.2296" layer="51"/>
<rectangle x1="10.8966" y1="9.7282" x2="11.0998" y2="10.2362" layer="51"/>
<rectangle x1="10.8966" y1="11.7602" x2="11.0998" y2="12.2682" layer="51"/>
<rectangle x1="10.8966" y1="13.7414" x2="11.0998" y2="14.2494" layer="51"/>
<rectangle x1="10.8966" y1="15.7226" x2="11.0998" y2="16.2306" layer="51"/>
<rectangle x1="10.8966" y1="17.7292" x2="11.0998" y2="18.2372" layer="51"/>
<rectangle x1="10.8966" y1="19.7358" x2="11.0998" y2="20.2438" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ACL-LOGO">
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="BATT">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<pin name="+" x="-10.16" y="2.54" length="middle"/>
<pin name="-" x="-10.16" y="-2.54" length="middle"/>
<text x="-5.08" y="5.842" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="BMP180">
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="CSB" x="-15.24" y="2.54" length="middle"/>
<pin name="VDD" x="-15.24" y="0" length="middle"/>
<pin name="VDDIO" x="-15.24" y="-2.54" length="middle"/>
<pin name="SDO" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="SCL" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="0" y="10.16" length="middle" rot="R270"/>
<text x="-10.16" y="5.588" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="A3L-LOC">
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="98.425" y1="0" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="0" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="193.675" y1="0" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="241.3" y1="0" x2="288.925" y2="0" width="0.1016" layer="94"/>
<wire x1="288.925" y1="0" x2="336.55" y2="0" width="0.1016" layer="94"/>
<wire x1="336.55" y1="0" x2="387.35" y2="0" width="0.1016" layer="94"/>
<wire x1="387.35" y1="0" x2="387.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="387.35" y1="53.975" x2="387.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="387.35" y1="104.775" x2="387.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="387.35" y1="155.575" x2="387.35" y2="206.375" width="0.1016" layer="94"/>
<wire x1="387.35" y1="206.375" x2="387.35" y2="260.35" width="0.1016" layer="94"/>
<wire x1="146.05" y1="260.35" x2="98.425" y2="260.35" width="0.1016" layer="94"/>
<wire x1="98.425" y1="260.35" x2="50.8" y2="260.35" width="0.1016" layer="94"/>
<wire x1="50.8" y1="260.35" x2="0" y2="260.35" width="0.1016" layer="94"/>
<wire x1="0" y1="260.35" x2="0" y2="206.375" width="0.1016" layer="94"/>
<wire x1="0" y1="206.375" x2="0" y2="155.575" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="0" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="0" y2="53.975" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="3.175" y1="3.175" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="50.8" y1="3.175" x2="98.425" y2="3.175" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="146.05" y2="3.175" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.175" x2="193.675" y2="3.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.175" x2="241.3" y2="3.175" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.175" x2="288.925" y2="3.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.175" x2="288.925" y2="3.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.175" x2="384.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="384.175" y1="3.175" x2="384.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="384.175" y1="53.975" x2="384.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="384.175" y1="104.775" x2="384.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="384.175" y1="155.575" x2="384.175" y2="206.375" width="0.1016" layer="94"/>
<wire x1="384.175" y1="206.375" x2="384.175" y2="257.175" width="0.1016" layer="94"/>
<wire x1="384.175" y1="257.175" x2="336.55" y2="257.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="257.175" x2="288.925" y2="257.175" width="0.1016" layer="94"/>
<wire x1="288.925" y1="257.175" x2="241.3" y2="257.175" width="0.1016" layer="94"/>
<wire x1="241.3" y1="257.175" x2="193.675" y2="257.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="257.175" x2="146.05" y2="257.175" width="0.1016" layer="94"/>
<wire x1="146.05" y1="257.175" x2="98.425" y2="257.175" width="0.1016" layer="94"/>
<wire x1="98.425" y1="257.175" x2="50.8" y2="257.175" width="0.1016" layer="94"/>
<wire x1="50.8" y1="257.175" x2="3.175" y2="257.175" width="0.1016" layer="94"/>
<wire x1="3.175" y1="257.175" x2="3.175" y2="206.375" width="0.1016" layer="94"/>
<wire x1="3.175" y1="206.375" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="3.175" y1="155.575" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="3.175" y1="104.775" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="3.175" y1="53.975" x2="3.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="387.35" y1="260.35" x2="336.55" y2="260.35" width="0.1016" layer="94"/>
<wire x1="336.55" y1="260.35" x2="288.925" y2="260.35" width="0.1016" layer="94"/>
<wire x1="288.925" y1="260.35" x2="241.3" y2="260.35" width="0.1016" layer="94"/>
<wire x1="241.3" y1="260.35" x2="193.675" y2="260.35" width="0.1016" layer="94"/>
<wire x1="193.675" y1="260.35" x2="146.05" y2="260.35" width="0.1016" layer="94"/>
<wire x1="193.675" y1="260.35" x2="193.675" y2="257.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.175" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="384.175" y1="155.575" x2="387.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="98.425" y1="257.175" x2="98.425" y2="260.35" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="288.925" y1="260.35" x2="288.925" y2="257.175" width="0.1016" layer="94"/>
<wire x1="288.925" y1="3.175" x2="288.925" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="384.175" y1="104.775" x2="387.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="384.175" y1="206.375" x2="387.35" y2="206.375" width="0.1016" layer="94"/>
<wire x1="50.8" y1="257.175" x2="50.8" y2="260.35" width="0.1016" layer="94"/>
<wire x1="0" y1="206.375" x2="3.175" y2="206.375" width="0.1016" layer="94"/>
<wire x1="384.175" y1="53.975" x2="387.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="146.05" y1="257.175" x2="146.05" y2="260.35" width="0.1016" layer="94"/>
<wire x1="241.3" y1="260.35" x2="241.3" y2="257.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="260.35" x2="336.55" y2="257.175" width="0.1016" layer="94"/>
<wire x1="336.55" y1="3.175" x2="336.55" y2="0" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.175" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.175" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<text x="24.384" y="0.254" size="2.54" layer="94" font="vector">A</text>
<text x="74.422" y="0.254" size="2.54" layer="94" font="vector">B</text>
<text x="121.158" y="0.254" size="2.54" layer="94" font="vector">C</text>
<text x="169.418" y="0.254" size="2.54" layer="94" font="vector">D</text>
<text x="216.916" y="0.254" size="2.54" layer="94" font="vector">E</text>
<text x="263.652" y="0.254" size="2.54" layer="94" font="vector">F</text>
<text x="310.642" y="0.254" size="2.54" layer="94" font="vector">G</text>
<text x="360.934" y="0.254" size="2.54" layer="94" font="vector">H</text>
<text x="385.064" y="28.702" size="2.54" layer="94" font="vector">1</text>
<text x="384.81" y="79.502" size="2.54" layer="94" font="vector">2</text>
<text x="384.81" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="384.81" y="181.864" size="2.54" layer="94" font="vector">4</text>
<text x="384.81" y="231.14" size="2.54" layer="94" font="vector">5</text>
<text x="361.188" y="257.556" size="2.54" layer="94" font="vector">H</text>
<text x="311.404" y="257.556" size="2.54" layer="94" font="vector">G</text>
<text x="262.89" y="257.556" size="2.54" layer="94" font="vector">F</text>
<text x="215.9" y="257.556" size="2.54" layer="94" font="vector">E</text>
<text x="168.148" y="257.556" size="2.54" layer="94" font="vector">D</text>
<text x="120.904" y="257.556" size="2.54" layer="94" font="vector">C</text>
<text x="72.898" y="257.556" size="2.54" layer="94" font="vector">B</text>
<text x="24.384" y="257.556" size="2.54" layer="94" font="vector">A</text>
<text x="0.762" y="231.14" size="2.54" layer="94" font="vector">5</text>
<text x="0.762" y="181.61" size="2.54" layer="94" font="vector">4</text>
<text x="0.762" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="0.762" y="79.248" size="2.54" layer="94" font="vector">2</text>
<text x="1.016" y="26.67" size="2.54" layer="94" font="vector">1</text>
<wire x1="282.448" y1="3.302" x2="353.568" y2="3.302" width="0.254" layer="94"/>
<wire x1="384.048" y1="18.542" x2="370.078" y2="18.542" width="0.254" layer="94"/>
<wire x1="282.448" y1="3.302" x2="282.448" y2="8.382" width="0.254" layer="94"/>
<wire x1="282.448" y1="8.382" x2="353.568" y2="8.382" width="0.254" layer="94"/>
<wire x1="282.448" y1="8.382" x2="282.448" y2="18.542" width="0.254" layer="94"/>
<wire x1="384.048" y1="18.542" x2="384.048" y2="8.382" width="0.254" layer="94"/>
<wire x1="353.568" y1="8.382" x2="353.568" y2="3.302" width="0.254" layer="94"/>
<wire x1="353.568" y1="8.382" x2="370.078" y2="8.382" width="0.254" layer="94"/>
<wire x1="353.568" y1="3.302" x2="384.048" y2="3.302" width="0.254" layer="94"/>
<wire x1="370.078" y1="18.542" x2="370.078" y2="8.382" width="0.254" layer="94"/>
<wire x1="370.078" y1="18.542" x2="282.448" y2="18.542" width="0.254" layer="94"/>
<wire x1="370.078" y1="8.382" x2="384.048" y2="8.382" width="0.254" layer="94"/>
<wire x1="384.048" y1="8.382" x2="384.048" y2="3.302" width="0.254" layer="94"/>
<wire x1="282.448" y1="18.542" x2="282.448" y2="26.162" width="0.254" layer="94"/>
<wire x1="384.048" y1="38.862" x2="282.448" y2="38.862" width="0.254" layer="94"/>
<wire x1="384.048" y1="38.862" x2="384.048" y2="26.162" width="0.254" layer="94"/>
<wire x1="282.448" y1="26.162" x2="384.048" y2="26.162" width="0.254" layer="94"/>
<wire x1="282.448" y1="26.162" x2="282.448" y2="38.862" width="0.254" layer="94"/>
<wire x1="384.048" y1="26.162" x2="384.048" y2="18.542" width="0.254" layer="94"/>
<text x="283.718" y="4.572" size="2.54" layer="94" font="vector">Date:</text>
<text x="295.148" y="4.572" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="354.838" y="4.572" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="368.808" y="4.572" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="371.348" y="14.732" size="2.54" layer="94" font="vector">REV:</text>
<text x="283.972" y="21.082" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="297.942" y="21.082" size="2.7432" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="284.988" y="35.052" size="1.9304" layer="94">Developed by the Aerospace Controls Lab</text>
<text x="284.988" y="31.242" size="1.9304" layer="94">Massachusetts Institute of Technology</text>
<text x="284.988" y="27.432" size="1.9304" layer="94">http://acl.mit.edu</text>
<text x="283.718" y="14.732" size="2.54" layer="94">Design by:</text>
</symbol>
<symbol name="TMS320C32">
<wire x1="-53.34" y1="63.5" x2="-53.34" y2="-45.72" width="0.254" layer="94"/>
<wire x1="-53.34" y1="-45.72" x2="55.88" y2="-45.72" width="0.254" layer="94"/>
<wire x1="55.88" y1="-45.72" x2="55.88" y2="63.5" width="0.254" layer="94"/>
<wire x1="55.88" y1="63.5" x2="-53.34" y2="63.5" width="0.254" layer="94"/>
<text x="-5.08" y="15.24" size="1.778" layer="95">F28M35H32B</text>
<pin name="VDD18" x="-58.42" y="53.34" length="middle"/>
<pin name="VDDIO" x="-58.42" y="50.8" length="middle"/>
<pin name="VDDIO_2" x="-58.42" y="48.26" length="middle"/>
<pin name="XRS" x="-58.42" y="45.72" length="middle"/>
<pin name="PA0_GPIO0" x="-58.42" y="43.18" length="middle"/>
<pin name="PA1_GPIO1" x="-58.42" y="40.64" length="middle"/>
<pin name="PA2_GPIO2" x="-58.42" y="38.1" length="middle"/>
<pin name="PA3_GPIO3" x="-58.42" y="35.56" length="middle"/>
<pin name="PA4_GPIO4" x="-58.42" y="33.02" length="middle"/>
<pin name="VDDIO_3" x="-58.42" y="30.48" length="middle"/>
<pin name="VDD12" x="-58.42" y="27.94" length="middle"/>
<pin name="PA5_GPIO5" x="-58.42" y="25.4" length="middle"/>
<pin name="PA6_GPIO6" x="-58.42" y="22.86" length="middle"/>
<pin name="PA7_GPIO7" x="-58.42" y="20.32" length="middle"/>
<pin name="PB0_GPIO8" x="-58.42" y="17.78" length="middle"/>
<pin name="FLT1" x="-58.42" y="15.24" length="middle"/>
<pin name="VDDIO_4" x="-58.42" y="12.7" length="middle"/>
<pin name="PB1_GPIO9" x="-58.42" y="10.16" length="middle"/>
<pin name="PB2_GPIO10" x="-58.42" y="7.62" length="middle"/>
<pin name="PB3_GPIO11" x="-58.42" y="5.08" length="middle"/>
<pin name="FLT2" x="-58.42" y="2.54" length="middle"/>
<pin name="PE6_GPIO30" x="-58.42" y="0" length="middle"/>
<pin name="PE7_GPIO31" x="-58.42" y="-2.54" length="middle"/>
<pin name="VDD12_2" x="-58.42" y="-5.08" length="middle"/>
<pin name="VDDIO_5" x="-58.42" y="-7.62" length="middle"/>
<pin name="PB6_GPIO14" x="-58.42" y="-10.16" length="middle"/>
<pin name="PB7_GPIO15" x="-58.42" y="-12.7" length="middle"/>
<pin name="PD2_GPIO18" x="-58.42" y="-15.24" length="middle"/>
<pin name="PD3_GPIO19" x="-58.42" y="-17.78" length="middle"/>
<pin name="PB4_GPIO12" x="-58.42" y="-20.32" length="middle"/>
<pin name="PB5_GPIO13" x="-58.42" y="-22.86" length="middle"/>
<pin name="PE2_GPIO26" x="-58.42" y="-25.4" length="middle"/>
<pin name="PE3_GPIO27" x="-58.42" y="-27.94" length="middle"/>
<pin name="VDDIO_6" x="-58.42" y="-30.48" length="middle"/>
<pin name="PH3_GPIO51" x="-58.42" y="-33.02" length="middle"/>
<pin name="PH2_GPIO50" x="-58.42" y="-35.56" length="middle"/>
<pin name="PC4_GPIO68" x="-43.18" y="-50.8" length="middle" rot="R90"/>
<pin name="PC5_GPIO69" x="-40.64" y="-50.8" length="middle" rot="R90"/>
<pin name="PC6_GPIO70" x="-38.1" y="-50.8" length="middle" rot="R90"/>
<pin name="PC7_GPIO71" x="-35.56" y="-50.8" length="middle" rot="R90"/>
<pin name="PH0_GPIO48" x="-33.02" y="-50.8" length="middle" rot="R90"/>
<pin name="PH1_GPIO49" x="-30.48" y="-50.8" length="middle" rot="R90"/>
<pin name="PE0_GPIO24" x="-27.94" y="-50.8" length="middle" rot="R90"/>
<pin name="VDDIO_7" x="-25.4" y="-50.8" length="middle" rot="R90"/>
<pin name="PE1_GPIO25" x="-22.86" y="-50.8" length="middle" rot="R90"/>
<pin name="PH4_GPIO52" x="-20.32" y="-50.8" length="middle" rot="R90"/>
<pin name="PH5_GPIO53" x="-17.78" y="-50.8" length="middle" rot="R90"/>
<pin name="PF4_GPIO36" x="-15.24" y="-50.8" length="middle" rot="R90"/>
<pin name="PG0_GPIO40" x="-12.7" y="-50.8" length="middle" rot="R90"/>
<pin name="PG1_GPIO41" x="-10.16" y="-50.8" length="middle" rot="R90"/>
<pin name="PF5_GPIO37" x="-7.62" y="-50.8" length="middle" rot="R90"/>
<pin name="PG7_GPIO47" x="-5.08" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ6_GPIO62" x="-2.54" y="-50.8" length="middle" rot="R90"/>
<pin name="VDDIO_8" x="0" y="-50.8" length="middle" rot="R90"/>
<pin name="VDD12_3" x="2.54" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ5_GPIO61" x="5.08" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ4_GPIO60" x="7.62" y="-50.8" length="middle" rot="R90"/>
<pin name="VDD12_4" x="10.16" y="-50.8" length="middle" rot="R90"/>
<pin name="VDDIO_9" x="12.7" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ3_GPIO59" x="15.24" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ2_GPIO58" x="17.78" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ1_GPIO57" x="20.32" y="-50.8" length="middle" rot="R90"/>
<pin name="PJ0_GPIO56" x="22.86" y="-50.8" length="middle" rot="R90"/>
<pin name="PD5_GPIO21" x="25.4" y="-50.8" length="middle" rot="R90"/>
<pin name="PD4_GPIO20" x="27.94" y="-50.8" length="middle" rot="R90"/>
<pin name="VDD12_5" x="30.48" y="-50.8" length="middle" rot="R90"/>
<pin name="VDDIO_10" x="33.02" y="-50.8" length="middle" rot="R90"/>
<pin name="PD7_GPIO23" x="35.56" y="-50.8" length="middle" rot="R90"/>
<pin name="PF6_GPIO38" x="38.1" y="-50.8" length="middle" rot="R90"/>
<pin name="PG6_GPIO46" x="40.64" y="-50.8" length="middle" rot="R90"/>
<pin name="PG2_GPIO42" x="43.18" y="-50.8" length="middle" rot="R90"/>
<pin name="PG5_GPIO45" x="45.72" y="-50.8" length="middle" rot="R90"/>
<pin name="PD6_GPIO22" x="60.96" y="-35.56" length="middle" rot="R180"/>
<pin name="VDDIO_11" x="60.96" y="-33.02" length="middle" rot="R180"/>
<pin name="VDD12_6" x="60.96" y="-30.48" length="middle" rot="R180"/>
<pin name="PE5_GPIO29" x="60.96" y="-27.94" length="middle" rot="R180"/>
<pin name="PE4_GPIO28" x="60.96" y="-25.4" length="middle" rot="R180"/>
<pin name="PG3_GPIO43" x="60.96" y="-22.86" length="middle" rot="R180"/>
<pin name="PH6_GPIO54" x="60.96" y="-20.32" length="middle" rot="R180"/>
<pin name="PH7_GPIO55" x="60.96" y="-17.78" length="middle" rot="R180"/>
<pin name="PF3_GPIO35" x="60.96" y="-15.24" length="middle" rot="R180"/>
<pin name="PF2_GPIO34" x="60.96" y="-12.7" length="middle" rot="R180"/>
<pin name="EMU0" x="60.96" y="-10.16" length="middle" rot="R180"/>
<pin name="TDO" x="60.96" y="-7.62" length="middle" rot="R180"/>
<pin name="TRST" x="60.96" y="-5.08" length="middle" rot="R180"/>
<pin name="EMU1" x="60.96" y="-2.54" length="middle" rot="R180"/>
<pin name="TMS" x="60.96" y="0" length="middle" rot="R180"/>
<pin name="TDI" x="60.96" y="2.54" length="middle" rot="R180"/>
<pin name="TCK" x="60.96" y="5.08" length="middle" rot="R180"/>
<pin name="VDD12_7" x="60.96" y="7.62" length="middle" rot="R180"/>
<pin name="NC" x="60.96" y="10.16" length="middle" rot="R180"/>
<pin name="VDDIO_12" x="60.96" y="12.7" length="middle" rot="R180"/>
<pin name="X1" x="60.96" y="15.24" length="middle" rot="R180"/>
<pin name="VSSOSC" x="60.96" y="17.78" length="middle" rot="R180"/>
<pin name="X2" x="60.96" y="20.32" length="middle" rot="R180"/>
<pin name="VDDIO_13" x="60.96" y="22.86" length="middle" rot="R180"/>
<pin name="PJ7_GPIO63" x="60.96" y="25.4" length="middle" rot="R180"/>
<pin name="PD1_GPIO17" x="60.96" y="27.94" length="middle" rot="R180"/>
<pin name="VDD12_8" x="60.96" y="30.48" length="middle" rot="R180"/>
<pin name="VDDIO_14" x="60.96" y="33.02" length="middle" rot="R180"/>
<pin name="VREG12EN" x="60.96" y="35.56" length="middle" rot="R180"/>
<pin name="PD0_GPIO16" x="60.96" y="38.1" length="middle" rot="R180"/>
<pin name="PF1_GPIO33" x="60.96" y="40.64" length="middle" rot="R180"/>
<pin name="PF0_GPIO32" x="60.96" y="43.18" length="middle" rot="R180"/>
<pin name="VDDIO_15" x="60.96" y="45.72" length="middle" rot="R180"/>
<pin name="VDDIO_16" x="60.96" y="48.26" length="middle" rot="R180"/>
<pin name="VDDIO_17" x="60.96" y="50.8" length="middle" rot="R180"/>
<pin name="VDD18_2" x="60.96" y="53.34" length="middle" rot="R180"/>
<pin name="GPIO135/COMP5OUT" x="45.72" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO134" x="43.18" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO133/COMP4OUT" x="40.64" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO132/COMP3OUT" x="38.1" y="68.58" length="middle" rot="R270"/>
<pin name="VREG18EN" x="35.56" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INB7" x="33.02" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INB4" x="30.48" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INB3" x="27.94" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INB0" x="25.4" y="68.58" length="middle" rot="R270"/>
<pin name="VSSA1" x="22.86" y="68.58" length="middle" rot="R270"/>
<pin name="VDDA1" x="20.32" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1VREFHI" x="17.78" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INA0" x="15.24" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INA2" x="12.7" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INA3" x="10.16" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INA4" x="7.62" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INA6" x="5.08" y="68.58" length="middle" rot="R270"/>
<pin name="ADC1INA7" x="2.54" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INA7" x="0" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INA6" x="-2.54" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INA4" x="-5.08" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INA3" x="-7.62" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INA2" x="-10.16" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INA0" x="-12.7" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2VREFHI" x="-15.24" y="68.58" length="middle" rot="R270"/>
<pin name="VDDA2" x="-17.78" y="68.58" length="middle" rot="R270"/>
<pin name="VSSA2" x="-20.32" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INB0" x="-22.86" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INB3" x="-25.4" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INB4" x="-27.94" y="68.58" length="middle" rot="R270"/>
<pin name="ADC2INB7" x="-30.48" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO128" x="-33.02" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO129/COMP1OUT" x="-35.56" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO130/COMP6OUT" x="-38.1" y="68.58" length="middle" rot="R270"/>
<pin name="GPIO131/COMP2OUT" x="-40.64" y="68.58" length="middle" rot="R270"/>
<pin name="ARS" x="-43.18" y="68.58" length="middle" rot="R270"/>
<pin name="PP_GND" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="V_REG_4_PIN">
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-6.731" y="1.905" size="1.524" layer="95">IN</text>
<text x="3.175" y="1.905" size="1.524" layer="95">OUT</text>
<text x="-1.905" y="-6.731" size="1.524" layer="95">GND</text>
<text x="-6.731" y="-5.207" size="1.524" layer="95">INHIB</text>
<pin name="IN" x="-10.16" y="2.54" visible="off" length="short" direction="in"/>
<pin name="OUT" x="10.16" y="2.54" visible="off" length="short" direction="out" rot="R180"/>
<pin name="GND" x="0" y="-10.16" visible="off" length="short" rot="R90"/>
<pin name="INHIB" x="-10.16" y="-5.08" visible="off" length="short"/>
</symbol>
<symbol name="CRYSTAL">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<pin name="X1" x="-10.16" y="2.54" length="middle"/>
<pin name="X2" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="GND1" x="-2.54" y="-12.7" length="middle" rot="R90"/>
<pin name="GND2" x="2.54" y="-12.7" length="middle" rot="R90"/>
<text x="-5.08" y="6.096" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="BUFFER-DUAL">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="1.016" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="4.064" x2="-1.524" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="5.08" x2="-1.524" y2="6.096" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="6.096" x2="0.254" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0.254" y1="5.08" x2="-1.524" y2="4.064" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="5.08" x2="-2.286" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="5.08" x2="0.762" y2="5.08" width="0.1524" layer="94"/>
<text x="-7.62" y="-10.16" size="1.778" layer="95">&gt;Name</text>
<text x="-7.62" y="7.62" size="1.778" layer="96">&gt;Value</text>
<pin name="A2" x="-10.16" y="5.08" length="short"/>
<pin name="A1" x="-10.16" y="0" length="short"/>
<pin name="GND" x="-10.16" y="-5.08" length="short"/>
<pin name="Y1" x="10.16" y="-5.08" length="short" rot="R180"/>
<pin name="VCC" x="10.16" y="5.08" length="short" rot="R180"/>
<pin name="Y2" x="10.16" y="0" length="short" rot="R180"/>
</symbol>
<symbol name="ITG-3200">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="10.922" size="1.778" layer="95">&gt;Name</text>
<text x="-10.16" y="-12.446" size="1.778" layer="96">&gt;Value</text>
<pin name="CLKIN" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="VLOGIC" x="-12.7" y="2.54" length="short"/>
<pin name="AD0" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="REGOUT" x="-12.7" y="0" length="short"/>
<pin name="RESV-G" x="-12.7" y="-5.08" length="short"/>
<pin name="INT" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="SDA" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="SCL" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="CPOUT" x="-12.7" y="-2.54" length="short"/>
<pin name="GND" x="-12.7" y="-7.62" length="short"/>
<pin name="VDD" x="-12.7" y="7.62" length="short"/>
</symbol>
<symbol name="XBEE-1">
<wire x1="-15.24" y1="12.7" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<text x="-15.24" y="13.462" size="1.6764" layer="95">&gt;Name</text>
<text x="-15.24" y="-17.78" size="1.6764" layer="96">&gt;Value</text>
<pin name="VDD" x="-20.32" y="10.16" length="middle" direction="pwr"/>
<pin name="DOUT" x="-20.32" y="7.62" length="middle" direction="out"/>
<pin name="DIN" x="-20.32" y="5.08" length="middle" direction="in"/>
<pin name="DIO12" x="-20.32" y="2.54" length="middle"/>
<pin name="RESET" x="-20.32" y="0" length="middle" direction="in" function="dot"/>
<pin name="RSSI" x="-20.32" y="-2.54" length="middle"/>
<pin name="DIO11" x="-20.32" y="-5.08" length="middle"/>
<pin name="RES@8" x="-20.32" y="-7.62" length="middle"/>
<pin name="DTR" x="-20.32" y="-10.16" length="middle" function="dot"/>
<pin name="GND" x="-20.32" y="-12.7" length="middle" direction="pwr"/>
<pin name="DIO4" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="CTS" x="20.32" y="-10.16" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="DIO9" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="RES@14" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="DIO5" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="RTS" x="20.32" y="0" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="DIO3" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="DIO2" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="DIO1" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="DIO0" x="20.32" y="10.16" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ACL-LOGO">
<gates>
<gate name="G$1" symbol="ACL-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="LARGE" package="ACL-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="ACL-LOGO-BOTTOM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATT">
<description>Battery connection for ACL power distribution boards</description>
<gates>
<gate name="G$1" symbol="BATT" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="BATT">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="ESC">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="MINIQUADBATT">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BMP180">
<gates>
<gate name="G$1" symbol="BMP180" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="7-PIN-LGA">
<connects>
<connect gate="G$1" pin="CSB" pad="1"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="SDO" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VDDIO" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FRAME-A3" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame&lt;/b&gt;&lt;p&gt;
A3 Larger Frame</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="F28M35H52C" uservalue="yes">
<gates>
<gate name="G$1" symbol="TMS320C32" x="0" y="-7.62"/>
</gates>
<devices>
<device name="" package="PQFP144">
<connects>
<connect gate="G$1" pin="ADC1INA0" pad="121"/>
<connect gate="G$1" pin="ADC1INA2" pad="122"/>
<connect gate="G$1" pin="ADC1INA3" pad="123"/>
<connect gate="G$1" pin="ADC1INA4" pad="124"/>
<connect gate="G$1" pin="ADC1INA6" pad="125"/>
<connect gate="G$1" pin="ADC1INA7" pad="126"/>
<connect gate="G$1" pin="ADC1INB0" pad="117"/>
<connect gate="G$1" pin="ADC1INB3" pad="116"/>
<connect gate="G$1" pin="ADC1INB4" pad="115"/>
<connect gate="G$1" pin="ADC1INB7" pad="114"/>
<connect gate="G$1" pin="ADC1VREFHI" pad="120"/>
<connect gate="G$1" pin="ADC2INA0" pad="132"/>
<connect gate="G$1" pin="ADC2INA2" pad="131"/>
<connect gate="G$1" pin="ADC2INA3" pad="130"/>
<connect gate="G$1" pin="ADC2INA4" pad="129"/>
<connect gate="G$1" pin="ADC2INA6" pad="128"/>
<connect gate="G$1" pin="ADC2INA7" pad="127"/>
<connect gate="G$1" pin="ADC2INB0" pad="136"/>
<connect gate="G$1" pin="ADC2INB3" pad="137"/>
<connect gate="G$1" pin="ADC2INB4" pad="138"/>
<connect gate="G$1" pin="ADC2INB7" pad="139"/>
<connect gate="G$1" pin="ADC2VREFHI" pad="133"/>
<connect gate="G$1" pin="ARS" pad="144"/>
<connect gate="G$1" pin="EMU0" pad="83"/>
<connect gate="G$1" pin="EMU1" pad="86"/>
<connect gate="G$1" pin="FLT1" pad="16"/>
<connect gate="G$1" pin="FLT2" pad="21"/>
<connect gate="G$1" pin="GPIO128" pad="140"/>
<connect gate="G$1" pin="GPIO129/COMP1OUT" pad="141"/>
<connect gate="G$1" pin="GPIO130/COMP6OUT" pad="142"/>
<connect gate="G$1" pin="GPIO131/COMP2OUT" pad="143"/>
<connect gate="G$1" pin="GPIO132/COMP3OUT" pad="112"/>
<connect gate="G$1" pin="GPIO133/COMP4OUT" pad="111"/>
<connect gate="G$1" pin="GPIO134" pad="110"/>
<connect gate="G$1" pin="GPIO135/COMP5OUT" pad="109"/>
<connect gate="G$1" pin="NC" pad="91"/>
<connect gate="G$1" pin="PA0_GPIO0" pad="5"/>
<connect gate="G$1" pin="PA1_GPIO1" pad="6"/>
<connect gate="G$1" pin="PA2_GPIO2" pad="7"/>
<connect gate="G$1" pin="PA3_GPIO3" pad="8"/>
<connect gate="G$1" pin="PA4_GPIO4" pad="9"/>
<connect gate="G$1" pin="PA5_GPIO5" pad="12"/>
<connect gate="G$1" pin="PA6_GPIO6" pad="13"/>
<connect gate="G$1" pin="PA7_GPIO7" pad="14"/>
<connect gate="G$1" pin="PB0_GPIO8" pad="15"/>
<connect gate="G$1" pin="PB1_GPIO9" pad="18"/>
<connect gate="G$1" pin="PB2_GPIO10" pad="19"/>
<connect gate="G$1" pin="PB3_GPIO11" pad="20"/>
<connect gate="G$1" pin="PB4_GPIO12" pad="30"/>
<connect gate="G$1" pin="PB5_GPIO13" pad="31"/>
<connect gate="G$1" pin="PB6_GPIO14" pad="26"/>
<connect gate="G$1" pin="PB7_GPIO15" pad="27"/>
<connect gate="G$1" pin="PC4_GPIO68" pad="37"/>
<connect gate="G$1" pin="PC5_GPIO69" pad="38"/>
<connect gate="G$1" pin="PC6_GPIO70" pad="39"/>
<connect gate="G$1" pin="PC7_GPIO71" pad="40"/>
<connect gate="G$1" pin="PD0_GPIO16" pad="102"/>
<connect gate="G$1" pin="PD1_GPIO17" pad="98"/>
<connect gate="G$1" pin="PD2_GPIO18" pad="28"/>
<connect gate="G$1" pin="PD3_GPIO19" pad="29"/>
<connect gate="G$1" pin="PD4_GPIO20" pad="65"/>
<connect gate="G$1" pin="PD5_GPIO21" pad="64"/>
<connect gate="G$1" pin="PD6_GPIO22" pad="73"/>
<connect gate="G$1" pin="PD7_GPIO23" pad="68"/>
<connect gate="G$1" pin="PE0_GPIO24" pad="43"/>
<connect gate="G$1" pin="PE1_GPIO25" pad="45"/>
<connect gate="G$1" pin="PE2_GPIO26" pad="32"/>
<connect gate="G$1" pin="PE3_GPIO27" pad="33"/>
<connect gate="G$1" pin="PE4_GPIO28" pad="77"/>
<connect gate="G$1" pin="PE5_GPIO29" pad="76"/>
<connect gate="G$1" pin="PE6_GPIO30" pad="22"/>
<connect gate="G$1" pin="PE7_GPIO31" pad="23"/>
<connect gate="G$1" pin="PF0_GPIO32" pad="104"/>
<connect gate="G$1" pin="PF1_GPIO33" pad="103"/>
<connect gate="G$1" pin="PF2_GPIO34" pad="82"/>
<connect gate="G$1" pin="PF3_GPIO35" pad="81"/>
<connect gate="G$1" pin="PF4_GPIO36" pad="48"/>
<connect gate="G$1" pin="PF5_GPIO37" pad="51"/>
<connect gate="G$1" pin="PF6_GPIO38" pad="69"/>
<connect gate="G$1" pin="PG0_GPIO40" pad="49"/>
<connect gate="G$1" pin="PG1_GPIO41" pad="50"/>
<connect gate="G$1" pin="PG2_GPIO42" pad="71"/>
<connect gate="G$1" pin="PG3_GPIO43" pad="78"/>
<connect gate="G$1" pin="PG5_GPIO45" pad="72"/>
<connect gate="G$1" pin="PG6_GPIO46" pad="70"/>
<connect gate="G$1" pin="PG7_GPIO47" pad="52"/>
<connect gate="G$1" pin="PH0_GPIO48" pad="41"/>
<connect gate="G$1" pin="PH1_GPIO49" pad="42"/>
<connect gate="G$1" pin="PH2_GPIO50" pad="36"/>
<connect gate="G$1" pin="PH3_GPIO51" pad="35"/>
<connect gate="G$1" pin="PH4_GPIO52" pad="46"/>
<connect gate="G$1" pin="PH5_GPIO53" pad="47"/>
<connect gate="G$1" pin="PH6_GPIO54" pad="79"/>
<connect gate="G$1" pin="PH7_GPIO55" pad="80"/>
<connect gate="G$1" pin="PJ0_GPIO56" pad="63"/>
<connect gate="G$1" pin="PJ1_GPIO57" pad="62"/>
<connect gate="G$1" pin="PJ2_GPIO58" pad="61"/>
<connect gate="G$1" pin="PJ3_GPIO59" pad="60"/>
<connect gate="G$1" pin="PJ4_GPIO60" pad="57"/>
<connect gate="G$1" pin="PJ5_GPIO61" pad="56"/>
<connect gate="G$1" pin="PJ6_GPIO62" pad="53"/>
<connect gate="G$1" pin="PJ7_GPIO63" pad="97"/>
<connect gate="G$1" pin="PP_GND" pad="GND"/>
<connect gate="G$1" pin="TCK" pad="89"/>
<connect gate="G$1" pin="TDI" pad="88"/>
<connect gate="G$1" pin="TDO" pad="84"/>
<connect gate="G$1" pin="TMS" pad="87"/>
<connect gate="G$1" pin="TRST" pad="85"/>
<connect gate="G$1" pin="VDD12" pad="11"/>
<connect gate="G$1" pin="VDD12_2" pad="24"/>
<connect gate="G$1" pin="VDD12_3" pad="55"/>
<connect gate="G$1" pin="VDD12_4" pad="58"/>
<connect gate="G$1" pin="VDD12_5" pad="66"/>
<connect gate="G$1" pin="VDD12_6" pad="75"/>
<connect gate="G$1" pin="VDD12_7" pad="90"/>
<connect gate="G$1" pin="VDD12_8" pad="99"/>
<connect gate="G$1" pin="VDD18" pad="1"/>
<connect gate="G$1" pin="VDD18_2" pad="108"/>
<connect gate="G$1" pin="VDDA1" pad="119"/>
<connect gate="G$1" pin="VDDA2" pad="134"/>
<connect gate="G$1" pin="VDDIO" pad="2"/>
<connect gate="G$1" pin="VDDIO_10" pad="67"/>
<connect gate="G$1" pin="VDDIO_11" pad="74"/>
<connect gate="G$1" pin="VDDIO_12" pad="92"/>
<connect gate="G$1" pin="VDDIO_13" pad="96"/>
<connect gate="G$1" pin="VDDIO_14" pad="100"/>
<connect gate="G$1" pin="VDDIO_15" pad="105"/>
<connect gate="G$1" pin="VDDIO_16" pad="106"/>
<connect gate="G$1" pin="VDDIO_17" pad="107"/>
<connect gate="G$1" pin="VDDIO_2" pad="3"/>
<connect gate="G$1" pin="VDDIO_3" pad="10"/>
<connect gate="G$1" pin="VDDIO_4" pad="17"/>
<connect gate="G$1" pin="VDDIO_5" pad="25"/>
<connect gate="G$1" pin="VDDIO_6" pad="34"/>
<connect gate="G$1" pin="VDDIO_7" pad="44"/>
<connect gate="G$1" pin="VDDIO_8" pad="54"/>
<connect gate="G$1" pin="VDDIO_9" pad="59"/>
<connect gate="G$1" pin="VREG12EN" pad="101"/>
<connect gate="G$1" pin="VREG18EN" pad="113"/>
<connect gate="G$1" pin="VSSA1" pad="118"/>
<connect gate="G$1" pin="VSSA2" pad="135"/>
<connect gate="G$1" pin="VSSOSC" pad="94"/>
<connect gate="G$1" pin="X1" pad="93"/>
<connect gate="G$1" pin="X2" pad="95"/>
<connect gate="G$1" pin="XRS" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V_REG_LF33CPT-TR" prefix="U">
<description>LF33CPT-TR 3.3V 500mA LDO Voltage Regulator
http://www.digikey.com/product-detail/en/LF33CPT-TR/497-5226-1-ND/1121679</description>
<gates>
<gate name="G$1" symbol="V_REG_4_PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO252-4">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="VIN"/>
<connect gate="G$1" pin="INHIB" pad="INHIBIT"/>
<connect gate="G$1" pin="OUT" pad="VOUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-SMD-ABM3B">
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="CRYSTAL-SMD-ABM3B">
<connects>
<connect gate="G$1" pin="GND1" pad="2"/>
<connect gate="G$1" pin="GND2" pad="4"/>
<connect gate="G$1" pin="X1" pad="1"/>
<connect gate="G$1" pin="X2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="A" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC2G07DCKR">
<description>Ultra-high speed dual open-drain output buffer (non-inverting)</description>
<gates>
<gate name="G$1" symbol="BUFFER-DUAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC-70-6">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y1" pad="6"/>
<connect gate="G$1" pin="Y2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ITG-3200" prefix="U">
<description>ITG-3200 3-axis gyro, digital output, qfn24 package</description>
<gates>
<gate name="G$1" symbol="ITG-3200" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-24">
<connects>
<connect gate="G$1" pin="AD0" pad="9"/>
<connect gate="G$1" pin="CLKIN" pad="1"/>
<connect gate="G$1" pin="CPOUT" pad="20"/>
<connect gate="G$1" pin="GND" pad="18"/>
<connect gate="G$1" pin="INT" pad="12"/>
<connect gate="G$1" pin="REGOUT" pad="10"/>
<connect gate="G$1" pin="RESV-G" pad="11"/>
<connect gate="G$1" pin="SCL" pad="23"/>
<connect gate="G$1" pin="SDA" pad="24"/>
<connect gate="G$1" pin="VDD" pad="13"/>
<connect gate="G$1" pin="VLOGIC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XBEE-1">
<description>Xbee module footprints&lt;br&gt;
SMT header is CONN-09042.</description>
<gates>
<gate name="G$1" symbol="XBEE-1" x="0" y="0"/>
</gates>
<devices>
<device name="B1" package="XBEE-1">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B2" package="XBEE-SILK">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B3" package="XBEE-SMD">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09042"/>
</technology>
</technologies>
</device>
<device name="XBEE-1_LOCK" package="XBEE-1_LOCK">
<connects>
<connect gate="G$1" pin="CTS" pad="12"/>
<connect gate="G$1" pin="DIN" pad="3"/>
<connect gate="G$1" pin="DIO0" pad="20"/>
<connect gate="G$1" pin="DIO1" pad="19"/>
<connect gate="G$1" pin="DIO11" pad="7"/>
<connect gate="G$1" pin="DIO12" pad="4"/>
<connect gate="G$1" pin="DIO2" pad="18"/>
<connect gate="G$1" pin="DIO3" pad="17"/>
<connect gate="G$1" pin="DIO4" pad="11"/>
<connect gate="G$1" pin="DIO5" pad="15"/>
<connect gate="G$1" pin="DIO9" pad="13"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="DTR" pad="9"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="RES@14" pad="14"/>
<connect gate="G$1" pin="RES@8" pad="8"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="RSSI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="16"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DigitalIC">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find all manner of digital ICs- microcontrollers, memory chips, logic chips, FPGAs, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; Fits JEDEC packages (narrow SOIC-8)</description>
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-0.9906" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-1.27" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.27" y="0" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
</package>
<package name="SO08-EIAJ">
<description>Fits EIAJ packages (wide version of the SOIC-8).</description>
<wire x1="-2.362" y1="-2.565" x2="2.362" y2="-2.565" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-2.565" x2="2.362" y2="2.5396" width="0.1524" layer="21"/>
<wire x1="2.362" y1="2.5396" x2="-2.362" y2="2.5396" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="2.5396" x2="-2.362" y2="-2.565" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-1.7526" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-3.3782" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="3.3528" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-1.27" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-0.762" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-3.6322" x2="-1.7272" y2="-2.6162" layer="51"/>
<rectangle x1="-0.8128" y1="-3.6322" x2="-0.4572" y2="-2.6162" layer="51"/>
<rectangle x1="0.4572" y1="-3.6322" x2="0.8128" y2="-2.6162" layer="51"/>
<rectangle x1="1.7272" y1="-3.6322" x2="2.0828" y2="-2.6162" layer="51"/>
<rectangle x1="-2.0828" y1="2.5908" x2="-1.7272" y2="3.6068" layer="51"/>
<rectangle x1="-0.8128" y1="2.5908" x2="-0.4572" y2="3.6068" layer="51"/>
<rectangle x1="0.4572" y1="2.5908" x2="0.8128" y2="3.6068" layer="51"/>
<rectangle x1="1.7272" y1="2.5908" x2="2.0828" y2="3.6068" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="EEPROM-I2C">
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VSS" x="-12.7" y="-2.54" length="middle" direction="pwr"/>
<pin name="WP" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="VCC" x="12.7" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="SCL" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="SDA" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="A2" x="-12.7" y="0" length="middle"/>
<pin name="A0" x="-12.7" y="5.08" length="middle"/>
<pin name="A1" x="-12.7" y="2.54" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EEPROM-I2C" prefix="U">
<description>I2C EEPROM chips, 24LC256 (and others)</description>
<gates>
<gate name="G$1" symbol="EEPROM-I2C" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="SO08">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EIAJ" package="SO08-EIAJ">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="USD-SOCKET-PP">
<description>OLD Production Socket. DO NOT USE for production boards.</description>
<wire x1="-6.725" y1="5.5" x2="-6.725" y2="17" width="0.2032" layer="21"/>
<wire x1="4" y1="17" x2="7.825" y2="17" width="0.2032" layer="21"/>
<wire x1="7.825" y1="17" x2="7.825" y2="1.7" width="0.2032" layer="21"/>
<wire x1="6" y1="0" x2="4" y2="0" width="0.2032" layer="21"/>
<wire x1="4" y1="0" x2="4" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.65" x2="-4" y2="1" width="0.2032" layer="21"/>
<wire x1="-4" y1="1" x2="-5" y2="1" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.65" x2="4" y2="1.65" width="0.2032" layer="21" curve="-69.416099"/>
<wire x1="4" y1="-0.7" x2="-4" y2="-0.7" width="0.127" layer="51"/>
<wire x1="4" y1="-1.6" x2="-4" y2="-1.6" width="0.127" layer="51"/>
<wire x1="4" y1="-5.4" x2="-4" y2="-5.4" width="0.127" layer="51"/>
<wire x1="-6.725" y1="3.6" x2="-6.725" y2="2.6" width="0.2032" layer="21"/>
<smd name="GND1" x="-6.275" y="1.475" dx="1.5" dy="1.35" layer="1"/>
<smd name="GND2" x="7.375" y="0.475" dx="1.5" dy="1.35" layer="1"/>
<smd name="CD2" x="-6.425" y="4.5" dx="1.4" dy="1" layer="1"/>
<smd name="CD1" x="-5.85" y="16.75" dx="1" dy="1.3" layer="1"/>
<smd name="8" x="-4.5" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="7" x="-3.4" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="6" x="-2.3" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="5" x="-1.2" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="4" x="-0.1" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="3" x="1" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="2" x="2.1" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<smd name="1" x="3.2" y="16.8" dx="0.75" dy="1.5" layer="1"/>
<text x="-5.08" y="7.62" size="0.4064" layer="25">&gt;Name</text>
<text x="-5.08" y="6.35" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="MICRO-SD-SOCKET-PP">
<wire x1="-14" y1="0" x2="-14" y2="13.2" width="0.2032" layer="21"/>
<wire x1="0" y1="12.1" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="-11.7" y1="15.3" x2="-12.5" y2="15.3" width="0.2032" layer="21"/>
<wire x1="-11" y1="16" x2="0" y2="16" width="0.2032" layer="51"/>
<wire x1="-10" y1="13.6" x2="-1.6" y2="13.6" width="0.2032" layer="21"/>
<wire x1="-14" y1="0" x2="-9.1" y2="0" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="0" x2="-6.4" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.7" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="17.6" x2="-11" y2="17.6" width="0.2032" layer="51"/>
<wire x1="0" y1="20.7" x2="-11" y2="20.7" width="0.2032" layer="51"/>
<wire x1="-11.7" y1="15.3" x2="-11" y2="14.6" width="0.2032" layer="21" curve="-98.797411"/>
<wire x1="-11" y1="14.6" x2="-10" y2="13.6" width="0.2032" layer="21" curve="87.205638"/>
<smd name="CD2" x="-7.75" y="0.4" dx="1.8" dy="1.4" layer="1"/>
<smd name="CD1" x="-2.05" y="0.4" dx="1.8" dy="1.4" layer="1"/>
<smd name="GND3" x="-0.45" y="13.55" dx="1.4" dy="1.9" layer="1"/>
<smd name="GND1" x="-13.6" y="14.55" dx="1.4" dy="1.9" layer="1"/>
<smd name="1" x="-8.94" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="2" x="-7.84" y="10.3" dx="0.8" dy="1.5" layer="1"/>
<smd name="3" x="-6.74" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="4" x="-5.64" y="10.9" dx="0.8" dy="1.5" layer="1"/>
<smd name="5" x="-4.54" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="6" x="-3.44" y="10.9" dx="0.8" dy="1.5" layer="1"/>
<smd name="7" x="-2.34" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="8" x="-1.24" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<text x="-8.89" y="6.35" size="0.8128" layer="25">&gt;Name</text>
<text x="-8.89" y="5.08" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="1X07">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X07_LOCK">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X07_LOCK_LONGPADS">
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0" x2="16.51" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.9906" x2="16.2306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0" x2="16.51" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.9906" x2="16.2306" y2="1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.905" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
</package>
<package name="1X07_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<hole x="0" y="0" drill="1.016"/>
<hole x="2.54" y="0" drill="1.016"/>
<hole x="5.08" y="0" drill="1.016"/>
<hole x="7.62" y="0" drill="1.016"/>
<hole x="10.16" y="0" drill="1.016"/>
<hole x="12.7" y="0" drill="1.016"/>
<hole x="15.24" y="0" drill="1.016"/>
</package>
<package name="1X07_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="15.24" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
<hole x="15.24" y="0" drill="1.4732"/>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<wire x1="-4.5" y1="-5" x2="-5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5" x2="-5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-6.3" x2="-6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6.3" x2="-6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="-6" y1="1.1" x2="6" y2="1.1" width="0.2032" layer="21"/>
<wire x1="6" y1="1.1" x2="6" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="6" y1="-6.3" x2="5.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-6.3" x2="5.2" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-5" x2="4.5" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-5" drill="0.7"/>
<pad name="2" x="-1" y="-5" drill="0.7"/>
<pad name="3" x="1" y="-5" drill="0.7"/>
<pad name="4" x="3" y="-5" drill="0.7"/>
<text x="-2.27" y="0.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="-1" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="-4.3" size="1.27" layer="51">+</text>
<text x="-1.4" y="-4.3" size="1.27" layer="51">-</text>
<text x="0.7" y="-4.1" size="0.8" layer="51">S</text>
<text x="2.7" y="-4.1" size="0.8" layer="51">S</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="USDCARD">
<wire x1="-2.54" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-20.32" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="15.748" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="CS" x="-7.62" y="10.16" length="middle"/>
<pin name="DI" x="-7.62" y="7.62" length="middle"/>
<pin name="GND" x="-7.62" y="0" length="middle"/>
<pin name="VCC" x="-7.62" y="5.08" length="middle"/>
<pin name="SCK" x="-7.62" y="2.54" length="middle"/>
<pin name="RSV" x="-7.62" y="-5.08" length="middle"/>
<pin name="DO" x="-7.62" y="-2.54" length="middle"/>
<pin name="NC" x="-7.62" y="12.7" length="middle"/>
<pin name="SHIELD@3" x="-7.62" y="-15.24" length="middle"/>
<pin name="SHIELD@1" x="-7.62" y="-10.16" length="middle"/>
<pin name="SHIELD@2" x="-7.62" y="-12.7" length="middle"/>
<pin name="SHIELD@4" x="-7.62" y="-17.78" length="middle"/>
</symbol>
<symbol name="M07">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USD-SOCKET" prefix="U">
<description>&lt;b&gt;microSD Socket&lt;/b&gt;
Push-push type uSD socket. Schematic element and footprint production proven. Spark Fun Electronics SKU : PRT-00127. tDoc lines correctly indicate media card edge positions when inserting (unlocked, locked, depressed).</description>
<gates>
<gate name="G$1" symbol="USDCARD" x="0" y="0"/>
</gates>
<devices>
<device name="USD" package="USD-SOCKET-PP">
<connects>
<connect gate="G$1" pin="CS" pad="2"/>
<connect gate="G$1" pin="DI" pad="3"/>
<connect gate="G$1" pin="DO" pad="7"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="NC" pad="1"/>
<connect gate="G$1" pin="RSV" pad="8"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="SHIELD@1" pad="CD1"/>
<connect gate="G$1" pin="SHIELD@2" pad="CD2"/>
<connect gate="G$1" pin="SHIELD@3" pad="GND1"/>
<connect gate="G$1" pin="SHIELD@4" pad="GND2"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NEW" package="MICRO-SD-SOCKET-PP">
<connects>
<connect gate="G$1" pin="CS" pad="2"/>
<connect gate="G$1" pin="DI" pad="3"/>
<connect gate="G$1" pin="DO" pad="7"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="NC" pad="1"/>
<connect gate="G$1" pin="RSV" pad="8"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="SHIELD@1" pad="GND3"/>
<connect gate="G$1" pin="SHIELD@2" pad="CD1"/>
<connect gate="G$1" pin="SHIELD@3" pad="GND1"/>
<connect gate="G$1" pin="SHIELD@4" pad="CD2"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M07" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 7&lt;/b&gt;
Standard 7-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).  NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS... This footprint was designed to help hold the alignment of a through-hole component (i.e. 7-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is usually a perfectly straight line). This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes. Because they are alternating, it causes a "brace" to hold the header in place. 0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice. Also, if you push a header all the way into place, it is covered up entirely on the bottom side. This idea of altering the position of holes to aid alignment will be further integrated into the Sparkfun Library for other footprints. It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M07" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X07">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X07_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X07_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X07_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPINS_HOLES_ONLY" package="1X07_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Electromechanical">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find anything that moves- switches, relays, buttons, potentiometers. Also, anything that goes on a board but isn't electrical in nature- screws, standoffs, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SWITCH-SPST-SMD-A">
<wire x1="-3.35" y1="1.3" x2="-3.35" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-3.35" y1="-1.3" x2="3.35" y2="-1.3" width="0.127" layer="51"/>
<wire x1="3.35" y1="-1.3" x2="3.35" y2="1.3" width="0.127" layer="51"/>
<wire x1="3.35" y1="1.3" x2="-0.1" y2="1.3" width="0.127" layer="51"/>
<wire x1="-0.1" y1="1.3" x2="-1.4" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.4" y1="1.3" x2="-3.35" y2="1.3" width="0.127" layer="51"/>
<wire x1="-0.1" y1="1.3" x2="-0.1" y2="2.8" width="0.127" layer="51"/>
<wire x1="-0.1" y1="2.8" x2="-1.4" y2="2.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="2.8" x2="-1.4" y2="1.3" width="0.127" layer="51"/>
<wire x1="-3.35" y1="0.3" x2="-3.35" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="3.35" y1="0.3" x2="3.35" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.3" x2="-2.7" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-1.3" x2="0" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="-1.75" dx="0.7" dy="1.5" layer="1" rot="R180"/>
<smd name="2" x="-0.75" y="-1.75" dx="0.7" dy="1.5" layer="1" rot="R180"/>
<smd name="3" x="2.25" y="-1.75" dx="0.7" dy="1.5" layer="1" rot="R180"/>
<smd name="GND1" x="-3.65" y="1" dx="1" dy="0.6" layer="1"/>
<smd name="GND2" x="-3.65" y="-1.1" dx="1" dy="0.8" layer="1"/>
<smd name="GND3" x="3.65" y="1" dx="1" dy="0.6" layer="1"/>
<smd name="GND4" x="3.65" y="-1.1" dx="1" dy="0.8" layer="1"/>
<text x="-1.27" y="0.635" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.6096" layer="27">&gt;Value</text>
<hole x="-1.5" y="0" drill="0.9"/>
<hole x="1.5" y="0" drill="0.9"/>
</package>
<package name="SWITCH-SPDT">
<wire x1="2.175" y1="5.815" x2="-2.175" y2="5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="5.815" x2="-2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="-5.815" x2="2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="2.175" y1="-5.815" x2="2.175" y2="5.815" width="0.2032" layer="21"/>
<pad name="1" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<text x="-3.81" y="7.62" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-9.525" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="AYZ0202">
<description>&lt;b&gt;DPDT Slide Switch SMD&lt;/b&gt;
www.SparkFun.com SKU : Comp-SMDS</description>
<wire x1="-3.6" y1="1.75" x2="-3.6" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.75" x2="3.6" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="3.6" y1="-1.75" x2="3.6" y2="1.75" width="0.2032" layer="21"/>
<wire x1="3.6" y1="1.75" x2="-3.6" y2="1.75" width="0.2032" layer="21"/>
<smd name="3" x="2.5" y="2.825" dx="1" dy="1.15" layer="1"/>
<smd name="2" x="0" y="2.825" dx="1" dy="1.15" layer="1"/>
<smd name="1" x="-2.5" y="2.825" dx="1" dy="1.15" layer="1"/>
<smd name="6" x="2.5" y="-2.825" dx="1" dy="1.15" layer="1"/>
<smd name="5" x="0" y="-2.825" dx="1" dy="1.15" layer="1"/>
<smd name="4" x="-2.5" y="-2.825" dx="1" dy="1.15" layer="1"/>
<text x="-2.54" y="1.143" size="0.4064" layer="25">&gt;Name</text>
<text x="0.508" y="1.143" size="0.4064" layer="27">&gt;Value</text>
<hole x="1.5" y="0" drill="0.85"/>
<hole x="-1.5" y="0" drill="0.85"/>
</package>
<package name="SWITCHE-DPDT">
<wire x1="8" y1="3.25" x2="-8" y2="3.25" width="0.127" layer="51"/>
<wire x1="-8" y1="3.25" x2="-8" y2="-3.25" width="0.127" layer="51"/>
<wire x1="-8" y1="-3.25" x2="8" y2="-3.25" width="0.127" layer="51"/>
<wire x1="8" y1="-3.25" x2="8" y2="3.25" width="0.127" layer="51"/>
<wire x1="-6" y1="3.25" x2="6" y2="3.25" width="0.2032" layer="21"/>
<wire x1="8" y1="1" x2="8" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-3.25" x2="-6" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-8" y1="-1" x2="-8" y2="1" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.5" y="3" drill="1.5" diameter="2.54"/>
<pad name="P$2" x="-7.5" y="-3" drill="1.5" diameter="2.54"/>
<pad name="P$3" x="7.5" y="3" drill="1.5" diameter="2.54"/>
<pad name="P$4" x="7.5" y="-3" drill="1.5" diameter="2.54"/>
<pad name="1" x="-4" y="1.25" drill="0.7" diameter="1.6764"/>
<pad name="2" x="0" y="1.25" drill="0.7" diameter="1.6764"/>
<pad name="3" x="4" y="1.25" drill="0.7" diameter="1.6764"/>
<pad name="4" x="-4" y="-1.25" drill="0.7" diameter="1.6764"/>
<pad name="5" x="0" y="-1.25" drill="0.7" diameter="1.6764"/>
<pad name="6" x="4" y="-1.25" drill="0.7" diameter="1.6764"/>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SWITCH-SPDT-SMD">
<wire x1="-4.5" y1="1.75" x2="-4.5" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-1.75" x2="4.5" y2="-1.75" width="0.127" layer="51"/>
<wire x1="4.5" y1="-1.75" x2="4.5" y2="1.75" width="0.127" layer="51"/>
<wire x1="4.5" y1="1.75" x2="2" y2="1.75" width="0.127" layer="51"/>
<wire x1="2" y1="1.75" x2="0.5" y2="1.75" width="0.127" layer="51"/>
<wire x1="0.5" y1="1.75" x2="-4.5" y2="1.75" width="0.127" layer="51"/>
<wire x1="0.5" y1="1.75" x2="0.5" y2="3.75" width="0.127" layer="51"/>
<wire x1="0.5" y1="3.75" x2="2" y2="3.75" width="0.127" layer="51"/>
<wire x1="2" y1="3.75" x2="2" y2="1.75" width="0.127" layer="51"/>
<wire x1="-4" y1="-1.75" x2="-4.5" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-1.75" x2="-4.5" y2="1.75" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.75" x2="4.5" y2="1.75" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.75" x2="4.5" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.75" x2="4" y2="-1.75" width="0.2032" layer="21"/>
<smd name="1" x="-2.5" y="-2.75" dx="1.2" dy="2.5" layer="1" rot="R180"/>
<smd name="2" x="0" y="-2.75" dx="1.2" dy="2.5" layer="1" rot="R180"/>
<smd name="3" x="2.5" y="-2.75" dx="1.2" dy="2.5" layer="1" rot="R180"/>
<text x="-1.27" y="0.635" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.6096" layer="27">&gt;Value</text>
<hole x="-3.55" y="0" drill="0.9"/>
<hole x="3.55" y="0" drill="0.9"/>
</package>
<package name="SWITCH-SPDT_LOCK.007S">
<wire x1="2.175" y1="5.815" x2="-2.175" y2="5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="5.815" x2="-2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="-5.815" x2="2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="2.175" y1="-5.815" x2="2.175" y2="5.815" width="0.2032" layer="21"/>
<pad name="1" x="0" y="2.7178" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="-2.7178" drill="1.016" diameter="1.8796"/>
<text x="-3.81" y="7.62" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-9.525" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="-0.3048" x2="0.2286" y2="0.3048" layer="51"/>
<rectangle x1="-0.2286" y1="2.2352" x2="0.2286" y2="2.8448" layer="51"/>
<rectangle x1="-0.2286" y1="-2.8448" x2="0.2286" y2="-2.2352" layer="51"/>
</package>
<package name="SWITCH-SPDT_KIT">
<description>&lt;h3&gt;SWITCH-SPDT_KIT&lt;/h3&gt;
Through-hole SPDT Switch&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.175" y1="5.815" x2="-2.175" y2="5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="5.815" x2="-2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="-2.175" y1="-5.815" x2="2.175" y2="-5.815" width="0.2032" layer="21"/>
<wire x1="2.175" y1="-5.815" x2="2.175" y2="5.815" width="0.2032" layer="21"/>
<pad name="1" x="0" y="2.7178" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="3" x="0" y="-2.7178" drill="1.016" diameter="1.8796" stop="no"/>
<text x="-3.81" y="7.62" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-9.525" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="-0.3048" x2="0.2286" y2="0.3048" layer="51"/>
<rectangle x1="-0.2286" y1="2.2352" x2="0.2286" y2="2.8448" layer="51"/>
<rectangle x1="-0.2286" y1="-2.8448" x2="0.2286" y2="-2.2352" layer="51"/>
<polygon width="0.127" layer="30">
<vertex x="-0.0178" y="1.8414" curve="-90.039946"/>
<vertex x="-0.8787" y="2.6975" curve="-90"/>
<vertex x="-0.0026" y="3.5916" curve="-90.006409"/>
<vertex x="0.8738" y="2.6975" curve="-90.03214"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-0.0051" y="-3.5967" curve="-90.006558"/>
<vertex x="-0.8788" y="-2.7431" curve="-90.037923"/>
<vertex x="0.0128" y="-1.8363" curve="-90.006318"/>
<vertex x="0.8814" y="-2.7432" curve="-90.038792"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-0.0102" y="-0.8738" curve="-90.019852"/>
<vertex x="-0.8762" y="-0.0203" curve="-90.019119"/>
<vertex x="0.0153" y="0.8789" curve="-90"/>
<vertex x="0.8739" y="-0.0077" curve="-90.038897"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0" y="2.2758" curve="-90.012891"/>
<vertex x="-0.4445" y="2.7" curve="-90"/>
<vertex x="0" y="3.1673" curve="-90"/>
<vertex x="0.4419" y="2.7102" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0.0026" y="-3.1648" curve="-90.012891"/>
<vertex x="-0.4419" y="-2.7406" curve="-90"/>
<vertex x="0.0026" y="-2.2733" curve="-90"/>
<vertex x="0.4445" y="-2.7304" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0.0102" y="-0.4471" curve="-90.012891"/>
<vertex x="-0.4343" y="-0.0229" curve="-90"/>
<vertex x="0.0102" y="0.4444" curve="-90"/>
<vertex x="0.4521" y="-0.0127" curve="-90.012967"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="TOGGLE">
<wire x1="0" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0.2032" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0.2032" layer="94"/>
<circle x="0" y="0" radius="0.3592" width="0.2032" layer="94"/>
<text x="-1.905" y="-6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="S" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="O" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SWITCH-SPDT" prefix="S" uservalue="yes">
<description>&lt;b&gt;SPDT Switch&lt;/b&gt;&lt;br&gt;
Simple slide switch, Spark Fun Electronics SKU : COM-00102&lt;br&gt;
DPDT SMT slide switch, AYZ0202, SWCH-08179</description>
<gates>
<gate name="1" symbol="TOGGLE" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="SWITCH-SPDT">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="AYZ0202">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08179" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="SWITCHE-DPDT">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="R_SW_TH">
<connects>
<connect gate="1" pin="O" pad="P$1"/>
<connect gate="1" pin="P" pad="P$2"/>
<connect gate="1" pin="S" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="SWITCH-SPDT-SMD">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_LOCK" package="SWITCH-SPDT_LOCK.007S">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="SWITCH-SPDT_KIT">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD-A" package="SWITCH-SPST-SMD-A">
<connects>
<connect gate="1" pin="O" pad="1"/>
<connect gate="1" pin="P" pad="2"/>
<connect gate="1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="antenna" width="2.9743" drill="0">
</class>
</classes>
<parts>
<part name="R10" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="499"/>
<part name="R11" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="499"/>
<part name="U$2" library="SparkFun" deviceset="STAND-OFF" device=""/>
<part name="U$3" library="SparkFun" deviceset="STAND-OFF" device=""/>
<part name="U$4" library="SparkFun" deviceset="STAND-OFF" device=""/>
<part name="U$5" library="SparkFun" deviceset="STAND-OFF" device=""/>
<part name="STAT1" library="SparkFun" deviceset="LED" device="0603" value="red"/>
<part name="STAT2" library="SparkFun" deviceset="LED" device="0603" value="green"/>
<part name="P+8" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+9" library="SparkFun" deviceset="3.3V" device=""/>
<part name="C5" library="SparkFun" deviceset="CAP_POL" device="1206" value="10uF,25V"/>
<part name="GND13" library="SparkFun" deviceset="GND" device=""/>
<part name="P+5" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GYRO" library="ACL" deviceset="ITG-3200" device=""/>
<part name="GND5" library="SparkFun" deviceset="GND" device=""/>
<part name="C2" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2nF"/>
<part name="GND9" library="SparkFun" deviceset="GND" device=""/>
<part name="C9" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND14" library="SparkFun" deviceset="GND" device=""/>
<part name="C10" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND15" library="SparkFun" deviceset="GND" device=""/>
<part name="C11" library="SparkFun" deviceset="CAP" device="0603-CAP" value="10000pF"/>
<part name="GND16" library="SparkFun" deviceset="GND" device=""/>
<part name="GND18" library="SparkFun" deviceset="GND" device=""/>
<part name="XBEE" library="ACL" deviceset="XBEE-1" device="B3"/>
<part name="GND2" library="SparkFun" deviceset="GND" device=""/>
<part name="SERIAL1" library="SparkFun" deviceset="M04" device="SMD2" value="M04 molex"/>
<part name="GND8" library="SparkFun" deviceset="GND" device=""/>
<part name="U$7" library="ACL" deviceset="ACL-LOGO" device="LARGE"/>
<part name="C3" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF,25V"/>
<part name="DEBUGPINS" library="SparkFun-Connectors" deviceset="M04" device="SMD2"/>
<part name="R14" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="390k"/>
<part name="R20" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="120k"/>
<part name="GND7" library="SparkFun" deviceset="GND" device=""/>
<part name="S1" library="SparkFun-Electromechanical" deviceset="SWITCH-SPDT" device="-SMD-A"/>
<part name="U2" library="SparkFun" deviceset="ADXL345" device=""/>
<part name="C6" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.22uF"/>
<part name="C8" library="SparkFun" deviceset="CAP" device="0603-CAP" value="4.7uF"/>
<part name="C12" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND17" library="SparkFun" deviceset="GND" device=""/>
<part name="GND19" library="SparkFun" deviceset="GND" device=""/>
<part name="GND20" library="SparkFun" deviceset="GND" device=""/>
<part name="P+12" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+14" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+6" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+10" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+11" library="SparkFun" deviceset="3.3V" device=""/>
<part name="C13" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C14" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="U$1" library="SparkFun" deviceset="HMC5883L" device="SMD"/>
<part name="GND21" library="SparkFun" deviceset="GND" device=""/>
<part name="C15" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND23" library="SparkFun" deviceset="GND" device=""/>
<part name="P+16" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+18" library="SparkFun" deviceset="VCC" device=""/>
<part name="P+19" library="SparkFun" deviceset="VCC" device=""/>
<part name="GND11" library="SparkFun" deviceset="GND" device=""/>
<part name="P+20" library="SparkFun" deviceset="VCC" device=""/>
<part name="GND25" library="SparkFun" deviceset="GND" device=""/>
<part name="BATT" library="ACL" deviceset="BATT" device="C"/>
<part name="U$9" library="ACL" deviceset="BMP180" device="A"/>
<part name="GND26" library="SparkFun" deviceset="GND" device=""/>
<part name="FRAME3" library="ACL" deviceset="FRAME-A3" device=""/>
<part name="U$8" library="ACL" deviceset="F28M35H52C" device=""/>
<part name="FRAME1" library="ACL" deviceset="FRAME-A3" device=""/>
<part name="C16" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C17" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C18" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C19" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C20" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C21" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C22" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C23" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C24" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C25" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C26" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C27" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C28" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C29" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C30" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C31" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C32" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C33" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C34" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="L1" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L2" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L3" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L4" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L5" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L6" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L7" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L8" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L9" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L10" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L11" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L12" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L13" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L14" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L15" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L16" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L17" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L18" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="L19" library="SparkFun" deviceset="INDUCTOR" device="0603" value="BKP2125HS600-T"/>
<part name="P+3" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND6" library="SparkFun" deviceset="GND" device=""/>
<part name="C35" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C36" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C37" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="C38" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="GND27" library="SparkFun" deviceset="GND" device=""/>
<part name="U$11" library="SparkFun-DigitalIC" deviceset="EEPROM-I2C" device="SMD"/>
<part name="GND34" library="SparkFun" deviceset="GND" device=""/>
<part name="C42" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND35" library="SparkFun" deviceset="GND" device=""/>
<part name="U1" library="SparkFun-Connectors" deviceset="USD-SOCKET" device="NEW"/>
<part name="GND36" library="SparkFun" deviceset="GND" device=""/>
<part name="R27" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="R28" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="R29" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="C43" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND37" library="SparkFun" deviceset="GND" device=""/>
<part name="C44" library="SparkFun" deviceset="CAP" device="0603-CAP" value="15pF"/>
<part name="C45" library="SparkFun" deviceset="CAP" device="0603-CAP" value="15pF"/>
<part name="U$12" library="ACL" deviceset="SN74LVC2G07DCKR" device=""/>
<part name="GND39" library="SparkFun" deviceset="GND" device=""/>
<part name="C46" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="P+13" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND40" library="SparkFun" deviceset="GND" device=""/>
<part name="R30" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="R31" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="P+15" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R32" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="499"/>
<part name="R33" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="499"/>
<part name="STAT3" library="SparkFun" deviceset="LED" device="0603" value="yellow"/>
<part name="STAT4" library="SparkFun" deviceset="LED" device="0603" value="blue"/>
<part name="P+21" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+22" library="SparkFun" deviceset="3.3V" device=""/>
<part name="U$14" library="ACL" deviceset="SN74LVC2G07DCKR" device=""/>
<part name="GND41" library="SparkFun" deviceset="GND" device=""/>
<part name="C47" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="P+23" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND42" library="SparkFun" deviceset="GND" device=""/>
<part name="R34" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="R35" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="P+24" library="SparkFun" deviceset="3.3V" device=""/>
<part name="JP2" library="SparkFun-Connectors" deviceset="M07" device=""/>
<part name="JP3" library="SparkFun-Connectors" deviceset="M07" device=""/>
<part name="GND24" library="SparkFun" deviceset="GND" device=""/>
<part name="P+17" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+25" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND28" library="SparkFun" deviceset="GND" device=""/>
<part name="C39" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C40" library="SparkFun" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="R7" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="1.1K"/>
<part name="R9" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="57.6K"/>
<part name="R13" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="57.6K"/>
<part name="R15" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="57.6K"/>
<part name="R21" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="57.6K"/>
<part name="P+26" library="SparkFun" deviceset="3.3V" device=""/>
<part name="U3" library="ACL" deviceset="V_REG_LF33CPT-TR" device=""/>
<part name="R1" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10k"/>
<part name="C4" library="SparkFun" deviceset="CAP_POL" device="1206" value="10uF"/>
<part name="C1" library="SparkFun" deviceset="CAP" device="0603-CAP" value="2.2uF"/>
<part name="P+2" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R2" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="2.21K"/>
<part name="R4" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.99K"/>
<part name="R8" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.99K"/>
<part name="R22" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="2.21K"/>
<part name="GND4" library="SparkFun" deviceset="GND" device=""/>
<part name="R23" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="499"/>
<part name="STAT5" library="SparkFun" deviceset="LED" device="0603" value="green"/>
<part name="P+7" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+27" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND29" library="SparkFun" deviceset="GND" device=""/>
<part name="GND30" library="SparkFun" deviceset="GND" device=""/>
<part name="GND31" library="SparkFun" deviceset="GND" device=""/>
<part name="CRYSTAL" library="ACL" deviceset="CRYSTAL-SMD-ABM3B" device="A"/>
<part name="PWM_1" library="SparkFun" deviceset="M04" device="SMD2" value="M04 molex"/>
<part name="PWM_2" library="SparkFun" deviceset="M04" device="SMD2" value="M04 molex"/>
<part name="R3" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.7K"/>
<part name="R5" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.7K"/>
<part name="P+4" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+28" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R6" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.7K"/>
<part name="R12" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.7K"/>
<part name="P+29" library="SparkFun" deviceset="VCC" device=""/>
<part name="GND1" library="SparkFun" deviceset="GND" device=""/>
<part name="BATT1" library="ACL" deviceset="BATT" device="C"/>
<part name="I2C0" library="SparkFun" deviceset="M04" device="SMD2" value="M04 molex"/>
<part name="I2C1" library="SparkFun" deviceset="M04" device="SMD2" value="M04 molex"/>
<part name="R16" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="56"/>
<part name="I2C2" library="SparkFun" deviceset="M04" device="SMD2" value="M04 molex"/>
<part name="C7" library="SparkFun" deviceset="CAP" device="0603-CAP" value="3300pF"/>
<part name="GND3" library="SparkFun" deviceset="GND" device=""/>
<part name="P+1" library="SparkFun" deviceset="3.3V" device=""/>
<part name="R17" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.7K"/>
<part name="R18" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="4.7K"/>
<part name="GND10" library="SparkFun" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun" deviceset="GND" device=""/>
<part name="P+30" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+31" library="SparkFun" deviceset="3.3V" device=""/>
<part name="GND22" library="SparkFun" deviceset="GND" device=""/>
<part name="GND32" library="SparkFun" deviceset="GND" device=""/>
<part name="GND33" library="SparkFun" deviceset="GND" device=""/>
<part name="P+32" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+33" library="SparkFun" deviceset="3.3V" device=""/>
<part name="P+34" library="SparkFun" deviceset="3.3V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="162.814" y="4.318" size="1.778" layer="97">Mark Cutler, ACL, acl.mit.edu</text>
<text x="68.58" y="187.96" size="1.778" layer="97">Note: All caps rated to 10V unless noted otherwise</text>
<text x="246.38" y="68.58" size="1.778" layer="97">ACCELEROMETER</text>
<text x="325.12" y="63.5" size="1.778" layer="97">MAGNETOMETER</text>
<text x="182.88" y="76.2" size="1.778" layer="97">BAROMETER</text>
<text x="308.356" y="14.986" size="2.54" layer="94" ratio="10">Mark Cutler</text>
<text x="373.634" y="10.16" size="2.54" layer="94" font="vector" ratio="10">1.4</text>
<wire x1="3.302" y1="184.404" x2="180.34" y2="184.404" width="0.6096" layer="94"/>
<text x="20.32" y="248.92" size="3.81" layer="94" ratio="10">Power and LEDs</text>
<wire x1="180.34" y1="184.404" x2="180.34" y2="244.348" width="0.6096" layer="94"/>
<wire x1="384.048" y1="120.396" x2="180.34" y2="120.396" width="0.6096" layer="94"/>
<wire x1="180.34" y1="120.396" x2="180.34" y2="184.404" width="0.6096" layer="94"/>
<wire x1="154.94" y1="3.048" x2="154.94" y2="120.396" width="0.6096" layer="94"/>
<wire x1="154.94" y1="120.396" x2="180.34" y2="120.396" width="0.6096" layer="94"/>
<text x="162.56" y="111.76" size="3.81" layer="94" ratio="10">Sensors</text>
<text x="10.16" y="175.26" size="3.81" layer="94" ratio="10">Connectors, Logos, and Comm</text>
<text x="22.86" y="200.66" size="1.778" layer="97">Voltage Divider</text>
<wire x1="146.05" y1="257.048" x2="146.05" y2="244.602" width="0.6096" layer="94"/>
<wire x1="146.05" y1="244.602" x2="241.3" y2="244.602" width="0.6096" layer="94"/>
<wire x1="241.3" y1="244.602" x2="241.3" y2="257.048" width="0.6096" layer="94"/>
<text x="165.1" y="248.92" size="5.08" layer="94" font="vector" ratio="13">UberPilot v1.4</text>
<text x="198.12" y="27.94" size="1.778" layer="97">GYRO</text>
</plain>
<instances>
<instance part="R10" gate="G$1" x="360.68" y="238.76" rot="R90"/>
<instance part="R11" gate="G$1" x="373.38" y="238.76" rot="R90"/>
<instance part="U$2" gate="G$1" x="147.32" y="167.64"/>
<instance part="U$3" gate="G$1" x="142.24" y="167.64"/>
<instance part="U$4" gate="G$1" x="137.16" y="167.64"/>
<instance part="U$5" gate="G$1" x="132.08" y="167.64"/>
<instance part="STAT1" gate="G$1" x="360.68" y="226.06"/>
<instance part="STAT2" gate="G$1" x="373.38" y="226.06"/>
<instance part="P+8" gate="G$1" x="360.68" y="246.38"/>
<instance part="P+9" gate="G$1" x="373.38" y="246.38"/>
<instance part="C5" gate="G$1" x="27.94" y="228.6"/>
<instance part="GND13" gate="1" x="63.5" y="205.74"/>
<instance part="P+5" gate="G$1" x="101.6" y="238.76"/>
<instance part="XBEE" gate="G$1" x="121.92" y="134.62"/>
<instance part="GND2" gate="1" x="96.52" y="119.38"/>
<instance part="SERIAL1" gate="G$1" x="81.28" y="53.34"/>
<instance part="GND8" gate="1" x="99.06" y="50.8"/>
<instance part="C3" gate="G$1" x="10.16" y="228.6"/>
<instance part="DEBUGPINS" gate="G$1" x="12.7" y="53.34"/>
<instance part="R14" gate="G$1" x="22.86" y="195.58" rot="R180"/>
<instance part="R20" gate="G$1" x="43.18" y="195.58" rot="R180"/>
<instance part="GND7" gate="1" x="53.34" y="187.96"/>
<instance part="S1" gate="1" x="50.8" y="215.9" rot="R180"/>
<instance part="U2" gate="G$1" x="256.54" y="91.44"/>
<instance part="C6" gate="G$1" x="358.14" y="76.2"/>
<instance part="C8" gate="G$1" x="312.42" y="76.2"/>
<instance part="C12" gate="G$1" x="365.76" y="91.44"/>
<instance part="GND17" gate="1" x="317.5" y="63.5"/>
<instance part="GND19" gate="1" x="241.3" y="73.66"/>
<instance part="GND20" gate="1" x="274.32" y="73.66"/>
<instance part="P+12" gate="G$1" x="241.3" y="104.14"/>
<instance part="P+14" gate="G$1" x="274.32" y="104.14"/>
<instance part="P+6" gate="G$1" x="353.06" y="106.68"/>
<instance part="P+10" gate="G$1" x="91.44" y="60.96"/>
<instance part="P+11" gate="G$1" x="93.98" y="147.32"/>
<instance part="C13" gate="G$1" x="241.3" y="93.98" rot="R180"/>
<instance part="C14" gate="G$1" x="292.1" y="91.44" rot="R180"/>
<instance part="U$1" gate="G$1" x="335.28" y="86.36"/>
<instance part="GND21" gate="1" x="365.76" y="83.82"/>
<instance part="C15" gate="G$1" x="167.64" y="83.82" rot="R180"/>
<instance part="GND23" gate="1" x="167.64" y="73.66"/>
<instance part="P+16" gate="G$1" x="167.64" y="99.06"/>
<instance part="P+18" gate="1" x="10.16" y="238.76"/>
<instance part="P+19" gate="1" x="10.16" y="203.2"/>
<instance part="GND11" gate="1" x="30.48" y="45.72"/>
<instance part="P+20" gate="1" x="152.4" y="228.6"/>
<instance part="GND25" gate="1" x="152.4" y="215.9"/>
<instance part="BATT" gate="G$1" x="167.64" y="223.52"/>
<instance part="U$9" gate="G$1" x="187.96" y="88.9"/>
<instance part="GND26" gate="1" x="195.58" y="99.06"/>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="GYRO" gate="G$1" x="208.28" y="45.72"/>
<instance part="GND5" gate="1" x="190.5" y="12.7"/>
<instance part="C2" gate="G$1" x="180.34" y="33.02"/>
<instance part="GND9" gate="1" x="180.34" y="12.7"/>
<instance part="C9" gate="G$1" x="241.3" y="33.02"/>
<instance part="GND14" gate="1" x="241.3" y="22.86"/>
<instance part="C10" gate="G$1" x="170.18" y="33.02"/>
<instance part="GND15" gate="1" x="170.18" y="12.7"/>
<instance part="C11" gate="G$1" x="160.02" y="33.02"/>
<instance part="GND16" gate="1" x="160.02" y="12.7"/>
<instance part="GND18" gate="1" x="236.22" y="12.7"/>
<instance part="U$7" gate="G$1" x="147.32" y="175.26"/>
<instance part="U$12" gate="G$1" x="337.82" y="203.2"/>
<instance part="GND39" gate="1" x="325.12" y="193.04"/>
<instance part="C46" gate="G$1" x="350.52" y="218.44"/>
<instance part="P+13" gate="G$1" x="350.52" y="231.14"/>
<instance part="GND40" gate="1" x="353.06" y="208.28"/>
<instance part="R30" gate="G$1" x="320.04" y="223.52" rot="R270"/>
<instance part="R31" gate="G$1" x="307.34" y="223.52" rot="R270"/>
<instance part="P+15" gate="G$1" x="314.96" y="233.68"/>
<instance part="R32" gate="G$1" x="360.68" y="177.8" rot="R90"/>
<instance part="R33" gate="G$1" x="373.38" y="177.8" rot="R90"/>
<instance part="STAT3" gate="G$1" x="360.68" y="165.1"/>
<instance part="STAT4" gate="G$1" x="373.38" y="165.1"/>
<instance part="P+21" gate="G$1" x="360.68" y="185.42"/>
<instance part="P+22" gate="G$1" x="373.38" y="185.42"/>
<instance part="U$14" gate="G$1" x="337.82" y="142.24"/>
<instance part="GND41" gate="1" x="325.12" y="132.08"/>
<instance part="C47" gate="G$1" x="350.52" y="157.48"/>
<instance part="P+23" gate="G$1" x="350.52" y="170.18"/>
<instance part="GND42" gate="1" x="353.06" y="147.32"/>
<instance part="R34" gate="G$1" x="320.04" y="162.56" rot="R270"/>
<instance part="R35" gate="G$1" x="307.34" y="162.56" rot="R270"/>
<instance part="P+24" gate="G$1" x="314.96" y="172.72"/>
<instance part="JP2" gate="G$1" x="132.08" y="91.44"/>
<instance part="JP3" gate="G$1" x="119.38" y="96.52" rot="R180"/>
<instance part="GND24" gate="1" x="144.78" y="91.44"/>
<instance part="P+17" gate="G$1" x="96.52" y="106.68"/>
<instance part="U3" gate="G$1" x="63.5" y="233.68"/>
<instance part="R1" gate="G$1" x="45.72" y="228.6" rot="R270"/>
<instance part="C4" gate="G$1" x="81.28" y="228.6"/>
<instance part="C1" gate="G$1" x="91.44" y="226.06"/>
<instance part="R23" gate="G$1" x="101.6" y="226.06" rot="R90"/>
<instance part="STAT5" gate="G$1" x="101.6" y="215.9"/>
<instance part="PWM_1" gate="G$1" x="111.76" y="35.56"/>
<instance part="PWM_2" gate="G$1" x="111.76" y="15.24"/>
<instance part="P+29" gate="1" x="152.4" y="203.2"/>
<instance part="GND1" gate="1" x="152.4" y="190.5"/>
<instance part="BATT1" gate="G$1" x="167.64" y="198.12"/>
<instance part="I2C0" gate="G$1" x="12.7" y="101.6"/>
<instance part="I2C1" gate="G$1" x="12.7" y="78.74"/>
<instance part="I2C2" gate="G$1" x="12.7" y="127"/>
<instance part="GND22" gate="1" x="35.56" y="76.2"/>
<instance part="GND32" gate="1" x="35.56" y="99.06"/>
<instance part="GND33" gate="1" x="35.56" y="124.46"/>
<instance part="P+32" gate="G$1" x="35.56" y="86.36"/>
<instance part="P+33" gate="G$1" x="35.56" y="109.22"/>
<instance part="P+34" gate="G$1" x="35.56" y="137.16"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="XBEE" gate="G$1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="101.6" y1="121.92" x2="96.52" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="48.26" y1="195.58" x2="53.34" y2="195.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="195.58" x2="53.34" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@5"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="246.38" y1="78.74" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<wire x1="241.3" y1="78.74" x2="241.3" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND@4"/>
<wire x1="246.38" y1="81.28" x2="241.3" y2="81.28" width="0.1524" layer="91"/>
<wire x1="241.3" y1="81.28" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<junction x="241.3" y="78.74"/>
<pinref part="U2" gate="G$1" pin="GND@2"/>
<wire x1="246.38" y1="83.82" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<wire x1="241.3" y1="83.82" x2="241.3" y2="81.28" width="0.1524" layer="91"/>
<junction x="241.3" y="81.28"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="241.3" y1="88.9" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<junction x="241.3" y="83.82"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SDO"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="269.24" y1="96.52" x2="274.32" y2="96.52" width="0.1524" layer="91"/>
<wire x1="274.32" y1="96.52" x2="274.32" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="274.32" y1="86.36" x2="274.32" y2="76.2" width="0.1524" layer="91"/>
<wire x1="292.1" y1="86.36" x2="274.32" y2="86.36" width="0.1524" layer="91"/>
<junction x="274.32" y="86.36"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="317.5" y1="71.12" x2="317.5" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="312.42" y1="73.66" x2="312.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="312.42" y1="71.12" x2="317.5" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND@2"/>
<wire x1="320.04" y1="76.2" x2="317.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND@1"/>
<wire x1="320.04" y1="78.74" x2="317.5" y2="78.74" width="0.1524" layer="91"/>
<wire x1="317.5" y1="78.74" x2="317.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="317.5" y="76.2"/>
<wire x1="317.5" y1="76.2" x2="317.5" y2="71.12" width="0.1524" layer="91"/>
<junction x="317.5" y="71.12"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="365.76" y1="86.36" x2="365.76" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="167.64" y1="78.74" x2="167.64" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DEBUGPINS" gate="G$1" pin="1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="17.78" y1="50.8" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="30.48" y1="50.8" x2="30.48" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="152.4" y1="220.98" x2="152.4" y2="218.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="220.98" x2="157.48" y2="220.98" width="0.1524" layer="91"/>
<pinref part="BATT" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="U$9" gate="G$1" pin="GND"/>
<wire x1="195.58" y1="101.6" x2="187.96" y2="101.6" width="0.1524" layer="91"/>
<wire x1="187.96" y1="101.6" x2="187.96" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="195.58" y1="38.1" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="190.5" y1="38.1" x2="190.5" y2="15.24" width="0.1524" layer="91"/>
<wire x1="195.58" y1="40.64" x2="190.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="190.5" y1="40.64" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<junction x="190.5" y="38.1"/>
<pinref part="GYRO" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="GYRO" gate="G$1" pin="RESV-G"/>
</segment>
<segment>
<wire x1="180.34" y1="15.24" x2="180.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="241.3" y1="25.4" x2="241.3" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C9" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="170.18" y1="15.24" x2="170.18" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="160.02" y1="15.24" x2="160.02" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="236.22" y1="15.24" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="223.52" y1="38.1" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
<label x="228.6" y="35.56" size="1.778" layer="95"/>
<pinref part="GYRO" gate="G$1" pin="CLKIN"/>
<pinref part="GYRO" gate="G$1" pin="AD0"/>
<wire x1="223.52" y1="40.64" x2="236.22" y2="40.64" width="0.1524" layer="91"/>
<wire x1="236.22" y1="40.64" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
<junction x="236.22" y="38.1"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="350.52" y1="215.9" x2="353.06" y2="215.9" width="0.1524" layer="91"/>
<wire x1="353.06" y1="215.9" x2="353.06" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="GND"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="327.66" y1="198.12" x2="325.12" y2="198.12" width="0.1524" layer="91"/>
<wire x1="325.12" y1="198.12" x2="325.12" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="350.52" y1="154.94" x2="353.06" y2="154.94" width="0.1524" layer="91"/>
<wire x1="353.06" y1="154.94" x2="353.06" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="GND"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="327.66" y1="137.16" x2="325.12" y2="137.16" width="0.1524" layer="91"/>
<wire x1="325.12" y1="137.16" x2="325.12" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="6"/>
<wire x1="137.16" y1="99.06" x2="139.7" y2="99.06" width="0.1524" layer="91"/>
<wire x1="139.7" y1="99.06" x2="144.78" y2="99.06" width="0.1524" layer="91"/>
<wire x1="144.78" y1="99.06" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="4"/>
<wire x1="137.16" y1="93.98" x2="139.7" y2="93.98" width="0.1524" layer="91"/>
<wire x1="139.7" y1="93.98" x2="139.7" y2="99.06" width="0.1524" layer="91"/>
<junction x="139.7" y="99.06"/>
<pinref part="JP2" gate="G$1" pin="3"/>
<wire x1="137.16" y1="91.44" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
<wire x1="139.7" y1="91.44" x2="139.7" y2="93.98" width="0.1524" layer="91"/>
<junction x="139.7" y="93.98"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="137.16" y1="88.9" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
<wire x1="139.7" y1="88.9" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
<junction x="139.7" y="91.44"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="63.5" y1="223.52" x2="63.5" y2="215.9" width="0.1524" layer="91"/>
<pinref part="S1" gate="1" pin="P"/>
<wire x1="63.5" y1="215.9" x2="63.5" y2="208.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="215.9" x2="63.5" y2="215.9" width="0.1524" layer="91"/>
<junction x="63.5" y="215.9"/>
<wire x1="27.94" y1="223.52" x2="10.16" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="-"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="10.16" y1="226.06" x2="10.16" y2="223.52" width="0.1524" layer="91"/>
<wire x1="27.94" y1="223.52" x2="27.94" y2="208.28" width="0.1524" layer="91"/>
<junction x="27.94" y="223.52"/>
<wire x1="27.94" y1="208.28" x2="63.5" y2="208.28" width="0.1524" layer="91"/>
<junction x="63.5" y="208.28"/>
<wire x1="63.5" y1="215.9" x2="81.28" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="-"/>
<wire x1="81.28" y1="215.9" x2="81.28" y2="218.44" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="81.28" y1="218.44" x2="81.28" y2="223.52" width="0.1524" layer="91"/>
<wire x1="91.44" y1="223.52" x2="91.44" y2="218.44" width="0.1524" layer="91"/>
<wire x1="91.44" y1="218.44" x2="81.28" y2="218.44" width="0.1524" layer="91"/>
<junction x="81.28" y="218.44"/>
<pinref part="STAT5" gate="G$1" pin="C"/>
<wire x1="63.5" y1="208.28" x2="101.6" y2="208.28" width="0.1524" layer="91"/>
<wire x1="101.6" y1="208.28" x2="101.6" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="152.4" y1="195.58" x2="152.4" y2="193.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="195.58" x2="157.48" y2="195.58" width="0.1524" layer="91"/>
<pinref part="BATT1" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="I2C0" gate="G$1" pin="3"/>
<label x="20.32" y="104.14" size="1.778" layer="95"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="17.78" y1="104.14" x2="35.56" y2="104.14" width="0.1524" layer="91"/>
<wire x1="35.56" y1="104.14" x2="35.56" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C2" gate="G$1" pin="3"/>
<label x="20.32" y="129.54" size="1.778" layer="95"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="17.78" y1="129.54" x2="35.56" y2="129.54" width="0.1524" layer="91"/>
<wire x1="35.56" y1="129.54" x2="35.56" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERIAL1" gate="G$1" pin="3"/>
<label x="88.9" y="55.88" size="1.778" layer="95"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="86.36" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<wire x1="99.06" y1="55.88" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C1" gate="G$1" pin="3"/>
<label x="20.32" y="81.28" size="1.778" layer="95"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="17.78" y1="81.28" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="35.56" y1="81.28" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<wire x1="373.38" y1="243.84" x2="373.38" y2="246.38" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="P+9" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<wire x1="360.68" y1="246.38" x2="360.68" y2="243.84" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="P+8" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="XBEE" gate="G$1" pin="VDD"/>
<wire x1="93.98" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<pinref part="P+11" gate="G$1" pin="3.3V"/>
<wire x1="93.98" y1="144.78" x2="93.98" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERIAL1" gate="G$1" pin="4"/>
<pinref part="P+10" gate="G$1" pin="3.3V"/>
<wire x1="86.36" y1="58.42" x2="91.44" y2="58.42" width="0.1524" layer="91"/>
<wire x1="91.44" y1="58.42" x2="91.44" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VSS"/>
<pinref part="P+12" gate="G$1" pin="3.3V"/>
<wire x1="246.38" y1="99.06" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="241.3" y1="99.06" x2="241.3" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VDD"/>
<wire x1="241.3" y1="101.6" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<wire x1="246.38" y1="101.6" x2="241.3" y2="101.6" width="0.1524" layer="91"/>
<junction x="241.3" y="101.6"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="241.3" y1="99.06" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<junction x="241.3" y="99.06"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="CS"/>
<pinref part="P+14" gate="G$1" pin="3.3V"/>
<wire x1="269.24" y1="101.6" x2="274.32" y2="101.6" width="0.1524" layer="91"/>
<wire x1="274.32" y1="101.6" x2="274.32" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="292.1" y1="93.98" x2="292.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="292.1" y1="101.6" x2="274.32" y2="101.6" width="0.1524" layer="91"/>
<junction x="274.32" y="101.6"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="S1"/>
<pinref part="P+6" gate="G$1" pin="3.3V"/>
<wire x1="350.52" y1="93.98" x2="353.06" y2="93.98" width="0.1524" layer="91"/>
<wire x1="353.06" y1="93.98" x2="353.06" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VDDIO"/>
<wire x1="353.06" y1="96.52" x2="353.06" y2="99.06" width="0.1524" layer="91"/>
<wire x1="353.06" y1="99.06" x2="353.06" y2="106.68" width="0.1524" layer="91"/>
<wire x1="350.52" y1="96.52" x2="353.06" y2="96.52" width="0.1524" layer="91"/>
<junction x="353.06" y="96.52"/>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<wire x1="350.52" y1="99.06" x2="353.06" y2="99.06" width="0.1524" layer="91"/>
<junction x="353.06" y="99.06"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="365.76" y1="96.52" x2="353.06" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="P+16" gate="G$1" pin="3.3V"/>
<wire x1="167.64" y1="86.36" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="VDDIO"/>
<wire x1="167.64" y1="88.9" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<wire x1="172.72" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="86.36" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="VDD"/>
<wire x1="172.72" y1="88.9" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<wire x1="170.18" y1="88.9" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<junction x="170.18" y="88.9"/>
<junction x="167.64" y="88.9"/>
</segment>
<segment>
<wire x1="195.58" y1="53.34" x2="190.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="190.5" y1="53.34" x2="190.5" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GYRO" gate="G$1" pin="VDD"/>
<label x="190.5" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="241.3" y1="43.18" x2="241.3" y2="38.1" width="0.1524" layer="91"/>
<label x="241.3" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="195.58" y1="48.26" x2="160.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="160.02" y1="48.26" x2="160.02" y2="38.1" width="0.1524" layer="91"/>
<label x="182.88" y="53.34" size="1.778" layer="95"/>
<pinref part="GYRO" gate="G$1" pin="VLOGIC"/>
<pinref part="C11" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<pinref part="P+13" gate="G$1" pin="3.3V"/>
<wire x1="350.52" y1="223.52" x2="350.52" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="VCC"/>
<wire x1="350.52" y1="226.06" x2="350.52" y2="231.14" width="0.1524" layer="91"/>
<wire x1="347.98" y1="208.28" x2="347.98" y2="215.9" width="0.1524" layer="91"/>
<wire x1="347.98" y1="215.9" x2="345.44" y2="215.9" width="0.1524" layer="91"/>
<wire x1="345.44" y1="215.9" x2="345.44" y2="226.06" width="0.1524" layer="91"/>
<wire x1="345.44" y1="226.06" x2="350.52" y2="226.06" width="0.1524" layer="91"/>
<junction x="350.52" y="226.06"/>
</segment>
<segment>
<pinref part="P+15" gate="G$1" pin="3.3V"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="314.96" y1="233.68" x2="307.34" y2="233.68" width="0.1524" layer="91"/>
<wire x1="307.34" y1="233.68" x2="307.34" y2="228.6" width="0.1524" layer="91"/>
<wire x1="314.96" y1="233.68" x2="320.04" y2="233.68" width="0.1524" layer="91"/>
<junction x="314.96" y="233.68"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="320.04" y1="233.68" x2="320.04" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="373.38" y1="182.88" x2="373.38" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<pinref part="P+22" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<wire x1="360.68" y1="185.42" x2="360.68" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="P+21" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<pinref part="P+23" gate="G$1" pin="3.3V"/>
<wire x1="350.52" y1="162.56" x2="350.52" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="VCC"/>
<wire x1="350.52" y1="165.1" x2="350.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="347.98" y1="147.32" x2="347.98" y2="154.94" width="0.1524" layer="91"/>
<wire x1="347.98" y1="154.94" x2="345.44" y2="154.94" width="0.1524" layer="91"/>
<wire x1="345.44" y1="154.94" x2="345.44" y2="165.1" width="0.1524" layer="91"/>
<wire x1="345.44" y1="165.1" x2="350.52" y2="165.1" width="0.1524" layer="91"/>
<junction x="350.52" y="165.1"/>
</segment>
<segment>
<pinref part="P+24" gate="G$1" pin="3.3V"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="314.96" y1="172.72" x2="307.34" y2="172.72" width="0.1524" layer="91"/>
<wire x1="307.34" y1="172.72" x2="307.34" y2="167.64" width="0.1524" layer="91"/>
<wire x1="314.96" y1="172.72" x2="320.04" y2="172.72" width="0.1524" layer="91"/>
<junction x="314.96" y="172.72"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="320.04" y1="172.72" x2="320.04" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="3"/>
<wire x1="114.3" y1="96.52" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="96.52" y1="96.52" x2="96.52" y2="106.68" width="0.1524" layer="91"/>
<pinref part="P+17" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="P+5" gate="G$1" pin="3.3V"/>
<wire x1="81.28" y1="236.22" x2="91.44" y2="236.22" width="0.1524" layer="91"/>
<wire x1="91.44" y1="236.22" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="238.76" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="OUT"/>
<wire x1="73.66" y1="236.22" x2="81.28" y2="236.22" width="0.1524" layer="91"/>
<junction x="81.28" y="236.22"/>
<pinref part="C4" gate="G$1" pin="+"/>
<wire x1="81.28" y1="231.14" x2="81.28" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="91.44" y1="231.14" x2="91.44" y2="236.22" width="0.1524" layer="91"/>
<junction x="91.44" y="236.22"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="101.6" y1="231.14" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<junction x="101.6" y="236.22"/>
</segment>
<segment>
<pinref part="I2C0" gate="G$1" pin="4"/>
<label x="20.32" y="106.68" size="1.778" layer="95"/>
<pinref part="P+33" gate="G$1" pin="3.3V"/>
<wire x1="17.78" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<wire x1="35.56" y1="106.68" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C2" gate="G$1" pin="4"/>
<label x="20.32" y="132.08" size="1.778" layer="95"/>
<pinref part="P+34" gate="G$1" pin="3.3V"/>
<wire x1="17.78" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<wire x1="35.56" y1="132.08" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C1" gate="G$1" pin="4"/>
<label x="20.32" y="83.82" size="1.778" layer="95"/>
<pinref part="P+32" gate="G$1" pin="3.3V"/>
<wire x1="17.78" y1="83.82" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<wire x1="35.56" y1="83.82" x2="35.56" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<label x="358.14" y="213.36" size="1.778" layer="95"/>
<pinref part="STAT1" gate="G$1" pin="C"/>
<pinref part="U$12" gate="G$1" pin="Y2"/>
<wire x1="347.98" y1="203.2" x2="360.68" y2="203.2" width="0.1524" layer="91"/>
<wire x1="360.68" y1="203.2" x2="360.68" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<label x="370.84" y="213.36" size="1.778" layer="95"/>
<pinref part="STAT2" gate="G$1" pin="C"/>
<pinref part="U$12" gate="G$1" pin="Y1"/>
<wire x1="347.98" y1="198.12" x2="373.38" y2="198.12" width="0.1524" layer="91"/>
<wire x1="373.38" y1="198.12" x2="373.38" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<wire x1="116.84" y1="15.24" x2="121.92" y2="15.24" width="0.1524" layer="91"/>
<label x="116.84" y="15.24" size="1.778" layer="95"/>
<pinref part="PWM_2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<wire x1="116.84" y1="20.32" x2="121.92" y2="20.32" width="0.1524" layer="91"/>
<label x="116.84" y="20.32" size="1.778" layer="95"/>
<pinref part="PWM_2" gate="G$1" pin="4"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<wire x1="116.84" y1="33.02" x2="121.92" y2="33.02" width="0.1524" layer="91"/>
<label x="116.84" y="33.02" size="1.778" layer="95"/>
<pinref part="PWM_1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<wire x1="116.84" y1="35.56" x2="121.92" y2="35.56" width="0.1524" layer="91"/>
<label x="116.84" y="35.56" size="1.778" layer="95"/>
<pinref part="PWM_1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<wire x1="116.84" y1="38.1" x2="121.92" y2="38.1" width="0.1524" layer="91"/>
<label x="116.84" y="38.1" size="1.778" layer="95"/>
<pinref part="PWM_1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<wire x1="116.84" y1="17.78" x2="121.92" y2="17.78" width="0.1524" layer="91"/>
<label x="116.84" y="17.78" size="1.778" layer="95"/>
<pinref part="PWM_2" gate="G$1" pin="3"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<wire x1="116.84" y1="12.7" x2="121.92" y2="12.7" width="0.1524" layer="91"/>
<label x="116.84" y="12.7" size="1.778" layer="95"/>
<pinref part="PWM_2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<wire x1="116.84" y1="40.64" x2="121.92" y2="40.64" width="0.1524" layer="91"/>
<label x="116.84" y="40.64" size="1.778" layer="95"/>
<pinref part="PWM_1" gate="G$1" pin="4"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="27.94" y1="231.14" x2="27.94" y2="236.22" width="0.1524" layer="91"/>
<wire x1="27.94" y1="236.22" x2="10.16" y2="236.22" width="0.1524" layer="91"/>
<wire x1="10.16" y1="236.22" x2="10.16" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="+"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="10.16" y1="233.68" x2="10.16" y2="236.22" width="0.1524" layer="91"/>
<junction x="10.16" y="236.22"/>
<pinref part="P+18" gate="1" pin="VCC"/>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="27.94" y1="236.22" x2="45.72" y2="236.22" width="0.1524" layer="91"/>
<junction x="27.94" y="236.22"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="45.72" y1="236.22" x2="53.34" y2="236.22" width="0.1524" layer="91"/>
<wire x1="45.72" y1="233.68" x2="45.72" y2="236.22" width="0.1524" layer="91"/>
<junction x="45.72" y="236.22"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="17.78" y1="195.58" x2="10.16" y2="195.58" width="0.1524" layer="91"/>
<wire x1="10.16" y1="195.58" x2="10.16" y2="203.2" width="0.1524" layer="91"/>
<pinref part="P+19" gate="1" pin="VCC"/>
</segment>
<segment>
<pinref part="P+20" gate="1" pin="VCC"/>
<wire x1="152.4" y1="226.06" x2="152.4" y2="228.6" width="0.1524" layer="91"/>
<wire x1="157.48" y1="226.06" x2="152.4" y2="226.06" width="0.1524" layer="91"/>
<pinref part="BATT" gate="G$1" pin="+"/>
</segment>
<segment>
<pinref part="P+29" gate="1" pin="VCC"/>
<wire x1="152.4" y1="200.66" x2="152.4" y2="203.2" width="0.1524" layer="91"/>
<wire x1="157.48" y1="200.66" x2="152.4" y2="200.66" width="0.1524" layer="91"/>
<pinref part="BATT1" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="312.42" y1="83.82" x2="312.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="C1"/>
<wire x1="320.04" y1="83.82" x2="312.42" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="SETP"/>
<wire x1="358.14" y1="81.28" x2="350.52" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SETC"/>
<wire x1="350.52" y1="78.74" x2="353.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="353.06" y1="78.74" x2="353.06" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="353.06" y1="71.12" x2="358.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="358.14" y1="71.12" x2="358.14" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="195.58" y1="43.18" x2="180.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="43.18" x2="180.34" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GYRO" gate="G$1" pin="CPOUT"/>
<pinref part="C2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="195.58" y1="45.72" x2="170.18" y2="45.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="45.72" x2="170.18" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GYRO" gate="G$1" pin="REGOUT"/>
<pinref part="C10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="STAT2" gate="G$1" pin="A"/>
<wire x1="373.38" y1="233.68" x2="373.38" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="STAT1" gate="G$1" pin="A"/>
<wire x1="360.68" y1="233.68" x2="360.68" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_1" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="A2"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="327.66" y1="208.28" x2="307.34" y2="208.28" width="0.1524" layer="91"/>
<wire x1="307.34" y1="208.28" x2="307.34" y2="218.44" width="0.1524" layer="91"/>
<wire x1="299.72" y1="208.28" x2="307.34" y2="208.28" width="0.1524" layer="91"/>
<junction x="307.34" y="208.28"/>
<label x="299.72" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="STAT4" gate="G$1" pin="A"/>
<wire x1="373.38" y1="172.72" x2="373.38" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="STAT3" gate="G$1" pin="A"/>
<wire x1="360.68" y1="172.72" x2="360.68" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_4" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="320.04" y1="157.48" x2="320.04" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="A1"/>
<wire x1="320.04" y1="142.24" x2="327.66" y2="142.24" width="0.1524" layer="91"/>
<wire x1="320.04" y1="142.24" x2="299.72" y2="142.24" width="0.1524" layer="91"/>
<junction x="320.04" y="142.24"/>
<label x="299.72" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_3" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="A2"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="327.66" y1="147.32" x2="307.34" y2="147.32" width="0.1524" layer="91"/>
<wire x1="307.34" y1="147.32" x2="307.34" y2="157.48" width="0.1524" layer="91"/>
<wire x1="299.72" y1="147.32" x2="307.34" y2="147.32" width="0.1524" layer="91"/>
<junction x="307.34" y="147.32"/>
<label x="299.72" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="106.68" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<label x="106.68" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="106.68" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<label x="106.68" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="4"/>
<wire x1="106.68" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<label x="106.68" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="5"/>
<wire x1="106.68" y1="91.44" x2="111.76" y2="91.44" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="6"/>
<wire x1="111.76" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="88.9" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="88.9" x2="111.76" y2="91.44" width="0.1524" layer="91"/>
<junction x="111.76" y="91.44"/>
<label x="106.68" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="EMU0" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="7"/>
<wire x1="106.68" y1="86.36" x2="114.3" y2="86.36" width="0.1524" layer="91"/>
<label x="106.68" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="TRSTN" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="7"/>
<wire x1="144.78" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<label x="142.24" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="EMU1" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="144.78" y1="86.36" x2="137.16" y2="86.36" width="0.1524" layer="91"/>
<label x="142.24" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="INHIB"/>
<wire x1="53.34" y1="228.6" x2="50.8" y2="228.6" width="0.1524" layer="91"/>
<wire x1="50.8" y1="228.6" x2="50.8" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="45.72" y1="223.52" x2="50.8" y2="223.52" width="0.1524" layer="91"/>
<pinref part="S1" gate="1" pin="S"/>
<wire x1="45.72" y1="223.52" x2="45.72" y2="218.44" width="0.1524" layer="91"/>
<junction x="45.72" y="223.52"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="STAT5" gate="G$1" pin="A"/>
<wire x1="101.6" y1="220.98" x2="101.6" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="U1RX" class="0">
<segment>
<pinref part="SERIAL1" gate="G$1" pin="2"/>
<wire x1="91.44" y1="53.34" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
<label x="88.9" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="U1TX" class="0">
<segment>
<pinref part="SERIAL1" gate="G$1" pin="1"/>
<wire x1="86.36" y1="50.8" x2="91.44" y2="50.8" width="0.1524" layer="91"/>
<label x="88.9" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C0SDA" class="0">
<segment>
<wire x1="223.52" y1="53.34" x2="238.76" y2="53.34" width="0.1524" layer="91"/>
<label x="231.14" y="53.34" size="1.778" layer="95"/>
<pinref part="GYRO" gate="G$1" pin="SDA"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="269.24" y1="93.98" x2="279.4" y2="93.98" width="0.1524" layer="91"/>
<label x="276.86" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C0" gate="G$1" pin="1"/>
<wire x1="25.4" y1="99.06" x2="17.78" y2="99.06" width="0.1524" layer="91"/>
<label x="20.32" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C0SCL" class="0">
<segment>
<wire x1="223.52" y1="50.8" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<label x="231.14" y="50.8" size="1.778" layer="95"/>
<pinref part="GYRO" gate="G$1" pin="SCL"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="279.4" y1="91.44" x2="269.24" y2="91.44" width="0.1524" layer="91"/>
<label x="276.86" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C0" gate="G$1" pin="2"/>
<wire x1="25.4" y1="101.6" x2="17.78" y2="101.6" width="0.1524" layer="91"/>
<label x="20.32" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1SCL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SCL"/>
<wire x1="353.06" y1="88.9" x2="350.52" y2="88.9" width="0.1524" layer="91"/>
<label x="353.06" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="SCL"/>
<wire x1="208.28" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<label x="208.28" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C1" gate="G$1" pin="2"/>
<wire x1="25.4" y1="78.74" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<label x="20.32" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1SDA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SDA"/>
<wire x1="353.06" y1="86.36" x2="350.52" y2="86.36" width="0.1524" layer="91"/>
<label x="353.06" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="SDA"/>
<wire x1="208.28" y1="91.44" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<label x="208.28" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<label x="20.32" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="U0RX" class="0">
<segment>
<pinref part="XBEE" gate="G$1" pin="DOUT"/>
<wire x1="96.52" y1="142.24" x2="101.6" y2="142.24" width="0.1524" layer="91"/>
<label x="91.44" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="U0TX" class="0">
<segment>
<pinref part="XBEE" gate="G$1" pin="DIN"/>
<wire x1="96.52" y1="139.7" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<label x="91.44" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<label x="358.14" y="152.4" size="1.778" layer="95"/>
<pinref part="STAT3" gate="G$1" pin="C"/>
<pinref part="U$14" gate="G$1" pin="Y2"/>
<wire x1="347.98" y1="142.24" x2="360.68" y2="142.24" width="0.1524" layer="91"/>
<wire x1="360.68" y1="142.24" x2="360.68" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED4" class="0">
<segment>
<label x="370.84" y="152.4" size="1.778" layer="95"/>
<pinref part="STAT4" gate="G$1" pin="C"/>
<pinref part="U$14" gate="G$1" pin="Y1"/>
<wire x1="347.98" y1="137.16" x2="373.38" y2="137.16" width="0.1524" layer="91"/>
<wire x1="373.38" y1="137.16" x2="373.38" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_2" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="320.04" y1="218.44" x2="320.04" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="A1"/>
<wire x1="320.04" y1="203.2" x2="327.66" y2="203.2" width="0.1524" layer="91"/>
<wire x1="320.04" y1="203.2" x2="299.72" y2="203.2" width="0.1524" layer="91"/>
<junction x="320.04" y="203.2"/>
<label x="299.72" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB3_GPIO11" class="0">
<segment>
<pinref part="DEBUGPINS" gate="G$1" pin="2"/>
<wire x1="25.4" y1="53.34" x2="17.78" y2="53.34" width="0.1524" layer="91"/>
<label x="25.4" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB4_GPIO12" class="0">
<segment>
<pinref part="DEBUGPINS" gate="G$1" pin="3"/>
<wire x1="25.4" y1="55.88" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
<label x="25.4" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB5_GPIO13" class="0">
<segment>
<pinref part="DEBUGPINS" gate="G$1" pin="4"/>
<wire x1="25.4" y1="58.42" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
<label x="25.4" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC2INB0" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="27.94" y1="195.58" x2="33.02" y2="195.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="195.58" x2="38.1" y2="195.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="187.96" x2="33.02" y2="195.58" width="0.1524" layer="91"/>
<junction x="33.02" y="195.58"/>
<label x="33.02" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2SCL" class="0">
<segment>
<pinref part="I2C2" gate="G$1" pin="2"/>
<wire x1="25.4" y1="127" x2="17.78" y2="127" width="0.1524" layer="91"/>
<label x="20.32" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2SDA" class="0">
<segment>
<pinref part="I2C2" gate="G$1" pin="1"/>
<wire x1="25.4" y1="124.46" x2="17.78" y2="124.46" width="0.1524" layer="91"/>
<label x="20.32" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="353.06" y="251.46" size="3.81" layer="94" ratio="10">Processor</text>
<text x="239.014" y="215.138" size="1.778" layer="97">These caps/inductors</text>
<text x="239.014" y="212.598" size="1.778" layer="97">should be placed</text>
<text x="239.014" y="210.058" size="1.778" layer="97">at VDDx inputs</text>
<text x="91.694" y="227.838" size="1.778" layer="97">Place one cap at</text>
<text x="91.694" y="225.298" size="1.778" layer="97">each corner of device</text>
</plain>
<instances>
<instance part="U$8" gate="G$1" x="180.34" y="119.38"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="C16" gate="G$1" x="330.2" y="223.52"/>
<instance part="C17" gate="G$1" x="320.04" y="223.52"/>
<instance part="C18" gate="G$1" x="309.88" y="223.52"/>
<instance part="C19" gate="G$1" x="299.72" y="223.52"/>
<instance part="C20" gate="G$1" x="289.56" y="223.52"/>
<instance part="C21" gate="G$1" x="279.4" y="223.52"/>
<instance part="C22" gate="G$1" x="269.24" y="223.52"/>
<instance part="C23" gate="G$1" x="259.08" y="223.52"/>
<instance part="C24" gate="G$1" x="248.92" y="223.52"/>
<instance part="C25" gate="G$1" x="238.76" y="223.52"/>
<instance part="C26" gate="G$1" x="228.6" y="223.52"/>
<instance part="C27" gate="G$1" x="218.44" y="223.52"/>
<instance part="C28" gate="G$1" x="208.28" y="223.52"/>
<instance part="C29" gate="G$1" x="198.12" y="223.52"/>
<instance part="C30" gate="G$1" x="187.96" y="223.52"/>
<instance part="C31" gate="G$1" x="177.8" y="223.52"/>
<instance part="C32" gate="G$1" x="167.64" y="223.52"/>
<instance part="C33" gate="G$1" x="157.48" y="223.52"/>
<instance part="C34" gate="G$1" x="147.32" y="223.52"/>
<instance part="L1" gate="G$1" x="147.32" y="243.84"/>
<instance part="L2" gate="G$1" x="157.48" y="243.84"/>
<instance part="L3" gate="G$1" x="167.64" y="243.84"/>
<instance part="L4" gate="G$1" x="177.8" y="243.84"/>
<instance part="L5" gate="G$1" x="187.96" y="243.84"/>
<instance part="L6" gate="G$1" x="198.12" y="243.84"/>
<instance part="L7" gate="G$1" x="208.28" y="243.84"/>
<instance part="L8" gate="G$1" x="218.44" y="243.84"/>
<instance part="L9" gate="G$1" x="228.6" y="243.84"/>
<instance part="L10" gate="G$1" x="238.76" y="243.84"/>
<instance part="L11" gate="G$1" x="248.92" y="243.84"/>
<instance part="L12" gate="G$1" x="259.08" y="243.84"/>
<instance part="L13" gate="G$1" x="269.24" y="243.84"/>
<instance part="L14" gate="G$1" x="279.4" y="243.84"/>
<instance part="L15" gate="G$1" x="289.56" y="243.84"/>
<instance part="L16" gate="G$1" x="299.72" y="243.84"/>
<instance part="L17" gate="G$1" x="309.88" y="243.84"/>
<instance part="L18" gate="G$1" x="320.04" y="243.84"/>
<instance part="L19" gate="G$1" x="330.2" y="243.84"/>
<instance part="P+3" gate="G$1" x="137.16" y="254"/>
<instance part="GND6" gate="1" x="340.36" y="213.36"/>
<instance part="C35" gate="G$1" x="124.46" y="241.3"/>
<instance part="C36" gate="G$1" x="114.3" y="241.3"/>
<instance part="C37" gate="G$1" x="104.14" y="241.3"/>
<instance part="C38" gate="G$1" x="93.98" y="241.3"/>
<instance part="GND27" gate="1" x="83.82" y="228.6"/>
<instance part="U$11" gate="G$1" x="86.36" y="38.1"/>
<instance part="GND34" gate="1" x="71.12" y="27.94"/>
<instance part="C42" gate="G$1" x="96.52" y="58.42"/>
<instance part="GND35" gate="1" x="96.52" y="50.8"/>
<instance part="U1" gate="G$1" x="50.8" y="35.56"/>
<instance part="GND36" gate="1" x="38.1" y="10.16"/>
<instance part="R27" gate="G$1" x="30.48" y="25.4" rot="R270"/>
<instance part="R28" gate="G$1" x="40.64" y="58.42" rot="R270"/>
<instance part="R29" gate="G$1" x="33.02" y="58.42" rot="R270"/>
<instance part="C43" gate="G$1" x="15.24" y="58.42"/>
<instance part="GND37" gate="1" x="15.24" y="50.8"/>
<instance part="C44" gate="G$1" x="48.26" y="213.36"/>
<instance part="C45" gate="G$1" x="20.32" y="213.36"/>
<instance part="P+25" gate="G$1" x="27.94" y="190.5"/>
<instance part="GND28" gate="1" x="27.94" y="147.32"/>
<instance part="C39" gate="G$1" x="38.1" y="160.02"/>
<instance part="C40" gate="G$1" x="15.24" y="160.02"/>
<instance part="R7" gate="G$1" x="27.94" y="180.34" rot="R270"/>
<instance part="R9" gate="G$1" x="15.24" y="124.46" rot="R270"/>
<instance part="R13" gate="G$1" x="22.86" y="124.46" rot="R270"/>
<instance part="R15" gate="G$1" x="30.48" y="124.46" rot="R270"/>
<instance part="R21" gate="G$1" x="38.1" y="124.46" rot="R270"/>
<instance part="P+26" gate="G$1" x="27.94" y="134.62"/>
<instance part="P+2" gate="G$1" x="276.86" y="147.32"/>
<instance part="R2" gate="G$1" x="269.24" y="134.62" rot="R270"/>
<instance part="R4" gate="G$1" x="276.86" y="134.62" rot="R270"/>
<instance part="R8" gate="G$1" x="284.48" y="134.62" rot="R270"/>
<instance part="R22" gate="G$1" x="271.78" y="101.6" rot="R90"/>
<instance part="GND4" gate="1" x="271.78" y="91.44"/>
<instance part="P+7" gate="G$1" x="25.4" y="71.12"/>
<instance part="P+27" gate="G$1" x="106.68" y="71.12"/>
<instance part="GND29" gate="1" x="256.54" y="149.86"/>
<instance part="GND30" gate="1" x="226.06" y="203.2"/>
<instance part="GND31" gate="1" x="170.18" y="114.3"/>
<instance part="CRYSTAL" gate="G$1" x="35.56" y="226.06"/>
<instance part="R3" gate="G$1" x="104.14" y="119.38" rot="R270"/>
<instance part="R5" gate="G$1" x="96.52" y="119.38" rot="R270"/>
<instance part="P+4" gate="G$1" x="99.06" y="129.54"/>
<instance part="P+28" gate="G$1" x="289.56" y="86.36"/>
<instance part="R6" gate="G$1" x="294.64" y="78.74" rot="R270"/>
<instance part="R12" gate="G$1" x="284.48" y="78.74" rot="R270"/>
<instance part="R16" gate="G$1" x="162.56" y="208.28"/>
<instance part="C7" gate="G$1" x="147.32" y="205.74"/>
<instance part="GND3" gate="1" x="147.32" y="198.12"/>
<instance part="P+1" gate="G$1" x="269.24" y="190.5"/>
<instance part="R17" gate="G$1" x="274.32" y="182.88" rot="R270"/>
<instance part="R18" gate="G$1" x="264.16" y="182.88" rot="R270"/>
<instance part="GND10" gate="1" x="208.28" y="195.58"/>
<instance part="GND12" gate="1" x="167.64" y="200.66"/>
<instance part="P+30" gate="G$1" x="177.8" y="200.66"/>
<instance part="P+31" gate="G$1" x="198.12" y="205.74"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="147.32" y1="220.98" x2="147.32" y2="218.44" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="147.32" y1="218.44" x2="157.48" y2="218.44" width="0.1524" layer="91"/>
<wire x1="157.48" y1="218.44" x2="167.64" y2="218.44" width="0.1524" layer="91"/>
<wire x1="167.64" y1="218.44" x2="177.8" y2="218.44" width="0.1524" layer="91"/>
<wire x1="177.8" y1="218.44" x2="187.96" y2="218.44" width="0.1524" layer="91"/>
<wire x1="187.96" y1="218.44" x2="198.12" y2="218.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="218.44" x2="208.28" y2="218.44" width="0.1524" layer="91"/>
<wire x1="208.28" y1="218.44" x2="218.44" y2="218.44" width="0.1524" layer="91"/>
<wire x1="218.44" y1="218.44" x2="228.6" y2="218.44" width="0.1524" layer="91"/>
<wire x1="228.6" y1="218.44" x2="238.76" y2="218.44" width="0.1524" layer="91"/>
<wire x1="238.76" y1="218.44" x2="248.92" y2="218.44" width="0.1524" layer="91"/>
<wire x1="248.92" y1="218.44" x2="259.08" y2="218.44" width="0.1524" layer="91"/>
<wire x1="259.08" y1="218.44" x2="269.24" y2="218.44" width="0.1524" layer="91"/>
<wire x1="269.24" y1="218.44" x2="279.4" y2="218.44" width="0.1524" layer="91"/>
<wire x1="279.4" y1="218.44" x2="289.56" y2="218.44" width="0.1524" layer="91"/>
<wire x1="289.56" y1="218.44" x2="299.72" y2="218.44" width="0.1524" layer="91"/>
<wire x1="299.72" y1="218.44" x2="309.88" y2="218.44" width="0.1524" layer="91"/>
<wire x1="309.88" y1="218.44" x2="320.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="320.04" y1="218.44" x2="330.2" y2="218.44" width="0.1524" layer="91"/>
<wire x1="330.2" y1="218.44" x2="340.36" y2="218.44" width="0.1524" layer="91"/>
<wire x1="340.36" y1="218.44" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="330.2" y1="220.98" x2="330.2" y2="218.44" width="0.1524" layer="91"/>
<junction x="330.2" y="218.44"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="320.04" y1="220.98" x2="320.04" y2="218.44" width="0.1524" layer="91"/>
<junction x="320.04" y="218.44"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="309.88" y1="220.98" x2="309.88" y2="218.44" width="0.1524" layer="91"/>
<junction x="309.88" y="218.44"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="299.72" y1="220.98" x2="299.72" y2="218.44" width="0.1524" layer="91"/>
<junction x="299.72" y="218.44"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="289.56" y1="220.98" x2="289.56" y2="218.44" width="0.1524" layer="91"/>
<junction x="289.56" y="218.44"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="279.4" y1="218.44" x2="279.4" y2="220.98" width="0.1524" layer="91"/>
<junction x="279.4" y="218.44"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="269.24" y1="220.98" x2="269.24" y2="218.44" width="0.1524" layer="91"/>
<junction x="269.24" y="218.44"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="259.08" y1="220.98" x2="259.08" y2="218.44" width="0.1524" layer="91"/>
<junction x="259.08" y="218.44"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="248.92" y1="220.98" x2="248.92" y2="218.44" width="0.1524" layer="91"/>
<junction x="248.92" y="218.44"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="238.76" y1="220.98" x2="238.76" y2="218.44" width="0.1524" layer="91"/>
<junction x="238.76" y="218.44"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="228.6" y1="220.98" x2="228.6" y2="218.44" width="0.1524" layer="91"/>
<junction x="228.6" y="218.44"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="218.44" y1="220.98" x2="218.44" y2="218.44" width="0.1524" layer="91"/>
<junction x="218.44" y="218.44"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="208.28" y1="220.98" x2="208.28" y2="218.44" width="0.1524" layer="91"/>
<junction x="208.28" y="218.44"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="198.12" y1="220.98" x2="198.12" y2="218.44" width="0.1524" layer="91"/>
<junction x="198.12" y="218.44"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="187.96" y1="220.98" x2="187.96" y2="218.44" width="0.1524" layer="91"/>
<junction x="187.96" y="218.44"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="177.8" y1="220.98" x2="177.8" y2="218.44" width="0.1524" layer="91"/>
<junction x="177.8" y="218.44"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="167.64" y1="220.98" x2="167.64" y2="218.44" width="0.1524" layer="91"/>
<junction x="167.64" y="218.44"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="157.48" y1="220.98" x2="157.48" y2="218.44" width="0.1524" layer="91"/>
<junction x="157.48" y="218.44"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="124.46" y1="238.76" x2="124.46" y2="233.68" width="0.1524" layer="91"/>
<wire x1="124.46" y1="233.68" x2="114.3" y2="233.68" width="0.1524" layer="91"/>
<wire x1="114.3" y1="233.68" x2="104.14" y2="233.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="233.68" x2="93.98" y2="233.68" width="0.1524" layer="91"/>
<wire x1="93.98" y1="233.68" x2="83.82" y2="233.68" width="0.1524" layer="91"/>
<wire x1="83.82" y1="233.68" x2="83.82" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="93.98" y1="238.76" x2="93.98" y2="233.68" width="0.1524" layer="91"/>
<junction x="93.98" y="233.68"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="104.14" y1="238.76" x2="104.14" y2="233.68" width="0.1524" layer="91"/>
<junction x="104.14" y="233.68"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="114.3" y1="238.76" x2="114.3" y2="233.68" width="0.1524" layer="91"/>
<junction x="114.3" y="233.68"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="VSS"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="73.66" y1="35.56" x2="71.12" y2="35.56" width="0.1524" layer="91"/>
<wire x1="71.12" y1="35.56" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="A2"/>
<wire x1="73.66" y1="38.1" x2="71.12" y2="38.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="38.1" x2="71.12" y2="35.56" width="0.1524" layer="91"/>
<junction x="71.12" y="35.56"/>
<pinref part="U$11" gate="G$1" pin="A1"/>
<wire x1="73.66" y1="40.64" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<wire x1="71.12" y1="40.64" x2="71.12" y2="38.1" width="0.1524" layer="91"/>
<junction x="71.12" y="38.1"/>
<pinref part="U$11" gate="G$1" pin="A0"/>
<wire x1="73.66" y1="43.18" x2="71.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="43.18" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<junction x="71.12" y="40.64"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="SHIELD@4"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="43.18" y1="17.78" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="17.78" x2="38.1" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SHIELD@3"/>
<wire x1="43.18" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<wire x1="38.1" y1="20.32" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<junction x="38.1" y="17.78"/>
<pinref part="U1" gate="G$1" pin="SHIELD@2"/>
<wire x1="43.18" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<junction x="38.1" y="20.32"/>
<pinref part="U1" gate="G$1" pin="SHIELD@1"/>
<wire x1="43.18" y1="25.4" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<wire x1="38.1" y1="25.4" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<junction x="38.1" y="22.86"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="30.48" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="43.18" y1="35.56" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="35.56" x2="25.4" y2="17.78" width="0.1524" layer="91"/>
<wire x1="25.4" y1="17.78" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="15.24" y1="55.88" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="38.1" y1="157.48" x2="38.1" y2="154.94" width="0.1524" layer="91"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="38.1" y1="154.94" x2="27.94" y2="154.94" width="0.1524" layer="91"/>
<wire x1="27.94" y1="154.94" x2="27.94" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="15.24" y1="157.48" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="15.24" y1="154.94" x2="27.94" y2="154.94" width="0.1524" layer="91"/>
<junction x="27.94" y="154.94"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="271.78" y1="96.52" x2="271.78" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VREG12EN"/>
<wire x1="241.3" y1="154.94" x2="256.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="256.54" y1="154.94" x2="256.54" y2="152.4" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VREG18EN"/>
<wire x1="215.9" y1="187.96" x2="215.9" y2="208.28" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="215.9" y1="208.28" x2="226.06" y2="208.28" width="0.1524" layer="91"/>
<wire x1="226.06" y1="208.28" x2="226.06" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PP_GND"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="175.26" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<wire x1="170.18" y1="119.38" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="147.32" y1="203.2" x2="147.32" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VSSA1"/>
<wire x1="203.2" y1="187.96" x2="203.2" y2="200.66" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="203.2" y1="200.66" x2="208.28" y2="200.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="200.66" x2="208.28" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VSSA2"/>
<wire x1="160.02" y1="187.96" x2="160.02" y2="205.74" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="160.02" y1="205.74" x2="167.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="167.64" y1="205.74" x2="167.64" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="P+3" gate="G$1" pin="3.3V"/>
<pinref part="L19" gate="G$1" pin="1"/>
<wire x1="137.16" y1="254" x2="147.32" y2="254" width="0.1524" layer="91"/>
<wire x1="147.32" y1="254" x2="157.48" y2="254" width="0.1524" layer="91"/>
<wire x1="157.48" y1="254" x2="167.64" y2="254" width="0.1524" layer="91"/>
<wire x1="167.64" y1="254" x2="177.8" y2="254" width="0.1524" layer="91"/>
<wire x1="177.8" y1="254" x2="187.96" y2="254" width="0.1524" layer="91"/>
<wire x1="187.96" y1="254" x2="198.12" y2="254" width="0.1524" layer="91"/>
<wire x1="198.12" y1="254" x2="208.28" y2="254" width="0.1524" layer="91"/>
<wire x1="208.28" y1="254" x2="218.44" y2="254" width="0.1524" layer="91"/>
<wire x1="218.44" y1="254" x2="228.6" y2="254" width="0.1524" layer="91"/>
<wire x1="228.6" y1="254" x2="238.76" y2="254" width="0.1524" layer="91"/>
<wire x1="238.76" y1="254" x2="248.92" y2="254" width="0.1524" layer="91"/>
<wire x1="248.92" y1="254" x2="259.08" y2="254" width="0.1524" layer="91"/>
<wire x1="259.08" y1="254" x2="269.24" y2="254" width="0.1524" layer="91"/>
<wire x1="269.24" y1="254" x2="279.4" y2="254" width="0.1524" layer="91"/>
<wire x1="279.4" y1="254" x2="289.56" y2="254" width="0.1524" layer="91"/>
<wire x1="289.56" y1="254" x2="299.72" y2="254" width="0.1524" layer="91"/>
<wire x1="299.72" y1="254" x2="309.88" y2="254" width="0.1524" layer="91"/>
<wire x1="309.88" y1="254" x2="320.04" y2="254" width="0.1524" layer="91"/>
<wire x1="320.04" y1="254" x2="330.2" y2="254" width="0.1524" layer="91"/>
<wire x1="330.2" y1="254" x2="330.2" y2="251.46" width="0.1524" layer="91"/>
<pinref part="L18" gate="G$1" pin="1"/>
<wire x1="320.04" y1="251.46" x2="320.04" y2="254" width="0.1524" layer="91"/>
<junction x="320.04" y="254"/>
<pinref part="L17" gate="G$1" pin="1"/>
<wire x1="309.88" y1="254" x2="309.88" y2="251.46" width="0.1524" layer="91"/>
<junction x="309.88" y="254"/>
<pinref part="L16" gate="G$1" pin="1"/>
<wire x1="299.72" y1="251.46" x2="299.72" y2="254" width="0.1524" layer="91"/>
<junction x="299.72" y="254"/>
<pinref part="L15" gate="G$1" pin="1"/>
<wire x1="289.56" y1="251.46" x2="289.56" y2="254" width="0.1524" layer="91"/>
<junction x="289.56" y="254"/>
<pinref part="L14" gate="G$1" pin="1"/>
<wire x1="279.4" y1="251.46" x2="279.4" y2="254" width="0.1524" layer="91"/>
<junction x="279.4" y="254"/>
<pinref part="L13" gate="G$1" pin="1"/>
<wire x1="269.24" y1="251.46" x2="269.24" y2="254" width="0.1524" layer="91"/>
<junction x="269.24" y="254"/>
<pinref part="L12" gate="G$1" pin="1"/>
<wire x1="259.08" y1="251.46" x2="259.08" y2="254" width="0.1524" layer="91"/>
<junction x="259.08" y="254"/>
<pinref part="L11" gate="G$1" pin="1"/>
<wire x1="248.92" y1="251.46" x2="248.92" y2="254" width="0.1524" layer="91"/>
<junction x="248.92" y="254"/>
<pinref part="L10" gate="G$1" pin="1"/>
<wire x1="238.76" y1="251.46" x2="238.76" y2="254" width="0.1524" layer="91"/>
<junction x="238.76" y="254"/>
<pinref part="L9" gate="G$1" pin="1"/>
<wire x1="228.6" y1="251.46" x2="228.6" y2="254" width="0.1524" layer="91"/>
<junction x="228.6" y="254"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="218.44" y1="251.46" x2="218.44" y2="254" width="0.1524" layer="91"/>
<junction x="218.44" y="254"/>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="208.28" y1="251.46" x2="208.28" y2="254" width="0.1524" layer="91"/>
<junction x="208.28" y="254"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="198.12" y1="251.46" x2="198.12" y2="254" width="0.1524" layer="91"/>
<junction x="198.12" y="254"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="187.96" y1="251.46" x2="187.96" y2="254" width="0.1524" layer="91"/>
<junction x="187.96" y="254"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="177.8" y1="254" x2="177.8" y2="251.46" width="0.1524" layer="91"/>
<junction x="177.8" y="254"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="167.64" y1="254" x2="167.64" y2="251.46" width="0.1524" layer="91"/>
<junction x="167.64" y="254"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="157.48" y1="254" x2="157.48" y2="251.46" width="0.1524" layer="91"/>
<junction x="157.48" y="254"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="147.32" y1="254" x2="147.32" y2="251.46" width="0.1524" layer="91"/>
<junction x="147.32" y="254"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="93.98" y1="246.38" x2="93.98" y2="248.92" width="0.1524" layer="91"/>
<wire x1="93.98" y1="248.92" x2="104.14" y2="248.92" width="0.1524" layer="91"/>
<wire x1="104.14" y1="248.92" x2="114.3" y2="248.92" width="0.1524" layer="91"/>
<wire x1="114.3" y1="248.92" x2="124.46" y2="248.92" width="0.1524" layer="91"/>
<wire x1="124.46" y1="248.92" x2="137.16" y2="248.92" width="0.1524" layer="91"/>
<wire x1="137.16" y1="248.92" x2="137.16" y2="254" width="0.1524" layer="91"/>
<junction x="137.16" y="254"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="124.46" y1="246.38" x2="124.46" y2="248.92" width="0.1524" layer="91"/>
<junction x="124.46" y="248.92"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="114.3" y1="246.38" x2="114.3" y2="248.92" width="0.1524" layer="91"/>
<junction x="114.3" y="248.92"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="104.14" y1="246.38" x2="104.14" y2="248.92" width="0.1524" layer="91"/>
<junction x="104.14" y="248.92"/>
</segment>
<segment>
<pinref part="P+25" gate="G$1" pin="3.3V"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="27.94" y1="190.5" x2="27.94" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="15.24" y1="129.54" x2="15.24" y2="132.08" width="0.1524" layer="91"/>
<pinref part="P+26" gate="G$1" pin="3.3V"/>
<wire x1="15.24" y1="132.08" x2="22.86" y2="132.08" width="0.1524" layer="91"/>
<wire x1="22.86" y1="132.08" x2="27.94" y2="132.08" width="0.1524" layer="91"/>
<wire x1="27.94" y1="132.08" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="22.86" y1="132.08" x2="22.86" y2="129.54" width="0.1524" layer="91"/>
<junction x="22.86" y="132.08"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="30.48" y1="129.54" x2="30.48" y2="132.08" width="0.1524" layer="91"/>
<wire x1="30.48" y1="132.08" x2="27.94" y2="132.08" width="0.1524" layer="91"/>
<junction x="27.94" y="132.08"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="30.48" y1="132.08" x2="38.1" y2="132.08" width="0.1524" layer="91"/>
<wire x1="38.1" y1="132.08" x2="38.1" y2="129.54" width="0.1524" layer="91"/>
<junction x="30.48" y="132.08"/>
</segment>
<segment>
<pinref part="P+2" gate="G$1" pin="3.3V"/>
<wire x1="276.86" y1="147.32" x2="276.86" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="276.86" y1="144.78" x2="269.24" y2="144.78" width="0.1524" layer="91"/>
<wire x1="269.24" y1="144.78" x2="269.24" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="276.86" y1="139.7" x2="276.86" y2="144.78" width="0.1524" layer="91"/>
<junction x="276.86" y="144.78"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="284.48" y1="139.7" x2="284.48" y2="144.78" width="0.1524" layer="91"/>
<wire x1="284.48" y1="144.78" x2="276.86" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="43.18" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="25.4" y1="66.04" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<wire x1="40.64" y1="63.5" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<wire x1="40.64" y1="66.04" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="25.4" y="66.04"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="33.02" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="33.02" y1="63.5" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="33.02" y="66.04"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="15.24" y1="63.5" x2="15.24" y2="66.04" width="0.1524" layer="91"/>
<wire x1="15.24" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<label x="25.4" y="71.12" size="1.778" layer="95"/>
<pinref part="P+7" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<wire x1="106.68" y1="71.12" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="VCC"/>
<wire x1="106.68" y1="66.04" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<wire x1="106.68" y1="43.18" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<label x="106.68" y="71.12" size="1.778" layer="95"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="96.52" y1="63.5" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<wire x1="96.52" y1="66.04" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
<junction x="106.68" y="66.04"/>
<label x="106.68" y="71.12" size="1.778" layer="95"/>
<pinref part="P+27" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="127" width="0.1524" layer="91"/>
<pinref part="P+4" gate="G$1" pin="3.3V"/>
<wire x1="104.14" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
<wire x1="99.06" y1="127" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="96.52" y1="124.46" x2="96.52" y2="127" width="0.1524" layer="91"/>
<wire x1="96.52" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
<junction x="99.06" y="127"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="P+28" gate="G$1" pin="3.3V"/>
<wire x1="294.64" y1="83.82" x2="289.56" y2="83.82" width="0.1524" layer="91"/>
<wire x1="289.56" y1="83.82" x2="289.56" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="284.48" y1="83.82" x2="289.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="289.56" y="83.82"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="P+1" gate="G$1" pin="3.3V"/>
<wire x1="274.32" y1="187.96" x2="269.24" y2="187.96" width="0.1524" layer="91"/>
<wire x1="269.24" y1="187.96" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="264.16" y1="187.96" x2="269.24" y2="187.96" width="0.1524" layer="91"/>
<junction x="269.24" y="187.96"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="ADC1VREFHI"/>
<pinref part="P+31" gate="G$1" pin="3.3V"/>
<wire x1="198.12" y1="187.96" x2="198.12" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="ADC2VREFHI"/>
<wire x1="165.1" y1="187.96" x2="165.1" y2="195.58" width="0.1524" layer="91"/>
<pinref part="P+30" gate="G$1" pin="3.3V"/>
<wire x1="165.1" y1="195.58" x2="177.8" y2="195.58" width="0.1524" layer="91"/>
<wire x1="177.8" y1="195.58" x2="177.8" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA6_GPIO6"/>
<wire x1="114.3" y1="142.24" x2="121.92" y2="142.24" width="0.1524" layer="91"/>
<label x="114.3" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA4_GPIO4"/>
<wire x1="114.3" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<label x="114.3" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA3_GPIO3"/>
<wire x1="114.3" y1="154.94" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<label x="114.3" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA2_GPIO2"/>
<wire x1="114.3" y1="157.48" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<label x="114.3" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA1_GPIO1"/>
<wire x1="114.3" y1="160.02" x2="121.92" y2="160.02" width="0.1524" layer="91"/>
<label x="114.3" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA5_GPIO5"/>
<wire x1="114.3" y1="144.78" x2="121.92" y2="144.78" width="0.1524" layer="91"/>
<label x="114.3" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA7_GPIO7"/>
<wire x1="114.3" y1="139.7" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
<label x="114.3" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PA0_GPIO0"/>
<wire x1="114.3" y1="162.56" x2="121.92" y2="162.56" width="0.1524" layer="91"/>
<label x="114.3" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC1INA0" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="ADC1INA0"/>
<wire x1="195.58" y1="195.58" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<label x="195.58" y="190.5" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RSV"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="43.18" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NC"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="43.18" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
<wire x1="40.64" y1="48.26" x2="40.64" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="M3_SSIOFSS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CS"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="43.18" y1="45.72" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="33.02" y1="45.72" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="10.16" y1="45.72" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
<junction x="33.02" y="45.72"/>
<label x="10.16" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PD3_GPIO19"/>
<wire x1="114.3" y1="101.6" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<label x="114.3" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="M3_SSIOTX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DI"/>
<wire x1="10.16" y1="43.18" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<label x="10.16" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PD0_GPIO16"/>
<wire x1="248.92" y1="157.48" x2="241.3" y2="157.48" width="0.1524" layer="91"/>
<label x="243.84" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="M3_SSIOCLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCK"/>
<wire x1="10.16" y1="38.1" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
<label x="10.16" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PD2_GPIO18"/>
<wire x1="114.3" y1="104.14" x2="121.92" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="M3_SSIORX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DO"/>
<wire x1="10.16" y1="33.02" x2="43.18" y2="33.02" width="0.1524" layer="91"/>
<label x="10.16" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PD1_GPIO17"/>
<wire x1="248.92" y1="147.32" x2="241.3" y2="147.32" width="0.1524" layer="91"/>
<label x="243.84" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="B_X1" class="0">
<segment>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="48.26" y1="218.44" x2="48.26" y2="228.6" width="0.1524" layer="91"/>
<label x="48.26" y="236.22" size="1.778" layer="95"/>
<pinref part="CRYSTAL" gate="G$1" pin="X2"/>
<wire x1="48.26" y1="228.6" x2="48.26" y2="236.22" width="0.1524" layer="91"/>
<wire x1="45.72" y1="228.6" x2="48.26" y2="228.6" width="0.1524" layer="91"/>
<junction x="48.26" y="228.6"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="X1"/>
<wire x1="248.92" y1="134.62" x2="241.3" y2="134.62" width="0.1524" layer="91"/>
<label x="243.84" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="B_X2" class="0">
<segment>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="20.32" y1="218.44" x2="20.32" y2="228.6" width="0.1524" layer="91"/>
<label x="20.32" y="236.22" size="1.778" layer="95"/>
<pinref part="CRYSTAL" gate="G$1" pin="X1"/>
<wire x1="20.32" y1="228.6" x2="20.32" y2="236.22" width="0.1524" layer="91"/>
<wire x1="25.4" y1="228.6" x2="20.32" y2="228.6" width="0.1524" layer="91"/>
<junction x="20.32" y="228.6"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="X2"/>
<wire x1="248.92" y1="139.7" x2="241.3" y2="139.7" width="0.1524" layer="91"/>
<label x="243.84" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PC7_GPIO71"/>
<wire x1="144.78" y1="60.96" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<label x="144.78" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED_4" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PC4_GPIO68"/>
<wire x1="137.16" y1="60.96" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<label x="137.16" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED_3" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PC5_GPIO69"/>
<wire x1="139.7" y1="60.96" x2="139.7" y2="68.58" width="0.1524" layer="91"/>
<label x="139.7" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="TMS"/>
<wire x1="248.92" y1="119.38" x2="241.3" y2="119.38" width="0.1524" layer="91"/>
<label x="243.84" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="TDI"/>
<wire x1="248.92" y1="121.92" x2="241.3" y2="121.92" width="0.1524" layer="91"/>
<label x="243.84" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="TDO"/>
<wire x1="248.92" y1="111.76" x2="241.3" y2="111.76" width="0.1524" layer="91"/>
<label x="243.84" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="TCK"/>
<label x="243.84" y="124.46" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="241.3" y1="124.46" x2="269.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="269.24" y1="124.46" x2="269.24" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EMU0" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="EMU0"/>
<label x="243.84" y="109.22" size="1.778" layer="95"/>
<wire x1="241.3" y1="109.22" x2="284.48" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="284.48" y1="129.54" x2="284.48" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TRSTN" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="TRST"/>
<label x="243.84" y="114.3" size="1.778" layer="95"/>
<wire x1="241.3" y1="114.3" x2="271.78" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="271.78" y1="114.3" x2="271.78" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EMU1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="EMU1"/>
<label x="243.84" y="116.84" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="241.3" y1="116.84" x2="276.86" y2="116.84" width="0.1524" layer="91"/>
<wire x1="276.86" y1="116.84" x2="276.86" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XRS" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="27.94" y1="175.26" x2="27.94" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="27.94" y1="170.18" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="15.24" y1="170.18" x2="15.24" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="27.94" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="170.18" x2="38.1" y2="165.1" width="0.1524" layer="91"/>
<junction x="27.94" y="170.18"/>
<wire x1="45.72" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<junction x="38.1" y="170.18"/>
<wire x1="10.16" y1="170.18" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
<junction x="15.24" y="170.18"/>
<label x="43.18" y="170.18" size="1.778" layer="95"/>
<label x="10.16" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="XRS"/>
<wire x1="114.3" y1="165.1" x2="121.92" y2="165.1" width="0.1524" layer="91"/>
<label x="114.3" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="ARS"/>
<wire x1="137.16" y1="195.58" x2="137.16" y2="187.96" width="0.1524" layer="91"/>
<label x="137.16" y="190.5" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PF2_GPIO34" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="38.1" y1="119.38" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<label x="38.1" y="116.84" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PF2_GPIO34"/>
<wire x1="248.92" y1="106.68" x2="241.3" y2="106.68" width="0.1524" layer="91"/>
<label x="243.84" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="PF3_GPIO35" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="30.48" y1="114.3" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
<label x="30.48" y="116.84" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PF3_GPIO35"/>
<wire x1="248.92" y1="104.14" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<label x="243.84" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="PG3_GPIO43" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="15.24" y1="114.3" x2="15.24" y2="119.38" width="0.1524" layer="91"/>
<label x="15.24" y="116.84" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="PG3_GPIO43"/>
<wire x1="248.92" y1="96.52" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<label x="243.84" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="PG7_GPIO47" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PG7_GPIO47"/>
<wire x1="175.26" y1="60.96" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
<label x="175.26" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="22.86" y1="114.3" x2="22.86" y2="119.38" width="0.1524" layer="91"/>
<label x="22.86" y="116.84" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="U1RX" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB0_GPIO8"/>
<wire x1="114.3" y1="137.16" x2="121.92" y2="137.16" width="0.1524" layer="91"/>
<label x="114.3" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="U1TX" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB1_GPIO9"/>
<wire x1="114.3" y1="129.54" x2="121.92" y2="129.54" width="0.1524" layer="91"/>
<label x="114.3" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C0SDA" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB6_GPIO14"/>
<label x="114.3" y="109.22" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="121.92" y1="109.22" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<wire x1="104.14" y1="109.22" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I2C0SCL" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB7_GPIO15"/>
<label x="114.3" y="106.68" size="1.778" layer="95"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="121.92" y1="106.68" x2="96.52" y2="106.68" width="0.1524" layer="91"/>
<wire x1="96.52" y1="106.68" x2="96.52" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I2C1SCL" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PD7_GPIO23"/>
<wire x1="215.9" y1="58.42" x2="215.9" y2="68.58" width="0.1524" layer="91"/>
<label x="215.9" y="60.96" size="1.778" layer="95" rot="R90"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="215.9" y1="58.42" x2="294.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="294.64" y1="58.42" x2="294.64" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="SCL"/>
<wire x1="99.06" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<label x="119.38" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1SDA" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PD6_GPIO22"/>
<label x="243.84" y="83.82" size="1.778" layer="95"/>
<wire x1="241.3" y1="83.82" x2="261.62" y2="83.82" width="0.1524" layer="91"/>
<wire x1="261.62" y1="83.82" x2="261.62" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="261.62" y1="71.12" x2="284.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="284.48" y1="71.12" x2="284.48" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="SDA"/>
<wire x1="119.38" y1="35.56" x2="99.06" y2="35.56" width="0.1524" layer="91"/>
<label x="119.38" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="U0RX" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PJ3_GPIO59"/>
<wire x1="195.58" y1="60.96" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
<label x="195.58" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="U0TX" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PJ2_GPIO58"/>
<wire x1="198.12" y1="60.96" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<label x="198.12" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PF4_GPIO36" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PF4_GPIO36"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="68.58" width="0.1524" layer="91"/>
<label x="165.1" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PH2_GPIO50" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PH2_GPIO50"/>
<wire x1="114.3" y1="83.82" x2="121.92" y2="83.82" width="0.1524" layer="91"/>
<label x="114.3" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDDIO_1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO"/>
<wire x1="114.3" y1="170.18" x2="121.92" y2="170.18" width="0.1524" layer="91"/>
<label x="114.3" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="147.32" y1="236.22" x2="147.32" y2="228.6" width="0.1524" layer="91"/>
<label x="147.32" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_2" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_2"/>
<wire x1="114.3" y1="167.64" x2="121.92" y2="167.64" width="0.1524" layer="91"/>
<label x="114.3" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="157.48" y1="236.22" x2="157.48" y2="228.6" width="0.1524" layer="91"/>
<label x="157.48" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_3" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_3"/>
<wire x1="114.3" y1="149.86" x2="121.92" y2="149.86" width="0.1524" layer="91"/>
<label x="114.3" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="167.64" y1="236.22" x2="167.64" y2="228.6" width="0.1524" layer="91"/>
<label x="167.64" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_4" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_4"/>
<wire x1="114.3" y1="132.08" x2="121.92" y2="132.08" width="0.1524" layer="91"/>
<label x="114.3" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="177.8" y1="228.6" x2="177.8" y2="236.22" width="0.1524" layer="91"/>
<label x="177.8" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_5" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_5"/>
<wire x1="114.3" y1="111.76" x2="121.92" y2="111.76" width="0.1524" layer="91"/>
<label x="114.3" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="187.96" y1="236.22" x2="187.96" y2="228.6" width="0.1524" layer="91"/>
<label x="187.96" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_6" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_6"/>
<wire x1="114.3" y1="88.9" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<label x="114.3" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="198.12" y1="228.6" x2="198.12" y2="236.22" width="0.1524" layer="91"/>
<label x="198.12" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_7" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_7"/>
<wire x1="154.94" y1="60.96" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<label x="154.94" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="208.28" y1="236.22" x2="208.28" y2="228.6" width="0.1524" layer="91"/>
<label x="208.28" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_8" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_8"/>
<wire x1="180.34" y1="60.96" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<label x="180.34" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="L8" gate="G$1" pin="2"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="218.44" y1="228.6" x2="218.44" y2="236.22" width="0.1524" layer="91"/>
<label x="218.44" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_9" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_9"/>
<wire x1="193.04" y1="60.96" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
<label x="193.04" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="L9" gate="G$1" pin="2"/>
<wire x1="228.6" y1="236.22" x2="228.6" y2="228.6" width="0.1524" layer="91"/>
<label x="228.6" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_10" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_10"/>
<wire x1="213.36" y1="60.96" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
<label x="213.36" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="1"/>
<pinref part="L10" gate="G$1" pin="2"/>
<wire x1="238.76" y1="236.22" x2="238.76" y2="228.6" width="0.1524" layer="91"/>
<label x="238.76" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_11" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_11"/>
<wire x1="248.92" y1="86.36" x2="241.3" y2="86.36" width="0.1524" layer="91"/>
<label x="243.84" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="1"/>
<pinref part="L11" gate="G$1" pin="2"/>
<wire x1="248.92" y1="236.22" x2="248.92" y2="228.6" width="0.1524" layer="91"/>
<label x="248.92" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_12" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_12"/>
<wire x1="248.92" y1="132.08" x2="241.3" y2="132.08" width="0.1524" layer="91"/>
<label x="243.84" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="L12" gate="G$1" pin="2"/>
<wire x1="259.08" y1="236.22" x2="259.08" y2="228.6" width="0.1524" layer="91"/>
<label x="259.08" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_13" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_13"/>
<wire x1="248.92" y1="142.24" x2="241.3" y2="142.24" width="0.1524" layer="91"/>
<label x="243.84" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="L13" gate="G$1" pin="2"/>
<wire x1="269.24" y1="236.22" x2="269.24" y2="228.6" width="0.1524" layer="91"/>
<label x="269.24" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_14" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_14"/>
<wire x1="248.92" y1="152.4" x2="241.3" y2="152.4" width="0.1524" layer="91"/>
<label x="243.84" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="L14" gate="G$1" pin="2"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="228.6" width="0.1524" layer="91"/>
<label x="279.4" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_15" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_15"/>
<wire x1="248.92" y1="165.1" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<label x="243.84" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="L15" gate="G$1" pin="2"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="228.6" width="0.1524" layer="91"/>
<label x="289.56" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_16" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_16"/>
<wire x1="248.92" y1="167.64" x2="241.3" y2="167.64" width="0.1524" layer="91"/>
<label x="243.84" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="L16" gate="G$1" pin="2"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="228.6" width="0.1524" layer="91"/>
<label x="299.72" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDIO_17" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDIO_17"/>
<wire x1="248.92" y1="170.18" x2="241.3" y2="170.18" width="0.1524" layer="91"/>
<label x="243.84" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="L17" gate="G$1" pin="2"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="228.6" width="0.1524" layer="91"/>
<label x="309.88" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDA1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDA1"/>
<wire x1="200.66" y1="195.58" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<label x="200.66" y="190.5" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="L18" gate="G$1" pin="2"/>
<wire x1="320.04" y1="236.22" x2="320.04" y2="228.6" width="0.1524" layer="91"/>
<label x="320.04" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDDA2" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VDDA2"/>
<wire x1="162.56" y1="195.58" x2="162.56" y2="187.96" width="0.1524" layer="91"/>
<label x="162.56" y="190.5" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="L19" gate="G$1" pin="2"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="330.2" y1="236.22" x2="330.2" y2="228.6" width="0.1524" layer="91"/>
<label x="330.2" y="228.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED_2" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PC6_GPIO70"/>
<wire x1="142.24" y1="60.96" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<label x="142.24" y="60.96" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="EEPROM_WP" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB2_GPIO10"/>
<wire x1="114.3" y1="127" x2="121.92" y2="127" width="0.1524" layer="91"/>
<label x="114.3" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="114.3" y1="45.72" x2="114.3" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="WP"/>
<wire x1="114.3" y1="40.64" x2="99.06" y2="40.64" width="0.1524" layer="91"/>
<label x="114.3" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB3_GPIO11" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB3_GPIO11"/>
<wire x1="114.3" y1="124.46" x2="121.92" y2="124.46" width="0.1524" layer="91"/>
<label x="114.3" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB4_GPIO12" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB4_GPIO12"/>
<wire x1="114.3" y1="99.06" x2="121.92" y2="99.06" width="0.1524" layer="91"/>
<label x="114.3" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB5_GPIO13" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PB5_GPIO13"/>
<wire x1="114.3" y1="96.52" x2="121.92" y2="96.52" width="0.1524" layer="91"/>
<label x="114.3" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSSOSC" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="VSSOSC"/>
<wire x1="248.92" y1="137.16" x2="241.3" y2="137.16" width="0.1524" layer="91"/>
<label x="243.84" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="20.32" y1="210.82" x2="20.32" y2="208.28" width="0.1524" layer="91"/>
<wire x1="20.32" y1="208.28" x2="33.02" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="33.02" y1="208.28" x2="35.56" y2="208.28" width="0.1524" layer="91"/>
<wire x1="35.56" y1="208.28" x2="38.1" y2="208.28" width="0.1524" layer="91"/>
<wire x1="38.1" y1="208.28" x2="48.26" y2="208.28" width="0.1524" layer="91"/>
<wire x1="48.26" y1="208.28" x2="48.26" y2="210.82" width="0.1524" layer="91"/>
<pinref part="CRYSTAL" gate="G$1" pin="GND2"/>
<wire x1="38.1" y1="213.36" x2="38.1" y2="208.28" width="0.1524" layer="91"/>
<junction x="38.1" y="208.28"/>
<pinref part="CRYSTAL" gate="G$1" pin="GND1"/>
<wire x1="33.02" y1="208.28" x2="33.02" y2="213.36" width="0.1524" layer="91"/>
<junction x="33.02" y="208.28"/>
<wire x1="35.56" y1="205.74" x2="35.56" y2="208.28" width="0.1524" layer="91"/>
<junction x="35.56" y="208.28"/>
<label x="35.56" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="ADC2INB0"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="157.48" y1="187.96" x2="157.48" y2="208.28" width="0.1524" layer="91"/>
<wire x1="157.48" y1="208.28" x2="157.48" y2="213.36" width="0.1524" layer="91"/>
<junction x="157.48" y="208.28"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="157.48" y1="213.36" x2="147.32" y2="213.36" width="0.1524" layer="91"/>
<wire x1="147.32" y1="213.36" x2="147.32" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC2INB0" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="172.72" y1="208.28" x2="167.64" y2="208.28" width="0.1524" layer="91"/>
<label x="170.18" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2SCL" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PF1_GPIO33"/>
<label x="243.84" y="160.02" size="1.778" layer="95"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="241.3" y1="160.02" x2="274.32" y2="160.02" width="0.1524" layer="91"/>
<wire x1="274.32" y1="160.02" x2="274.32" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I2C2SDA" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="PF0_GPIO32"/>
<label x="243.84" y="162.56" size="1.778" layer="95"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="241.3" y1="162.56" x2="264.16" y2="162.56" width="0.1524" layer="91"/>
<wire x1="264.16" y1="162.56" x2="264.16" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
